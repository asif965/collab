<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_lib {

	var $menu_items = array(); 

	

	public function __construct(){
	
		$this->menu_items ['home'] =  base_url(array('home',':lang',''));
		$this->menu_items ['property_for_sale'] =  site_url(array('search',':lang','property-for-sale')); 
		$this->menu_items ['property_for_rent'] =  site_url(array('search',':lang','property-for-rent')); 
		$this->menu_items ['property'] =  site_url(array('property',':lang',''));
		$this->menu_items ['agents'] =  site_url(array('search',':lang','agents'));
		$this->menu_items ['contact'] =  site_url(array('contact',':lang',''));
		$this->menu_items ['search'] =  site_url(array('search',':lang',''));
		$this->menu_items ['register'] =  site_url(array('register',':lang',''));
		$this->menu_items ['blogs'] =  site_url(array('blogs',':lang',''));
		$this->menu_items ['blog'] =  site_url(array('blog',':lang',''));
	}
	
	public function get_url($menu_item = ''){
		
		$CI =& get_instance();
		
		$menu_item = str_replace("& ","",$menu_item);
		$menu_item = str_replace(" ","-",$menu_item);
		
		
		
		if(array_key_exists($menu_item,$this->menu_items))
		{	$return_menu_item =  $this->menu_items[$menu_item];
			
			
			if( isset($CI->enable_multi_lang ) && $CI->enable_multi_lang  != true  && $menu_item = 'home')
			{	
				$return_menu_item = str_replace("home/","",$return_menu_item);
				  	
				
			}
			
			
		}else if(preg_match("/type=/", $menu_item))
		{
			$return_menu_item = str_replace("type=","",$menu_item);
			$return_menu_item = site_url(array('search',':lang','property-type-'.$return_menu_item));
			
		}
		else if(preg_match("/page=/", $menu_item))
		{
			$return_menu_item = str_replace("page=","",$menu_item);
			$return_menu_item = site_url(array(':lang',$return_menu_item));
			
		}	
		else	
			$return_menu_item  = base_url();
		
		
		
		$return_menu_item  =  $this->remove_lang_from_url($return_menu_item);
		
		return $return_menu_item ;
		
	}	
	
	public function remove_lang_from_url($menu_item){
	
		$CI =& get_instance();
		$return_menu_item  =  "";
		if(isset($CI->enable_multi_lang ) && $CI->enable_multi_lang  == true )
		{	$lang = '';
			
			/*$lang = $CI->default_language;	*/
			$lang = $CI->default_lang_code;	
			$return_menu_item  =  str_replace(":lang",$lang,$menu_item  );	
		
		}else{
			$return_menu_item  =  str_replace("/:lang/","",$menu_item  );
			 $return_menu_item  =  str_replace("/:lang","",$menu_item  );
		}
		
		
		
		return $return_menu_item ;
	
	}
	
	
}
