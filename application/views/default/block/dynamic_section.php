<?php 
global $settings;
$dynamic_feature = true;
$limit = 6;
if(isset($settings['show_as']) && $settings['show_as'] == 'grid'){ 
	$limit = $settings['no_of_item_in_grid_list'];
}
else if(isset($settings['show_as']) && $settings['show_as'] == 'grid'){ 
	$limit = $settings['no_of_item_in_carousel'];
}

$def_lang_code = $this->default_language;
/* $def_lang_code = $this->default_lang_code;*/

$where_cond = '';
$where_lang_cond = '';
if(isset($settings['property_for']) && !empty($settings['property_for']))
{
	$where_cond .= " AND prop.property_for = '".$settings['property_for']."' ";
}
if(isset($settings['property_type']) && !empty($settings['property_type']) && $settings['property_type'] != 'all')
{
	$where_cond .= " AND prop.property_type = '".$settings['property_type']."' ";
}
if(isset($settings['property_for_lang']) && !empty($settings['property_for_lang']))
{
	/*echo $settings['property_for_lang'];
	echo $def_lang_code ." == ".$settings['property_for_lang'] ; */
	/*if($def_lang_code == $settings['property_for_lang'])*/
	if( $settings['property_for_lang'] != 'all')
	{
		$where_lang_cond = " and pld.language = '$def_lang_code' ";
		
		$locations = $this->global_lib->get_option('locations');
		$location_array = json_decode($locations,true);
		
		$country_code = $settings['property_country'];
		if(!empty($country_code) && $country_code != 'all')
		{
			$exp_country_code = explode('~',$country_code);
			if(!empty($exp_country_code))
			{
				
				$where_cond .= " and pld.country = '".$exp_country_code[0]."' ";
			}	
		}
			
	
if(isset($settings['property_state']) && !empty($settings['property_state']) && $settings['property_state'] != 'all')
{
	$pse = explode('~',$settings['property_state']);
	$where_cond .= " AND pld.state = '".$pse[0]."' ";
}
if(isset($settings['property_city']) && !empty($settings['property_city']) && $settings['property_city'] != 'all')
{
	$pse = explode('~',$settings['property_city']);
	$where_cond .= " AND pld.city = '".$pse[0]."' ";
}
if(isset($settings['property_zipcode']) && !empty($settings['property_zipcode']) && $settings['property_zipcode'] != 'all')
{
	$where_cond .= " AND pld.zip_code = '".$settings['property_zipcode']."' ";
}
if(isset($settings['property_sub_area']) && !empty($settings['property_sub_area']) && $settings['property_sub_area'] != 'all')
{
	$where_cond .= " AND pld.sub_area = '".$settings['property_sub_area']."' ";
}	
	
	
	}
}

if(isset($_SESSION['agent_id']))
{
	$sql = "select * from properties as prop 
	inner join property_lang_details as pld on pld.p_id = prop.p_id and pld.language = '$def_lang_code'
	inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
	where prop.is_feat = 'Y' and prop.property_status = 'Public'
	or
	prop.is_feat = 'Y' and u.user_id = ".$_SESSION['agent_id']."
	order by prop.p_id DESC limit $limit";
	
	$sql = "select * from properties as prop 
				inner join property_lang_details as pld on pld.p_id = prop.p_id $where_lang_cond
				inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
				where prop.status = 'publish' and prop.deleted = 'N' and prop.property_status = 'Public'
				$where_cond
				or
				prop.status = 'publish' and prop.deleted = 'N' and u.user_id = ".$_SESSION['agent_id']."
				$where_cond
				group by prop.p_id order by prop.p_id DESC limit $limit";	
}
else
{
	$logged_in = $this->session->userdata('logged_in');
	if(isset($logged_in) && $logged_in == TRUE )
	{		
		$sql = "select * from properties as prop 
				inner join property_lang_details as pld on pld.p_id = prop.p_id $where_lang_cond
				inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
				where prop.status = 'publish' and prop.deleted = 'N'
				$where_cond
				group by prop.p_id order by prop.p_id DESC limit $limit";
		
	}
	else
	{
		$sql = "select * from properties as prop 
				inner join property_lang_details as pld on pld.p_id = prop.p_id $where_lang_cond
				inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
				where prop.status = 'publish' and prop.deleted = 'N' and prop.property_status = 'Public'
				$where_cond
				group by prop.p_id order by prop.p_id DESC limit $limit";			
	}	
	
}

$recent_properties = $this->Common_model->commonQuery($sql );

if(isset($recent_properties) && $recent_properties->num_rows() > 0){ 

?>
<div class="site-section site-section-sm">
  <div class="container">
	
	<div class="row justify-content-center mb-5">
	  <div class="col-md-10 text-center">
		<div class="site-section-title">
		 	<?php 
			if(isset($settings['heading']) && $settings['heading'] != ''){?>
			<h2> <?php echo mlx_get_lang($settings['heading']); ?></h2>
			<?php } ?>
			<?php if(isset($settings['sub_heading']) && $settings['sub_heading'] != ''){?>
			<p class="subheading"><?php echo mlx_get_lang($settings['sub_heading']); ?></p>
			<?php } ?>

		</div>
	  </div>
	</div>
	
	<?php 
		if(isset($settings['show_as']) && $settings['show_as'] == 'grid'){ 
	?>
		<div class="row justify-content-center mb-5">
		<?php 
		foreach($recent_properties->result() as $prop_row)
		{
		?>
			<div class="col-md-6 col-lg-4 mb-4">
				<?php include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
			</div>
		<?php } ?>
		</div>
	<?php }else{
		
		?>
		<div class="row mb-5">
		<?php if($recent_properties->num_rows() > 3 ){?>
			<div class="col-md-12">
				<div class="grid-carousel owl-carousel owl-theme mb50" 
									 data-dots="<?php echo $settings['show_nav_dots']; ?>"
									 data-nav="<?php echo $settings['show_nav']; ?>"
									 data-autoplay="<?php echo $settings['auto_start']; ?>"
									 data-interval="<?php echo $settings['carousel_interval']; ?>"> 
					
						<?php 
						 
						foreach($recent_properties->result() as $prop_row){
							
						?>
						<div class="item">
							<?php include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
						</div>
					<?php  } ?>
						
				</div>
			</div>
			<?php } else { ?> 
			
			<?php 
				foreach($recent_properties->result() as $prop_row){
			?>
			<div class="col-md-6 col-lg-4 mb-4">
				<div class="item">
					<?php include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
				</div>
			</div>	
			<?php  } ?>
			
			
			<?php } ?>	
			
		</div>
	<?php } ?>
	
	<?php if(isset($settings['show_view_more']) && $settings['show_view_more'] == 'yes') { ?>
		<div class="row">
		  <div class="col-md-12 text-center">
			<a href="<?php if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true)
			{
				$def_lang_code = $this->default_language;
				echo site_url(array('property',$def_lang_code)); 
			}
			else
			{
				echo site_url('property'); 
			} ?>" class="btn custom-btn py-2 px-4 rounded-0 text-white"><?php echo mlx_get_lang('View More'); ?></a>
		  </div>  
		</div>
	<?php } ?>
	
  </div>
</div>
<?php } ?>