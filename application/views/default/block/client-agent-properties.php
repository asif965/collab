<?php 
	
	$def_lang_code = $this->default_language;
	if(isset($_SESSION['agent_id']))
	{
		$sql = "select * from properties as prop 
		inner join property_lang_details as pld on pld.p_id = prop.p_id and pld.language = '$def_lang_code'
		inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
		where prop.status = 'publish' and u.user_id = ".$_SESSION['agent_id']." order by prop.p_id DESC limit 6";			
	}
	else
	{
		$sql = "select * from properties as prop 
		inner join property_lang_details as pld on pld.p_id = prop.p_id and pld.language = '$def_lang_code'
		inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
		where prop.status = 'publish' order by prop.p_id DESC limit 6";
	}
	
	$recent_properties = $this->Common_model->commonQuery($sql );
	if(isset($recent_properties) && $recent_properties->num_rows() > 0){ 
	$username = ' ';
	if(isset($_SESSION['agent_id']))
	{
		$userSql = "select user_name from users 
		where user_status = 'Y' and user_verified = 'Y' and user_id = ".$_SESSION['agent_id']."";
		$user_result = $this->Common_model->commonQuery($userSql );
		$username = ' ('.$user_result->row()->user_name.') ';			
	}	
	?>
    <div class="site-section site-section-sm bg-light">
      <div class="container">
		
		<div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center">
            <div class="site-section-title">
              <h2>Agent<?php echo $username;?>Properties</h2>
            </div>
          </div>
        </div>
		
        <div class="row justify-content-center mb-5">
		<?php
		if($recent_properties->num_rows() > 3 )
		{
		?>	
		  <div class="agent-carousel grid-carousel owl-carousel owl-theme mb50" 
							 data-dots="yes"
							 data-nav="yes"
							 data-autoplay="no"
							 data-interval="5000"> 
			
				<?php 
				 
				foreach($recent_properties->result() as $prop_row){
					$agent_page = true;
				?>
				<div class="item">
					<?php include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
				</div>
			<?php  } ?>
				
		</div>	
	<?php } else { ?> 
			
			<?php 
				foreach($recent_properties->result() as $prop_row){
					$agent_page = true;
			?>
			<div class="col-md-6 col-lg-4 mb-4">
				<div class="item">
					<?php include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
				</div>
			</div>	
			<?php  } ?>
			
			
			<?php } ?>			
		  
		  <?php foreach($recent_properties->result() as $prop_row){ ?>
			  <div class="col-md-6 col-lg-4 mb-4">
					<?php //include(__DIR__ . '../../property/template-part/single-property-grid.php'); ?>
			  </div>
		  <?php } ?>
        </div>
		<?php if(isset($settings['show_view_more']) && $settings['show_view_more'] == 'yes') { ?>
		<div class="row">
		  <div class="col-md-12 text-center">
			<a href="<?php if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true)
			{
				$def_lang_code = $this->default_language;
				echo site_url(array('property',$def_lang_code)); 
			}
			else
			{
				echo site_url('property'); 
			} ?>" class="btn custom-btn py-2 px-4 rounded-0 text-white"><?php echo mlx_get_lang('View More'); ?></a>
		  </div>  
		</div>
	<?php } ?>
	
      </div>
    </div>
    <?php } ?>