<?php
$enable_homepage_section = $this->global_lib->get_option('enable_homepage_section');
if($enable_homepage_section == 'Y' && isset($homepage_section) && count($homepage_section) > 0 )
{
	
	$sections = $homepage_section; 
	
	echo '<div class="homepage-section-block">';
	global $settings;
	foreach($sections as $section_key => $section_settings)
	{
		
		if($section_settings['is_enable'] == 'Y')
		{
			
			$section_settings['section_id'] = $section_key;
			$settings = $section_settings;
			if(isset($section_settings['section_type']) && $section_settings['section_type'] == 'dynamic')
			{	
				$this->load->view("$theme/block/dynamic_section");
			}
			else if(isset($section_settings['section_type']) && $section_settings['section_type'] == 'video')
			{	
				$this->load->view("$theme/block/video_section");
			}
			else
			{				
				$this->load->view("$theme/block/$section_key");
			}	
		}
	
	}
	echo '</div>';	
}
else
{
	$this->load->view("$theme/client_home_page_default");
}
?>