<?php
	$type = $myHelpers->menu_lib->get_url('type='.strtolower($prop_row->title));
	
?><div class="single-property-type-block">
	<a href="<?php echo $type; ?>">
	<?php 
		if(isset($prop_row->img_url) && !empty($prop_row->img_url) && file_exists('uploads/prop_type/'.$prop_row->img_url))
		{
			$post_image_url = base_url().'uploads/prop_type/'.$prop_row->img_url;
			
			echo '<img src="'.$post_image_url.'" class="img-fluid">';
			
		}
		else
		{
			$post_image_url = base_url().'themes/'.$theme.'/images/no-property-image.jpg';
			echo '<img src="'.$post_image_url.'" class="img-fluid">';
		}
		?>
  </a>
  
  <div class="pt-title text-center">
		<a href="<?php echo $type; ?>"><?php echo mlx_get_lang(ucfirst($prop_row->title)); ?></a>
  </div>
</div>






