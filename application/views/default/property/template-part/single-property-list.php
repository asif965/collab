<?php

$enable_compare_property = $myHelpers->global_lib->get_option('enable_compare_property');
$enbale_favourite = $myHelpers->global_lib->get_option('enbale_favourite');

$logged_in = $this->session->userdata('logged_in');

if($logged_in == true)
{
	$user_id = $this->session->userdata('user_id');
	$bookmar_checker = $myHelpers->global_lib->get_bookmarks($user_id);
}
$def_lang_code = $this->default_language;
$is_price_set = true;
$property_url = $myHelpers->global_lib->get_property_url($prop_row->p_id,$prop_row);
if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true)
{
	
	$title = $myHelpers->global_lib->get_property_lang($prop_row->p_id,'title',$def_lang_code);
	if($title != '')
	{
		$prop_row->title = stripslashes($title); 
	}
	
	$short_description = $myHelpers->global_lib->get_property_lang($prop_row->p_id,'short_description',$def_lang_code);
	if($short_description != '')
	{
		$prop_row->short_description = $short_description;
	}
	
	$ret_data = $myHelpers->global_lib->get_property_price_by_lang($prop_row->p_id,$def_lang_code);
	
	if(!empty($ret_data))
	{
		$prop_row->price = $ret_data['price'];
		/*$currency_symbol = $ret_data['currency'];*/
	}
	else
	{
		$is_price_set = false;
	}
	
	
	$currency_symbol = $this->site_currency_symbol ;
}
else
{
	$ret_data = $myHelpers->global_lib->get_property_price_by_lang($prop_row->p_id,$def_lang_code);
	
	if(!empty($ret_data))
	{
		$prop_row->price = $ret_data['price'];
		/*$currency_symbol = $ret_data['currency'];*/
	}
	else
	{
		$is_price_set = false;
	}
}
?>
<?php 
$has_gallery = false;
$gallery_images = array();
$post_image_url = base_url().'themes/'.$theme.'/images/no-property-image.jpg';

if(!empty($prop_row->property_images))
{
	$img_type = 'original';
	$p_images = $myHelpers->global_lib->get_property_gallery($prop_row->p_id,$img_type);
	if(!empty($p_images) && count($p_images) > 0)
	{
		$n=0;
		foreach($p_images as $k=>$v)
		{
			if(file_exists($v[$img_type]))
			{
				if($n>2)
					break;
				$post_image_url = base_url().$v[$img_type];
				$has_gallery = true;
				$gallery_images[] = $v[$img_type];
				$n++;
			}
		} 
		
		if(count($gallery_images) == 1 && $has_gallery == true)
		{
			$has_gallery = false;
		}
	}
}

$prop_attr = '';
if(isset($enable_compare_property) && $enable_compare_property == 'Y') {
	$prop_attr .= ' data-title="'.ucfirst(stripslashes($prop_row->title)).'" '; 
	$prop_attr .= ' data-url="'.$prop_row->slug.'~'.$prop_row->p_id.'"  '; 
	$prop_attr .= ' data-id="'.$myHelpers->global_lib->EncryptClientId($prop_row->p_id).'" '; 
}
if(isset($enbale_favourite) && $enbale_favourite == 'Y') {
	
	
	$prop_attr .= ' data-title="'.ucfirst(stripslashes($prop_row->title)).'" '; 
	$prop_attr .= ' data-url="'.$prop_row->slug.'~'.$prop_row->p_id.'"  '; 
	$prop_attr .= ' data-id="'.$myHelpers->global_lib->EncryptClientId($prop_row->p_id).'" '; 
}
?>

<div class="property-entry horizontal " <?php echo $prop_attr; ?>>
	<div class="row">
 
  <div class="col-md-4">
	  <div class="fixed-property-btn-conainter">
		  <?php if(isset($enable_compare_property) && $enable_compare_property == 'Y') { ?>
			<a href="#" class="property-favorite add_to_compare prop-list-section" data-toggle="tooltip" title="Add To Compare"><span class="icon-plus"></span></a>
		  <?php } ?>
		  
		  <?php     
			if(isset($enbale_favourite) && $enbale_favourite == 'Y') { 
			$selected='';
			$fav_id = '';
			   if(isset($bookmar_checker) && !empty($bookmar_checker) && array_key_exists($prop_row->p_id,$bookmar_checker)){
				   $selected = 'fa fa-bookmark';
				   $fav_id = 'favorite_btn_remove';
				   $fev_title = 'Remove from Favorite';
			   }else{
				   $fav_id = 'favorite_btn';
				   $selected = 'fa fa-bookmark-o';
				   $fev_title = 'Add to Favorite';
			   }
				
				if($logged_in == true)
				{
			?>
				<button  class="btn property-favorite gird-favirate_btn <?php if(isset($fav_id)) echo $fav_id; ?> " 
				data-toggle="tooltip" title="<?php echo $fev_title; ?>"><span  
				class="<?php if(isset($selected)) echo $selected; ?> bookmark_icon" ></span></button>
			<?php }else{ ?>
				<button  class="btn not-logged-in property-favorite gird-favirate_btn <?php if(isset($fav_id)) echo $fav_id; ?> " 
				data-toggle="modal" data-target="#loginModal"
				data-toggle="tooltip" title="<?php echo $fev_title; ?>"><span  title="<?php echo $fev_title; ?>" data-toggle="tooltip" 
				class="<?php if(isset($selected)) echo $selected; ?> bookmark_icon" ></span></button>
			<?php }} ?>
	  </div>
	  
	  
		<div class="offer-type-wrap">
		  <?php /*if(strtolower($prop_row->property_for) == 'sale'){ ?>
			<span class="offer-type bg-danger"><?php echo $prop_row->property_for; ?></span>
		  <?php }else if(strtolower($prop_row->property_for) == 'rent'){ ?>
			<span class="offer-type bg-warning"><?php echo $prop_row->property_for; ?></span>
		  <?php }*/ ?>
		  
		   <?php 
		   /*if(isset($agent_page))
		   {				   
			  if( strtolower($prop_row->property_status) == 'private'){ ?>
				<span class="offer-type bg-danger"><?php echo mlx_get_lang($prop_row->property_status); ?></span>
			  <?php }else if(strtolower($prop_row->property_status) == 'public'){ ?>
				<span class="offer-type bg-success"><?php echo mlx_get_lang($prop_row->property_status); ?></span>
	  <?php }
		   }*/
	   ?>
		</div>
		
		<?php if($has_gallery && !empty($gallery_images)) { ?>
		<div class="product-gallery-carousel owl-carousel owl-theme" data-nav="yes" data-dots="yes" data-items="1" data-autoplay="yes" data-interval="5000">
			<?php 
			foreach($gallery_images as $gik=>$giv)
			{
				$post_image_url = base_url().$giv;
			?>
				<a href="<?php echo $property_url; ?>">
					<img src="<?php echo $post_image_url; ?>" class="img-fluid" alt="<?php echo ucfirst(stripslashes($prop_row->title)); ?>"> 
				</a>
			<?php
			}
			?>
		</div>
	<?php }else{ ?>
		<a href="<?php echo $property_url; ?>">
			<img src="<?php echo $post_image_url; ?>" class="img-fluid" alt="<?php echo ucfirst(stripslashes($prop_row->title)); ?>"> 
		</a>
	<?php } ?>
		
	  
  </div>
  <div class="col-md-8">
		  <div class="p-4 property-body " style="border-top:0px;">
			
			<h3 class="property-title">
			<a href="<?php echo $property_url; ?>"><?php echo ucfirst(stripcslashes($prop_row->title)); ?></a></h3>
			<span class="property-location d-block mb-3"><span class="property-icon icon-room"></span> <?php echo ucfirst($prop_row->address); ?></span>
			<strong class="property-price text-primary mb-3 d-block text-success">
				<?php 
				if($is_price_set) {
					$args = array("currency_symbol"=>$currency_symbol);
						echo $myHelpers->global_lib->moneyFormatDollar($prop_row->price,$args);

					if($prop_row->property_for == 'Rent') echo '/'.mlx_get_lang('Month');
				}else{
					echo mlx_get_lang('Price Not Set');
				}
				?>
				</strong>
			<?php if(isset($prop_row->short_description) && !empty($prop_row->short_description)){ ?>
				<p><?php echo $prop_row->short_description; ?></p>
			<?php } ?>
			
			<div class="row" >
			  
			  <?php if(!empty($prop_row->bedroom)){ ?>
			  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
				<span class="property-specs"><?php echo mlx_get_lang('Beds'); ?></span>
				<span class="property-specs-number"><?php echo $prop_row->bedroom; ?></span>
			  </div>
			  <?php } ?>
			  
			  <?php if(!empty($prop_row->bathroom)){ ?>
			  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
				<span class="property-specs"><?php echo mlx_get_lang('Baths'); ?></span>
				<span class="property-specs-number"><?php echo $prop_row->bathroom; ?></span>
			  </div>
			  <?php } ?>
			  
			  <?php if(!empty($prop_row->size)){ ?>
			  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" >
				<span class="property-specs"><?php echo mlx_get_lang('Size'); ?></span>
				<span class="property-specs-number"><?php echo str_replace('~',' ',$prop_row->size); ?></span>
			  </div>
			  <?php } ?>
			</div>
		  </div>
		</div>
	</div>
</div>





