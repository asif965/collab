<style>
.disableContBtn{background:#acacac;}	
</style>
<?php 
	
    /*$currency_symbol = $myHelpers->global_lib->get_currency_symbol();*/
	$currency_symbol = $this->site_currency_symbol ;
	$enable_compare_property = $myHelpers->global_lib->get_option('enable_compare_property');
	$enbale_favourite = $myHelpers->global_lib->get_option('enbale_favourite');
	$enbale_print_priview = $myHelpers->global_lib->get_option('enbale_print_priview');
	$enbale_pdf_export = $myHelpers->global_lib->get_option('enbale_pdf_export');
	
	$enbale_mortgage_calculator = $myHelpers->global_lib->get_option('enbale_mortgage_calculator');
	$enbale_agent_contact_form = $myHelpers->global_lib->get_option('enbale_agent_contact_form');
	$enbale_social_share = $myHelpers->global_lib->get_option('enbale_social_share');
	
	$logged_in = $this->session->userdata('logged_in');

	if($logged_in == true)
	{
		$user_id = $this->session->userdata('user_id');
		$bookmar_checker = $myHelpers->global_lib->get_bookmarks($user_id);
	}
?>

<?php if(isset($banner_row) && isset($banner_row->b_image) && !empty($banner_row->b_image) && file_exists('uploads/banner/'.$banner_row->b_image)){ ?>
<section class="page-top-section set-bg d-print-none" 
	data-setbg="<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>" 
	style="background-image: url(<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>);">
	<div class="container text-white">
		<h1><?php echo ucfirst(stripslashes($single_property->title)); ?></h1>
	</div>
</section>
<?php } ?>

<?php 
$prop_attr = '';
if(isset($enable_compare_property) && $enable_compare_property == 'Y') {
	$prop_attr .= ' data-title="'.ucfirst(stripslashes($single_property->title)).'" '; 
	$prop_attr .= ' data-url="'.$single_property->slug.'~'.$single_property->p_id.'"  '; 
	$prop_attr .= ' data-id="'.$myHelpers->global_lib->EncryptClientId($single_property->p_id).'" '; 
}
if(isset($enbale_favourite) && $enbale_favourite == 'Y') {
	
	
	$prop_attr .= ' data-title="'.ucfirst(stripslashes($single_property->title)).'" '; 
	$prop_attr .= ' data-url="'.$single_property->slug.'~'.$single_property->p_id.'"  '; 
	$prop_attr .= ' data-id="'.$myHelpers->global_lib->EncryptClientId($single_property->p_id).'" '; 
}


?>

<style>
/*
.owl-carousel.slide-one-item.home-slider .owl-item{
    height:500px;
    width:100%;
}
.owl-carousel.sl-thumb-slider .owl-item{
    height:120px;
    width:100%;
}
.owl-carousel.sl-thumb-slider .owl-item .sl-thumb {
    width: 100%;
    height: 100%;
}
.owl-carousel.sl-thumb-slider .owl-item img{
    height:100%;
    width:100%;
}
.img-container {
    padding: 0.25rem;
    background-color: #fff;
    border: 1px solid #dee2e6;
    border-radius: 0.25rem;
    width: 100%;
    height: 150px;
    display: inline-block;
    overflow: hidden;
	position:relative;
}
.img-container img{
	position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
*/
</style>
<style>
.whatsapp-share-btn a i{
    display: inline-block;
    padding: 12px 12px 9px;
    text-align: center;
	color:#fff;
	font-size:14px;
	border-radius: 3px;
	cursor:pointer;
	background: #4FCE5D;
}
.share-whatsapp	{
    color : rgba(0, 0, 0, 0.6);
}
.offer-type {
      display: inline-block;
      margin-bottom: 5px;
      padding: 1px 7px;
      color: #fff;
      text-transform: uppercase;
      letter-spacing: .2em;
      font-size: 9px;
      border-radius: 4px;
      width: 60px;
      margin-left: 10px; 
      }
</style>
<div class="site-section site-section-sm single-property-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ">
		 
            <div class="property_slider property-entry " <?php echo $prop_attr; ?>>
				<div class="fixed-property-btn-conainter">
					<?php if(isset($enable_compare_property) && $enable_compare_property == 'Y') { ?>
						<a href="#" class="single-prop-add-compare-btn add_to_compare d-print-none" data-toggle="tooltip" title="Add to Compare"><span class="icon-plus"></span></a>
					<?php } ?>
				   <?php     
				
					if(isset($enbale_favourite) && $enbale_favourite == 'Y') { 
					$selected='';
					$fav_id = '';
					   if(isset($bookmar_checker) && !empty($bookmar_checker) && array_key_exists($single_property->p_id,$bookmar_checker)){
						   $selected = 'fa fa-bookmark';
						   $fav_id = 'favorite_btn_remove';
					   }else{
						   $fav_id = 'favorite_btn';
						   $selected = 'fa fa-bookmark-o';
					   }
					
					?>
					<?php 
					if($logged_in == true)
					{
						if(isset($_SESSION['agent_id']))
						{				   
							  if( strtolower($single_property->property_status) == 'private' && ($single_property->created_by == $_SESSION['agent_id'])){ ?>
								<span class="offer-type bg-danger"><?php echo mlx_get_lang($single_property->property_status); ?></span>
							  <?php }else if(strtolower($single_property->property_status) == 'public' && ($single_property->created_by == $_SESSION['agent_id'])){ ?>
								<span class="offer-type bg-success"><?php echo mlx_get_lang($single_property->property_status); ?></span>
					<?php
										
							  }
							  else 
							  {
								 if(strtolower($single_property->property_status) == 'public'){ ?>
									<span class="offer-type bg-success"><?php echo mlx_get_lang($single_property->property_status); ?></span> 
							  <?php
							   	}
							  }
								  
						}
						elseif(isset($_SESSION['adminId']))
						{
							if( strtolower($single_property->property_status) == 'private')
							{ ?>
								<span class="offer-type bg-danger"><?php echo mlx_get_lang($single_property->property_status); ?></span>
							  <?php }else if(strtolower($single_property->property_status) == 'public'){ ?>
								<span class="offer-type bg-success"><?php echo mlx_get_lang($single_property->property_status); ?></span>
						<?php	
							}	
						}		
								  
					?>
						<button  class="btn property-favorite single_page_favirate_btn d-print-none <?php if(isset($fav_id)) echo $fav_id; ?>" id="<?php if(isset($fav_id)) echo $fav_id; ?>" data-toggle="tooltip" title="Add to Favorite"><span class="bookmark_icon <?php if(isset($selected)) echo $selected; ?>"></span></button>
					<?php }else{ ?>
						<button  data-toggle="modal" data-target="#loginModal" class="btn property-favorite not-logged-in single_page_favirate_btn d-print-none <?php if(isset($fav_id)) echo $fav_id; ?>" id="<?php if(isset($fav_id)) echo $fav_id; ?>" data-toggle="tooltip" title="Add to Favorite"><span class="bookmark_icon <?php if(isset($selected)) echo $selected; ?>"></span></button>
					<?php } ?>
					
					<?php 
			
					if(isset($enbale_print_priview) && $enbale_print_priview == 'Y') {?>
							<button class="btn d-print-none print_btn" id="print_pre" data-toggle="tooltip" title="Print"><i class="fa fa-print" style="margin-left: -2px;"></i></button>
								
					<?php  }
					?>
					<?php if(isset($enbale_pdf_export) && $enbale_pdf_export == 'Y') {?>
						<form method="post" action="" class="print_page_form">
						<input type="hidden" name="prop" value="<?php echo $single_property->p_id; ?>"/>
							<button class="btn d-print-none pdf_btn" type="submit" name="export_pdf" data-toggle="tooltip" title="Save as PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>
						</form> 
					<?php }?>
				<?php
				}
				
				?>
				</div>	
					<?php 
					
					if(!empty($single_property->property_images))
					{
						$p_images = $myHelpers->global_lib->get_property_gallery($single_property->p_id,'original');
						if(!empty($p_images))
						{
							foreach($p_images as $k=>$v)
								{
									$post_image_url = base_url().$v['original'];
									echo '<div class="media-print"><img src="'.$post_image_url.'" class="img-fluid" style="width:100%;"></div>';
									break;
								}
						}
					} ?>
					
                <?php 
				
				
					if(!empty($single_property->property_images))
					{
						$p_images = $myHelpers->global_lib->get_property_gallery($single_property->p_id);
						
						
						if(!empty($p_images))
						{
							if(count($p_images) > 1)
							{
								echo '<div class="slide-one-item home-slider owl-carousel d-print-none" id="sl-slider">';
								$i=0;
								foreach($p_images as $k=>$v)
								{
									$i++;
									$post_image_url = base_url().$v['original'];
									echo '<div class="sl-item set-bg';
									if($i>1){
										echo ' d-print-none ';
									}else{
										echo ' d-print-none ';
									}
									echo '"><img src="'.$post_image_url.'" class="img-responsive" id="media_image"></div>';
								}
								echo '</div>';
							}
							else
							{
								foreach($p_images as $k=>$v)
								{
									$post_image_url = base_url().$v['original'];
									echo '<div class="sl-item set-bg  d-print-none "><img src="'.$post_image_url.'" class="img-fluid"></div>';
								}
							}
						}
						else
						{
							$post_image_url = base_url().'themes/'.$theme.'/images/single-no-property-image.jpg';
							echo '<div ><img src="'.$post_image_url.'" class="img-fluid"></div> ';
						}
						
					}
					else
					{
						$post_image_url = base_url().'themes/'.$theme.'/images/single-no-property-image.jpg';
						echo '<div><img src="'.$post_image_url.'" class="img-fluid"></div>';
					}
					?>
				
              
			  <?php 
			  if(!empty($single_property->property_images))
			  {
					$p_images = $myHelpers->global_lib->get_property_gallery($single_property->p_id,'thumbnail');
					
					if(!empty($p_images) && count($p_images) > 1)
					{
						echo '<div class="owl-carousel sl-thumb-slider d-print-none" id="sl-slider-thumb">';
						foreach($p_images as $k=>$v)
						{
							$post_image_url = base_url().$v['thumbnail'];
							echo '<div class="sl-thumb set-bg" style="background-image:url('.$post_image_url.')"></div>';
							/*
							echo '<div class="sl-thumb set-bg"><img src="'.$post_image_url.'" class="img-fluid"></div>';
							*/
						}
						echo '</div>';
					}
			  }
			  ?>
            </div>
            <div class="bg-white property-body border-bottom border-left border-right">
			
			
			<?php 
			
			$is_price_set = true;
			$is_desc_set = true;
			$is_address_set = true;
			if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true)
			{
				$def_lang_code = $this->default_language;
				
				$ret_data = $myHelpers->global_lib->get_property_price_by_lang($single_property->p_id,$def_lang_code);
				if(!empty($ret_data))
				{
					$single_property->price = $ret_data['price'];
					/*$currency_symbol = $ret_data['currency'];*/
				}
				else
				{
					$is_price_set = false;
				}
				
				$dec_data = $myHelpers->global_lib->get_property_description_by_lang($single_property->p_id,$def_lang_code);
				if(!empty($dec_data))
				{
					$single_property->description = $dec_data;
				}
				else
				{
					$is_desc_set = false;
				}
				
				$add_data = $myHelpers->global_lib->get_property_address_by_lang($single_property->p_id,$def_lang_code);
				if(!empty($add_data))
				{
					$single_property->address = $add_data;
				}
				else
				{
					$is_address_set = false;
				}
				
			}
			
			?>
			
			
			
			<div class="row mb-2">
			  
				<div class="col-md-8 text-left">
					<h4 class="text-black"><?php echo ucfirst(stripslashes($single_property->title)); ?></h4>
					<p class="lead">
						<?php if($is_address_set) { ?>
							<i class="fa fa-map-marker"></i> <?php echo $single_property->address; ?>
						<?php }else{ 
							echo mlx_get_lang('Address Not Set');
						} ?>
					</p>
					
					
				</div>
                <div class="col-md-4 text-right">
					<strong class=" price-block-btn btn-block btn-lg text-black h1 mb-3 text-center">
					<?php 
					if($is_price_set)
					{
						$args = array("currency_symbol"=>$currency_symbol);
						echo $myHelpers->global_lib->moneyFormatDollar($single_property->price,$args);
						
						if($single_property->property_for == 'Rent'){ echo '/'.mlx_get_lang('Month'); } 
					}
					else{
						echo mlx_get_lang('Price Not Set');
					}		
						?></strong>
                </div>
				
              </div>
              <div class="row border-bottom border-top">
                
				<?php if($single_property->prop_type_title ){ ?>
					<div class="col-md-6 col-lg-3 text-center py-3">
					  <span class="d-inline-block text-black mb-0 caption-text"><?php echo mlx_get_lang('Property Type'); ?></span>
					  <strong class="d-block"><?php echo mlx_get_lang(ucfirst($single_property->prop_type_title)); ?></strong>
					</div>
				<?php } ?>
				
				<?php if($single_property->size ){ ?>
					<div class="col-md-6 col-lg-3 text-center py-3">
						<span class="d-inline-block text-black mb-0 caption-text"><?php echo mlx_get_lang('Size'); ?></span>
						<strong class="d-block"><?php echo str_replace('~',' ',$single_property->size); ?></strong>
					</div>
                <?php } ?>
				
				<?php if($single_property->bedroom ){ ?>
					<div class="col-md-6 col-lg-3 text-center py-3">
						<span class="d-inline-block text-black mb-0 caption-text"><?php echo mlx_get_lang('Beds'); ?></span>
						<strong class="d-block"><?php echo $single_property->bedroom; ?></strong>
					</div>
				<?php } ?>
				
				<?php if($single_property->bathroom ){ ?>
					<div class="col-md-6 col-lg-3 text-center py-3">
						<span class="d-inline-block text-black mb-0 caption-text"><?php echo mlx_get_lang('Baths'); ?></span>
						<strong class="d-block"><?php echo $single_property->bathroom; ?></strong>
					</div>
				<?php } ?>
				
				
              </div>
			  
			  <?php 
			  
				$isBlogAct = $myHelpers->isPluginActive('google_map');
				if($isBlogAct == true  && !empty($single_property->lat) && !empty($single_property->long) )
				{ 
					$enable_google_map_js_api = $myHelpers->global_lib->get_option('enable_google_map_js_api');
					$google_map_js_api_key = $myHelpers->global_lib->get_option('google_map_js_api_key');
					
					if($enable_google_map_js_api == 'Y' && !empty($google_map_js_api_key))
					{
						$gmap_url = "https://maps.google.com/maps?key=$google_map_js_api_key&q=".$single_property->lat.", ".$single_property->long."&z=15&output=embed";
			  ?>
			  <div class="row mt-5">
				<div class="col-md-12">
					<h2 class="h4 text-black text-left"><?php echo mlx_get_lang('Google Map'); ?></h2>
				</div>
				<div class="col-md-12 embed_iframe_container">
					<div id="gMap" style="width:100%;height:350px;"></div>

					<script>
					function myMap() {

						var map = new google.maps.Map(document.getElementById("gMap"), mapOptions);
						  var myCenter = new google.maps.LatLng(<?php echo $single_property->lat; ?>, <?php echo $single_property->long; ?>);
						  var mapCanvas = document.getElementById("gMap");
						  var mapOptions = {center: myCenter, zoom: 15};
						  var map = new google.maps.Map(mapCanvas, mapOptions);
						  var marker = new google.maps.Marker({position:myCenter});
						  marker.setMap(map);
					}
					</script>

					<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $google_map_js_api_key; ?>&callback=myMap"></script>
				</div>
			  </div>
			  <?php  
				  }
				} 
			  ?>
			  
			     

			  
			  <?php 
				$isBlogAct = $myHelpers->isPluginActive('openstreetmap');
				if($isBlogAct == true)
				{ 
					
					if(isset($meta_result['openstreetmap_embed_code']) && !empty($meta_result['openstreetmap_embed_code']))
					{
						$meta_value = html_entity_decode(trim($meta_result['openstreetmap_embed_code']));
						if(!empty($meta_value)){
					
				?>
				
				  <div class="row mt-5">
					<div class="col-md-12">
						<h2 class="h4 text-black text-left"><?php echo mlx_get_lang('Open Street Map'); ?></h2>
					</div>
					<div class="col-md-12 embed_iframe_container">
						<?php echo $meta_value;  ?>
					</div>
				  </div>
						<?php  }
					}
					
			  } 
			  ?>
			  
			  <?php if(isset($single_property->description) && !empty($single_property->description)) { ?>
				
				<h2 class="h4 text-black text-left mt-5"><?php echo mlx_get_lang('Description'); ?></h2>
				<div class="prop_desc text-left">
					<?php 
						if($is_desc_set)
						{
							echo $single_property->description; 
						}
						else
						{
							echo mlx_get_lang('Property description not avaiable in current language.');
						}
					?>
				</div>
              <?php } ?>
			  
			  <?php
			  	
				$def_indoor_amenities = $def_outdoor_amenities = array();
				
				if($this->global_lib->get_option('property_amenities'))
				{
					$amenities_list = json_decode($this->global_lib->get_option('property_amenities'),true);
					$def_indoor_amenities = (isset($amenities_list['indoor_amenities'])? $amenities_list['indoor_amenities']: array()   );
					$def_outdoor_amenities = (isset($amenities_list['outdoor_amenities'])? $amenities_list['outdoor_amenities']: array()   );
				}
		
			  ?>
			  
			   <?php if(isset($single_property->indoor_amenities) && !empty($single_property->indoor_amenities) && !empty($def_indoor_amenities )) { 
				$indoor_amenities = json_decode($single_property->indoor_amenities,true);
				foreach($indoor_amenities as $k=>$v){
					if(!in_array($v,$def_indoor_amenities)) unset($indoor_amenities[$k]);
				}
				if(!empty($indoor_amenities) ){
				
				
			   ?>
				<h2 class="h4 text-black no-gutters mt-5  text-left"><?php echo mlx_get_lang('Indoor Amenities'); ?></h2>
				<div class="row">
					<?php foreach($indoor_amenities as $k=>$v){ ?>	
						<div class="col-md-4 text-left"><i class="fa fa-check"></i> <?php echo mlx_get_lang(ucfirst($v)); ?></div>
					<?php } ?>
				</div>
			   <?php }} ?>
			  
			   <?php if(isset($single_property->outdoor_amenities) && !empty($single_property->outdoor_amenities) && !empty($def_outdoor_amenities )) { 
				$outdoor_amenities = json_decode($single_property->outdoor_amenities,true);
				foreach($outdoor_amenities as $k=>$v){
					if(!in_array($v,$def_outdoor_amenities)) unset($outdoor_amenities[$k]);
				}
				if(!empty($outdoor_amenities)){
			   ?>
				<h2 class="h4 text-black no-gutters mt-5  text-left"><?php echo mlx_get_lang('Outdoor Amenities'); ?></h2>
				<div class="row">
					<?php foreach($outdoor_amenities as $k=>$v){ ?>	
						<div class="col-md-4 text-left"><i class="fa fa-check"></i> <?php echo mlx_get_lang(ucfirst($v)); ?></div>
					<?php } ?>
				</div>
			   <?php }} ?>
			  
			  <?php if(isset($single_property->distance_list) && !empty($single_property->distance_list)) { 
				$distance_list = json_decode($single_property->distance_list,true);
				if(!empty($distance_list)){
			   ?>
				<h2 class="h4 text-black no-gutters mt-5  text-left"><?php echo mlx_get_lang('Distances'); ?></h2>
				<div class="row">
					<?php foreach($distance_list as $k=>$v){ 
					
					
					?>	
						<div class="col-md-6 text-left"><i class="fa fa-arrows"></i> <?php echo mlx_get_lang(ucfirst($k)); ?> 
						
						<?php if($v['direction'] != ''){ ?>
						<small>(<?php echo mlx_get_lang(ucfirst($v['direction'])); ?>)</small> 
						<?php } ?>
						
						: <strong><?php echo ucfirst($v['distance']); ?> <?php echo mlx_get_lang(ucfirst($v['distance_text'])); ?></strong></div>
					<?php } ?>
				</div>
			   <?php }} ?>
			  
			  <?php if(isset($single_property->video_urls) && !empty($single_property->video_urls)){ 
				$video_url_array = explode(',',$single_property->video_urls);
			  ?>
				<?php 
				function getYoutubeIdFromUrl($url) {    
					$parts = parse_url($url);
					if(($parts["host"]=="m.youtube.com" || $parts["host"]=="youtube.com" || $parts["host"]=="www.youtube.com" || $parts["host"]=="youtu.be" || $parts["host"]=="www.youtu.be") && !strstr($url,"/c/") && !strstr($url,"/channel/") && !strstr($url,"/user/")){
					if(isset($parts['query'])){
						parse_str($parts['query'], $qs);
						if(isset($qs['v'])){
							return $qs['v'];
						}else if(isset($qs['vi'])){
							return $qs['vi'];
						}
					}
					if(($parts["host"]=="youtu.be" || $parts["host"]=="www.youtu.be") && isset($parts['path'])){
						$path = explode('/', trim($parts['path'], '/'));
						return $path[count($path)-1];
					}
					}
					if(strlen($url)==11 && (!strstr($url, "http://") && !strstr($url, "https://") && !strstr($url, "www.") && !strstr($url, "youtube") && !strstr($url, "www.") && !strstr($url, "youtu.be"))) return $url;
					return false;
				}

				function validateFbVideoUrl($url){
					$parts = parse_url($url);
					if(($parts["host"]=="facebook.com" || $parts["host"]=="www.facebook.com" || $parts["host"]=="fb.me" || $parts["host"]=="fb.com") && !strstr($url,"/pg/")){
						return $url;
					}
					return false;
				}

				function getVimeoId($url){
					$parts = parse_url($url);
					
					if($parts['host'] == 'www.vimeo.com' || $parts['host'] == 'vimeo.com' || $parts['host'] == 'player.vimeo.com'){
						$vidid=substr($parts['path'], 1);
						return $vidid;
					}
					return false;
				}

				function getvidinfo($url){
					 $getYT=getYoutubeIdFromUrl($url);
					 if($getYT){
						$result["type"]="yt";
						$result["string"]=$getYT;
						$result["img"]="https://img.youtube.com/vi/".$getYT."/mqdefault.jpg";
						return($result);
					 }
					 else{
						 $getFB=validateFbVideoUrl($url);
						 if($getFB){
							$result["type"]="fb";
							$result["string"]=$getFB;
							$result["img"]="example.com/your-image-here.jpg";// I DIDN'T FIND A WAY TO GET FB VIDEO THUMBNAIL
							return($result);
						 }
						 else{
							 
							 $vimeoid=getVimeoId($url);
							 
							 if($vimeoid){
								$result["type"]="vim";
								$result["string"]=$vimeoid;
								return($result);
							 }
							
						 }
					}
					return false;
				}
				function echovideo($url){
					if($url){
						
						$vidinfo=getvidinfo($url);
						
						if($vidinfo){
							if($vidinfo["type"]=="yt"){
							?><div >
								<iframe width="100%" height="350" src="https://www.youtube-nocookie.com/embed/<?echo $vidinfo["string"]?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
								<?php
							}elseif($vidinfo["type"]=="fb"){
								?>
								<div>
								 <iframe src="https://www.facebook.com/plugins/video.php?href=<? echo $vidinfo["string"]; ?>&show_text=0&width=100%&height=350" width="100%" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
								 <?php
							}elseif($vidinfo["type"]=="vim"){
								?><div><iframe src="https://player.vimeo.com/<?echo $vidinfo["string"];?>" width="100%" height="350" frameborder="0" allow="autoplay; fullscreen" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
								<?php
							}
							else{
								$array = explode('.', $url);
								$extension = end($array);
								?>
									<video width="100%" height="350" controls>
									  <source src="<?php echo $url; ?>" type="video/<?php echo $extension; ?>">
									</video>
								<?php
							}
						}
						else{ $array = explode('.', $url);
								$extension = end($array);
								?>
									<video width="100%" height="350" controls>
									  <source src="<?php echo $url; ?>" type="video/<?php echo $extension; ?>">
									</video>
								<?php
							}
					}
				}
				?>
				<h2 class="h4 text-black no-gutters mt-5  text-left d-print-none"><?php echo mlx_get_lang('Videos'); ?></h2>
				<div class="row d-print-none">
					<div class="col-md-12">
						<div class="video_url_container owl-carousel">
							<?php if(!empty($video_url_array)) { 
								foreach($video_url_array as $k=>$v){
							?>
								<div class="single_video_url">
									<?php echo echovideo($v); ?>
								</div>
							<?php }} ?>
						</div>
					</div>
				</div>
				
				<script>
					
				</script>
			   <?php } ?>
			  
			  <?php 
			  if(!empty($single_property->property_images))
				{
					$p_images = $myHelpers->global_lib->get_property_gallery($single_property->p_id);
					
					if(!empty($p_images))
					{
						
						
			  ?>
              <div class="row mt-5 d-print-none">
                <div class="col-12">
                  <h2 class="h4 text-black mb-3 text-left"><?php echo mlx_get_lang('Gallery'); ?></h2>
                </div>
					<div class="card-columns">
						<?php 
						foreach($p_images as $k=>$v)
						{
							$post_image_url = base_url().$v['original'];
						?>
						<div class="card"><!--col-sm-6 col-md-4 col-lg-3 mb-4-->
							<a href="<?php echo $post_image_url; ?>" class="image-popup gal-item img-container">
								<img src="<?php echo $post_image_url; ?>" alt="Image" class="img-fluid img-responsive">
							</a>
						</div>
						<?php } ?>
					</div>
              </div>
			  <?php }
				}
				?>
			 
            </div>
			
          </div>
          <div class="col-lg-4 d-print-none p_single_sidebar">
			
			<div class="bg-white widget border rounded agent_details" id="agent_detail">
			  <?php 
			  $user_id = $single_property->created_by;
			  $first_name = $myHelpers->global_lib->get_user_meta($user_id,'first_name');
			  $last_name = $myHelpers->global_lib->get_user_meta($user_id,'last_name');
			  $mobile_no = $myHelpers->global_lib->get_user_meta($user_id,'mobile_no');
			  $address = $myHelpers->global_lib->get_user_meta($user_id,'address');
			  $photo_url = $myHelpers->global_lib->get_user_meta($user_id,'photo_url');
			  
			  $full_name = strtolower($first_name).' '.strtolower($last_name);
			  $agent_url = site_url(array('user','agent',$this->default_language,str_replace(' ','-',strtolower($full_name)).'~'.$this->global_lib->EncryptClientID($user_id))); 
			  
			  ?>
              <form action="" class="">
                <div class="form-group">
                  <div class="row">
					<div class="col-md-4 text-center">
						<?php 
						if(isset($photo_url) && !empty($photo_url))
						{
							if(file_exists('uploads/user/'.$photo_url))
							{
								echo '<img src="'.base_url().'uploads/user/'.$photo_url.'" class="agent_img">';
							}
							else
							{
								echo '<img src="'.base_url().'themes/default/images/no-user-image.png'.'" class="agent_img">';
							}
						}
						else
						{
							echo '<img src="'.base_url().'themes/default/images/no-user-image.png'.'" class="agent_img">';
						}
						?>
						<hr>
					</div>
					<div class="col-md-8 text-left">
						<h5><a ><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5><!--href="<?php //echo $agent_url; ?>"-->
						<p><?php echo mlx_get_lang(ucfirst($single_property->user_type)); ?></p>
					</div>
				  </div>
                </div>
                <?php
                if(isset($_SESSION['agent_id']))
				{
	                if($single_property->created_by == $_SESSION['agent_id'])
					{
                ?>
                		<a href="javascript:void(0)" class="room-price showContactInfo" isOpen="0" style="margin-bottom: 15px;">Show Contact Details</a>
                <?php
					}
				}
				else
				{
					 if(isset($_SESSION['adminId']))
					 {
				?>
						<a href="javascript:void(0)" class="room-price showContactInfo" isOpen="0" style="margin-bottom: 15px;">Show Contact Details</a>
				<?php	 	
					 }	
				}
                ?>
                <div id="contactInfo" style="display: none;"> 
				<?php if(!empty($mobile_no)){ ?>
					<div class="form-group text-left">
						<i class="fa fa-phone"></i> <a href="tel:<?php echo $mobile_no; ?>"><?php echo $mobile_no; ?></a>
					</div>
				<?php } ?>
				
				<?php if(!empty($single_property->user_email)){ ?>
					<div class="form-group text-left"><!--mailto:<?php echo $single_property->user_email; ?>-->
						<i class="fa fa-envelope"></i> <a href="javascript:void(0);" style="cursor: default;"><?php echo $single_property->user_email; ?></a>
					</div>
				<?php } ?>
				
				<?php if(!empty($address)){ ?>
					<div class=" text-left">
					  <i class="fa fa-map-marker"></i> <?php echo $address; ?>
					</div>
				<?php } ?>
				<br />
				<?php 
		$site_whatsapp_no = ''; 
			//$site_whatsapp_no = $myHelpers->global_lib->get_option('site_whatsapp_no');
		if(!empty($mobile_no))
		{
			$site_whatsapp_no = $mobile_no;
		}	 
		
		$site_whatsapp_group_link = $myHelpers->global_lib->get_option('site_whatsapp_group_link');
		$whatsapp_no = $myHelpers->global_lib->get_user_meta($user_id,'whatsapp_no');
		$property_title = ucfirst(stripslashes($single_property->title));
		$property_url = $myHelpers->global_lib->get_property_url($single_property->p_id,$single_property);
		
		if(!empty($site_whatsapp_no))
		{
			$site_whatsapp_no = $site_whatsapp_no;
		}
		if(!empty($site_whatsapp_group_link))
		{
			$site_whatsapp_group_link = $site_whatsapp_group_link;
		}
		if(!empty($whatsapp_no))
		{
			$owner_whatsapp_no = $whatsapp_no;
		}		
		$whastapp_text = 'I want to get more detail about '.$property_title.' property. '.$property_url.'';
				
		if(isset($site_whatsapp_no) && !empty($site_whatsapp_no))
		{
		?>
		<div class="form-group whatsapp-share-btn">
			<a class="w-inline-block social-share-btn share-whatsapp" 
				title="Join Our Whatsapp Group" target="_blank" 
				href="https://api.whatsapp.com/send?phone=<?php echo $site_whatsapp_no; ?>&text=<?php echo urlencode($whastapp_text); ?>">
				<i class="fa fa-whatsapp"></i> &nbsp;
				<?php //echo mlx_get_lang('Contact Site Owner'); ?>Contact Agent
			</a>
			
		</div>
		<?php } ?>
		
		<?php 
		if(isset($owner_whatsapp_no) && !empty($owner_whatsapp_no))
		{
		?>
		<div class="form-group whatsapp-share-btn">
			<a class="w-inline-block social-share-btn share-whatsapp" 
				title="Join Our Whatsapp Group" target="_blank" 
				href="https://api.whatsapp.com/send?phone=<?php echo $owner_whatsapp_no; ?>&text=<?php echo urlencode($whastapp_text); ?>">
				<i class="fa fa-whatsapp"></i> &nbsp;
				<?php echo mlx_get_lang('Contact Property Owner'); ?>
			</a>
			
		</div>
		<?php } ?>
				
				</div>
              </form>
            </div>
			
			<?php
			/*$site_whatsapp_no = ''; 
			//$site_whatsapp_no = $myHelpers->global_lib->get_option('site_whatsapp_no');
			if(!empty($mobile_no))
			{
				$site_whatsapp_no = $mobile_no;
			}	 
			
			$site_whatsapp_group_link = $myHelpers->global_lib->get_option('site_whatsapp_group_link');
			$whatsapp_no = $myHelpers->global_lib->get_user_meta($user_id,'whatsapp_no');
			if(!empty($site_whatsapp_no))
			{
				$data['site_whatsapp_no'] = $site_whatsapp_no;
			}
			if(!empty($site_whatsapp_group_link))
			{
				$data['site_whatsapp_group_link'] = $site_whatsapp_group_link;
			}
			if(!empty($whatsapp_no))
			{
				$data['owner_whatsapp_no'] = $whatsapp_no;
			}
			$data['property_title'] = ucfirst(stripslashes($single_property->title));
			$data['property_url'] = $myHelpers->global_lib->get_property_url($single_property->p_id,$single_property);
			$this->load->view('default/whatsapp-share-template',$data); */
			
			?>
			<?php 
			if(isset($enbale_agent_contact_form) && $enbale_agent_contact_form == 'Y')
			{                                                                                                                                                                                                 
			?>
            <div class="bg-white widget border rounded text-left" id="agent_contact_form">
			  <?php 
			  $recaptcha_site_key = $myHelpers->global_lib->get_option('recaptcha_site_key');
			  $recaptcha_secret_key = $myHelpers->global_lib->get_option('recaptcha_secret_key');
			  
			  $is_recaptcha_enable = false;
			  $isBlogAct = $myHelpers->isPluginActive('google_recaptcha');
			  if($isBlogAct == true)
			  {
					$is_recaptcha_enable = true;
			  }
			  
			  ?>
			  <?php if($is_recaptcha_enable && !empty($recaptcha_site_key) && !empty($recaptcha_secret_key)) { ?>
				<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=<?php echo $this->default_language; ?>"
						async defer>
					</script>
				<script type="text/javascript">
				  var onloadCallback = function() {
					grecaptcha.render('recaptcha_element', {
					  'sitekey' : '<?php echo $recaptcha_site_key; ?>'
					});
				  };
				</script>
				
				
				<?php } ?>
				
              <h3 class="h4 text-black widget-title mb-3"><?php echo mlx_get_lang('Contact Agent'); ?></h3>
              <?php 
				$args = array('class' => 'form-contact-agent', 'id' => 'contact_agent_form');
				echo form_open_multipart('',$args);?> 
				<input type="hidden" name="p_id" value="<?php echo $myHelpers->global_lib->EncryptClientID($single_property->p_id); ?>">
                <div class="form-group">
                  <label for="name"><?php echo mlx_get_lang('Name'); ?> <span class="required text-danger">*</span></label>
                  <input type="text" id="name" name="name" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="email"><?php echo mlx_get_lang('Email'); ?> <span class="required text-danger">*</span></label>
                  <input type="email" id="email" name="email" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="message"><?php echo mlx_get_lang('Message'); ?> <span class="required text-danger">*</span></label>
                  <textarea id="message" name="message" class="form-control" required style="height:auto;"></textarea>
                </div>
				 <?php if($is_recaptcha_enable && !empty($recaptcha_site_key) && !empty($recaptcha_secret_key)) { ?>
				  <div class="row form-group">
					<div class="col-md-12">
					  <div id="recaptcha_element"></div>
					</div>
				  </div>
				  <?php } ?>
                <div class="form-group">
                  <button type="submit" name="submit" class="btn custom-btn text-white submit-contact-agent-form-btn" ><?php echo mlx_get_lang('Send Message'); ?></button>
                </div>
              </form>
            </div>
			<?php } ?>
			<?php 
			  if(isset($enbale_social_share) && $enbale_social_share == 'Y')
			  {
				include(__dir__ .'/../share-template.php'); 
			  }
			?>
			
			
			<?php 
			  
			include(__dir__ .'/recent-viewed-properties.php'); 
			  
			?>						
			
			<?php 
			if(isset($enbale_mortgage_calculator) && $enbale_mortgage_calculator == 'Y')
			{
			?>
			<div class="bg-white widget border rounded  text-left" id="mortgage_calculator">
			  
			  <form class="form-contact-agent" name="MortgageCalculator">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-2">
					<h3 class="h4 text-black widget-title mb-3"><?php echo mlx_get_lang('Mortgage Calculator'); ?></h3>
					<small><?php echo mlx_get_lang('Use this calculator to estimate your monthly mortgage payment'); ?></small></div>
				</div>  
				<div class="form-group">
				  <label for="house_price"><?php echo mlx_get_lang('House Price'); ?></label>
				  <input type="number" id="house_price"  class="form-control" value="" name="price" 
				  min="0" >
				</div>
				
				<div class="form-group">
				  <label for="interest_rate"><?php echo mlx_get_lang('Interest Rate'); ?> %</label>
				  <input id="interest_rate" type="number"  class="form-control" value="" name="ir" min="0">
				</div>
				
				<div class="form-group">
				  <label for="years"><?php echo mlx_get_lang('Years'); ?></label>
				  <input id="years" type="number"  class="form-control" value="" name="term" min="0">
				</div>
				
				<div class="form-group">
				  <label for="down_payment"><?php echo mlx_get_lang('Down Payment'); ?></label>
				  <input id="down_payment" type="number" class="form-control" onchange="calculatePayment(this.form)" value="" name="dp" min="0">
				</div>
				
				<br>
				<div class="form-group">
				  <label for="mortgage_principle"><?php echo mlx_get_lang('Mortgage Principle'); ?></label>
				  <input id="mortgage_principle" type="text" class="form-control" readonly="" name="principle">
				</div>
				<div class="form-group">
				  <label for="total_payments"><?php echo mlx_get_lang('Total Payments'); ?></label>
				  <input id="total_payments" type="text" class="form-control" readonly="" name="payments">
				</div>
				<div class="form-group">
				  <label for="payment_month"><?php echo mlx_get_lang('Payment/Month'); ?></label>
				  <input id="payment_month" type="text" class="form-control" readonly="" name="pmt">
				</div>
				<div class="form-group">
				  <input type="button" class="btn custom-btn btn-large text-white" onclick="cmdCalc_Click(this.form)" value="<?php echo mlx_get_lang('Calculate'); ?>" name="cmdCalc">
				</div>
			  </form>
			</div>
			<?php } ?>
			
          </div>
          
        </div>
		
      </div>
    </div>
<input type="hidden" name="propId" id="propId" value="<?php echo $propertyId;?>" />	
<style>
.right-side-fixed-menu {
    position: fixed;
	right: 15px;
	top: 50%;
	z-index: 999;
	transform: translateY(-50%);
}
.right-side-fixed-menu ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
}
.right-side-fixed-menu ul li a {
    background-color: #667792;
    color: #fff;
    width: 40px;
    height: 40px;
    display: inline-block;
    margin-bottom: 10px;
    text-align: center;
    border-radius: 50%;
    line-height: 40px;
	
	-webkit-transition: .3s all ease;
	-o-transition: .3s all ease;
	transition: .3s all ease;
}
.right-side-fixed-menu ul li a:hover{
	background-color:rgba(0,0,0,.5);
}
.right-side-fixed-menu ul li:last-child a{
	margin-bottom:0px;
}
.right-side-fixed-menu{
	display:none;
}
@media (max-width: 512px) {
	.right-side-fixed-menu{
		display:block;
	}
}
</style>
<script>
$(document).ready(function() {
	$(".right-side-fixed-menu li a").click(function() {
		var thiss = $(this);
		var href_text = thiss.attr('href');
		var header_height = $('header .site-navbar').outerHeight() + 15;
		$('html, body').animate({
			scrollTop: $(href_text).offset().top - header_height
		}, 1000);
		return false;
	});
});
</script>
<div class="right-side-fixed-menu">
	<ul>
		<li>
			<a href="#agent_detail"><i class="fa fa-phone"></i></a>
		</li>
		
		<?php  
		if(isset($enbale_social_share) && $enbale_social_share == 'Y'){ ?>
		<li>
			<a href="#whatsapp_link"><i class="fa fa-whatsapp"></i></a>
		</li>
		<?php } ?>
		
		<?php  
		if(isset($enbale_social_share) && $enbale_social_share == 'Y'){ ?>
		<li>
			<a href="#social_share"><i class="fa fa-share"></i></a>
		</li>
		<?php } ?>
		
		<?php 
		if(!empty($recentlyViewed) && count($recentlyViewed)) {
		?>
		<li>
			<a href="#recent_viewed"><i class="fa fa-history"></i></a>
		</li>
		<?php } ?>
		
		<?php if(isset($enbale_agent_contact_form) && $enbale_agent_contact_form == 'Y'){ ?>
		<li>
			<a href="#agent_contact_form"><i class="fa fa-info"></i></a>
		</li>
		<?php } ?>
		
		<?php if(isset($enbale_mortgage_calculator) && $enbale_mortgage_calculator == 'Y') { ?>
		<li>
			<a href="#mortgage_calculator"><i class="fa fa-calculator"></i></a>
		</li>
		<?php } ?>
	</ul>
</div>
	
<?php if(isset($related_properties) && $related_properties->num_rows() > 0) { ?>	
<div class="site-section site-section-sm bg-light d-print-none">
	<div class="container">
		<div class="row">
          <div class="col-12">
            <div class="site-section-title mb-5">
              <h2 class="text-left"><?php echo mlx_get_lang('Related Properties'); ?></h2>
            </div>
          </div>
        </div>
		<div class="row mb-5">
			<div class="related-property owl-carousel col-md-12">
				<?php foreach($related_properties->result() as $prop_row) { ?>
				
					<?php include('template-part/single-property-grid.php'); ?>
				
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script>
var BASEURL = '<?php echo base_url();?>';
$(document).ready(function(){
	$("#print_pre").on('click',function(){
		window.print();
	});	
	
	$('.showContactInfo').click(function(e){
		var is_open = $(this).attr('isOpen');
		var pid = $('#propId').val();
		if(is_open == '0')
		{		
			$('#contactInfo').show();
			$(this).attr('isOpen', '1');
			$(this).hide();
			$.ajax({
				     url: BASEURL+'ajax/addContactShowLog',
				      type: 'POST',
				      async:false,
				      data: {prop_id: pid}, // /converting the form data into array and sending it to server
				      dataType: 'html',
				      /*beforeSend:function(response) {
				      	$(this).prop('disabled', true);
				      	$('.showContactInfo').addClass("disableContBtn");				      													            
				      },*/
				      success:function(response) {				      	
				      	//$(this).prop('disabled', false);		
				      	//$('.showContactInfo').removeClass("disableContBtn");								            
				      }
		   });
			
		}
		/*else
		{
			$('#contactInfo').hide();
			$(this).attr('isOpen', '0');
		}*/	
		return false;
	});	
});

</script>
	