<?php 
$footer_text = $myHelpers->global_lib->get_option_lang('footer_text',$this->default_language);
$copyright_text = $myHelpers->global_lib->get_option_lang('copyright_text',$this->default_language);
$enable_compare_property = $myHelpers->global_lib->get_option('enable_compare_property');
$social_media = $myHelpers->global_lib->get_option('social_media');
$sql = "select * from property_types where status = 'Y' order by title";
$property_type_list = $myHelpers->Common_model->commonQuery($sql);
?>

<?php echo script_tag("themes/$theme/js/modernizr-2.6.2.min.js"); ?>
<?php echo script_tag("themes/$theme/js/jquery-migrate-3.0.1.min.js"); ?>
<?php echo script_tag("themes/$theme/js/jquery-ui.js"); ?>
<?php echo script_tag("themes/$theme/js/popper.min.js"); ?>
<?php echo script_tag("themes/$theme/js/bootstrap.min.js"); ?>
<?php echo script_tag("themes/$theme/js/owl.carousel.min.js"); ?>
<?php echo script_tag("themes/$theme/js/mediaelement-and-player.min.js"); ?>
<?php echo script_tag("themes/$theme/js/jquery.stellar.min.js"); ?>
<?php echo script_tag("themes/$theme/js/jquery.countdown.min.js"); ?>
<?php echo script_tag("themes/$theme/js/jquery.magnific-popup.min.js"); ?>
<?php echo script_tag("themes/$theme/js/bootstrap-datepicker.min.js"); ?>
<?php echo script_tag("themes/$theme/js/icheck.min.js"); ?>
<?php echo script_tag("themes/$theme/js/price-range.js"); ?>
<?php echo script_tag("themes/$theme/js/validation/jquery.validate.min.js"); ?>
<?php echo script_tag("themes/$theme/plugins/lazy-load/jquery.lazy.min.js"); ?>
<?php echo script_tag("themes/$theme/js/price-range.js"); ?>
<?php echo script_tag("themes/$theme/js/main.js"); ?>
<?php echo script_tag("themes/$theme/js/custom.js"); ?>
<?php echo script_tag("themes/$theme/js/advance_serach.js"); ?>



  </div>

<!--
<style type="text/css">
.pos-abs { position:absolute;}
.pos-fixed { position:fixed;}
.pos-top-center { top:0px; left: 0;    right: 0; }
.info-ad .ad-panel	{
	z-index: 1111;
	background-color: #fff;
	padding: 15px 25px;
	color: saddlebrown;
	font-size: 16px;
	}
.text-center{
	text-align:center;
}	
body{	margin-top:58px;   }
</style>
  <div class="info-ad outer pos-fixed pos-top-center text-center">
		<div class="ad-panel inner">
			This is a demo site for Multi-lingual Real-Estate Portal
		</div>
  </div>
  
  
  <div class="container">
		<div class="row ">
			<div class="col-md-12">
				This is a demo site for Multi-lingual Real-Estate Portal
			</div>
		</div>
  </div>
-->

<?php if(isset($enable_compare_property) && $enable_compare_property == 'Y') { ?>
<div class="container ">
	
	<div class="row comparePanle">
		<span class="compare-block-head pull-right">
			<a href="#" class="btn btn-success cmprBtn text-white" target="_blank" disabled ><?php echo mlx_get_lang('Compare'); ?></a>
			<i class="fa fa-caret-down show-hide-compare-block close_compare_block"></i>
		</span>
		<div class="header-block" style="display:none;">
			<div class="col-md-12">
				<h5 class="text-left"><?php echo mlx_get_lang('Added for Comparison'); ?>
				</h5>
			</div>
		</div>
		<div class=" titleMargin comparePan text-center" style="display:none;">
			<?php 
			if(isset($_SESSION['comparable_properties']) &&  !empty($_SESSION['comparable_properties'])) 
			{ 
				foreach($_SESSION['comparable_properties'] as $pk=>$pv)
				{
			?>
				<div id="<?php echo $pk; ?>" data-url="<?php echo $pv['url']; ?>" class="relPos w3-col l3 m3 s3">
					<div class="bg-white w3-ripple titleMargin">
						<a class="selectedItemCloseBtn w3-closebtn cursor">×</a>
						<img src="<?php echo $pv['img']; ?>" alt="<?php echo $pv['title']; ?>">
						<p id="<?php echo $pk; ?>" class="titleMargin1"><?php echo $pv['title']; ?></p>
					</div>
				</div>
			<?php 
				}
			} 
			?>
		</div>
	</div>
</div>
 <?php } 
	
	$isPlugAct = $myHelpers->isPluginActive('cookie_consent');
	if($isPlugAct == true)
	{
		$cookie_text = $myHelpers->global_lib->get_option_lang('cookie_text',$this->default_language);
		if(!empty($cookie_text)) {
			$data['cookie_text'] = $cookie_text;
			$this->load->view('default/includes/cookie_temp',$data);
		}
	}
	
	
	$google_analytics_tracking_id = $myHelpers->global_lib->get_option('google_analytics_tracking_id');
	$isGaAct = $myHelpers->isPluginActive('google_analytics');
	
	if($isGaAct == true && $google_analytics_tracking_id !='')
	{
			
				echo $google_analytics_tracking_code = "<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src=\"https://www.googletagmanager.com/gtag/js?id=$google_analytics_tracking_id\"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '$google_analytics_tracking_id');
		</script>
		";
			
	}
 ?>
 
	
  <a href="#" class="btn btn-white btn-outline-white d-print-none" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
	
  </body>
</html>