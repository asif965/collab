<?php if(isset($banner_row) && isset($banner_row->b_image) && !empty($banner_row->b_image) && file_exists('uploads/banner/'.$banner_row->b_image)){ ?>
<section class="page-top-section set-bg" 
	data-setbg="<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>" 
	style="background-image: url(<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>);">
	<div class="container text-white">
		<h1><?php echo mlx_get_lang('Register'); ?></h1>
	</div>
</section>
<?php } ?>

<div class="site-section" style="padding: 3em 0;">
      <div class="container">		
        <div class="row">
			<div class="alert alert-danger" style="float: left;width: 100%;">
				Key is <strong><?php echo $key;?></strong> invalid. Please again copy the link and register it.
			</div>

		</div>
	 </div>
</div>