<style>
.datepicker-days{display: block !important;}	
</style>
<?php if(isset($banner_row) && isset($banner_row->b_image) && !empty($banner_row->b_image) && file_exists('uploads/banner/'.$banner_row->b_image)){ ?>
<section class="page-top-section set-bg" 
	data-setbg="<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>" 
	style="background-image: url(<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>);">
	<div class="container text-white">
		<h1><?php echo mlx_get_lang('Register'); ?></h1>
	</div>
</section>
<?php } ?>

<?php 
$country_list = '[ 
{"name": "Afghanistan", "code": "AF"}, 
{"name": "land Islands", "code": "AX"}, 
{"name": "Albania", "code": "AL"}, 
{"name": "Algeria", "code": "DZ"}, 
{"name": "American Samoa", "code": "AS"}, 
{"name": "AndorrA", "code": "AD"}, 
{"name": "Angola", "code": "AO"}, 
{"name": "Anguilla", "code": "AI"}, 
{"name": "Antarctica", "code": "AQ"}, 
{"name": "Antigua and Barbuda", "code": "AG"}, 
{"name": "Argentina", "code": "AR"}, 
{"name": "Armenia", "code": "AM"}, 
{"name": "Aruba", "code": "AW"}, 
{"name": "Australia", "code": "AU"}, 
{"name": "Austria", "code": "AT"}, 
{"name": "Azerbaijan", "code": "AZ"}, 
{"name": "Bahamas", "code": "BS"}, 
{"name": "Bahrain", "code": "BH"}, 
{"name": "Bangladesh", "code": "BD"}, 
{"name": "Barbados", "code": "BB"}, 
{"name": "Belarus", "code": "BY"}, 
{"name": "Belgium", "code": "BE"}, 
{"name": "Belize", "code": "BZ"}, 
{"name": "Benin", "code": "BJ"}, 
{"name": "Bermuda", "code": "BM"}, 
{"name": "Bhutan", "code": "BT"}, 
{"name": "Bolivia", "code": "BO"}, 
{"name": "Bosnia and Herzegovina", "code": "BA"}, 
{"name": "Botswana", "code": "BW"}, 
{"name": "Bouvet Island", "code": "BV"}, 
{"name": "Brazil", "code": "BR"}, 
{"name": "British Indian Ocean Territory", "code": "IO"}, 
{"name": "Brunei Darussalam", "code": "BN"}, 
{"name": "Bulgaria", "code": "BG"}, 
{"name": "Burkina Faso", "code": "BF"}, 
{"name": "Burundi", "code": "BI"}, 
{"name": "Cambodia", "code": "KH"}, 
{"name": "Cameroon", "code": "CM"}, 
{"name": "Canada", "code": "CA"}, 
{"name": "Cape Verde", "code": "CV"}, 
{"name": "Cayman Islands", "code": "KY"}, 
{"name": "Central African Republic", "code": "CF"}, 
{"name": "Chad", "code": "TD"}, 
{"name": "Chile", "code": "CL"}, 
{"name": "China", "code": "CN"}, 
{"name": "Christmas Island", "code": "CX"}, 
{"name": "Cocos (Keeling) Islands", "code": "CC"}, 
{"name": "Colombia", "code": "CO"}, 
{"name": "Comoros", "code": "KM"}, 
{"name": "Congo", "code": "CG"}, 
{"name": "Congo, The Democratic Republic of the", "code": "CD"}, 
{"name": "Cook Islands", "code": "CK"}, 
{"name": "Costa Rica", "code": "CR"}, 
{"name": "Cote D\"Ivoire", "code": "CI"}, 
{"name": "Croatia", "code": "HR"}, 
{"name": "Cuba", "code": "CU"}, 
{"name": "Cyprus", "code": "CY"}, 
{"name": "Czech Republic", "code": "CZ"}, 
{"name": "Denmark", "code": "DK"}, 
{"name": "Djibouti", "code": "DJ"}, 
{"name": "Dominica", "code": "DM"}, 
{"name": "Dominican Republic", "code": "DO"}, 
{"name": "Ecuador", "code": "EC"}, 
{"name": "Egypt", "code": "EG"}, 
{"name": "El Salvador", "code": "SV"}, 
{"name": "Equatorial Guinea", "code": "GQ"}, 
{"name": "Eritrea", "code": "ER"}, 
{"name": "Estonia", "code": "EE"}, 
{"name": "Ethiopia", "code": "ET"}, 
{"name": "Falkland Islands (Malvinas)", "code": "FK"}, 
{"name": "Faroe Islands", "code": "FO"}, 
{"name": "Fiji", "code": "FJ"}, 
{"name": "Finland", "code": "FI"}, 
{"name": "France", "code": "FR"}, 
{"name": "French Guiana", "code": "GF"}, 
{"name": "French Polynesia", "code": "PF"}, 
{"name": "French Southern Territories", "code": "TF"}, 
{"name": "Gabon", "code": "GA"}, 
{"name": "Gambia", "code": "GM"}, 
{"name": "Georgia", "code": "GE"}, 
{"name": "Germany", "code": "DE"}, 
{"name": "Ghana", "code": "GH"}, 
{"name": "Gibraltar", "code": "GI"}, 
{"name": "Greece", "code": "GR"}, 
{"name": "Greenland", "code": "GL"}, 
{"name": "Grenada", "code": "GD"}, 
{"name": "Guadeloupe", "code": "GP"}, 
{"name": "Guam", "code": "GU"}, 
{"name": "Guatemala", "code": "GT"}, 
{"name": "Guernsey", "code": "GG"}, 
{"name": "Guinea", "code": "GN"}, 
{"name": "Guinea-Bissau", "code": "GW"}, 
{"name": "Guyana", "code": "GY"}, 
{"name": "Haiti", "code": "HT"}, 
{"name": "Heard Island and Mcdonald Islands", "code": "HM"}, 
{"name": "Holy See (Vatican City State)", "code": "VA"}, 
{"name": "Honduras", "code": "HN"}, 
{"name": "Hong Kong", "code": "HK"}, 
{"name": "Hungary", "code": "HU"}, 
{"name": "Iceland", "code": "IS"}, 
{"name": "India", "code": "IN"}, 
{"name": "Indonesia", "code": "ID"}, 
{"name": "Iran, Islamic Republic Of", "code": "IR"}, 
{"name": "Iraq", "code": "IQ"}, 
{"name": "Ireland", "code": "IE"}, 
{"name": "Isle of Man", "code": "IM"}, 
{"name": "Israel", "code": "IL"}, 
{"name": "Italy", "code": "IT"}, 
{"name": "Jamaica", "code": "JM"}, 
{"name": "Japan", "code": "JP"}, 
{"name": "Jersey", "code": "JE"}, 
{"name": "Jordan", "code": "JO"}, 
{"name": "Kazakhstan", "code": "KZ"}, 
{"name": "Kenya", "code": "KE"}, 
{"name": "Kiribati", "code": "KI"}, 
{"name": "Korea, Democratic People\"S Republic of", "code": "KP"}, 
{"name": "Korea, Republic of", "code": "KR"}, 
{"name": "Kuwait", "code": "KW"}, 
{"name": "Kyrgyzstan", "code": "KG"}, 
{"name": "Lao People\"S Democratic Republic", "code": "LA"}, 
{"name": "Latvia", "code": "LV"}, 
{"name": "Lebanon", "code": "LB"}, 
{"name": "Lesotho", "code": "LS"}, 
{"name": "Liberia", "code": "LR"}, 
{"name": "Libyan Arab Jamahiriya", "code": "LY"}, 
{"name": "Liechtenstein", "code": "LI"}, 
{"name": "Lithuania", "code": "LT"}, 
{"name": "Luxembourg", "code": "LU"}, 
{"name": "Macao", "code": "MO"}, 
{"name": "Macedonia, The Former Yugoslav Republic of", "code": "MK"}, 
{"name": "Madagascar", "code": "MG"}, 
{"name": "Malawi", "code": "MW"}, 
{"name": "Malaysia", "code": "MY"}, 
{"name": "Maldives", "code": "MV"}, 
{"name": "Mali", "code": "ML"}, 
{"name": "Malta", "code": "MT"}, 
{"name": "Marshall Islands", "code": "MH"}, 
{"name": "Martinique", "code": "MQ"}, 
{"name": "Mauritania", "code": "MR"}, 
{"name": "Mauritius", "code": "MU"}, 
{"name": "Mayotte", "code": "YT"}, 
{"name": "Mexico", "code": "MX"}, 
{"name": "Micronesia, Federated States of", "code": "FM"}, 
{"name": "Moldova, Republic of", "code": "MD"}, 
{"name": "Monaco", "code": "MC"}, 
{"name": "Mongolia", "code": "MN"}, 
{"name": "Montenegro", "code": "ME"},
{"name": "Montserrat", "code": "MS"},
{"name": "Morocco", "code": "MA"}, 
{"name": "Mozambique", "code": "MZ"}, 
{"name": "Myanmar", "code": "MM"}, 
{"name": "Namibia", "code": "NA"}, 
{"name": "Nauru", "code": "NR"}, 
{"name": "Nepal", "code": "NP"}, 
{"name": "Netherlands", "code": "NL"}, 
{"name": "Netherlands Antilles", "code": "AN"}, 
{"name": "New Caledonia", "code": "NC"}, 
{"name": "New Zealand", "code": "NZ"}, 
{"name": "Nicaragua", "code": "NI"}, 
{"name": "Niger", "code": "NE"}, 
{"name": "Nigeria", "code": "NG"}, 
{"name": "Niue", "code": "NU"}, 
{"name": "Norfolk Island", "code": "NF"}, 
{"name": "Northern Mariana Islands", "code": "MP"}, 
{"name": "Norway", "code": "NO"}, 
{"name": "Oman", "code": "OM"}, 
{"name": "Pakistan", "code": "PK"}, 
{"name": "Palau", "code": "PW"}, 
{"name": "Palestinian Territory, Occupied", "code": "PS"}, 
{"name": "Panama", "code": "PA"}, 
{"name": "Papua New Guinea", "code": "PG"}, 
{"name": "Paraguay", "code": "PY"}, 
{"name": "Peru", "code": "PE"}, 
{"name": "Philippines", "code": "PH"}, 
{"name": "Pitcairn", "code": "PN"}, 
{"name": "Poland", "code": "PL"}, 
{"name": "Portugal", "code": "PT"}, 
{"name": "Puerto Rico", "code": "PR"}, 
{"name": "Qatar", "code": "QA"}, 
{"name": "Reunion", "code": "RE"}, 
{"name": "Romania", "code": "RO"}, 
{"name": "Russian Federation", "code": "RU"}, 
{"name": "RWANDA", "code": "RW"}, 
{"name": "Saint Helena", "code": "SH"}, 
{"name": "Saint Kitts and Nevis", "code": "KN"}, 
{"name": "Saint Lucia", "code": "LC"}, 
{"name": "Saint Pierre and Miquelon", "code": "PM"}, 
{"name": "Saint Vincent and the Grenadines", "code": "VC"}, 
{"name": "Samoa", "code": "WS"}, 
{"name": "San Marino", "code": "SM"}, 
{"name": "Sao Tome and Principe", "code": "ST"}, 
{"name": "Saudi Arabia", "code": "SA"}, 
{"name": "Senegal", "code": "SN"}, 
{"name": "Serbia", "code": "RS"}, 
{"name": "Seychelles", "code": "SC"}, 
{"name": "Sierra Leone", "code": "SL"}, 
{"name": "Singapore", "code": "SG"}, 
{"name": "Slovakia", "code": "SK"}, 
{"name": "Slovenia", "code": "SI"}, 
{"name": "Solomon Islands", "code": "SB"}, 
{"name": "Somalia", "code": "SO"}, 
{"name": "South Africa", "code": "ZA"}, 
{"name": "South Georgia and the South Sandwich Islands", "code": "GS"}, 
{"name": "Spain", "code": "ES"}, 
{"name": "Sri Lanka", "code": "LK"}, 
{"name": "Sudan", "code": "SD"}, 
{"name": "Suriname", "code": "SR"}, 
{"name": "Svalbard and Jan Mayen", "code": "SJ"}, 
{"name": "Swaziland", "code": "SZ"}, 
{"name": "Sweden", "code": "SE"}, 
{"name": "Switzerland", "code": "CH"}, 
{"name": "Syrian Arab Republic", "code": "SY"}, 
{"name": "Taiwan, Province of China", "code": "TW"}, 
{"name": "Tajikistan", "code": "TJ"}, 
{"name": "Tanzania, United Republic of", "code": "TZ"}, 
{"name": "Thailand", "code": "TH"}, 
{"name": "Timor-Leste", "code": "TL"}, 
{"name": "Togo", "code": "TG"}, 
{"name": "Tokelau", "code": "TK"}, 
{"name": "Tonga", "code": "TO"}, 
{"name": "Trinidad and Tobago", "code": "TT"}, 
{"name": "Tunisia", "code": "TN"}, 
{"name": "Turkey", "code": "TR"}, 
{"name": "Turkmenistan", "code": "TM"}, 
{"name": "Turks and Caicos Islands", "code": "TC"}, 
{"name": "Tuvalu", "code": "TV"}, 
{"name": "Uganda", "code": "UG"}, 
{"name": "Ukraine", "code": "UA"}, 
{"name": "United Arab Emirates", "code": "AE"}, 
{"name": "United Kingdom", "code": "GB"}, 
{"name": "United States", "code": "US"}, 
{"name": "United States Minor Outlying Islands", "code": "UM"}, 
{"name": "Uruguay", "code": "UY"}, 
{"name": "Uzbekistan", "code": "UZ"}, 
{"name": "Vanuatu", "code": "VU"}, 
{"name": "Venezuela", "code": "VE"}, 
{"name": "Viet Nam", "code": "VN"}, 
{"name": "Virgin Islands, British", "code": "VG"}, 
{"name": "Virgin Islands, U.S.", "code": "VI"}, 
{"name": "Wallis and Futuna", "code": "WF"}, 
{"name": "Western Sahara", "code": "EH"}, 
{"name": "Yemen", "code": "YE"}, 
{"name": "Zambia", "code": "ZM"}, 
{"name": "Zimbabwe", "code": "ZW"} 
]';
	$cid = 0;
	
	$enbale_reg_img_upload = $myHelpers->global_lib->get_option('enbale_reg_img_upload');
	$register_message = $myHelpers->global_lib->get_option('register_message'); 
	$disclaimer_message = $myHelpers->global_lib->get_option('disclaimer_message'); 
	if($query)
	{
		foreach($query as $row)
		{
			$fname = $row->first_name;
			$lname = $row->last_name;
			$email = $row->email;
			$phone = $row->phone;
			$cid = $row->client_id;
		}
	}
?>

<div class="site-section" style="padding: 3em 0;">
      <div class="container">
		
        <div class="row">
		
		<?php if( form_error('contact_name')) 	  { 	echo form_error('contact_name'); 	  } ?>
		<?php if( form_error('contact_email')) 	  { 	echo form_error('contact_email'); 	  } ?>
		<?php if( form_error('contact_subject')) 	  { 	echo form_error('contact_subject'); 	  } ?>
		<?php if( form_error('contact_message')) 	  { 	echo form_error('contact_message'); 	  } ?>


          
		  <div class="col-md-6 col-lg-6 mb-5 offset-lg-3 offset-md-3">
          
			
          <?php 
			$args = array('action' => base_url().'client/addRegister/add', 'class' => 'register_form_client  text-left', 'id' => 'register_form_client', 'autocomplete'=>"off",
			'enctype' => 'application/x-www-form-urlencoded', 'method' => 'POST');
			echo form_open('',$args);?> 
			
			  <?php if(isset($_SESSION['register_msg']) && !empty($_SESSION['register_msg'])) {
					echo $_SESSION['register_msg'];
					unset($_SESSION['register_msg']);
			   } 
			   
			   ?>
			   <input type="hidden" name="currentemail" id="currentemail" value="<?php echo $email;?>" />
			   <input type="hidden" name="cid" id="cid" value="<?php echo $cid;?>" />
			   <h4 class="text-center mb-4">Register an Account</h4>
			   
				<div class="row form-group">
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="first_name"><?php echo mlx_get_lang('First Name'); ?> <span class="required text-danger">*</span></label>
					  <input type="text" id="first_name" name="first_name"  required class="form-control" value="<?php echo $fname;?>" >
					</div>
			   
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="last_name"><?php echo mlx_get_lang('Last Name'); ?> <span class="required text-danger">*</span></label>
					  <input type="text" id="last_name" name="last_name"  required class="form-control" value="<?php echo $lname;?>" >
					</div>
			    </div>
				  
				  <div class="row form-group">
					<div class="col-md-12 mb-3 mb-md-0 validation-field">
					  <label class="font-weight-bold" for="user_name"><?php echo mlx_get_lang('Username'); ?> <span class="required text-danger">*</span></label>
					  <input type="text" id="username" name="username" autocomplete="off" required class="form-control " value="<?php echo $email;?>" >
					  <i class="fa fa-spinner fa-spin"></i>
					  <!--<p class="help-block" style="margin:0px; font-style:italic;">Use lower case without space and special characters are allowed.</p>-->
					</div>
				  </div>
				
			
				<!--<div class="row form-group">
					<div class="col-md-12 mb-3 mb-md-0 validation-field">
					  <label class="font-weight-bold" for="email"><?php echo mlx_get_lang('Email'); ?> <span class="required text-danger">*</span></label>
					  <input type="email" id="email" name="email" autocomplete="off"  required class="form-control " value="<?php echo $fname;?>" >
					  <i class="fa fa-spinner fa-spin"></i>
					</div>
				</div>-->
			  
              <div class="row form-group">
                <div class="col-md-6">
                  <label class="font-weight-bold" for="password"><?php echo mlx_get_lang('Password'); ?> <span class="required text-danger">*</span></label>
                  <input type="password" id="password"  name="password" required class="form-control" autocomplete="new-password">
                </div>
              
                <div class="col-md-6">
                  <label class="font-weight-bold" for="repeat_password"><?php echo mlx_get_lang('Repeat Password'); ?> <span class="required text-danger">*</span></label>
                  <input type="password" id="repeat_password"  name="repeat_password" required class="form-control" >
                </div>
              </div>			  
			    <?php
			    $countryList = json_decode($country_list);
			   // print_r($countryList);
			    ?>
			    <div class="row form-group">
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="gender">Country</label>
					  <select class="form-control" name="country" id="country">
					  	<?php
					  	$selCountryName = 'Pakistan';
					  	foreach($countryList as $rec)
						{
							$sel_name = ($rec->name == $selCountryName)? "selected=selected": "";
					  	?>
					  		<option value="<?php echo $rec->name;?>" <?php echo $sel_name;?>><?php echo $rec->name;?></option>
					  	<?php
						}						
					  	?>						  	
					  </select>
					</div>
			   
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="state">State</label>
                  	  <input type="text" id="state" name="state" class="form-control" >
					</div>
					
			    </div>
			    
			     <div class="row form-group">
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="city">City</label>
                  	  <input type="text" id="city" name="city" class="form-control" >					  
					</div>
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="zip">Zip</label>
                  	  <input type="text" id="zip" name="zip" class="form-control" >
					</div>
				</div>	
				<div class="row form-group">
					<div class="col-md-12 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="address">Address</label>
					  <input type="text" id="address" name="address" class="form-control" >
					</div>
					
			    </div>
				<div class="row form-group">
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="address">Phone</label>
					  <input type="text" id="phone" name="phone" class="form-control" value="<?php echo $phone;?>" >
					</div>
					<div class="col-md-6 mb-3 mb-md-0">
					  <label class="font-weight-bold" for="gender">Gender</label>
					  <select class="form-control" name="gender" id="gender">
					  	<option value="Male">Male</option>
					  	<option value="Female">Female</option>
					  </select>
					</div>
			   
			    </div>
			  <?php //if($enbale_reg_img_upload == 'Y'){ ?>
			  <div class="form-group row">
				<div class="col-md-12">
					
					<label class="custom-file-upload rounded-2">
						<input type="file" accept="image/*" id="att_photo" name="attachments" data-type="photo" data-user-type="user"/>
						<i class="fa fa-cloud-upload"></i> <?php echo mlx_get_lang('Upload Image'); ?>
					</label>
					<progress id="att_photo_progress" value="0" max="100" style="display:none;"></progress>
					<a id="att_photo_link" href="" download="" style="display:none;">
						<img src="">
					</a>
					<a class="remove_img" id="att_photo_remove_img" data-name="att_photo" title="Remove Image" href="#" style="display:none;"><i class="fa fa-remove"></i></a>
					<!--photo_url_field-->
					<input type="text" name="att_photo_hidden" class="" id="att_photo_hidden" style="visibility:hidden">
				</div>
			  </div>
			  <?php //} ?>
			  
			  <?php if($disclaimer_message != '' && $register_message != ''){ ?>
				<div class="form-group row">
					<div class="col-md-12">
						<label id="disclaimer_checkbox-error" class="error" for="disclaimer_checkbox" style="display:none;">This field is required.</label>
					</div>
					<div class="col-md-12">
						<label style="margin:0px;">
							<input type="checkbox" required class="" name="disclaimer_checkbox" value="Y" id="disclaimer_checkbox"> &nbsp; <?php echo $register_message; ?></label>
					</div>
					<div class="col-md-12">
						<br>
						<?php echo $disclaimer_message; ?>
					</div>
				</div>
			  <?php } ?>
			  
              <div class="row form-group mt-4">
                <div class="col-md-12">
                  <button type="submit" id="btnRegister" name="register" class="btn submit-contact-form-btn py-2 px-4 rounded-2 text-white btn-block"><?php echo mlx_get_lang('Register'); ?></button>
                </div>
              </div>
			</form>
				
          </div>
		  
        </div>
      </div>
    </div>
<script>
$(function () {

});	
</script>    