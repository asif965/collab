<?php if(isset($banner_row) && isset($banner_row->b_image) && !empty($banner_row->b_image) && file_exists('uploads/banner/'.$banner_row->b_image)){ ?>
<section class="page-top-section set-bg" 
	data-setbg="<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>" 
	style="background-image: url(<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>);">
	<div class="container text-white">
		<h1><?php 
		if(isset($banner_title) && !empty($banner_title))
			echo mlx_get_lang($banner_title); ?>
		</h1>
	</div>
</section>
<?php } ?>
<div class="site-section site-section-sm pb-0" style="margin-top: 60px;">
  <div class="container">
	<div class="row">
		<div class="col-md-12">
			
	  <form action="<?php echo base_url();?>main/" name="add_form_post" class="form-search col-md-12" style="margin-top: -64px;" method="post" enctype="multipart/form-data" accept-charset="utf-8">
		
		<div class="row  align-items-end">
		
				
		  <div class="col-md-9 cols-5 search-filter-block">
			<label for="list-types">Agent Name</label>
			<div class="select-wrap">
			  <input type="text" name="agentSearch" id="agentSearch" class="form-control" value="<?php echo $search_keyword;?>" />
			</div>
		  </div>
		 
				
		  
		  
		  
		  <div class="col-md-3 cols-5">
			<button type="submit" class="btn btn-success text-white btn-block rounded-0">Search</button>
		  </div>
		</div>
		
		<link rel="stylesheet" href="<?php echo base_url();?>themes/default/css/icheck.min_all.css">
<link rel="stylesheet" href="<?php echo base_url();?>themes/default/css/price-range.css">



<style type="text/css">
.slider.disabled .slider-handle {
  background-image: -webkit-linear-gradient(top, #dfdfdf 0%, #bebebe 100%);
  background-image: -o-linear-gradient(top, #dfdfdf 0%, #bebebe 100%);
  background-image: linear-gradient(to bottom, #dfdfdf 0%, #bebebe 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffdfdfdf', endColorstr='#ffbebebe', GradientType=0);
  background-repeat: repeat-x;
}
.slider.disabled .slider-track {
  background-image: -webkit-linear-gradient(top, #e5e5e5 0%, #e9e9e9 100%);
  background-image: -o-linear-gradient(top, #e5e5e5 0%, #e9e9e9 100%);
  background-image: linear-gradient(to bottom, #e5e5e5 0%, #e9e9e9 100%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffe5e5e5', endColorstr='#ffe9e9e9', GradientType=0);
  background-repeat: repeat-x;
  cursor: not-allowed;
}

.slider.disabled .slider-selection {
    background: none repeat scroll 0 0 #fff;
}
.adv-search-wrapper .tooltip.in {
	filter:alpha(opacity=90);
	opacity:.9
}	
.adv-search-wrapper .tooltip {
    font-size: 14px;
    font-weight: bold;
}
.adv-search-wrapper .tooltip-arrow{
    display: none;
    opacity: 0;
}
.adv-search-wrapper .tooltip-inner {
    background-color: #FAE6A4;
    border-radius: 4px;
    box-shadow: 0 1px 13px rgba(0, 0, 0, 0.14), 0 0 0 1px rgba(115, 71, 38, 0.23);
    color: #734726;
    max-width: 200px;
    padding: 6px 10px;
    text-align: center;
    text-decoration: none;
}
.adv-search-wrapper .tooltip-inner:after {
    content: "";
    display: inline-block;
    left: 100%;
    margin-left: -56%;
    position: absolute;
}
.adv-search-wrapper .tooltip-inner:before {
    content: "";
    display: inline-block;
    left: 100%;
    margin-left: -56%;
    position: absolute;
}
.adv-search-wrapper .tooltip.top {
    margin-top: -11px;
    padding: 0;
}
.adv-search-wrapper .tooltip.top .tooltip-inner:after {
    border-top: 11px solid #FAE6A4;
    border-left: 11px solid rgba(0, 0, 0, 0);
    border-right: 11px solid rgba(0, 0, 0, 0);
    bottom: -10px;
}
.adv-search-wrapper .tooltip.top .tooltip-inner:before {
    border-top: 11px solid rgba(0, 0, 0, 0.2);
    border-left: 11px solid rgba(0, 0, 0, 0);
    border-right: 11px solid rgba(0, 0, 0, 0);
    bottom: -11px;
}
.checkbox label{
	overflow: hidden;
	white-space: nowrap;
	text-overflow: ellipsis;
	display: inline-block;
	width: 100%;
	margin-bottom:0px;
}
</style>

<script type="text/javascript">

$(document).ready(function () {
	
	//console.log($("#list-types option:selected").val());
	function reset_adv_search_options(list_type){
		
		if(list_type == '') return false;
		var list_types_bed_enable = [];    //["flat","apartment","villa"];
		var list_types_bath_enable = [];   ///["flat","apartment","villa"];
		
		
		if($.inArray(list_type , list_types_bed_enable) > -1 )
		{
			$('#min-bed').slider('enable');
			
			$('#min-bed').attr('disabled',false);
			
		}else	
		{
			$('#min-bed').slider('disable');
			
			$('#min-bed').attr('disabled',true);
			
		}
		
		if($.inArray(list_type , list_types_bath_enable) > -1 )
		{
			$('#min-baths').slider('enable');
			
			$('#min-baths').attr('disabled',false);
			
			
		}else	
		{
			$('#min-baths').slider('disable');
			
			$('#min-baths').attr('disabled',true);
			
		}
		
		
	}
	var list_type = $("#list-types option:selected").val();
	
	reset_adv_search_options( list_type);
	
	$("#list-types").on('change',function(){
		
		//console.log('change');
		var list_type = $(this).val();
		//console.log('change' + list_type);
		reset_adv_search_options( list_type);	
		
	});
	
	
	$(".price_range").on('change',function(){
		
		var price_range_min = $('input[name=price_range_min]').val();
		var price_range_max = $('input[name=price_range_max]').val();
		$('input[name=price_ranges]').val( price_range_min +  ',' + price_range_max);
		
	});
	
	$(".bath_range").on('change',function(){
		
		var bath_range_min = $('input[name=bath_range_min]').val();
		var bath_range_max = $('input[name=bath_range_max]').val();
		$('input[name=bath_ranges]').val( bath_range_min +  ',' + bath_range_max);
		
	});
	
	$(".bed_range").on('change',function(){
		
		var bed_range_min = $('input[name=bed_range_min]').val();
		var bed_range_max = $('input[name=bed_range_max]').val();
		
		
		$('input[name=bed_ranges]').val( bed_range_min +  ',' + bed_range_max);
		
	});
});	

</script>
	
				
		
	  </form>
	  </div>
	</div>  
		<div class="row">
	  <div class="col-md-12">
		<div class="view-options bg-white py-3 px-3 d-md-flex align-items-center">
		  <?php
		  if($view_list == 'list')
		  {
		  	$active_list_cls = 'active';
			$active_grid_cls = '';
		  }
		 else 
		 {
			$active_grid_cls = 'active';	
			$active_list_cls = '';	
		 }
		if($search_keyword == '')
		{
		  ?>
		  <div class="mr-auto">
		  			<a href="/main/?view=grid" class="icon-view view-module <?php echo $active_grid_cls;?>"><span class="icon-view_module"></span> Grid</a>
			&nbsp;&nbsp;
			<a href="/main/?view=list" class="icon-view view-list <?php echo $active_list_cls;?>"><span class="icon-view_list"></span> List</a>
			
		  </div>
		 <?php
		}
		else
		{
		?>
			<div class="mr-auto">
		  			<a href="/main/?view=grid&schkey=<?php echo $search_keyword;?>" class="icon-view view-module <?php echo $active_grid_cls;?>"><span class="icon-view_module"></span> Grid</a>
			&nbsp;&nbsp;
			<a href="/main/?view=list&schkey=<?php echo $search_keyword;?>" class="icon-view view-list <?php echo $active_list_cls;?>"><span class="icon-view_list"></span> List</a>
			
		  </div>
		<?php	
		}	
		 ?> 
		  <div class="ml-auto d-flex align-items-center">
			<div style="display: none;">
			  <a href="http://local.collab.com/search/" class="view-list px-3 border-right active">All</a>
			  <a href="http://local.collab.com/search/property-for-rent" class="view-list px-3 border-right">Rent</a>
			  <a href="http://local.collab.com/search/property-for-sale" class="view-list px-3">Sale</a>
			</div>
			
		  </div>
		</div>
	  </div>
	</div>
	  </div>
</div>
<div class="site-section" style="padding-top: 25px;">
      <div class="container">	
		<?php
		//echo $search_keyword.'AAA';exit;
			$flag = 0;
			if($search_keyword == '')
			{ 
				$sql = "SELECT u . *
						FROM users AS u
						WHERE u.user_type = 'agent'
						GROUP BY u.user_id";
			}
			else
			{
				$sqlSearch = "SELECT user_id
						FROM user_meta
						WHERE meta_key = 'first_name' and meta_value like '%".$search_keyword."%'
						OR meta_key = 'last_name' and meta_value like '%".$search_keyword."%' 
						";
				$search_agents = $this->Common_model->commonQuery($sqlSearch);		
				if($search_agents->num_rows() == 0)
				{
					$flag = 1;
					$sch_key = '';
				}
				else
				{
					$flag = 0;
					$sch_key = ' AND u.user_id = '.$search_agents->row()->user_id;
				}			
				$sql = "SELECT u . *
						FROM users AS u
						WHERE u.user_type = 'agent' ".$sch_key." 
						GROUP BY u.user_id";
			}				
			$all_agents = $this->Common_model->commonQuery($sql );
			
			if($all_agents->num_rows() > 0 && $flag == 0){
		?>
		<div class="section-heading mb-2">
			<h3 class="mb-3 pb-2 section-title">All Agents </h3>
			
		</div>
			
		<div class="row mb-5">
			<?php 
			
			foreach($all_agents->result() as $a_row ) {
				
			$qry = "SELECT COUNT(*) as totalClient
					FROM clients
					WHERE agent_id = '".$a_row->user_id."'
					";
			$agents_detail = $this->Common_model->commonQuery($qry );	 
			
			$Qry = "SELECT COUNT(*) as totalProperty
					FROM properties
					WHERE created_by = '".$a_row->user_id."'
					";
			$agents_property_detail = $this->Common_model->commonQuery($Qry);	 
			
			$p_images = $myHelpers->global_lib->get_user_meta($a_row->user_id,'photo_url');
			if(!empty($p_images) && file_exists('uploads/user/'.$p_images))
			{
				$user_image_url = base_url().'uploads/user/'.$p_images;
				$width = 'auto';
				$height = '180px;';
			}
			else
			{
				$user_image_url = base_url().'themes/'.$theme.'/images/no-user-image.png';
				$width = '73%;';
				$height = 'auto';
			}
			$first_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'first_name');
			$last_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'last_name');
			$full_name = strtolower($first_name).' '.strtolower($last_name);
			$agent_url = site_url(array('main','agent',str_replace(' ','-',strtolower($full_name)).'~'.$this->global_lib->EncryptClientID($a_row->user_id))); 
			$address = ($myHelpers->global_lib->get_user_meta($a_row->user_id,'address') != '')? $myHelpers->global_lib->get_user_meta($a_row->user_id,'address'): '<i class="fa fa-close" aria-hidden="true" style="color:#f00;padding-left:30px;"></i>';
			if($view_list == 'list')
			{
				if(!empty($p_images) && file_exists('uploads/user/'.$p_images))
				{
					$user_image_url = base_url().'uploads/user/'.$p_images;
					$width = 'auto';
					$height = '150px;';
				}
				else
				{
					$user_image_url = base_url().'themes/'.$theme.'/images/no-user-image.png';
					$width = '55%;';
					$height = 'auto';
				}
			?>
			<a href="<?php echo $agent_url; ?>" class="text-dark">
				<div class="col-md-12 col-lg-12 mb-12 text-center" style="margin-bottom: 15px;border: 1px solid #eee;border-radius: 5px;">
					<div class="row" style="">
						<div class="col-md-3 col-lg-3">
							<div class="agent-image-block" style="margin: 10px 0px 10px 0px;">
								<a href="<?php echo $agent_url; ?>">
									<img src="<?php echo $user_image_url; ?>" class="img-fluid img-thumbnail p-0" style="width: <?php echo $width;?>;height: <?php echo $height;?>;border:0px;">
								</a>
							</div>
						</div>
						<div class="col-md-6 col-lg-6" style="border-right:1px solid #eee;background: #f9f9f9;">
							<h5 class="" style="text-align: left;margin: 10px 0px 10px 0px;"><a href="<?php echo $agent_url; ?>" class="text-dark"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5>
							<p class="pb-2"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;width: 90%;text-align: left;"><?php echo $address; ?></a></p>
						</div>
						<div class="col-md-3 col-lg-3">
							<p class="pb-2" style="margin: 10px 0px 10px 0px;"><i class="fa fa-user-o" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Number of Clients</strong></a><span style="float: right;"><strong><?php echo $agents_detail->row()->totalClient;?></strong></span></p><br />
							<p class="pb-2"><i class="fa fa-home" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Number of Properties</strong></a><span style="float: right;"><strong><?php echo $agents_property_detail->row()->totalProperty;?></strong></span></p><br />
							<p class="pb-2"><i class="fa fa-calendar-times-o" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Joined</strong></a><span style="float: right;"><strong><?php echo date("F j, Y", $a_row->user_registered_date);?></strong></span></p><br />
						</div>	
					</div>
				</div>
			</a>	
			<?php
			}
			else
			{	
			?>	
			<a href="<?php echo $agent_url; ?>" class="text-dark">
				<div class="col-md-3 col-lg-3 mb-4 text-center">
					<div class="agent_block">
						<div class="agent-image-block">
							<a href="<?php echo $agent_url; ?>">
								<img src="<?php echo $user_image_url; ?>" class="img-fluid img-thumbnail p-0" style="width: <?php echo $width;?>;height: <?php echo $height;?>;border:0px;">
							</a>
						</div>
						<div class="agent-detail-block px-2 py-2">
							<h5 class="mt-0" style="text-align: left;margin-bottom: 15px;"><a href="<?php echo $agent_url; ?>" class="text-dark"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5>
							<!--<p class="pb-2"><a href="<?php echo $agent_url; ?>"><strong><?php //echo $a_row->total_property; ?></strong> Active Properties</a></p>-->
							<p class="pb-2"><i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;width: 90%;text-align: left;"><?php echo $address; ?></a></p><br />
							<p class="pb-2"><i class="fa fa-user-o" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Number of Clients</strong></a><span style="float: right;"><strong><?php echo $agents_detail->row()->totalClient;?></strong></span></p><br />
							<p class="pb-2"><i class="fa fa-home" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Number of Properties</strong></a><span style="float: right;"><strong><?php echo $agents_property_detail->row()->totalProperty;?></strong></span></p><br />
							<p class="pb-2"><i class="fa fa-calendar-times-o" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i><a href="<?php echo $agent_url; ?>" class="text-dark" style="float: left;"><strong>Joined</strong></a><span style="float: right;"><strong><?php echo date("F j, Y", $a_row->user_registered_date);?></strong></span></p><br />
							<!--<a href="<?php echo $agent_url; ?>" class="btn custom-btn btn-block rounded-0 text-white">View Details</a>-->
						</div>
					</div>
	            </div>
	        </a>    
			<?php
			} 
			} ?>
        </div>
		<?php }
			else
			{
				echo '<p class="alert alert-danger">no record found!</p>';
			}
		//}
		?>
		
						
      </div>
    </div>