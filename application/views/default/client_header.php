<!DOCTYPE html>
<html lang="<?php echo $myHelpers->default_language ?>">  <!-- en-US"> -->
<head>
    
<?php 
$website_title = $myHelpers->global_lib->get_option('website_title');
$social_media = $myHelpers->global_lib->get_option('social_media');
$company_tel = $myHelpers->global_lib->get_option('company_tel');
$company_email = $myHelpers->global_lib->get_option('company_email');
$site_language = $myHelpers->global_lib->get_option('site_language');
$default_language = $myHelpers->global_lib->get_option('default_language');
$fevicon_icon = $myHelpers->global_lib->get_option('fevicon_icon');
$enbale_front_end_registration = $myHelpers->global_lib->get_option('enbale_front_end_registration');
$enbale_front_end_login = $myHelpers->global_lib->get_option('enbale_front_end_login');

?>
	<?php if(isset($og_meta) && !empty($og_meta)){ ?>
	<?php if(isset($og_meta['og_url'])){ ?>
		<meta property="og:url" content="<?php echo $og_meta['og_url']; ?>" />
	<?php } ?>
	<?php if(isset($og_meta['og_type'])){ ?>
		<meta property="og:type" content="<?php echo $og_meta['og_type']; ?>" />
	<?php } ?>
	<?php if(isset($og_meta['og_title'])){ ?>
		<meta property="og:title" content="<?php echo $og_meta['og_title']; ?>" />
	<?php } ?>
	<?php if(isset($og_meta['og_description'])){ ?>
		<meta property="og:description"  content="<?php echo $og_meta['og_description']; ?>" />
	<?php } ?>
	<?php if(isset($og_meta['og_image'])){ ?>
		<meta property="og:image" content="<?php echo $og_meta['og_image']; ?>" />
	<?php }} ?>
	
	
	
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
	<?php if(isset($_SESSION['default_lang_front'])){
			$lang = $_SESSION['default_lang_front'];
			$lang_exp = explode("~",$lang);
			if(isset($lang_exp[1])){
				$clang = $lang_exp[1];
		?>
	<meta http-equiv="content-language" content="<?php echo $clang; ?>">
	<?php }	} ?>
	
	<?php 
	if(isset($seometa_for))
	{	
		$myHelpers->seometa_lib->get_metadata($seometa_for);
	}
	?>
	
	<?php
	if(isset($fevicon_icon) && !empty($fevicon_icon) && file_exists('uploads/media/'.$fevicon_icon) )
		echo '<link rel="shortcut icon" href="'.base_url().'uploads/media/'.$fevicon_icon.'">';
	else
		echo '<link rel="shortcut icon" href="'.base_url().'themes/default/images/fav.png">';
	?>
	
	
	
	<?php if(isset($this->canonical_url)){	?>
	<link rel="canonical" href="<?php echo $this->canonical_url; ?>" />
	<?php	}	?>
	
	<?php if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true){	?>
	<link rel="alternate" hreflang="<?php echo $this->hreflang; ?>" href="<?php echo $this->hreflang_url; ?>" />
	<?php	}	?>
	
	
	
    <title><?php if(isset($page_title) && !empty($page_title)) { echo stripslashes($page_title).' | '; }?><?php echo $website_title; ?></title>
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500"> 
	
	<?php echo link_tag("themes/$theme/css/font-awesome.min.css"); ?>
	<?php echo link_tag("themes/$theme/fonts/icomoon/style.css"); ?>
	<?php echo link_tag("themes/$theme/css/flag-icon.min.css"); ?>
	<?php echo link_tag("themes/$theme/css/bootstrap.min.css"); ?>
	<?php echo link_tag("themes/$theme/css/magnific-popup.css"); ?>
	<?php echo link_tag("themes/$theme/css/jquery-ui.css"); ?>
	<?php echo link_tag("themes/$theme/css/owl.carousel.min.css"); ?>
	<?php echo link_tag("themes/$theme/css/owl.theme.default.min.css"); ?>
	<?php echo link_tag("themes/$theme/fonts/flaticon/font/flaticon.css"); ?>
	<?php echo link_tag("themes/$theme/css/style.css"); ?>
	<?php echo link_tag("themes/$theme/css/custom-style.css"); ?>
	
	
	<?php echo script_tag("themes/$theme/js/jquery-3.3.1.min.js"); ?>
	<?php echo script_tag("themes/$theme/dompurify/0.8.4/purify.min.js"); ?>
	<?php if($this->site_direction == 'rtl') { ?>
		<?php echo link_tag("themes/$theme/css/bootstrap-rtl.min.css"); ?>
		<?php echo link_tag("themes/$theme/css/style-rtl.css"); ?>
	<?php } ?>

<script>
	var is_rtl = false;
	<?php if($this->site_direction == 'rtl') { ?>
		is_rtl = true;
	<?php } ?>
	
	var def_lang = '';
	<?php if($this->enable_multi_lang == true ) { ?>
		def_lang = '/<?php echo $this->default_language; ?>';
	<?php } ?>
	var base_url = '<?php echo base_url(); ?>';
</script>
	
<style type="text/css">
  .room-info .rf-float:nth-child(odd) { float:left; clear:left; }
  .room-info .rf-float:nth-child(even) { float:right; }
  </style>	
 </head>
  <body>
 


  <div class="site-loader"></div>
  
  <?php 
	if(isset($homepage_section) && count($homepage_section) > 0)
	{
		$sections = $homepage_section; 
		foreach($sections as $section_key => $section_settings)
		{
			if($section_settings['is_enable'] != 'Y' && $section_key == 'slider_section')
			{
				$has_banner = false;
				break;
			}
		
		}
		
	}
  ?>
  
  <div class="site-wrap <?php if(!isset($has_banner) || (isset($has_banner) && $has_banner != true)) echo 'no-banner';?> d-print-none">
  
	
    
    </div>
	
<?php 

$this->load->view($content); ?>

<?php $this->load->view("$theme/footer"); ?>