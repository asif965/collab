<?php if(isset($banner_row) && isset($banner_row->b_image) && !empty($banner_row->b_image) && file_exists('uploads/banner/'.$banner_row->b_image)){ ?>
<section class="page-top-section set-bg" 
	data-setbg="<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>" 
	style="background-image: url(<?php echo base_url(); ?>uploads/banner/<?php echo $banner_row->b_image; ?>);">
	<div class="container text-white">
		<h1><?php 
		if(isset($banner_title) && !empty($banner_title))
			echo mlx_get_lang($banner_title); ?>
		</h1>
	</div>
</section>
<?php } ?>

<div class="site-section">
      <div class="container">
		
		<?php if(isset($prop_for) || (!isset($prop_for) && $is_inner_query)) { ?>
		
		<?php if((isset($prop_for) && $prop_for == 'sale') || (!isset($prop_for) && $is_inner_query)){ 
			$sql = "SELECT u . * , count( p.p_id ) AS total_property
					FROM users AS u
					inner JOIN properties AS p ON p.created_by = u.user_id AND p.status = 'Y' and p.property_for = 'sale'
					WHERE u.user_type = 'agent'
					GROUP BY u.user_id limit 8";
			$all_agents = $this->Common_model->commonQuery($sql );
			
			if($all_agents->num_rows() > 0 ){
		?>
		<div class="section-heading mb-2">
			<h3 class="mb-3 pb-2 section-title"><?php echo mlx_get_lang('Agents in Property for Sale'); ?> </h3>
			<?php if(!isset($prop_for) && $is_inner_query) { ?>
				<a class="btn btn-dark pull-right btn-sm " href="<?php echo site_url(array('search',$this->default_language,'agents-for-sale')); ?>">View All</a>
			<?php } ?>
		</div>
			
		<div class="row mb-5">
			<?php 
			
			foreach($all_agents->result() as $a_row ) { 
			
			$p_images = $myHelpers->global_lib->get_user_meta($a_row->user_id,'photo_url');
			if(!empty($p_images) && file_exists('uploads/user/'.$p_images))
			{
				$user_image_url = base_url().'uploads/user/'.$p_images;
			}
			else
			{
				$user_image_url = base_url().'themes/'.$theme.'/images/no-user-image.png';
			}
			$first_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'first_name');
			$last_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'last_name');
			$full_name = strtolower($first_name).' '.strtolower($last_name);
			$agent_url = site_url(array('user','agent',$this->default_language,str_replace(' ','-',strtolower($full_name)).'~'.$this->global_lib->EncryptClientID($a_row->user_id))); 
			?>
			<div class="col-md-3 col-lg-3 mb-4 text-center">
				<div class="agent_block">
					<div class="agent-image-block">
						<a href="<?php echo $agent_url; ?>">
							<img src="<?php echo $user_image_url; ?>" class="img-fluid img-thumbnail p-0">
						</a>
					</div>
					<div class="agent-detail-block px-2 py-2">
						<h5 class="mt-0"><a href="<?php echo $agent_url; ?>" class="text-dark"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5>
						<p class="pb-2"><a href="<?php echo $agent_url; ?>"><strong><?php echo $a_row->total_property; ?></strong> Active Properties</a></p>
						<a href="<?php echo $agent_url; ?>" class="btn custom-btn btn-block rounded-0 text-white">View Details</a>
					</div>
				</div>
            </div>
			<?php } ?>
        </div>
		<?php }
		}
		?>
		
		<?php if((isset($prop_for) && $prop_for == 'rent') || (!isset($prop_for) && $is_inner_query)){ 
			
			$sql = "SELECT u . * , count( p.p_id ) AS total_property
					FROM users AS u
					inner JOIN properties AS p ON p.created_by = u.user_id AND p.status = 'Y' and p.property_for = 'rent'
					WHERE u.user_type = 'agent'
					GROUP BY u.user_id limit 8";
			$all_agents = $this->Common_model->commonQuery($sql );
			if($all_agents->num_rows() > 0 ){
		?>
		
		<div class="section-heading mb-2">
			<h3 class="mb-3 pb-2 section-title"><?php echo mlx_get_lang('Agents in Property for Rent'); ?> </h3>
			<?php if(!isset($prop_for) && $is_inner_query) { ?>
				<a class="btn btn-dark pull-right btn-sm " href="<?php echo site_url(array('search',$this->default_language,'agents-for-rent')); ?>">View All</a>
			<?php } ?>
		</div>
		<div class="row mb-5">
			<?php 
			
			foreach($all_agents->result() as $a_row ) { 
			
			$p_images = $myHelpers->global_lib->get_user_meta($a_row->user_id,'photo_url');
			if(!empty($p_images) && file_exists('uploads/user/'.$p_images))
			{
				$user_image_url = base_url().'uploads/user/'.$p_images;
			}
			else
			{
				$user_image_url = base_url().'themes/'.$theme.'/images/no-user-image.png';
			}
			$first_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'first_name');
			$last_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'last_name');
			$mobile_no = $myHelpers->global_lib->get_user_meta($a_row->user_id,'mobile_no');
			$full_name = strtolower($first_name).' '.strtolower($last_name);
			$agent_url = site_url(array('user','agent',$this->default_language,str_replace(' ','-',strtolower($full_name)).'~'.$this->global_lib->EncryptClientID($a_row->user_id))); 
			?>
			<div class="col-md-3 col-lg-3 mb-4 text-center">
				<div class="agent_block">
					<div class="agent-image-block">
						<a href="<?php echo $agent_url; ?>">
							<img src="<?php echo $user_image_url; ?>" class="img-fluid img-thumbnail p-0">
						</a>
					</div>
					<div class="agent-detail-block px-2 py-2">
						<h5 class="mt-0"><a href="<?php echo $agent_url; ?>" class="text-dark"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5>
						<p class="pb-2"><a href="<?php echo $agent_url; ?>"><strong><?php echo $a_row->total_property; ?></strong> Active Properties</a></p>
						<a href="<?php echo $agent_url; ?>" class="btn custom-btn btn-block rounded-0 text-white">View Details</a>
					</div>
				</div>
            </div>
			<?php } ?>
        </div>
		<?php } ?>
		<?php }
		}
		?>
		
		<?php 
		if(isset($prop_type) || (!isset($prop_type) && $is_inner_query))
		{
		$sql = "SELECT title,pt_id FROM property_types WHERE status = 'Y'";
		
		if(isset($prop_type) && !empty($prop_type))
			$sql .= ' and LOWER(title) = "'.str_replace('_',' ',$prop_type).'"';
		
		$property_types = $this->Common_model->commonQuery($sql);
			
			foreach($property_types->result() as $ptrow) {
				
				$sql = "SELECT u . * , count( p.p_id ) AS total_property
					FROM users AS u
					inner JOIN properties AS p ON p.created_by = u.user_id AND p.status = 'Y' and p.property_type = $ptrow->pt_id
					WHERE u.user_type = 'agent'
					GROUP BY u.user_id limit 8";
					
			$all_agents = $this->Common_model->commonQuery($sql );
			if($all_agents->num_rows() > 0){ ?>
			
			<div class="section-heading mb-2">
				<h3 class="mb-3 pb-2 section-title"><?php echo mlx_get_lang('Agents in Property Type'); ?> <?php echo $ptrow->title; ?>  </h3>
				<?php if(!isset($prop_type) && $is_inner_query) { ?>
					<a class="btn btn-dark pull-right btn-sm " href="<?php echo site_url(array('search',$this->default_language,'agents-in-'.str_replace(' ','_',strtolower($ptrow->title)))); ?>">View All</a>
				<?php } ?>
			</div>
		<div class="row mb-5">
			<?php foreach($all_agents->result() as $a_row ) { 
			
			$p_images = $myHelpers->global_lib->get_user_meta($a_row->user_id,'photo_url');
			if(!empty($p_images) && file_exists('uploads/user/'.$p_images))
			{
				$user_image_url = base_url().'uploads/user/'.$p_images;
			}
			else
			{
				$user_image_url = base_url().'themes/'.$theme.'/images/no-user-image.png';
			}
			$first_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'first_name');
			$last_name = $myHelpers->global_lib->get_user_meta($a_row->user_id,'last_name');
			$mobile_no = $myHelpers->global_lib->get_user_meta($a_row->user_id,'mobile_no');
			$full_name = strtolower($first_name).' '.strtolower($last_name);
			$agent_url = site_url(array('user','agent',$this->default_language,str_replace(' ','-',strtolower($full_name)).'~'.$this->global_lib->EncryptClientID($a_row->user_id))); 
			?>
			<div class="col-md-3 col-lg-3 mb-4 text-center">
				<div class="agent_block">
					<div class="agent-image-block">
						<a href="<?php echo $agent_url; ?>">
							<img src="<?php echo $user_image_url; ?>" class="img-fluid img-thumbnail p-0">
						</a>
					</div>
					<div class="agent-detail-block px-2 py-2">
						<h5 class="mt-0"><a href="<?php echo $agent_url; ?>" class="text-dark"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></a></h5>
						<p class="pb-2"><a href="<?php echo $agent_url; ?>"><strong><?php echo $a_row->total_property; ?></strong> Active Properties</a></p>
						<a href="<?php echo $agent_url; ?>" class="btn custom-btn btn-block rounded-0 text-white">View Details</a>
					</div>
				</div>
            </div>
			<?php } ?>
        </div>
		<?php }} }?>
      </div>
    </div>