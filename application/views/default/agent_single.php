<?php 
$user_id = $single_user->user_id;
$first_name = $myHelpers->global_lib->get_user_meta($user_id,'first_name');
$last_name = $myHelpers->global_lib->get_user_meta($user_id,'last_name');
$mobile_no = $myHelpers->global_lib->get_user_meta($user_id,'mobile_no');
$address = $myHelpers->global_lib->get_user_meta($user_id,'address');
$photo_url = $myHelpers->global_lib->get_user_meta($user_id,'photo_url');
$description = $myHelpers->global_lib->get_user_meta($user_id,'description');
$social_media = $myHelpers->global_lib->get_user_meta($user_id,'social_media');
?>
	
<!--
<section class="page-top-section set-bg" data-setbg="<?php echo site_url().'themes/'.$theme.'/'; ?>images/search_banner.jpg" style="background-image: url(<?php echo site_url().'themes/'.$theme.'/'; ?>images/search_banner.jpg);">
	<div class="container text-white">
		<h1><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></h1>
	</div>
</section>
-->
<div class="site-section">
    <div class="container">
		<div class="row mb-5">
			<div class="col-md-3 text-center">
				<?php 
				if(isset($photo_url) && !empty($photo_url))
				{
					if(file_exists('uploads/user/'.$photo_url))
					{
						echo '<img src="'.base_url().'uploads/user/'.$photo_url.'" class="agent_img img-responsive img-thumbnail">';
					}
					else
					{
						echo '<img src="'.base_url().'themes/default/images/no-user-image.png'.'" class="agent_img img-responsive img-thumbnail">';
					}
				}
				else
				{
					echo '<img src="'.base_url().'themes/default/images/no-user-image.png'.'" class="agent_img img-responsive img-thumbnail">';
				}
				?>
				<div class="agent_img_bottom_block bg-black text-white pt-2 pb-2 pl-3 pr-3">
					<h5 class="mb-0"><?php echo ucfirst($first_name).' '.ucfirst($last_name); ?></h5>
					<p class="mb-0"><?php echo mlx_get_lang('Real Estate').' '.mlx_get_lang(ucfirst($single_user->user_type)); ?></p>
				</div>
				
			</div>
			<div class="col-md-5 text-left">
				<?php if(isset($description) && !empty($description)){ ?>
					<h2><?php echo mlx_get_lang('About'); ?> <?php echo ucfirst($first_name); ?></h2>
					<hr class="mt-0">
					<div class="agent-description mb-5"><?php echo $description; ?></div>
				<?php } ?>
				
				<h4><?php echo mlx_get_lang('Contact Details'); ?></h4>
				<hr class="mt-0">
				<div class="row  mb-3">
				<div class="col-md-5">				
					  <i class="fa fa-calendar-times-o" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i>
					  <strong>
					  	Joined on 
					  </strong>
					  <p><?php echo date("F j, Y", $single_user->user_registered_date);?></p>
				</div>
				<div class="col-md-5">				
					  <i class="fa fa-envelope-open" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i>
					  <strong>
					  	Email
					  </strong>
					   <p><?php echo ($single_user->user_email)? $single_user->user_email: '<i class="fa fa-close" aria-hidden="true" style="color:#f00;padding-left:30px;"></i>'; ?></p>
				</div>	
				</div>	
				<div class="row  mb-3">	
				
				<div class="col-md-5">
				  <i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i>
				  <strong>Phone</strong>
				  <p><?php echo ($mobile_no)? $mobile_no: '<i class="fa fa-close" aria-hidden="true" style="color:#f00;padding-left:30px;"></i>'; ?></p>
				</div>
				
				
				
				<div class="col-md-5">
				  <i class="fa fa-map-marker" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i>	
				  <strong>Address</strong>
				  <p><?php echo ($address)? $address: '<i class="fa fa-close" aria-hidden="true" style="color:#f00;padding-left:30px;"></i>'; ?></p>
				</div>
				
				<div class="col-md-12">
				  <i class="fa fa-whatsapp" aria-hidden="true" style="margin-right: 5px;float: left;margin-top: 3px;"></i>	
				  <strong>WHATSAPP LINKS</strong>
				  <p>
				  	<?php
				  	$site_whatsapp_no = ''; 
				  	/*$mobile_no = $myHelpers->global_lib->get_user_meta($single_user->user_id,'mobile_no');
					if(!empty($mobile_no))
					{
						$site_whatsapp_no = $mobile_no;
					}*/
					$whastapp_text = 'I want to get more detail about property';
				  	?>
				  	<a class="w-inline-block social-share-btn share-whatsapp" 
						title="Join Our Whatsapp Group" target="_blank" 
						href="https://api.whatsapp.com/send?phone=<?php echo $owner_whatsapp_no; ?>&text=<?php echo urlencode($whastapp_text); ?>" style="color:#060;">
						
					<strong><?php echo mlx_get_lang('Contact Property Owner'); ?></strong>
					</a>
				  </p>
				</div>
				
				</div>
			
				<h4>Agent Property Information</h4>
				<hr class="mt-0">
				<div class="row">
					<div class="col-sm-12 col-lg-12 col-md-12 text-center">
						<a href="#recent-listings" class="row">
							<div class="col-lg-3 col-md-3 col-sm-3">
									<img class="style_saleSVG__3-VdS" src="<?php echo base_url();?>themes/default/assets/img/for-sale.svg" style="width: 65%;">
							</div>		
							<div class="col-lg-3 col-md-3 col-sm-3 text-center">
								<h4 class=""><?php echo $agent_sale_property;?></h3>
								<strong>For Sale</strong>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
							<img class="style_rentSVG__2RX1s" src="<?php echo base_url();?>themes/default/assets/img/for-rent.svg" style="width: 65%;">
						</div>	
							<div class="col-lg-3 col-md-3 col-sm-3 text-center">
								<h4 class=""><?php echo $agent_rent_property;?></h3>
								<strong>For Rent</strong>
							</div>
						</a>
						
						
					</div>
					
				</div>	
			
				
			</div>
			
			<div class="col-md-4 text-left">
				
					<div class="bg-white widget border rounded text-left">
					  
						
					  <h3 class="h4 text-black widget-title mb-3"><?php echo mlx_get_lang('Contact for any Inquiry'); ?></h3>
					  <?php 
						$args = array('class' => 'form-contact-agent', 'id' => 'contact_agent_form');
						echo form_open_multipart('',$args);?> 
						<input type="hidden" name="agent_email" 
						value="<?php if(isset($single_user->user_email) && !empty($single_user->user_email)) echo $single_user->user_email; ?>">
						<input type="hidden" name="agent_form" 
						value="1">
						<div class="form-group">
						  <label for="name"><?php echo mlx_get_lang('Name'); ?> <span class="required text-danger">*</span></label>
						  <input type="text" id="name" name="name" class="form-control" required>
						</div>
						<div class="form-group">
						  <label for="email"><?php echo mlx_get_lang('Email'); ?> <span class="required text-danger">*</span></label>
						  <input type="email" id="email" name="email" class="form-control" required>
						</div>
						<div class="form-group">
						  <label for="message"><?php echo mlx_get_lang('Message'); ?> <span class="required text-danger">*</span></label>
						  <textarea id="message" name="message" class="form-control" required style="height:auto;"></textarea>
						</div>
						 <?php if(!empty($recaptcha_site_key) && !empty($recaptcha_secret_key)) { ?>
						  <div class="row form-group">
							<div class="col-md-12">
							  <div id="recaptcha_element"></div>
							</div>
						  </div>
						  <?php } ?>
						<div class="form-group">
						  <button type="submit" name="submit" class="btn custom-btn text-white submit-contact-agent-form-btn" ><?php echo mlx_get_lang('Send Message'); ?></button>
						</div>
					  </form>
					</div>
			</div>
		
	  </div>
	  
		
		
	</div>
</div>