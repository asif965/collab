<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// referance link :-   https://www.itsolutionstuff.com/post/codeigniter-3-restful-api-tutorialexample.html


require APPPATH . 'libraries/REST_Controller.php';
     
class GetProperty extends REST_Controller{
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	
	public function index_get($lan='en',$id=0)
	{	
			
		if($this->input->method(true) == 'GET'){
			
			if(!empty($id) && !empty($lan)){
				$this->load->model('Common_model');
				$data = $this->db->get("properties")->result();
				
				
				$multi_lang = True; //$this->enable_multi_lang;
				
				$default_lang = $lan;//$this->default_language;
				
				
				$def_lang_code = $default_lang_code =$default_lang;
					if($multi_lang )
					{
						$where =' and prop.p_id='.$id.'';
						
						$inner_join='';
						$sql = "select prop.*,pld1.* from properties as prop 
						inner join property_lang_details as pld1 on pld1.p_id = prop.p_id and
						pld1.language = '$def_lang_code'
						inner join users as u on u.user_id = prop.created_by and
						u.user_status = 'Y'
						$inner_join where prop.status = 'publish'  
						$where  group by prop.p_id order by prop.p_id DESC";
						
						
						//var_dump($sql);exit;
						
						$qry = $this->Common_model->commonQuery($sql);
						
							if($qry->num_rows() == 0){
								$msg =array(
							'status_code'=>'404',
							'message'=>'Property Not Found'
							);
							$data=json_encode($msg);
							$this->response($data, REST_Controller::HTTP_OK);
							}else{
								$data = $qry->result();
								$this->response($data, REST_Controller::HTTP_OK);
							}
					}
					
			}else{
				//var_dump('here');
				$msg =array(
				'status_code'=>'500',
				'message'=>'Langauge code or Property Id are Missing',
				);
				$data=json_encode($msg);
				$this->response($data, REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
				
			}
			
		}else{
			
			$msg =array(
			'status_code'=>'405',
			'message'=>'Method Not Allowed'
			);
			$data=json_encode($msg);
			$this->response($data, REST_Controller::HTTP_METHOD_NOT_ALLOWED);
		}
        
	}
	
	
      
    
}
