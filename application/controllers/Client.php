<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Client extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		
		$this->load->library('Language_lib');
		
	}
	
	public function register($key = NULL )
	{
		$data['footer_display'] = '';
		$this->load->model('Common_model');
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		//if($key == null)
		//	redirect('/client/erropage','location');
		
		$this->load->library('Global_lib');
		$this->load->helper('text');
		
		$qry = "SELECT client_id FROM clients 
			   WHERE token = '".$key."' 
			   AND is_verified = 0
			   AND status = 'pending'
			   ";
		$rec = $this->Common_model->commonQuery($qry); 
		$data['theme']=$theme;
		$data['myHelpers']=$this;
		$data['page_title'] = "Client Register";
		$data['has_banner'] = false;
		$data['class'] = '';
		$data['key'] = $key;	
		if($rec->num_rows() > 0)
		{
			$qry = "SELECT * FROM clients 
			   WHERE token = '".$key."' 
			   AND is_verified = 0
			   AND status = 'pending'
			   ";
			$row = $this->Common_model->commonQuery($qry); 
			$data['query'] = $row->result();
			$data['content'] = "$theme/client/register_client";
			$this->load->view("$theme/client_header",$data);
		}													   
		else
		{
			$sql = "SELECT client_id FROM clients 
			   WHERE token = '".$key."' 
			   AND is_verified = 1
			   AND status = 'active'
			   ";
			$result = $this->Common_model->commonQuery($sql);
			if($result->num_rows() > 0)
			{
				$data['content'] = "$theme/client/login_page";
				$this->load->view("$theme/client_header",$data);
			}
			else
			{	
				$data['content'] = "$theme/client/error_page";
				$this->load->view("$theme/client_header",$data);
			}	
		}
		
		
	}

	public function addRegister()
	{
		$data['footer_display'] = '';
		//print_r($_POST);
		extract($_POST);
		$time = date('Y-m-d');
		if($_POST['first_name'] != '')
		{
			$datai = array( 
							'first_name' => trim($first_name),
							'last_name' => trim($last_name),
							'username' => trim($username),							
							'password' => password_hash($password, PASSWORD_DEFAULT),
							'country' => $country,
							'state' => $state,
							'city' => $city,
							'zip' => $zip,
							'updated_on' => strtotime($time),
							'status' => 'active',
							'gender' => $gender,
							'image' => $att_photo_hidden,
							'address' => $address,
							'phone' => $phone,
							'is_verified' => '1',
					 );
				
			$res = $this->Common_model->commonUpdate('clients',$datai,'client_id', $cid);
			if($res >=1){
				$this->session->set_flashdata('login_message', 'You have registered. Please login here.');
				echo 'true';
			}else{
				echo 'Error';
			}
		}
	}
	
	public function login($index = 0)
	{
		//print_r($_POST);
		$CI =& get_instance();
		$theme = $CI->config->item('theme');
		$this->load->library('Global_lib');
		$this->load->helper('text');
		$data['theme']=$theme;
		$data['myHelpers']=$this;
		$data['content'] = "$theme/client/login_page";
		$data['footer_display'] = 'none';
		$this->load->view("$theme/client_header",$data);
	}
	
	public function checklogin()
	{
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$CI->load->library('Global_lib');
		$site_url = site_url();	
		
		if(isset($_POST['submit']))
		{
			
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			$username = $_POST['username'];
			$userpass = $_POST['userpass'];
			$this->load->model('Common_model');
			$sql="select * from clients 
					   where username='".$username."'
					   or email='".$username."'
				  ";
			
			$detail = $this->Common_model->commonQuery($sql);
			
			if($detail->num_rows()>0)
			{
				$data = $detail->row();
				if (password_verify($userpass, $data->password))
				{
					$newdata = array(  
					'first_name' => $data->first_name,
					'last_name' => $data->last_name,
					'username'  => $data->username, 
					'email'     => $data->email, 
					'client_id' => $data->client_id,
					'agent_id' => $data->agent_id,
					'logged_in' => TRUE					
					);
					foreach($newdata as $k=>$v)
					{
						$_SESSION[$k] = $v;
					}
					
					$today = date('Y-m-d');
					$uptData = array(  
					'active_on' => strtotime($today),
					'token' => ''					
					);
			
				   $this->Common_model->commonUpdate('clients',$uptData,'client_id', $data->client_id);
				   redirect('/main/property','location');
				}
				else
				{
					$_SESSION['logmsg'] = '<p class="error_msg">'.mlx_get_lang("Username/Password Mismatch").'</p>';					 
					redirect(base_url().'client/login/index','location');
				}
			}
			else
			{	
				 $_SESSION['logmsg'] = '<p class="error_msg">'.mlx_get_lang("Username does not exists.").'</p>';
				 redirect(base_url().'client/login/index','location');			
			}

		}
		else
		{			
			redirect(base_url().'client/login/index','location');			 
		}
	}
	
	public function forgot_password($index = 0)
	{
		//print_r($_POST);
		$CI =& get_instance();
		$theme = $CI->config->item('theme');
		$this->load->library('Global_lib');
		$this->load->helper('text');
		$data['theme']=$theme;
		$data['myHelpers']=$this;
		$data['content'] = "$theme/client/forgot_password";
		$data['footer_display'] = 'none';
		$this->load->view("$theme/client_header",$data);
	}
	
	public function verify_forgot_password()
	{
		$CI =& get_instance();
		$theme = $CI->config->item('theme');
		$CI->load->library('Global_lib');
		
		if(isset($_POST['submit']))
		{
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			$email=$_POST['email'];
			$this->load->model('Common_model');
			$sql="select * from clients 
				  where email='".$email."'"; 
			$detail = $this->Common_model->commonQuery($sql);
			
			if($detail->num_rows()>0)
			{
				$data = $detail->row();
				
				$this->load->helper('string');
				$user_code = random_string('alnum',12);
				
				$datai = array( 						
					'user_code' => $user_code
				);				
				$this->Common_model->commonUpdate('users',$datai,'user_id',$data->user_id);	
				
				$to = $email; 
				
				$this->load->library('Email_lib');
				
				$args['to_email'] = $email;
		
				$args['email_template'] = "forgot_password_email";	
				$args['user_id'] = $data->user_id;	
				$args['user_code'] = $user_code;	
				$args['forgot_password_link_url'] = base_url(array('logins','reset_password?email='.$email.'&user_code='.$user_code));
				
				$this->email_lib->send_email_notification($args);
				
				/*$site_domain_name = $this->global_lib->get_option('site_domain');
				$site_domain_email = $this->global_lib->get_option('site_domain_email');
				
				$first_name = $this->global_lib->get_user_meta($data->user_id,'first_name');
				$last_name = $this->global_lib->get_user_meta($data->user_id,'last_name');
			    $full_name = strtolower($first_name).' '.strtolower($last_name);
				
				$reset_password_link = base_url(array('logins','reset_password?email='.$email.'&user_code='.$user_code));
				$subject = "Reset Password Link";
				$htmlContent = ' 
					<html> 
					<head> 
						<title>'.mlx_get_lang("Reset Password Link").'</title> 
					</head> 
					<body> 
						<p>'.mlx_get_lang("Hello").' <strong>'.$full_name.'</strong></p>
						<p>'.mlx_get_lang("Please click on folowing link to reset your password.").'</p>
						<p><a href="'.$reset_password_link.'" target="_blank">'.$reset_password_link.'</a></p>
					</body> 
					</html>'; 
				
				$args = array();
				$args['CI'] = $CI; 
				$args['to'] = $to; 
				$args['msg'] = $htmlContent; 
				$args['subject'] = $subject; 
				
				$default_mailer = $this->global_lib->get_option('default_mailer');
				
				$response = false;
				if($default_mailer == "php_mail" || empty($default_mailer))			
					$response = $this->sendMailByPHP($args);
				else if($default_mailer == "smtp")			
					$response = $this->sendMailBySMTP($args);*/
				
				$_SESSION['msg'] = '<p class="success_msg">'.mlx_get_lang("Please check your email for reset password link").'</p>';
				redirect('/client/forgot_password/index','location');
			}
			else
			{	
				 $_SESSION['msg'] = '<p class="error_msg">'.mlx_get_lang("Email not registered").'</p>';
			}
		}
		
		$this->load->view(base_url.'client/forgot_password/index', $data);
	}
}
