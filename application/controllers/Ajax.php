<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends MY_Controller {
	
	
	public function set_default_language_frontend() 
	{
		extract($_POST);
	  
		if(isset($default_lang))
		  $_SESSION['default_lang_front'] = $default_lang;
	}
	
	public function update_compare_settion_callback_func() {
	  extract($_POST);
	  
	   if(isset($p_id) && !empty($p_id) && isset($action) && !empty($action) )
	   {
		  if(!isset($_SESSION['comparable_properties']))
		  {
			  $_SESSION['comparable_properties'] = array();
		  }
		  if($action == 'add')
		  {
			  if(!array_key_exists($p_id,$_SESSION['comparable_properties']))
			  {
				   $_SESSION['comparable_properties'][$p_id] = array( 
																	  'title' => $productTitle,
																	  'url' => $productURL,
																	  'img' => $productIMG);
			  }
		  }
		  else if($action == 'remove')
		  {
			  if(array_key_exists($p_id,$_SESSION['comparable_properties']))
			  {
				   unset($_SESSION['comparable_properties'][$p_id]);
			  }
		  }
	   }
	}
	
	
	public function favirate_callback_func() {
		
	  extract($_POST);
	  
	  
	   $user_id = $this->session->userdata('user_id');
	   if(isset($p_id) && !empty($p_id) && isset($action) && !empty($action) )
	   {
		  if(!isset($_SESSION['favirate_properties']))
		  {
			  $_SESSION['favirate_properties'] = array();
		  }
		  
		  if($action == 'add')
		  {
			  
					$decId = $this->DecryptClientId($p_id);  
					$url = base_url().'property/'.$productURL;
					
					$page_title = $productTitle;
					$cur_time = time();
						$datai = array( 
									'p_id' => trim($decId),
									'title' => trim($page_title),
									'url' => trim($url),
									'user_id' => $user_id,
									'craeted_at' => $cur_time,
									'updated_at' => $cur_time,
									);
					
				$res = $this->Common_model->commonInsert('favorite_table',$datai);
				if($res >=1){
					echo 'Added to Favourite';
				}else{
					echo 'Error';
				}
		  }
		  else if($action == 'remove')
		  {
					$decId = $this->DecryptClientId($p_id);  
					
									
				$result = $this->Common_model->commonQuery("DELETE FROM `favorite_table` WHERE p_id=$decId and user_id=$user_id");
				
				echo 'Add to Favourite';
		  }
	   }
	}
	
	/*
	public function favirate_remove_callback_func(){
		 extract($_POST);
		 $pid= $this->DecryptClientId($p_id); ;
		 $userId=$user_id;
		 $result = $this->Common_model->commonQuery("DELETE FROM `favorite_table` WHERE p_id=$decId and user_id=$user_id");
		 if($result == ture){
					echo 'Add to Favourite';
				}else{
					echo 'Error';
				}
		 
	}
	*/
	
	public function submit_contact_form_callback_func() {
	  
	  $CI =& get_instance();		
	  $this->load->model('Common_model');
	  $this->load->library('Global_lib');
	  
	  extract($_POST);
	  
		$is_recaptcha_enable = false;
		$isPlugAct = $this->isPluginActive('google_recaptcha');
		if($isPlugAct == true)
		{
			$is_recaptcha_enable = true;
		}
	  
	  $is_verifieid = true;
	  if($is_recaptcha_enable)
	  {
	  
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
		{ 
				$recaptcha_secret_key = $this->global_lib->get_option('recaptcha_secret_key');
				$secretKey = $recaptcha_secret_key; 
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['g-recaptcha-response']); 
				$responseData = json_decode($verifyResponse); 
				if($responseData->success)
				{ 
					$is_verifieid = true;
				}
				else{ 
					$return_type = 'error';
					$output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						Robot verification failed, please try again.
					</div>';
					$is_verifieid = false;
				} 
		}
		else
		{
			$return_type = 'error';
			$output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Please check on the reCAPTCHA box.
			</div>';
			$is_verifieid = false;
		}
	  }
	  $this->load->library('Email_lib');
	  
	  
		
		
	  
		if($is_verifieid)
		{
			
			$args = array();
		  $args ['post'] = $_POST;
		  if(isset($contact_email))
		  {
			  $args['to_email'] = $contact_email;
				
			  $args['email_template'] = "contact_us_email";	
			  $this->email_lib->send_email_notification($args);	
		  }	
			
				
				
			$em_args = array();
			$em_args['post'] = $_POST;
			$this->global_lib->send_email_notifications_to_admin("contact_us_email_admin" ,$em_args );	
				
			$return_type = 'success';
							$output = '<div class="alert alert-success alert-dismissable" style="margin-top:0px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.mlx_get_lang('Your contact request has been submitted successfully.').'
							</div>';
		
		}		
				
		  
							
		  header('Content-type: application/json');
		  echo json_encode(array('return_type'=>$return_type,'output' => $output));
		  return;
		  
	}
	
	
	public function sendMailByPHP($args = array()){
		
		extract($args);
		
		$site_domain_name = $this->global_lib->get_option('site_domain');
		$site_domain_email = $this->global_lib->get_option('site_domain_email');
		
		$headers = "MIME-Version: 1.0" . "\r\n"; 
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
		$headers .= 'From: '.$site_domain_name.'<'.$site_domain_email.'>' . "\r\n";
		$headers .= "X-Mailer: PHP ". phpversion();
		
		if(mail($to, $subject, $msg, $headers)){
			return true;
		}else{
			return false;
		}
		
	}	
	
	
	
	public function submit_contact_agent_form_callback_func() {
	  $currency_symbol = '';
	  $CI =& get_instance();		
	  $this->load->model('Common_model');
	  $this->load->library('Global_lib');
	  
	  extract($_POST);
	  
	  
	  $is_recaptcha_enable = false;
	  
		$isPlugAct = $this->isPluginActive('google_recaptcha');
		if($isPlugAct == true)
		{			
			$is_recaptcha_enable = true;
		}
	  
	  $is_verifieid = true;
	  if($is_recaptcha_enable)
	  {
		  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
			{ 
				$recaptcha_secret_key = $this->global_lib->get_option('recaptcha_secret_key');
				$secretKey = $recaptcha_secret_key; 
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretKey.'&response='.$_POST['g-recaptcha-response']); 
				$responseData = json_decode($verifyResponse); 
				if($responseData->success)
				{ 
					$is_verifieid = true;
				}
				else
				{ 
					$return_type = 'error';
					$output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						Robot verification failed, please try again.
					</div>';
					$is_verifieid = false;
				} 
			}
			else
			{
				$return_type = 'error';
				$output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					Please check on the reCAPTCHA box.
				</div>';
				$is_verifieid = false;
			}
	  }
	  
	  if($is_verifieid)
	  {
	  	if(isset($agent_form))
		{
			 $this->load->library('Email_lib');
			$args = array();
			  $args ['post'] = $_POST;
			  if(isset($email))
			  {
				  $args['to_email'] = $email;
					
				  $args['email_template'] = "agent_inquiry_email";	
				  $this->email_lib->send_email_notification($args);	
			  }	
				
					
					
				$em_args = array();
				$em_args['post'] = $_POST;
				//$this->global_lib->send_email_notifications_to_admin("contact_us_email_admin" ,$em_args );	
					
				$return_type = 'success';
								$output = '<div class="alert alert-success alert-dismissable" style="margin-top:0px;">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									'.mlx_get_lang('Your contact request has been submitted successfully.').'
								</div>';
								 header('Content-type: application/json');
			  echo json_encode(array('return_type'=>$return_type,'output' => $output));
			  return;
			
			
					
			  
								
			 
		}
	    else
	  	{		
				$property_id = $this->global_lib->DecryptClientID($p_id);
				
				$sql = "select prop.*,
				   pld.title as title , 
				   pld.price as price,
				   u.user_email,
				   pt.title as prop_type_title
				   from properties  as prop 
				   inner join property_types as pt on pt.pt_id = prop.property_type
					inner join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
				   inner join property_lang_details as pld on pld.p_id = prop.p_id and pld.language = '$this->default_language'
				   where prop.p_id = '$property_id'";
				$property_result = $this->Common_model->commonQuery($sql );
				
				if($property_result->num_rows() >0)
				{
					$single_property = $property_result->row();
					
					$to = $single_property->user_email; 
					$from = trim($email); 
					$fromName = ucfirst(trim($name));
					
					$is_price_set = true;
					if(isset($this->enable_multi_lang) && $this->enable_multi_lang == true)
					{
						$def_lang_code = $this->default_language;
						
						$ret_data = $this->global_lib->get_property_price_by_lang($single_property->p_id,$def_lang_code);
						if(!empty($ret_data))
						{
							$single_property->price = $ret_data['price'];
							$currency_symbol = $ret_data['currency'];
						}
						else
						{
							$is_price_set = false;
						}
						
					}
					
					
					$subject = "A Contact Agent Form Submitted.";
					$htmlContent = ' 
						<html> 
						<head> 
							<title>A Contact Form Submitted</title> 
						</head> 
						<body> 
							<h1>A Contact agent form submitted with following details :- </h1> 
							<table cellspacing="0" style="border: 1px solid #dddddd; width: 100%; text-align:left;"> 
								<tr> 
									<th>Name </th><td>'.trim($name).'</td> 
								</tr> 
								<tr style="background-color: #e0e0e0;"> 
									<th>Email </th><td>'.trim($email).'</td> 
								</tr> 
								<tr> 
									<th>Message </th><td>'.trim($message).'</td> 
								</tr> 
							</table> 
							<br>
							<h3 style="text-align:center;">Property Details</h3>
							<table cellspacing="0" style="border: 1px solid #dddddd; width: 100%;  text-align:left;"> 
								<tr> 
									<th>Title </th><td>'.ucfirst(stripslashes($single_property->title)).'</td> 
								</tr> 
								<tr> 
									<th>Address </th><td>'.$single_property->address.'</td> 
								</tr> 
								<tr> 
									<th>Price </th><td>';
								if($is_price_set)
								{
									if($currency_symbol != '')
									{
										$htmlContent .=  $currency_symbol.$this->global_lib->moneyFormatDollar($single_property->price);
									}
									else
		 							{
		 								$htmlContent .=  $this->global_lib->moneyFormatDollar($single_property->price);
		 							}
									if($single_property->property_for == 'Rent'){ $htmlContent .= '/'.mlx_get_lang('Month'); } 
								}
								else{
									$htmlContent .=  mlx_get_lang('Price Not Set');
								}
					$htmlContent .= '</td> 
								</tr> 
								<tr> 
									<th>Type </th><td>'.mlx_get_lang(ucfirst($single_property->prop_type_title)).'</td> 
								</tr> 
								<tr> 
									<th>Size </th><td>'.$single_property->size.' '.mlx_get_lang('Sq. Foot').'</td> 
								</tr> 
								<tr> 
									<th>Bedroom</th><td>'.$single_property->bedroom.'</td> 
								</tr> 
								<tr> 
									<th>Bathroom </th><td>'.$single_property->bathroom.'</td> 
								</tr> 
							</table> 
						</body> 
						</html>'; 
					
					
					$args = array();
					$args['CI'] = $CI; 
					$args['to'] = $to; 
					$args['msg'] = $htmlContent; 
					$args['subject'] = $subject; 
					$args['from_email'] = $from;
					
					$default_mailer = $this->global_lib->get_option('default_mailer');
					
					$response = false;
					if($default_mailer == "php_mail" || empty($default_mailer))			
						$response = $this->sendMailByPHP($args);
					else if($default_mailer == "smtp")			
						$response = $this->sendMailBySMTP($args);
					
					if($response){  
						$return_type = 'success';
						$output = '<div class="alert alert-success alert-dismissable" style="margin-top:0px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							Your contact agent request has submitted successfully.
						</div>';
					}else{ 
					   $return_type = 'error';	
					   $output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							Email sending failed.
						</div>';
					}
				}	
				else{ 
					$return_type = 'error';
					$output = '<div class="alert alert-danger alert-dismissable" style="margin-top:0px;">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						Invalid Property.
					</div>';
				} 	
			}					
		}	
		  
		  header('Content-type: application/json');
		  echo json_encode(array('return_type'=>$return_type,'output' => $output));
		  return;
		  
	}
	
	public function user_field_validation_callback_func()
	{
		extract($_POST);
		$this->load->model('Common_model');
		$options=array('where'=>array($field_type=>$field_value));
		$user_exsist=$this->Common_model->commonSelect('users',$options);
		
		if($user_exsist->num_rows() > 0 )
		{
			echo 'false';
		}	
		else
		{	
			echo 'true';
		}
		return;
	}
	
	public function register_user_form_callback_func()
	{
		extract($_POST);
		$CI =& get_instance();
		$this->load->model('Common_model');
		$this->load->library('Global_lib');
		/*$this->load->library('Email_lib');*/
		$cur_time = time();
		
		if(!isset($user_type) || (isset($user_type) && empty($user_type)))
			$user_type = 'agent';
		
		$enbale_reg_auto_login = $this->global_lib->get_option('enbale_reg_auto_login');
		$default_user_status_after_reg = $this->global_lib->get_option('default_user_status_after_reg');
		$datai = array( 
						'user_name' => trim($username),	
						'user_pass' => md5(trim($password)),
						'user_email' => trim($email),
						'user_type' => $user_type,	
						'user_registered_date' => $cur_time,	
						'user_update_date' => $cur_time,
						'user_link_id' => '',
						'user_code' => '',
						'user_verified' => 'N',
						'user_status' => 'N',
						); 
		
		$this->load->helper('string');
		$user_code = random_string('alnum',12);
		
		if($enbale_reg_auto_login == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
		}
		else if($default_user_status_after_reg == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
		}else{
			$datai['user_code'] = $user_code;
		}
		
		$user_id = $this->Common_model->commonInsert('users',$datai);
		
		$user_meta = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					);
		if(isset($photo_url) && !empty($photo_url))
		{
			$user_meta['photo_url'] = $photo_url;
		}
		if(isset($att_photo_hidden) && !empty($att_photo_hidden))
		{
			$user_meta['photo_url'] = $att_photo_hidden;
		}
		foreach($user_meta as $key=>$val)
		{
			$datai = array( 
						'meta_key' => trim($key),	
						'meta_value' => trim($val),
						'user_id' => $user_id
						);
			$this->Common_model->commonInsert('user_meta',$datai);
		}
		
		$to = $email; 
		
		$args['to_email'] = $email;
		$args['user_id'] = $user_id;	
		
		
		
		if($enbale_reg_auto_login == 'Y'  || $default_user_status_after_reg == 'Y')
	    {
			$args['email_template'] = "register_email";	
			$this->email_lib->send_email_notification($args);	
			
		}else{
			
			$args['url'] = base_url(array('logins','account_confirm?email='.$email.'&user_code='.$user_code));
			$args['email_template'] = "account_confirmation_email";	
			$this->email_lib->send_email_notification($args);	 
			
		}
		
		
		
		
		/*
		$subject = "Registered Successfully";
		$htmlContent = ' 
			<html> 
			<head> 
				<title>Registered Successfully</title> 
			</head> 
			<body> 
				<p>Hello <strong>'.$first_name.' '.$last_name.'</strong></p>
				<p>Thank you for register. your account will be activate after admin verification.</p>
			</body> 
			</html>'; 
		
		$args = array();
		$args['CI'] = $CI; 
		$args['to'] = $to; 
		$args['msg'] = $htmlContent; 
		$args['subject'] = $subject; 
		
		$default_mailer = $this->global_lib->get_option('default_mailer');
		
		$response = false;
		if($default_mailer == "php_mail" || empty($default_mailer))			
			$response = $this->sendMailByPHP($args);
		else if($default_mailer == "smtp")			
			$response = $this->sendMailBySMTP($args);
			
		*/	
		
		/*$admin_emails = $this->global_lib->get_admin_user_emails();
		if(!empty($admin_emails))
		{
			
			foreach($admin_emails as $ak=>$av)
			{
				$to = $av; 
				
				
				$from = trim($email); 
				$fromName = $first_name.' '.$last_name;
				
				$site_domain_name = $this->global_lib->get_option('site_domain');
				$site_domain_email = $this->global_lib->get_option('site_domain_email');
				
				$subject = "A registration form submitted";
				$htmlContent = ' 
					<html> 
					<head> 
						<title>A registration form submitted</title> 
					</head> 
					<body> 
						<h1>A registered form submitted with following details :- </h1> 
						<table cellspacing="0" style="border: 1px solid #dddddd; width: 100%; text-align:left;"> 
							<tr> 
								<th>First Name : </th><td>'.trim($first_name).'</td> 
							</tr> 
							<tr > 
								<th>Last Name : </th><td>'.trim($last_name).'</td> 
							</tr> 
							<tr> 
								<th>Username : </th><td>'.trim($username).'</td> 
							</tr> 
							<tr> 
								<th>Email : </th><td>'.trim($email).'</td> 
							</tr> 
						</table> 
					</body> 
					</html>'; 
				
				$headers = "MIME-Version: 1.0" . "\r\n"; 
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
				$headers .= 'From: '.$site_domain_name.'<'.$site_domain_email.'>' . "\r\n";
				
				$args = array();
				$args['CI'] = $CI; 
				$args['to'] = $to; 
				$args['msg'] = $htmlContent; 
				$args['subject'] = $subject; 
				
				$default_mailer = $this->global_lib->get_option('default_mailer');
				
				$response = false;
				if($default_mailer == "php_mail" || empty($default_mailer))			
					$response = $this->sendMailByPHP($args);
				else if($default_mailer == "smtp")			
					$response = $this->sendMailBySMTP($args);
				
				
			}
		}*/
		
		
		$auto_redirect = 'N';
		
		
		$output = '<div class="alert alert-success alert-dismissable" >
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			Registered Successfully. You can login after admin verification.
		</div>';
		if($enbale_reg_auto_login == 'Y')
		{
			/*
			$newdata = array(  
							'first_name',
							'last_name',
							'username', 
							'user_name',
							'user_email', 
							'user_id', 
							'user_type', 
							'user_status', 
							'site_url'
							);
			foreach($newdata as $k=>$v)
			{
				unset($_SESSION[$v]);
			}
			$this->session->unset_userdata($newdata);
			$this->session->set_userdata('logged_in', false);
			$_SESSION['logged_in'] = false;	
			*/
			
			if (isset($_COOKIE["PHPSESSID"])) {
				session_destroy();
				session_unset();
				session_start();
			}
			
			$auto_redirect = 'Y';
			$sql="select * from users where user_id = '".$user_id."'"; 
			$detail = $this->Common_model->commonQuery($sql);
			$site_url = site_url();	
			$user_data=$detail->row();
			$newdata = array(  
				'first_name' => $first_name,
				'last_name' => $last_name,
				'username'  => $username, 
				'user_name'     => $username,
				'user_email'     => $email, 
				'user_id'     => $user_id, 
				'user_type'     => $user_data->user_type, 
				'user_status'     => $user_data->user_status, 
				'logged_in' => TRUE,
				'site_url' => $site_url
				);
			foreach($newdata as $k=>$v)
			{
				$_SESSION[$k] = $v;
			}
			$this->session->set_userdata($newdata);
			$output = '<div class="alert alert-success alert-dismissable" >
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Registered Successfully. Redirecting...
			</div>';
		}
		else if($default_user_status_after_reg == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
			$output = '<div class="alert alert-success alert-dismissable" >
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Registered Successfully. You can login with your login credentials.
			</div>';
		}
		
		header('Content-type: application/json');
		echo json_encode(array('status'=>'success','output' => $output,'auto_redirect' => $auto_redirect));
		return;
		
	}

	public function addContactShowLog()
	{
		$this->load->model('Common_model');
		$propertyId = $this->input->post('prop_id');
		$result = $this->Common_model->get_property_info($propertyId);
		$ipAddress  = $this->get_client_ip();
		$browser 	= $this->getBrowser();
		foreach ($result as $row) 
		{
			$datai = array( 
									'property_id' => trim($propertyId),
									'user_id' => trim($row['created_by']),
									'ip_address' => trim($ipAddress),
									'browser_name' => $browser['name'],
									'browser_agent' => $browser['userAgent'],
									'browser_version' => $browser['version'],
									'logged_date' => date('Y-m-d H:i:s'),
									);
			
			$rec = $this->Common_model->get_log_info($ipAddress, $propertyId, $row['created_by']);		
			if($rec == 0)
			{
				$res = $this->Common_model->logInsert('contact_details_log',$datai);
				echo 'Added';
			}
			else
			{
				echo 'You have also inserted with this current IP.';
			}		
		}
	}
	
	// Function to get the client IP address
	public function get_client_ip() 
	{
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}
	
	function getBrowser() 
	{ 
		  $u_agent = $_SERVER['HTTP_USER_AGENT'];
		  $bname = 'Unknown';
		  $platform = 'Unknown';
		  $version= "";
		
		  //First get the platform?
		  if (preg_match('/linux/i', $u_agent)) {
		    $platform = 'linux';
		  }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
		    $platform = 'mac';
		  }elseif (preg_match('/windows|win32/i', $u_agent)) {
		    $platform = 'windows';
		  }
		
		  // Next get the name of the useragent yes seperately and for good reason
		  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
		    $bname = 'Internet Explorer';
		    $ub = "MSIE";
		  }elseif(preg_match('/Firefox/i',$u_agent)){
		    $bname = 'Mozilla Firefox';
		    $ub = "Firefox";
		  }elseif(preg_match('/OPR/i',$u_agent)){
		    $bname = 'Opera';
		    $ub = "Opera";
		  }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
		    $bname = 'Google Chrome';
		    $ub = "Chrome";
		  }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
		    $bname = 'Apple Safari';
		    $ub = "Safari";
		  }elseif(preg_match('/Netscape/i',$u_agent)){
		    $bname = 'Netscape';
		    $ub = "Netscape";
		  }elseif(preg_match('/Edge/i',$u_agent)){
		    $bname = 'Edge';
		    $ub = "Edge";
		  }elseif(preg_match('/Trident/i',$u_agent)){
		    $bname = 'Internet Explorer';
		    $ub = "MSIE";
		  }
		
		  // finally get the correct version number
		  $known = array('Version', $ub, 'other');
		  $pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		  if (!preg_match_all($pattern, $u_agent, $matches)) {
		    // we have no matching number just continue
		  }
		  // see how many we have
		  $i = count($matches['browser']);
		  if ($i != 1) {
		    //we will have two since we are not using 'other' argument yet
		    //see if version is before or after the name
		    if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
		        $version= $matches['version'][0];
		    }else {
		        $version= $matches['version'][1];
		    }
		  }else {
		    $version= $matches['version'][0];
		  }
		
		  // check if we have a number
		  if ($version==null || $version=="") {$version="?";}
		
		  return array(
		    'userAgent' => $u_agent,
		    'name'      => $bname,
		    'version'   => $version,
		    'platform'  => $platform,
		    'pattern'    => $pattern
		  );
	} 
	
	public function sendMailBySMTP($args = array())
	{
		$this->load->library('Email_lib');
		$this->email_lib->sendMailBySMTP($args);
		return true;					
	}
	
	public function client_field_validation_callback_func()
	{
		extract($_POST);
		$this->load->model('Common_model');
		$options=array('where'=>array('username'=>$field_value));
		$user_exsist=$this->Common_model->commonSelectUser('clients',$options, $field_value);
		if($current_email != $field_value)
		{
			if($user_exsist->num_rows() > 0 )
			{
				echo 'false';
			}	
			else
			{	
				echo 'true';
			}
		}	
		else
		{
			echo 'true';
		}	
		return;
	}
	
}
