<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Controller extends CI_Controller {
	
	var $theme;
	var $site_users;
	var $site_user_access;
	var $site_user_settings;
	var $site_user_active_settings  = false;	 
	
	var $site_options;
	function __construct()
	{
		parent::__construct();
		
		$CI =& get_instance();
		$this->theme = $CI->config->item('theme') ;
		$this->load->model('Common_model');
		$this->site_users = $CI->config->item('site_users') ;
		$this->site_user_access = $CI->config->item('site_user_access') ;
		$this->site_user_settings = $CI->config->item('settings') ;
		$this->set_default_timezone();
		$this->load->library('global_lib');
		$this->load->library('package_lib');
		$this->load->helper('mlxlang');
		$this->has_warning = false;
		$this->warning_list = array();
		
		$this->site_payments = 'N';
		$this->cms_version = $CI->config->item('cms_version') ;
		
		$isPlugAct = $this->isPluginActive('payment');
		if($isPlugAct == true)
		{
			
			$enable_payment = $CI->global_lib->get_option('enable_payment_option');	
			if($enable_payment == 'Y')
			$this->site_payments = $enable_payment;
		}
		
		$this->load->config("site_config");
		
		$sidebar = $CI->config->item('sidebar_left');
		//['sidebar_left']
		/*$sidebar  [20] ['item'] [] = array( 	'class' => 'property',  'method'=> 'custom_fields',
												'text'=> 'Custom Fields', 'link'=> 'property/custom_fields',		
												'collapse_class'=> '','icon_class'=> 'fa fa-circle-o',	);
		
		$CI->config->set_item('sidebar_left',$sidebar);
		
		
		$sidebar_new = $CI->config->item('sidebar_left');*/
		/*echo "<pre>";print_r($sidebar_new); echo "</pre>";*/
		
		$this->is_subscription = $CI->global_lib->get_option('enable_subscription');
		
		$this->enable_property_posting = $CI->global_lib->get_option('enable_property_posting');
		$this->enable_featured_property_posting = $CI->global_lib->get_option('enable_featured_property_posting');
		$this->enable_blog_posting = $CI->global_lib->get_option('enable_blog_posting');
		
		$this->is_subscription_expires = $CI->package_lib->is_subscription_expires();
		
		$is_user_login = $this->is_user_login();
		
		$enbale_front_end_registration = $CI->global_lib->get_option('enbale_front_end_registration');
		$enbale_reg_auto_login = $CI->global_lib->get_option('language');
		if($enbale_front_end_registration != 'Y' && $enbale_reg_auto_login != 'Y')
		{
			if(!$is_user_login)
			{
				$is_expired = $this->session->userdata('is_expired');
				if(!$is_expired)
				{
					$newdata = array('username', 
									'user_name',
									'user_email', 
									'user_id', 
									'user_type',  
									'site_url',
									);
					foreach($newdata as $k=>$v)
					{
						unset($_SESSION[$v]);
					}
					
					$this->session->unset_userdata($newdata);
					$this->session->set_userdata('logged_in', false);
					$_SESSION['logged_in'] = false;		
					
					$this->session->set_userdata('is_expired', true);
					$_SESSION['is_expired'] = true;		
					
					$_SESSION['msg'] = '<p class="success_msg">'.mlx_get_lang("Your Session Expired").'</p>';
					
					redirect('/logins','location');
				}
				
			}
		}
		
		$mode = $CI->config->item('running_mode') ;
		
		$this->load->helper('mlxurl');
		
		
		$this->load_options();
		
		$user_type = $this->session->userdata('user_type');				
		$username = $this->session->userdata('username');		
		/*$uri_string =  $this->uri->uri_string();*/
		
		if($username == 'adminaziz')			
			$mode = 'working';		
		
		$currentURL = current_url(); //for simple URL
		$params = $_SERVER['QUERY_STRING']; //for parameters
		$uri_string = $currentURL . '?' . $params; //full URL with parameter
		
		/*
		if($mode == 'demo')
			echo " yes1 ";	
		if(!preg_match("/logins/", $uri_string)) 
			echo " yes2 ";
		if( $_SERVER['REQUEST_METHOD'] == 'POST' || preg_match("/delete/", $uri_string) 
			
			) echo " yes3";
		exit;
		*/	
		
		$this->update_user_credits_on_login();
		
		if($mode == 'demo' && 
			(!preg_match("/logins/", $uri_string)) && 
			( $_SERVER['REQUEST_METHOD'] == 'POST' || preg_match("/delete/", $uri_string)
			|| preg_match("/action/", $uri_string)
			) ){
			
				$_SESSION['msg'] = '<div class="alert alert-danger alert-dismissable">	
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>		
				'.mlx_get_lang("Add/Edit/Delete is not allowed in Demo Version").'</div>';	

				$redirect_uri = "main";
			
				if(preg_match("/edit/", $uri_string) || preg_match("/add_new/", $uri_string) 
				|| preg_match("/delete/", $uri_string) )
				{
					if(preg_match("/page/", $uri_string))
					{
						$redirect_uri = "page/manage";
					}
					if(preg_match("/property/", $uri_string))
					{
						$redirect_uri = "property/manage";
					}
					if(preg_match("/banner/", $uri_string))
					{
						$redirect_uri = "banner/manage";
					}
					if(preg_match("/user/", $uri_string))
					{
						$redirect_uri = "user/manage";
					}
				}
				
				if(preg_match("/settings/", $uri_string) )
				{
					$redirect_uri = $uri_string;
				}
			
			redirect($redirect_uri,'location');
		
		}
		
		
		$this->lang->load('english', 'english');
		
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		$def_lang = $this->global_lib->get_user_meta($user_id,'language');
		
		$default_language = $CI->global_lib->get_option('default_language');
		$this->default_language = 'en';
		$this->default_lang_code = 'en';
		$this->default_language_title = 'English';
		$this->site_currency = 'USD';
		$this->enable_multi_lang = false;
		
		$enable_multi_language = $CI->global_lib->get_option('enable_multi_language');
		if(!empty($enable_multi_language) && $enable_multi_language == 'Y')
		{
			$this->enable_multi_lang = true;
		}
		
		
		$site_language = $CI->global_lib->get_option('site_language');
		if(!empty($default_language) && !empty($site_language))
		{
		
			$lang_exp = explode('~',$default_language);
			$lang_code = $lang_exp[1];
			$lang_title = $lang_exp[0];
			$site_language_array = json_decode($site_language,true);
			
			
			$is_lang_exists = false;
			foreach($site_language_array as $slak=>$slav)
			{
				if($slav['language'] == $lang_title.'~'.$lang_code)
				{
					$is_lang_exists = true;
					$this->site_currency = $slav['currency'];
					$lang_exp = explode('~',$slav['language']);
					
					/*$lang_code = $lang_exp[1];*/
					$lang_code_combi = $lang_exp[1];
					$lang_code_exp = explode('-',$lang_code_combi);
					
					/*$flag_code = $lang_code = $lang_code_title = $lang_exp[1];*/
					if(isset($lang_code_exp[1]))
					{
						$lang_code = strtolower( $lang_code_exp[1]);
						
					}else
						$lang_code = $lang_code_exp[0];
					$lang_title = $lang_exp[0];
					$lang_slug = $CI->global_lib->get_slug($lang_title);
					
					$this->default_language = $lang_exp[1];
					$this->default_lang_code = $lang_code; 
					
					$this->set_default_timezone($slav['timezone']);
					
					break;
				}
			}
			if($is_lang_exists)
			{
				
				$lang_slug = $CI->global_lib->get_slug($lang_title);
				if($this->default_language == '')
					$this->default_language = $lang_code;
				$this->default_language_title = $lang_title;
			}
		}
		
		 
		if(empty($def_lang) || $def_lang == 'default')
		{
			$def_lang = $this->default_language_title;
		}
		else
		{
			$lang_slug = $this->global_lib->get_slug($def_lang,'_');
			
			if(!file_exists("../application/admin/language/$lang_slug/".$lang_slug."_lang.php"))
			{
				
				if(!is_dir("../application/admin/language"))
				{
					mkdir("../application/admin/language",0777);
				}
							
				if(!is_dir("../application/admin/language/$lang_slug"))
				{
					mkdir("../application/admin/language/$lang_slug",0777);
				}
				
				$fp = fopen("../application/admin/language/$lang_slug/".$lang_slug."_lang.php","wb");
				if($fp)
				{
					$output = "";
					fwrite($fp,$output);
					fclose($fp);
				}
				
			}
			
		}
		
		if(!empty($def_lang) && $def_lang != 'default')
		{
			
			$lang_slug = $this->global_lib->get_slug($def_lang,'_');
			$this->lang->load($lang_slug, $lang_slug);
			
		}
		
		
		$this->load->helper('mlxlang_helper');
		
		
		
		
		if(!$this->site_user_active_settings)
		{
			$user_type = $this->session->userdata('user_type');
			
			$options = $this->Common_model->commonQuery("select * from options where option_key like '$user_type%'");	
			$active_options = array();
			if(isset($options) && $options->num_rows()>0)
			{
				
				foreach($options->result() as $row)
				{
					if($row->option_value == 'yes')
						$active_options [$row->option_key] = $row->option_value;;
				}
				
			}
			
			if(count($active_options ) > 0)
			{
				if(isset($this->site_user_settings [$user_type]))
				{
				$settings = $this->site_user_settings [$user_type];
				foreach($settings as $setting)
				{
					if( isset($setting['controller']) &&   array_key_exists($setting['name'],$active_options))
					{
						
						if(isset($this->site_user_access[$user_type]['menu']['menu_items']))
						{
							
							$this->site_user_access[$user_type]['menu']['menu_items'][] = $setting['controller'];
							$this->site_user_access[$user_type]['menu']['menu_items'][] = $setting['controller'] 
										. "||" . $setting['method'];
										
							if($setting['method'] == 'add_new')
							{
								$this->site_user_access[$user_type]['menu']['menu_items'][] = $setting['controller'] 
											. "||manage" ;
							}								
						}
						
						if(isset($this->site_user_access[$user_type]['view']['all_items']))
						{
						
							if(isset(	$this->site_user_access[$user_type]['view']['all_items'][ $setting['controller'] ] ))
							{
								$arr = 	$this->site_user_access[$user_type]['view']['all_items'][ $setting['controller'] ] ;
								$arr [] = $setting['method'];  
								if($setting['method'] == 'add_new')
									$arr [] = 'manage';
								
								$this->site_user_access[$user_type]['view']['all_items'][ $setting['controller'] ] = $arr;		
								
							}
							else
							{
								$arr =  array();
								$arr [] = $setting['method'];  
								if($setting['method'] == 'add_new')
									$arr [] = 'manage';
									
								$this->site_user_access[$user_type]['view']['all_items'][$setting['controller'] ] = $arr;		
							
							}
										
						
						}
						
						
							
					}
				}
				}
			}
			$this->site_user_active_settings =  true;
		}	
		
    }
	
	
	public function update_user_credits_on_login(){
		
			$this->package_lib->update_user_credit_while_login();
			
	}
	
	public function load_options(){
		$this->load->model('Common_model');	
		
		$options = $this->Common_model->commonQuery("select * from options");	
		
		
		$options_arr = array();
		foreach($options->result() as $row){
			$options_arr[$row->option_key] = $row->option_value;
		}
		$this->site_options = $options_arr;
		
	}
	
	public function is_user_login()
	{
		$this->load->model('Common_model');	
		
		$user_type = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');
		
		if($user_type === FALSE && $user_id === FALSE)
		{
			return false;
		}
		else
		{
			$is_expired = $this->session->userdata('is_expired');
			if(!$is_expired)
			{
				return false;
			}
		}
		return true;
	}
	
	public function set_default_timezone()
	{
		
		$this->load->model('Common_model');	
		
		$options = $this->Common_model->commonQuery("select * from options where option_key  = 'default_timezone' and option_value != ''");	
	
		if(isset($options) && $options->num_rows()>0)
		{

				foreach($options->result() as $row)
				{
					 $default_timezone = $row->option_value;
				}

			date_default_timezone_set($default_timezone);
		}
		else
		{
			 date_default_timezone_set('Asia/Kolkata');
		}
	}
	
	public function has_menu_access($menu_item = "", $user_type )
	{
		if(array_key_exists($user_type, $this->site_users ))
		{
			$menu_access = $this->site_user_access [$user_type]['menu']	 ;
			if($menu_access['has_access'] == 'access_all')
			{
				return true;
			}
			else if($menu_access['has_access'] == 'exclude'){
				$menu_items = $menu_access['menu_items'];
				
				if(!in_array($menu_item, $menu_items ))
					return true;
			}	
			else if($menu_access['has_access'] == 'limited'){
				$menu_items = $menu_access['menu_items'];
				
				if(in_array($menu_item, $menu_items ))
					return true;
			}
		}
		return false;
	}
	
	public function has_class_access($class_item = "", $user_type = "" )
	{
		
		if($class_item == "")
		{
			$class_item =  $this->router->fetch_class();
		}
		
		$user_type = $this->session->userdata('user_type');
		
		if(isset($this->site_user_settings[$user_type]))
			$user_settings = $this->site_user_settings[$user_type];
		else
			$user_settings = array();
		
		
		if(in_array($user_type, $this->site_users ))
		{
			$class_access = $this->site_user_access [$user_type]['controller']	 ;
			
			if($class_access['has_access'] == 'access_all')
			{
				return true;
			}
			
			else if($class_access['has_access'] == 'limited'){
				
				$class_items = $class_access['all_items'];
				if(in_array($class_item, $class_items ))
					return true;
			}
		}
		
		return false;
		
	}
	
	public function has_method_access($method_item = "", $user_type = "" )
	{
		$class_item =  $this->router->fetch_class();
		$method_item =  $this->router->fetch_method();
		
		$user_type = $this->session->userdata('user_type');
		
		if(isset($this->site_user_settings[$user_type]))
			$user_settings = $this->site_user_settings[$user_type];
		else
			$user_settings = array();
		
		if(array_key_exists($user_type, $this->site_users ))
		{
			$method_access = $this->site_user_access [$user_type]['view']	 ;
			
			if($method_access['has_access'] == 'access_all')
			{
				return true;
			}
			else if($method_access['has_access'] == 'exclude'){
				if(!array_key_exists($class_item, $method_access ['all_items']) )
				{
					return true;
				}
			}
			else if($method_access['has_access'] == 'limited'){
				
				if(array_key_exists($class_item, $method_access ['all_items']) )
				{
					$method_items = $method_access['all_items'][$class_item];
					if(in_array($method_item, $method_items ))
						return true;
				}		
			}
		}
		return false;
		
	}
	
	public function has_widget_access($widget_item = "", $user_type = "" )
	{
		
		if($widget_item == "")
		{
			return false;
		}
		
		
		$user_type = $this->session->userdata('user_type');
		if(in_array($user_type, $this->site_users ))
		{
			if(isset($this->site_user_access [$user_type]['widget']	))
				$widget_access = $this->site_user_access [$user_type]['widget']	 ;
			else
				return false;
				
				
			if($widget_access['has_access'] == 'access_all')
			{
				return true;
			}else if($widget_access['has_access'] == 'limited'){
				
				if(in_array($widget_item, $widget_access ['all_items']))
				{
					return true;
				}		
			}
		}
		
		return false;
		
	}
	
	public function has_permission($item = "", $task = "", $user_type = "" )
	{
		
		
		$user_type = $this->session->userdata('user_type');
		if(in_array($user_type, $this->site_users ))
		{
			$access = $this->site_user_access [$user_type]['content']	 ;
			
			if($access['has_access'] == 'access_all')
			{
				return true;
			}else if($access['has_access'] == 'limited'){
				
				$all_items = $access['all_items'];
				if(array_key_exists($item,$all_items))
				{
					$current = $all_items[$item];
					if(in_array($task, $current))
						return true;
				}	
			}
		}
		
		return false;
		
	}
	
	public function get_default_status($item = "", $user_type = "" )
	{
		
		
		$user_type = $this->session->userdata('user_type');
		if(in_array($user_type, $this->site_users ))
		{
			$access = $this->site_user_access [$user_type]['content']	 ;
			
			if($access['default_status'] == 'publish_all')
			{
				return 'publish';
			}else if($access['default_status'] == 'limited'){
				
				$all_items = $access['statuses'];
				if(array_key_exists($item,$all_items))
				{
					$status = $all_items[$item];
					return $status;	
				}	
			}
		}
		
		return "draft";
		
	}
	
	public function EncryptClientId($id)
	{
		return substr(md5($id), 0, 8).dechex($id);
	}

	public function DecryptClientId($id)
	{
		$md5_8 = substr($id, 0, 8);
		$real_id = hexdec(substr($id, 8));
		return ($md5_8==substr(md5($real_id), 0, 8)) ? $real_id : 0;
	}
	
	public function isLogin()	{
		$site_url = site_url();		
		$logged_in = $this->session->userdata('logged_in');
		$sess_site_url = $this->session->userdata('site_url');
		if(isset($logged_in) && $logged_in == TRUE )
		{
			
			return true;		

		}		
		else
		{
			$_SESSION['msg'] = '<p class="error_msg">'.mlx_get_lang("You have to login first to proceed").'</p>';
			return false;
		}
	}
	
	public function isPluginActive($plugin_slug = null)
	{
		$site_plugins_json = $this->global_lib->get_option('site_plugins');  
		if(!empty($site_plugins_json) && $plugin_slug != null)
		{
			$site_plugins = json_decode($site_plugins_json,true);
			if(in_array($plugin_slug, $site_plugins)) 
			{
				return true;
			}
		}
		return false;
	}
	
}