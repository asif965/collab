<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_mailer extends MY_Controller {
	
	function __construct() 
	{
        parent::__construct();
		
        /*if(!$this->isLogin())
		{
			redirect('/logins','location');
		}*/
	}
	
	public function test_email_notifications($args =  array())	
	{
		
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		if(isset($to_email))
			$args['to_email'] = $to_email;
		
		if(isset($email_template))
			$args['email_template'] = $email_template;	
			
		$CI->load->library('Email_lib');
		$CI->email_lib->test_email_notifications($args);
		
		
		
	}
	
	
	
	
	
	public function register_user_form_callback_func()
	{
		extract($_POST);
		$CI =& get_instance();
		$this->load->model('Common_model');
		$this->load->library('Global_lib');
		$this->load->library('Email_lib');/**/
		$cur_time = time();
		
		if(!isset($user_type) || (isset($user_type) && empty($user_type)))
			$user_type = 'agent';
		
		$enbale_reg_auto_login = $this->global_lib->get_option('enbale_reg_auto_login');
		$default_user_status_after_reg = $this->global_lib->get_option('default_user_status_after_reg');
		$datai = array( 
						'user_name' => trim($username),	
						'user_pass' => md5(trim($password)),
						'user_email' => trim($email),
						'user_type' => $user_type,	
						'user_registered_date' => $cur_time,	
						'user_update_date' => $cur_time,
						'user_link_id' => '',
						'user_code' => '',
						'user_verified' => 'N',
						'user_status' => 'N',
						); 
		
		$this->load->helper('string');
		$user_code = random_string('alnum',12);
		
		if($enbale_reg_auto_login == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
		}
		else if($default_user_status_after_reg == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
		}else{
			$datai['user_code'] = $user_code;
		}
		
		$user_id = $this->Common_model->commonInsert('users',$datai);
		
		$user_meta = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					);
		if(isset($photo_url) && !empty($photo_url))
		{
			$user_meta['photo_url'] = $photo_url;
		}
		if(isset($att_photo_hidden) && !empty($att_photo_hidden))
		{
			$user_meta['photo_url'] = $att_photo_hidden;
		}
		foreach($user_meta as $key=>$val)
		{
			$datai = array( 
						'meta_key' => trim($key),	
						'meta_value' => trim($val),
						'user_id' => $user_id
						);
			$this->Common_model->commonInsert('user_meta',$datai);
		}
		
		$to = $email; 
		
		$args['to_email'] = $email;
		$args['user_id'] = $user_id;	
		
		
		
		if($enbale_reg_auto_login == 'Y'  || $default_user_status_after_reg == 'Y')
	    {
			$args['email_template'] = "register_email";	
			$this->email_lib->send_email_notification($args);	
			
		}else{
			
			
			$args['account_confirmation_link_url'] = base_url(array('logins','account_confirm?email='.$email.'&user_code='.$user_code));
			$args['email_template'] = "account_confirmation_email";	
			$this->email_lib->send_email_notification($args);	
			
		}
		
		$this->global_lib->send_email_notifications_to_admin("new_user_registered_email_admin");
		
		
		
		$auto_redirect = 'N';
		
		
		$output = '<div class="alert alert-success alert-dismissable" >
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			Registered Successfully. You can login after admin verification.
		</div>';
		if($enbale_reg_auto_login == 'Y')
		{
			
			
			if (isset($_COOKIE["PHPSESSID"])) {
				session_destroy();
				session_unset();
				session_start();
			}
			
			$auto_redirect = 'Y';
			$sql="select * from users where user_id = '".$user_id."'"; 
			$detail = $this->Common_model->commonQuery($sql);
			$site_url = site_url();	
			$user_data=$detail->row();
			$newdata = array(  
				'first_name' => $first_name,
				'last_name' => $last_name,
				'username'  => $username, 
				'user_name'     => $username,
				'user_email'     => $email, 
				'user_id'     => $user_id, 
				'user_type'     => $user_data->user_type, 
				'user_status'     => $user_data->user_status, 
				'logged_in' => TRUE,
				'site_url' => $site_url
				);
			foreach($newdata as $k=>$v)
			{
				$_SESSION[$k] = $v;
			}
			$this->session->set_userdata($newdata);
			$output = '<div class="alert alert-success alert-dismissable" >
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Registered Successfully. Redirecting...
			</div>';
		}
		else if($default_user_status_after_reg == 'Y')
		{
			$datai['user_verified'] = 'Y';
			$datai['user_status'] = 'Y';
			$output = '<div class="alert alert-success alert-dismissable" >
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				Registered Successfully. You can login with your login credentials.
			</div>';
		}
		
		header('Content-type: application/json');
		echo json_encode(array('status'=>'success','output' => $output,'auto_redirect' => $auto_redirect));
		return;
		
	}
	
}
