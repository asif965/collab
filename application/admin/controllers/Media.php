<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->isLogin())
		{
			
			redirect('/logins','location');
		}
		
		if(!$this->has_method_access())
		{
			redirect('/main/','location');
		}
    }
	
	public function index()
	{
		$this->manage();	
	}
	
	public function manage()
	{
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'admin')
		{
			$qry = "select pi1.* from post_images pi
			
			inner join post_images pi1 on pi1.parent_image_id = pi.image_id
			and pi1.image_type = 'thumbnail'
			
			order by pi.image_id DESC";
		}
		else
		{
			$qry = "select pi1.* from post_images pi
			
			inner join post_images pi1 on pi1.parent_image_id = pi.image_id
			and pi1.image_type = 'thumbnail'
			where pi.user_id = $user_id
			order by pi.image_id DESC";
		}
		$data['media_list'] = $this->Common_model->commonQuery($qry);	
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/media/manage";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function settings()
	{
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		if(isset($_POST['submit']))
		{
			
			extract($_POST);
			
			extract($_POST,EXTR_OVERWRITE);
			
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			if(isset($options) && !empty($options))
			{
				foreach($options as $key=>$value)
				{
					$this->global_lib->update_option($key,$value);
				}
			}
			$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Media Settings Updated Successfully").'
						</div>
						';
			redirect('/media/settings','location');	
			
		
		}
		
		$data['options_list'] = $this->Common_model->commonQuery("select * from options");	
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/media/settings";
		
		$this->load->view("$theme/header",$data);
		
	}
}
