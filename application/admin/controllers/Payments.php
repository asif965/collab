<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Payments extends MY_Controller {
		function __construct() {
			parent::__construct();
			if(!$this->isLogin())
			{
				redirect('/logins','location');
			}
			
			if(!$this->has_method_access())
			{
				redirect('/main/','location');
			}
			$CI =& get_instance();		
			$this->load->library('Global_lib');	
			
			$isPlugAct = $this->isPluginActive('payment');
			if($isPlugAct != true)
			{
				redirect('/main','location');
			}
		}
		
		public function index()	
		{		
			$this->manage();	
		}
		
		public function manage()	
		{		
			$CI =& get_instance();
			$theme = $CI->config->item('theme') ;
			$this->load->library('Global_lib');
			
			
			$data = $this->global_lib->uri_check();
			$data['myHelpers']=$this;
			$this->load->model('Common_model');
			$this->load->helper('text');
			$payments_methods = $CI->config->item('payment_methods') ;
			
			if(isset($_POST['submit']) || isset($_POST['draft']))		
			{			
				extract($_POST,EXTR_OVERWRITE);				 								
				
				$user_id = $this->session->userdata('user_id');
				
				$content = array();
				
				foreach($_POST as $k=>$v)
				{
					if(is_array($v) && $k != 'submit')
						$content[$k] = $v;
				}
				
				foreach($_POST as $k=>$v)
				{
					$_POST[$k] = $this->security->xss_clean($v);
					$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
				}
				
				
				$this->global_lib->update_option('payment_methods',json_encode($content));
				
				$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-top:10px;margin-bottom:0px;">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				'.mlx_get_lang("Payment Methods Updated Successfully").'
				</div>							';				
				redirect('/payments/manage','location');										
				
			}
			
			$payments_methods_section = $this->global_lib->get_option('payment_methods');

			if(isset($payments_methods_section) && !empty($payments_methods_section))
			{
				$data['meta_content_lists'] = json_decode($payments_methods_section,true);
			}					
				$data['theme']=$theme;	

				$data['payment_methods']=$payments_methods;	

				$data['content'] = "$theme/payments/manage";				
				$this->load->view("$theme/header",$data);			
		}
		
		// public function add_new()	
		// {				
							
		// 	$CI =& get_instance();		
		// 	$theme = $CI->config->item('theme') ;				
		// 	$this->load->library('Global_lib');		
							
		// 	$data = $this->global_lib->uri_check();				
		// 	$data['myHelpers']=$this;		
		// 	$this->load->model('Common_model');		
		// 	$this->load->helper('text');		
			
		// 	if(isset($_POST['submit']) || isset($_POST['draft']))		
		// 	{			
		// 		extract($_POST);		
				
		// 		foreach($_POST as $k=>$v)
		// 		{
		// 			$_POST[$k] = $this->security->xss_clean($v);
		// 			$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
		// 		}
				
				
		// 		extract($_POST,EXTR_OVERWRITE);				 								
		// 		if(empty($user_id) || $user_id == 0)				
		// 		{						
		// 			$_SESSION['msg'] = '<p class="error_msg">Session Expired.</p>';					
		// 			$_SESSION['logged_in'] = false;						
		// 			$this->session->set_userdata('logged_in', false);					
		// 			redirect('/logins','location');				
		// 		}
				
		// 		if(isset($_POST['submit']))					
		// 			$status = 'Y';				
		// 		else if(isset($_POST['draft']))					
		// 			$status = 'N';				
		// 		else					
		// 			$status = 'Y';
				
			
		// 		$applicabled_for ='';	
		// 		foreach ($applicable_for as $key => $value) {
		// 			$applicabled_for =  $applicable_for[$key];
		// 		}
		// 		$cur_time = time();
		// 			$datai = array( 			
		// 					'package_name' => $package,									
		// 					'package_price' => $packages_price,
		// 					'package_currency' => $currency_code,
		// 					'number_listing_limit' =>$number_of_listing,
		// 					'auto_active_property' => $auto_active,							
		// 					'days_limit_for_property_active' => $days_limit_for_property_property,	
		// 					'package_life' => $package_lifetime,									
		// 					'number_featured_properties' => $number_featered_property,
		// 					'days_limit__featured_properties' => $days_limit_for_featured_property,
		// 					'applicable_for' =>$applicabled_for,
		// 					'limit_purchase_by_acc' => $limit_purchase_by_user,							
		// 					'purchase_button_text' => $purchase_button_text,
		// 					'package_order' => $package_order,								
		// 					'is_default' => $is_default,
		// 					'created_at'=>$cur_time,
		// 					'updated_at'=>$cur_time
		// 				);
		// 			$this->Common_model->commonInsert('packages',$datai);
					
		// 		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-top:10px;margin-bottom:0px;">
		// 		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		// 		Package Added Successfully.							</div>							';				
		// 		redirect('payments/manage','location');										
				
		// 	}						
			
		// 	$data['blog_categories'] = $page_meta =  $this->Common_model->commonQuery("select title,c_id	
		// 	from blog_categories
		// 	where status = 'Y' order by title ASC");
			
		// 	$data['theme']=$theme;				
		// 	$data['content'] = "$theme/payments/add_new";				
		// 	$this->load->view("$theme/header",$data);			
		// }		
		
		
		// public function edit($b_id = NULL)	
		// {		
		// 	$CI =& get_instance();		
		// 	$theme = $CI->config->item('theme') ;				
		// 	$this->load->library('Global_lib');		
					
		// 	$data = $this->global_lib->uri_check();				
		// 	$data['myHelpers']=$this;		
		// 	$this->load->model('Common_model');		
		// 	$this->load->helper('text');						
		// 	if(isset($_POST['submit']) || isset($_POST['draft']))		
		// 	{			
		
		// 		extract($_POST);			
				
		// 		foreach($_POST as $k=>$v)
		// 		{
		// 			$_POST[$k] = $this->security->xss_clean($v);
		// 			$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
		// 		}
				
		// 			extract($_POST,EXTR_OVERWRITE);								
		// 			if(isset($_POST['submit']))					
		// 				$status = 'Y';				
		// 			else if(isset($_POST['draft']))					
		// 				$status = 'N';				
		// 			else					
		// 				$status = 'Y';		
					
					
		// 			//var_dump($_POST);exit;
		// 			$decId = $this->DecryptClientId($b_id);
					
		// 			$applicabled_for ='';	
		// 			foreach ($applicable_for as $key => $value) {
		// 				$applicabled_for =  $applicable_for[$key];
		// 			}
		// 			$cur_time = time();
		// 			$datai = array( 			
		// 					'package_name' => $package,									
		// 					'package_price' => $packages_price,
		// 					'package_currency' => $currency_code,
		// 					'number_listing_limit' =>$number_of_listing,
		// 					'auto_active_property' => $auto_active,							
		// 					'days_limit_for_property_active' => $days_limit_for_property_property,	
		// 					'package_life' => $package_lifetime,									
		// 					'number_featured_properties' => $number_featered_property,
		// 					'days_limit__featured_properties' => $days_limit_for_featured_property,
		// 					'applicable_for' =>$applicabled_for,
		// 					'limit_purchase_by_acc' => $limit_purchase_by_user,							
		// 					'purchase_button_text' => $purchase_button_text,
		// 					'package_order' => $package_order,								
		// 					'is_default' => $is_default,
		// 					'updated_at'=>$cur_time
		// 				);	

		// 				$data= $this->Common_model->commonUpdate('packages',$datai,'package_id',$decId);				
					
		// 			$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable"  style="margin-top:10px;margin-bottom:0px;">	
		// 			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>		
		// 			package Updated Successfully.							</div>							';	
		// 			redirect('/payments/manage','location');				
		// 		}
		// 		$decId = $this->DecryptClientId($b_id);		
		// 		$data['blog_meta'] = $blog_meta =  $this->Common_model->commonQuery("select *from packages where package_id = $decId ");
				
				
		// 		if($blog_meta->num_rows() == 0)
		// 		{
		// 			$_SESSION['msg'] = '<div class="alert alert-danger alert-dismissable" style="margin-top:10px;margin-bottom:0px;">
		// 			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		// 			Invalid Packages.</div>							';				
		// 			redirect('/payments/manage','location');
		// 		}
				
		// 		$data['blog_categories'] = $this->Common_model->commonQuery("select title,c_id	from blog_categories where status = 'Y' order by title ASC");
		// 		$data['theme']=$theme;				
		// 		$data['content'] = "$theme/payments/edit";			
		// 		$this->load->view("$theme/header",$data);
		// }		
		// public function delete($rowid)	
		// {								
		// 	$CI =& get_instance();		
		// 	$this->load->library('Global_lib');		
		// 	if(!is_array($rowid))			
		// 		$rowid	= $this->global_lib->DecryptClientId($rowid);		
		// 	$this->load->model('Common_model');				
		// 	$tbl='packages';		
		// 	$pid='package_id';		
		// 	$url='/payments/manage/';	 			
		// 	$fld='package';				

		// 	$rows= $this->Common_model->commonDelete($tbl,$rowid,$pid );					
		// 	$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-top:10px;margin-bottom:0px;">								
		// 	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>																
		// 	'.$rows.' '.$fld.' Deleted Successfully.							
		// 	</div>							';		
		// 	redirect($url,'location','301');		
		// }

}
