<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends MY_Controller {
	
	function __construct() 
	{
        parent::__construct();
		
        if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
	}
	/*Homepage Section Ajax Callbacks*/
	
	public function get_country_list_by_lang_callback_func()
	{
		
		extract($_POST);		
		$CI =& get_instance();	
		
		$loc_tax_settings = $this->global_lib->get_option('loc_tax_settings');
		
		$locations = $this->global_lib->get_option('locations');
		
		$country_name = '';
		$country_list = '<option value="all" selected>All Countries</option>';
		$state_list = '<option value="all" selected>All States</option>';
		$city_list = '<option value="all" selected>All Cities</option>';
		$zipcode_list = '<option value="all" selected>All Zipcodes</option>';
		$subarea_list = '<option value="all" selected>All Subarea</option>';
		$country_code = '';

		if(!empty($locations))
		{
			$location_array = json_decode($locations,true);
			
			$lc_val = $this->global_lib->get_option('language_country_'.$lang_code);
			if(!empty($lc_val))
			{
				$exp_lc_val = explode(',',$lc_val);
				foreach($exp_lc_val as $cc)
				{
					$country_code = $cc;
					if(isset($location_array['countries'][$country_code]))
					{
						$country_name = $location_array['countries'][$country_code]['loc_title'];
						
						$country_list .= '<option data-country_code="'.$country_code.'" value="'.mlx_get_norm_string($country_name).'~'.$country_code.'"';
									/*
									if(isset($lang_state) == $sval['loc_title'])
									{
										$state_code = $skey;
										$country_list .= ' selected="selected" ';
									}
									*/
									$country_list .= '>'.$country_name.'</option>';
					}
				}
			}
		}
		
		
		header('Content-type: application/json');				
		echo json_encode(array('country_list'=> $country_list,'state_list'=> $state_list,'city_list' => $city_list,
							   'zipcode_list'=> $zipcode_list,'subarea_list' => $subarea_list));
		
	}
	
	public function get_state_or_city_list_by_lang_callback_func()
	{
		
		extract($_POST);		
		$CI =& get_instance();	
		
		$loc_tax_settings = $this->global_lib->get_option('loc_tax_settings');
		$is_state_enable = false;
		if(!empty($loc_tax_settings))
		{
			$loc_tax_setting_array = json_decode($loc_tax_settings,true);
			if(isset($loc_tax_setting_array['state']['enabled']) && $loc_tax_setting_array['state']['enabled'] == true)
				$is_state_enable = true;
		}
		
		$locations = $this->global_lib->get_option('locations');
		
		$country_name = '';
		$state_list = '<option value="all" selected>All States</option>';
		$city_list = '<option value="all" selected>All Cities</option>';
		$zipcode_list = '<option value="all" selected>All Zipcodes</option>';
		$subarea_list = '<option value="all" selected>All Subarea</option>';

		$state_code = $city_code = '';

		if(!empty($locations) && $is_state_enable)
		{
			$location_array = json_decode($locations,true);
			
			$lc_val = $country_code;
			if(!empty($lc_val) && isset($location_array['countries'][$lc_val]))
			{
				$country_name = $location_array['countries'][$lc_val]['loc_title'];
				
				if(isset($location_array['countries'][$lc_val]['states']))
				{
					foreach($location_array['countries'][$lc_val]['states'] as $skey=>$sval)
					{
						if($skey != 'no_state')
							$state_list .= '<option data-country_code="'.$lc_val.'" data-state_code="'.$skey.'" value="'.mlx_get_norm_string($sval['loc_title']).'~'.$skey.'"';
							if(isset($lang_state) && $lang_state == $sval['loc_title'])
							{
								$state_code = $skey;
								$state_list .= ' selected="selected" ';
							}
							$state_list .= '>'.$sval['loc_title'].'</option>';
					}
				}
			}
			if(isset($is_edit) && !empty($state_code))
			{
				if(isset($location_array['countries'][$lc_val]['states'][$state_code]['cities']))
				{
					$cities = $location_array['countries'][$lc_val]['states'][$state_code]['cities'];
					if(!empty($cities))
					{
						foreach($cities as $skey=>$sval)
						{
							$city_list .= '<option data-country_code="'.$lc_val.'" data-state_code="'.$state_code.'" data-city_code="'.$skey.'" value="'.mlx_get_norm_string($sval['loc_title']).'~'.$skey.'"';
							if(isset($lang_city) && $lang_city == $sval['loc_title'])
							{
								$city_list .= ' selected="selected" ';
								
								if(isset($sval['zipcodes']) && !empty($sval['zipcodes']))
								{
									foreach($sval['zipcodes'] as $zipcode)
									{
										$zipcode_list .= '<option value="'.$zipcode.'"';
										if(isset($lang_zip_code) && $lang_zip_code == $zipcode)
											$zipcode_list .= ' selected="selected" ';
										$zipcode_list .= '>'.$zipcode.'</option>';
									}
								}
								if(isset($sval['sub_areas']) && !empty($sval['sub_areas']))
								{
									foreach($sval['sub_areas'] as $subarea)
									{
										$subarea_list .= '<option value="'.$subarea.'"';
										if(isset($lang_sub_area) && $lang_sub_area == $subarea)
											$subarea_list .= ' selected="selected" ';
										$subarea_list .= '>'.$subarea.'</option>';
									}
								}
								
							}
							$city_list .= '>'.$sval['loc_title'].'</option>';
						}
					}
				}
			}
		}
		else if(!empty($locations))
		{
			$location_array = json_decode($locations,true);
			
			$lc_val = $country_code;
			if(!empty($lc_val) && isset($location_array['countries'][$lc_val]))
			{
				$country_name = $location_array['countries'][$lc_val]['loc_title'];
				
				if(isset($location_array['countries'][$lc_val]['states']['no_state']['cities']))
				{
					foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'] as $skey=>$sval)
					{
						$city_list .= '<option data-country_code="'.$lc_val.'" data-state_code="no_state" data-city_code="'.$skey.'" value="'.mlx_get_norm_string($sval['loc_title']).'~'.$skey.'"';
						if(isset($lang_city) && $lang_city == $sval['loc_title'])
								$city_list .= ' selected="selected" ';
						$city_list .= '>'.$sval['loc_title'].'</option>';
					}
				}
			}
		}
		
		header('Content-type: application/json');				
		echo json_encode(array('state_list'=> $state_list,'city_list' => $city_list,'zipcode_list'=> $zipcode_list,'subarea_list' => $subarea_list));
		
	}
	
	public function get_state_list_from_country_by_lang_callback_func(){
		
		extract($_POST);		
		$CI =& get_instance();	
		
		$state_list = '<option value="all" selected>All States</option>';
		
		if(isset($country_code))
		{
			$locations = $this->global_lib->get_option('locations');	
			$location_array = json_decode($locations,true);
				
			$lc_val = $country_code;
			if(!empty($lc_val) && isset($location_array['countries'][$lc_val]))
			{
				if(isset($location_array['countries'][$lc_val]['states']))
				{
					foreach($location_array['countries'][$lc_val]['states'] as $skey=>$sval)
					{
						$state_list .= '<option data-country_code="'.$lc_val.'" data-state_code="'.$skey.'" 
										value="'.mlx_get_norm_string($sval['loc_title']).'~'.$skey.'"';
						$state_list .= '>'.$sval['loc_title'].'</option>';
					}
				}
			}
		}
		header('Content-type: application/json');				
		echo json_encode(array('state_list' => $state_list));
		
	}
	
	public function get_city_list_from_state_by_lang_callback_func(){
		
		extract($_POST);		
		$CI =& get_instance();	
		
		$locations = $this->global_lib->get_option('locations');
		
		$city_list = '<option value="all" selected>All Cities</option>';
		
		$location_array = json_decode($locations,true);
			
		$lc_val = $country_code;
		if(!empty($lc_val) && isset($location_array['countries'][$lc_val]))
		{
			if(isset($location_array['countries'][$lc_val]['states'][$state_code]['cities']))
			{
				foreach($location_array['countries'][$lc_val]['states'][$state_code]['cities'] as $skey=>$sval)
				{
					$city_list .= '<option data-country_code="'.$lc_val.'" data-state_code="'.$state_code.'" data-city_code="'.$skey.'" value="'.mlx_get_norm_string($sval['loc_title']).'~'.$skey.'"';
					$city_list .= '>'.$sval['loc_title'].'</option>';
				}
			}
		}
		
		header('Content-type: application/json');				
		echo json_encode(array('city_list' => $city_list));
		
	}
	
	public function get_zip_subarea_list_from_city_by_lang_callback_func()
	{
		extract($_POST);		
		$CI =& get_instance();	
		
		$loc_tax_settings = $this->global_lib->get_option('loc_tax_settings');
		$is_state_enable = false;
		if(!empty($loc_tax_settings))
		{
			$loc_tax_setting_array = json_decode($loc_tax_settings,true);
			if(isset($loc_tax_setting_array['state']['enabled']) && $loc_tax_setting_array['state']['enabled'] == true)
				$is_state_enable = true;
		}
		
		$locations = $this->global_lib->get_option('locations');
		
		$zipcode_list = '<option value="all" selected>All Zipcodes</option>';
		$subarea_list = '<option value="all" selected>All Sub Areas</option>';

		
		if(!empty($locations) && $is_state_enable)
		{
			
			$location_array = json_decode($locations,true);
			
			$lc_val = $country_code;
			
			if(!empty($state_code) && !empty($city_code))
			{
				if(isset($location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code]))
				{
					$city = $location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code];
					
							
					if(isset($city['zipcodes']) && !empty($city['zipcodes']))
					{
						foreach($city['zipcodes'] as $zipcode)
						{
							$zipcode_list .= '<option value="'.$zipcode.'"';
							$zipcode_list .= '>'.$zipcode.'</option>';
						}
					}
					if(isset($city['sub_areas']) && !empty($city['sub_areas']))
					{
						foreach($city['sub_areas'] as $subarea)
						{
							$subarea_list .= '<option value="'.$subarea.'"';
							$subarea_list .= '>'.$subarea.'</option>';
						}
					}
						
				}
			}
		}
		else if(!empty($locations))
		{
			$location_array = json_decode($locations,true);
			
			$lc_val = $country_code;
			if(!empty($lc_val) && isset($location_array['countries'][$lc_val]))
			{
				if(isset($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]))
				{
					$city = $location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code];
					
					if(isset($city['zipcodes']) && !empty($city['zipcodes']))
					{
						foreach($city['zipcodes'] as $zipcode)
						{
							$zipcode_list .= '<option value="'.$zipcode.'"';
							$zipcode_list .= '>'.$zipcode.'</option>';
						}
					}
					if(isset($city['sub_areas']) && !empty($city['sub_areas']))
					{
						foreach($city['sub_areas'] as $subarea)
						{
							$subarea_list .= '<option value="'.$subarea.'"';
							$subarea_list .= '>'.$subarea.'</option>';
						}
					}
					
				}
			}
		}
		
		header('Content-type: application/json');				
		echo json_encode(array('zipcode_list'=> $zipcode_list,'subarea_list' => $subarea_list));
		
	}
	
	/*End of Homepage Section Ajax Callbacks*/
	
	public function update_plugins_setting_callback_func()	
	{		 
		extract($_POST);		
		$CI =& get_instance();	
		
		$plugin_meta = $this->global_lib->get_option('site_plugins');
		
		if(!empty($plugin_meta))
		{
			$plugin_meta_array = json_decode($plugin_meta,true);
			if($cur_status == 'Y')
			{
				$plugin_meta_array[] = $plugin_name;
			}
			else if (($key = array_search($plugin_name, $plugin_meta_array)) !== false) {
				unset($plugin_meta_array[$key]);
			}
			$this->global_lib->update_option('site_plugins',json_encode($plugin_meta_array));
		}
		else
		{
			$plugin_meta = array( $plugin_name );
			$this->global_lib->update_option('site_plugins',json_encode($plugin_meta));
		}
		echo 'success';
	}
	
	public function update_keywords_callback_func()	
	{		 
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		
		$datai = array( 
				$lang_slug => addslashes($value)
		);
		$encId = $this->DecryptClientId($lang_id);	
		$this->Common_model->commonUpdate('languages',$datai,'lang_id',$encId);
		
		echo 'success';
	}
	
	public function import_lang_keyword_callback_func()
	{
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		if($lang_for == 'front')
		{
			$file_content = $this->global_lib->get_include_contents(FCPATH.'../application/language/import-language-files/'.$lang_file);
		}
		else
		{
			$file_content = $this->global_lib->get_include_contents(FCPATH.'../application/admin/language/import-language-files/'.$lang_file);
		}	
		
		$myarray = $this->global_lib->strtoarray(trim($file_content));
		
		/*echo "<pre>";
		print_r($myarray);
		echo "</pre>";*/
		
		$sql = "select * from languages where lang_for = '$lang_for'";
		$result = $this->Common_model->commonQuery($sql);
		if($result->num_rows() > 0 && !empty($myarray))
		{
			/*echo "<pre>";*/
			foreach($result->result() as $row)
			{
				$keyword = trim($row->keyword);
				if(array_key_exists($keyword,$myarray))
				{
					/*echo "keyword=[".$keyword."]<br>";*/
					/*echo "[".$keyword."] and val = [".$myarray[$keyword]."]".$row->lang_id;*/
					//if($overright_keyword == 'Y' && !empty($myarray[$row->keyword]))
					if($overright_keyword == 'Y' || empty($row->$lang))
					{
						$datai = array( 
								$lang => $myarray[$keyword]
						);
						/*print_r($datai); echo "<br>";*/
						$this->Common_model->commonUpdate('languages',$datai,'lang_id',$row->lang_id);
					}
					unset($myarray[$keyword]);
				}
			}
		}
		
		
		if(!empty($myarray))
		{
			/*echo "<pre>";
		print_r($myarray);
		echo "</pre>"; exit;*/
			foreach($myarray as $k=>$v)
			{
				$datai = array( 'keyword' => trim($k), 'lang_for' => $lang_for, $lang => $v);
				$this->Common_model->commonInsert('languages',$datai);
			}
		}
		
		$site_language = $this->global_lib->get_option('site_language');
		$site_language_array = json_decode($site_language,true);
		foreach($site_language_array as $k=>$v)
		{
			$lang_exp = explode('~',$v['language']);
			$lang_name = $lang_exp[0];
			$lang_code = $lang_exp[1];
			$lang_slug = $this->global_lib->get_slug($lang_name,'_');
			
			/*front*/
			if(!is_dir("../application/language"))
			{
				mkdir("../application/language",0777);
			}
						
			if(!is_dir("../application/language/$lang_slug"))
			{
				mkdir("../application/language/$lang_slug",0777);
			}
			if(file_exists("../application/language/$lang_slug/".$lang_slug."_lang.php"))
			{
				if($lang_slug!='english')
				unlink("../application/language/$lang_slug/".$lang_slug."_lang.php");
			}
			
			$fp = fopen("../application/language/$lang_slug/".$lang_slug."_lang.php","wb");
			if($fp)
			{
				$output = "<?php \n\n";
				$keyword_result = $this->Common_model->commonQuery("select keyword,$lang_slug from languages where lang_for = 'front'
									order by lang_id DESC");
			   if($keyword_result->num_rows() > 0) 
			   { 
					foreach($keyword_result->result() as $row)
					{
						//$output .= '$lang["'.$row->keyword.'"] = "'.$row->$lang_slug.'";'."\n";
						$output .= '$lang'."['".$row->keyword."'] = '".addslashes($row->$lang_slug)."';\n";
					}
			   }
				fwrite($fp,$output);
				fclose($fp);
			}
			/*back*/
			if(!is_dir("../application/admin/language"))
			{
				mkdir("../application/admin/language",0777);
			}
						
			if(!is_dir("../application/admin/language/$lang_slug"))
			{
				mkdir("../application/admin/language/$lang_slug",0777);
			}
			if(file_exists("../application/admin/language/$lang_slug/".$lang_slug."_lang.php"))
			{
				if($lang_slug!='english')
				unlink("../application/admin/language/$lang_slug/".$lang_slug."_lang.php");
			}
			
			$fp = fopen("../application/admin/language/$lang_slug/".$lang_slug."_lang.php","wb");
			if($fp)
			{
				$output = "<?php \n\n";
				$keyword_result = $this->Common_model->commonQuery("select keyword,$lang_slug from languages where lang_for = 'back'
									order by lang_id DESC");
			   if($keyword_result->num_rows() > 0) 
			   { 
					foreach($keyword_result->result() as $row)
					{
						//$output .= '$lang["'.$row->keyword.'"] = "'.$row->$lang_slug.'";'."\n";
						$output .= '$lang'."['".$row->keyword."'] = '".addslashes($row->$lang_slug)."';\n";
					}
			   }
				fwrite($fp,$output);
				fclose($fp);
			}
			
		}
		
		echo 'success';
	}
	
	public function export_lang_keyword_callback_func()
	{
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		$output = "";
		$sql = "select * from languages where lang_for = '$lang_for'";
		$result = $this->Common_model->commonQuery($sql);
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				$output .= '$lang'."['".$row->keyword."'] = '".addslashes($row->$lang)."';\n";
			}
		}
		$time_stamp = time();
		$file_name = '';
		$file_path = '';
		
		if($lang_for == 'front' && $export_type == 'save')
		{
			if(!is_dir("../application/language/import-language-files"))
			{
				mkdir("../application/language/import-language-files",0777);
			}
			
			$fp = fopen("../application/language/import-language-files/".$lang."-$time_stamp.lang","wb");
			$file_name = $lang."-$time_stamp.lang";
			$file_path = base_url("../application/language/import-language-files/");
			if($fp)
			{
				fwrite($fp,$output);
				fclose($fp);
			}
		}
		else if($lang_for == 'back' && $export_type == 'save')
		{
			if(!is_dir("../application/admin/language/import-language-files"))
			{
				mkdir("../application/admin/language/import-language-files",0777);
			}
			
			$fp = fopen("../application/admin/language/import-language-files/".$lang."-$time_stamp.lang","wb");
			$file_name = $lang."-$time_stamp.lang";
			$file_path = base_url("../application/admin/language/import-language-files/");
			if($fp)
			{
				fwrite($fp,$output);
				fclose($fp);
			}
		}
		else if($export_type == 'download')
		{
			$file_name = $lang."-$time_stamp.lang";
			
			header('Content-type: application/json');				
			echo json_encode(array('file_name'=> $file_name,'file_content' => $output));
		}
		else
		{
			echo 'success';
		}
	}
	
	public function hide_notifications_callback_func()	
	{		 
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		if(isset($notif_id) && !empty($notif_id))
		{
			$datai = array( 
					'notif_status' => 'H'
			);
			$encId = $this->DecryptClientId($notif_id);	
			$this->Common_model->commonUpdate('notifications',$datai,'notif_id',$encId);
		}
		echo 'success';
	}
	
	public function remove_notifications_callback_func()	
	{		 
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		
		$encId = $this->DecryptClientId($notif_id);	
		$this->Common_model->commonDelete('notifications',$encId,'notif_id' );
		echo 'success';
	}
	
	public function toggle_featured_property_callback_func()	
	{		 
		extract($_POST);		
		$CI =& get_instance();	
		$this->load->model('Common_model');		
		$user_type = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');
		$can_feature_property = false;
		
		if($user_type == 'admin')
			$can_feature_property = true;
		
		$update_credit = false;
		
		if($this->enable_featured_property_posting == 'Y' && $user_type != 'admin')
		{
			$credit =  $this->global_lib->get_user_meta($user_id , 'featured_property_credit');
			if($is_feat == 'Y' && $credit > 0){
				$can_feature_property = true;
				/** decrement*/
				if(isset($p_id) && !empty($p_id)){
					$encId = $this->DecryptClientId($p_id);	
					/*$credit_used = $this->package_lib->check_credit_used('featured_property' ,  $encId,'property');
					
					if(!$credit_used)
					{
						$this->package_lib->add_credit_uses('featured_property' ,  $encId,'property',$user_id);
						$update_credit = true;		
					}*/
					
					$this->credit_id = $this->package_lib->get_credit_id_by_user_id($user_id, 'property', 'featured_property' );
					$credit_used = $this->package_lib->check_credit_used('featured_property',$encId,'property');
					if(!$credit_used && $this->credit_id )
					{
						$this->package_lib->add_credit_uses('featured_property',$encId,'property',$user_id);
						$this->package_lib->update_credits_by_user_id($user_id,'featured_property_credit','minus_credit',1);
						$this->package_lib->update_credits_updated_credit_for_user($this->credit_id);
						$update_credit = true;	
					}	
				}
			}	
			else if($credit == 0)
			{
				echo '<div class="alert alert-warning alert-dismissable" style="margin-top:10px; margin-bottom:0px;">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					'.mlx_get_lang("You don't have sufficent credits for post featured porperty").' 
				</div>';
			}	
			else if($is_feat == 'N')
				$can_feature_property = true;
			
		}else
			$can_feature_property = true;	
		
		if($can_feature_property)
		{
			if(isset($p_id) && !empty($p_id))
			{
				$datai = array( 
						'is_feat' => $is_feat
				);
				$encId = $this->DecryptClientId($p_id);	
				$this->Common_model->commonUpdate('properties',$datai,'p_id',$encId);
				
				//if($update_credit)
					//$this->package_lib->update_credits_by_user_id($user_id,'featured_property_credit','minus_credit');
			}
			echo 'success';
		}	
		
		/*else if($is_feat == 'Y')
		{
			echo '<div class="alert alert-warning alert-dismissable" style="margin-top:10px; margin-bottom:0px;">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					'.mlx_get_lang("You don't have sufficent credits for post featured porperty").' 
				</div>';
		}*/
	}
	
	public function check_username_existence()	{		
		extract($_POST);		
		$CI =& get_instance();		
		$this->load->library('Global_lib');		
		$this->load->model('Common_model');
		$this->load->helper('text');
		$sql = "select * from users 
				where user_name = '$user_name' ";
		$result = $this->Common_model->commonQuery($sql);
		if($result->num_rows() > 0 )
		{
			echo 'error';
		}
		else
		{
			echo 'success';
		}
	}
	
	public function EncryptClientId($id)
	{
		return substr(md5($id), 0, 8).dechex($id);
	}

	public function DecryptClientId($id)
	{
		$md5_8 = substr($id, 0, 8);
		$real_id = hexdec(substr($id, 8));
		return ($md5_8==substr(md5($real_id), 0, 8)) ? $real_id : 0;
	}
	
	public function print_sql()
	{
		$CI =& get_instance();		
		$this->load->model('Common_model');
		$this->load->library('Global_lib');
		
		
		
		$qry = "select * from delivery_types";
		
		$result = $this->Common_model->commonQuery($qry);
		if($result->num_rows() > 0)
		{
			foreach($result->result() as $row)
			{
				echo '<pre>';
				print_r($row);
				echo '</pre>';
				
			}
		}
		
		
		
		
	}
	
	public function check_client_email()
	{
		$CI =& get_instance();		
		$this->load->model('Common_model');
		$this->load->library('Global_lib');
		
		
		extract($_POST);
		$qry = "select email from clients where email = '".$to_email."'";		
		$result = $this->Common_model->commonQuery($qry);
		if($result->num_rows() > 0)
		{
			echo true;
		}
		else
		{
			echo false;	
		}		
	}

}
