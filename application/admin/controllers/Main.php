<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
		
		
class Main extends MY_Controller {
	
	function __construct() {
        parent::__construct();
		
        if(!$this->isLogin())
		{
			
			redirect('/logins','location');
		}
		
		//print_r($_SESSION);
		
    }
	
	public function index()
	{
		
		
		if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$this->load->library('Global_lib');
		
		$data = $this->global_lib->uri_check();
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$user_type = $this->session->userdata('user_type');
		if($user_type == 'admin')
		{
			$data['total_properties'] = $this->Common_model->commonQuery("select * from properties as prop where prop.deleted = 'N'");
			/*$data['total_media'] = $this->Common_model->commonQuery("select * from post_images where parent_image_id = 0 ");*/
			
			$data['total_agents'] = $this->Common_model->commonQuery("select * from users where user_type = 'agent'");
			$data['total_owners'] = $this->Common_model->commonQuery("select * from users where user_type = 'owner'");
			$data['total_builders'] = $this->Common_model->commonQuery("select * from users where user_type = 'builder'");
			$data['total_landlords'] = $this->Common_model->commonQuery("select * from users where user_type = 'landlord'");
			$data['total_blogs'] = $this->Common_model->commonQuery("select * from blogs ");
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			$data['total_properties'] = $this->Common_model->commonQuery("select * from properties prop where prop.created_by = $user_id and prop.deleted = 'N'");
			$data['total_blogs'] = $this->Common_model->commonQuery("select * from blogs where created_by = $user_id ");
			/*$data['total_media'] = $this->Common_model->commonQuery("select * from post_images where parent_image_id = 0 and user_id = $user_id");*/
		}
		
		$data['theme']=$theme;
		$data['content'] = "$theme/dashboard";
		$this->load->view("$theme/header",$data);
			
	}
	
	public function home_page()
	{
		if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$content_sections = $CI->config->item('content_sections') ;
		
		if(isset($_POST['submit']) || isset($_POST['draft']))		
		{			
			
			extract($_POST,EXTR_OVERWRITE);				 								
			
			
			$user_id = $this->session->userdata('user_id');
			
			$content = array();
			
			foreach($_POST as $k=>$v)
			{
				if(is_array($v) && $k != 'submit')
					$content[$k] = $v;
			}
			
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			
			if(isset($content['properties_section']) && !empty($content['properties_section']))
			{
				foreach($content['properties_section'] as $k=>$v)
				{
					$v['section_type'] = 'dynamic';
					$content['properties_section_'.$k] = $v;
				}
				unset($content['properties_section']);
			}
			if(isset($content['video_section']) && !empty($content['video_section']))
			{
				foreach($content['video_section'] as $k=>$v)
				{
					$v['section_type'] = 'video';
					$content['video_section_'.$k] = $v;
				}
				unset($content['video_section']);
			}
			
			
			$this->global_lib->update_option('homepage_section',json_encode($content));
			
			$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-top:10px;margin-bottom:0px;">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			'.mlx_get_lang("Homepage Section Updated Successfully").'</div>							';				
			redirect('/main/home_page','location');										
			
		}
		
		$homepage_section = $this->global_lib->get_option('homepage_section');
		
		if(isset($homepage_section) && !empty($homepage_section))
		{
			$data['meta_content_lists'] = json_decode($homepage_section,true);
		}
		
		/*
		echo '<pre>';
		print_r($data['meta_content_lists']);
		echo '</pre>';
		exit;
		*/
		
		$data['theme']=$theme;		
		
		$data['content_sections']=$content_sections;		
				
		$data['content'] = "$theme/home_page";				
		$this->load->view("$theme/header",$data);	
		
	}	
	
	
	public function google_dashboard(){
		
		/*if(!$this->isLogin())
		{
			redirect('/logins','location');
		}*/
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$content_sections = $CI->config->item('content_sections') ;
		
		
		$data['theme']=$theme;		
		
		$data['content_sections']=$content_sections;		
				
		$data['content'] = "$theme/google_dashboard";				
		$this->load->view("$theme/header",$data);
		
	}
	
	public function google_analytics(){
		
		
		$isPlugAct = $this->isPluginActive('google_analytics');
		if($isPlugAct != true)
		{
			redirect('/main','location');
		}
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		//$content_sections = $CI->config->item('content_sections') ;
		
		
		$data['theme']=$theme;		
		
		//$data['content_sections']=$content_sections;		
				
		$data['content'] = "$theme/google_analytics/dashboard";				
		$this->load->view("$theme/header",$data);
		
	}
		

			
}

