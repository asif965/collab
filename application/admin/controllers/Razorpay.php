<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Razorpay\Api\Api;


class Razorpay extends MY_Controller {
	
	var $api_key , $api_secret;
	
	function __construct() {
        parent::__construct();
        if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		$CI =& get_instance();		
		$this->load->library('Global_lib');	
		
		$isPlugAct = $this->isPluginActive('payment');
		if($isPlugAct != true)
		{
			redirect('/main','location');
		}
		
		
		 include APPPATH.'third_party/razorpay/Razorpay.php';
		
		
		/* Paypal code*/
		$CI =& get_instance();	
		
		$payment_methods = $CI->global_lib->get_option('payment_methods');
		$payment_method = json_decode($payment_methods);
		
		if(!empty($payment_method) && $payment_method->payment_method_razorpay_section->is_enable == 'Y'){
			
			if(!empty($payment_method->payment_method_razorpay_section->razorpay_api_key) &&
			!empty($payment_method->payment_method_razorpay_section->razorpay_api_secret)){
				
				
					$this->api_key = $payment_method->payment_method_razorpay_section->razorpay_api_key;
					$this->api_secret = $payment_method->payment_method_razorpay_section->razorpay_api_secret;
			}
		
		}
		
		
    }
	
	public function index()	
	{		
		$this->manage();	
	}
	
	public function rp_createorder(){
        $amount = $this->input->post('totalAmount');
		$currency = $this->input->post('currency');
        $api = new Api($this->api_key, $this->api_secret);

        $order  = $api->order->create([
            'receipt'         => 'order_'.$this->gen_receipt(),
            'amount'          => $amount, // amount in the smallest currency unit
            'currency'        => $currency,// <a href="https://razorpay.freshdesk.com/support/solutions/articles/11000065530-what-currencies-does-razorpay-support" target="_blank">See the list of supported currencies</a>.)
            'payment_capture' =>  '1' // auto caputer amount
          ]);
        //var_dump($order);exit;
        //echo json_encode($order->id);
        header('Content-type: application/json');				
		echo json_encode(array('order_id' => $order->id,
							   'api_key' => $this->api_key));
    }
	
	public function gen_receipt($length = 14) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function rp_check(){
		$CI =& get_instance();		
		$theme = $CI->config->item('theme') ;				
		$this->load->library('Global_lib');		
						
		$data = $this->global_lib->uri_check();		
		
		$data['myHelpers']=$this;		
		$this->load->model('Common_model');		
		$this->load->helper('text');
		
		$api = new Api($this->api_key, $this->api_secret);
		
		extract($_POST);
		
		
		$payment_details = $api->payment->fetch('pay_Fz04cG2TGtp1be');//'pay_FsiaTnYU8Jifzi');//$rp_payment_id);
		echo "<pre>";print_r($payment_details);
		echo $payment_details->status;
		exit;
		
	}
	
	public function rp_payment_success(){
		$CI =& get_instance();		
		$theme = $CI->config->item('theme') ;				
		$this->load->library('Global_lib');		
						
		$data = $this->global_lib->uri_check();		
		
		$data['myHelpers']=$this;		
		$this->load->model('Common_model');		
		$this->load->helper('text');
		
		$api = new Api($this->api_key, $this->api_secret);
		
		extract($_POST);
		
		/*$rp_payment_id = 'pay_Fz04cG2TGtp1be';*/
		$payment_details = $api->payment->fetch($rp_payment_id);//'pay_FsiaTnYU8Jifzi');//$rp_payment_id);
		/*echo "<pre>";
		print_r($_POST);
		print_r($payment_details);
		echo $payment->status;
		exit;
		if(is_object($payment) )
			echo $payment->status;
		else echo "error";
		exit;*/
		$user_id = $this->session->userdata('user_id');
		
		$status = $payment_details->status;
		$created_at = $payment_details->created_at;
		
		
		$package_info = $this->Common_model->commonQuery('select * from packages where package_id='.$package_id);
			
		$details =[];
		foreach ($package_info->result() as $package_detail) {
			foreach ($package_detail as $key => $value) {
				$details[$key] = $value;
			}
		}
		$package_json_obj = json_encode($details);
		
		$payment_details_arr = array();
		foreach ($payment_details as $key => $value) {
				$payment_details_arr[$key] = $value;
			}
		
		$transaction_meta = array();
		$transaction_meta['response'] = $rp_response;
		$transaction_meta['payment_details'] = $payment_details_arr;
		$transaction_meta['status'] = $status;
		$transaction_meta['created_at'] = $created_at;
		
		
		
		$transaction_meta = json_encode($transaction_meta);
		
		$totalAmount = $payment_details->amount;
		
		$totalAmount = $totalAmount / 100;
		
		
		$transaction_key = $this->package_lib->getToken(16);
		
		$datai = array(
				'transaction_key' => $transaction_key,
				'packages_id' => $package_id,
				'package_detail' => $package_json_obj,
				'user_id' =>$user_id,
				'payment_mode' => 'razorpay',
				'transaction_meta' =>$transaction_meta,
				'transaction_amount' => $totalAmount,
				'transaction_date'=> $created_at,
				'status'=>ucfirst($status),
			);

			
		$transaction_id = $this->Common_model->commonInsert('transaction',$datai);
		
		if($status == 'captured'){
			
			
			$datai = array( 'status' => 'Completed',);
			$CI->Common_model->commonUpdate('transaction',$datai,'transaction_id',$transaction_id);
			
			$args = array();
			$args['payment_id'] = $rp_payment_id;
			$args['transaction_id'] = $transaction_id;
			$args['user_id'] = $user_id;
			$args['package_id'] = $package_id;
			
			$args['trans_type'] = 'razorpay';
			$args['transaction_type'] = 'Paid Via Razorpay';
			/*$args['redirect'] = '/packages/payment_success/';  no redirect needed for razorpay*/
			
			$this->package_lib->payment_paid_action($args);
			
			
		
			header('Content-type: application/json');				
			echo json_encode(array('status' => 'success','red_url' => 'packages/razorpay_success'));/**/
			
		}else{
			//echo "packages/razorpay_failed";
			header('Content-type: application/json');				
			echo json_encode(array('status' => 'error','red_url' => 'packages/razorpay_failed'));
		}
		exit;
		
	}
	
	
	
	
	
}
