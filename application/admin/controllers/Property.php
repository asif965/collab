<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property extends MY_Controller {
	
	var $post_property_credit;
	
	function __construct() 
	{
        parent::__construct();
        if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		
		$this->load->library('Language_lib');
		$this->load->library('Package_lib');
		$user_id = $this->session->userdata('user_id');
		$this->post_property_credit = $this->package_lib->get_credits_by_user_id($user_id,'post_property_credit');
		
	}
	
	public function index()
	{
		$this->manage();	
	}
	
	public function manage($slug = '')
	{
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$data['currency_symbol'] = $this->global_lib->get_currency_symbol();
		$user_type = $this->session->userdata('user_type');
		
		$data['page_heading'] = mlx_get_lang('Manage Active Properties');
		
		if($user_type == 'admin')
		{
			if($slug == 'all')
			{
				$data['page_heading'] = mlx_get_lang('Manage All Properties');
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			else if($slug == 'active' || $slug == '')
			{
				$data['page_heading'] = mlx_get_lang('Manage Active Properties');
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.is_feat = 'N' and prop.status = 'publish' and prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			else if($slug == 'inactive')
			{
				$data['page_heading'] = mlx_get_lang('Manage In-Active Properties');
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.is_feat = 'N' and prop.status = 'draft'  and prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			else if($slug == 'pending')
			{
				$data['page_heading'] = mlx_get_lang('Manage Pending Properties');
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.status = 'pending' and prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			else if($slug == 'featured')
			{
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.is_feat = 'Y' and prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			else if($slug == 'rejected')
			{
				$data['page_heading'] = mlx_get_lang('Manage Rejected Properties');
				$data['query'] = $this->Common_model->commonQuery("
						SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.status = 'reject' and prop.deleted = 'N'
						order by prop.p_id DESC");	
			}
			
			$data['all_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.deleted = 'N'
					order by prop.p_id DESC");	
					
			$data['active_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.is_feat = 'N' and prop.status = 'publish' and prop.deleted = 'N'
					order by prop.p_id DESC");	
			$data['inactive_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.is_feat = 'N' and prop.status = 'draft'  and prop.deleted = 'N'
					order by prop.p_id DESC");	
			
			$data['pending_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.status = 'pending' and prop.deleted = 'N'
					order by prop.p_id DESC");	
		
			$data['featured_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.is_feat = 'Y' and prop.deleted = 'N'
					order by prop.p_id DESC");	
		
			$data['rejected_properties'] = $this->Common_model->commonQuery("
					SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
					left join property_types as pt on pt.pt_id = prop.property_type
					where prop.status = 'reject' and prop.deleted = 'N'
					order by prop.p_id DESC");	
			
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			if($slug == 'all')
			{
				$data['page_heading'] = mlx_get_lang('Manage All Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id  and prop.deleted = 'N'
							order by p_id DESC");
			}
			else if($slug == 'active' || $slug == '')
			{
				$data['page_heading'] = mlx_get_lang('Manage Active Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id 
							and prop.is_feat = 'N' and prop.status = 'publish' and prop.deleted = 'N'
							order by p_id DESC");	
			}
			else if($slug == 'inactive')
			{
				$data['page_heading'] = mlx_get_lang('Manage In-Active Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id 
							and prop.is_feat = 'N' and prop.status = 'draft' and prop.deleted = 'N'
							order by p_id DESC");
			}
			else if($slug == 'pending')
			{
				$data['page_heading'] = mlx_get_lang('Manage Pending Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id 
							and prop.status = 'pending' and prop.deleted = 'N'
							order by p_id DESC");
			}
			else if($slug == 'featured')
			{
				$data['page_heading'] = mlx_get_lang('Manage Featured Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id 
							and prop.is_feat = 'Y'  and prop.deleted = 'N'
							order by p_id DESC");
			}
			else if($slug == 'rejected')
			{
				$data['page_heading'] = mlx_get_lang('Manage Rejected Properties');
				$data['query'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
							left join property_types as pt on pt.pt_id = prop.property_type
							where prop.created_by = $user_id 
							and prop.status = 'reject' and prop.deleted = 'N'
							order by p_id DESC");
			}
			
			$data['all_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						order by p_id DESC");
			$data['active_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						and prop.is_feat = 'N' and prop.status = 'publish'
						order by p_id DESC");	
			$data['inactive_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						and prop.is_feat = 'N' and prop.status = 'draft' 
						order by p_id DESC");
			$data['pending_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						and prop.status = 'pending'
						order by p_id DESC");
			$data['featured_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						and prop.is_feat = 'Y' 
						order by p_id DESC");
			$data['rejected_properties'] = $this->Common_model->commonQuery("SELECT prop.*, pt.title as prop_type_title FROM `properties` as prop 
						left join property_types as pt on pt.pt_id = prop.property_type
						where prop.created_by = $user_id  and prop.deleted = 'N'
						and prop.status = 'reject'
						order by p_id DESC");
			
		}
		
		$data['cur_active_tab'] = $slug;
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/manage";
		
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function add_new()
	{
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		$this->load->library('Package_lib');
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$data['size_units'] = $CI->config->item('size_units') ;
		
		$this->load->library('Email_lib');
		
		if(isset($_POST['submit']) || isset($_POST['draft']) || isset($_POST['pending']))
		{
			
			extract($_POST);
			
			$property_status = 'Public';
			$user_type = $this->session->userdata('user_type');
			
			if($this->site_payments == 'Y'){
				if($this->enable_property_posting == 'Y' && $this->post_property_credit <= 0 && $user_type != 'admin')
				{
					$_SESSION['msg'] = '
								<div class="alert alert-warning alert-dismissable" style="margin-top:10px; margin-bottom:10px;">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									'.mlx_get_lang("You don't have sufficent credits for post porperty.").' 
								</div>
								';
					redirect('/property/manage','location');
				}
				else if(!$this->is_subscription_expires && $user_type != 'admin')
				{
					$_SESSION['msg'] = '
						<div class="alert alert-warning alert-dismissable" style="margin-top:10px; margin-bottom:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("You don't have sufficent subscription credits.").' 
						</div>
						';
					redirect('/property/manage','location');
				}
			}
			
			extract($_POST,EXTR_OVERWRITE);
			
			
			foreach($_POST as $k=>$v)
			{
				
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
				
				$status = 'draft';
				$sess_msg = '';
				$user_email_template = 'property_submission_email';
				$em_args = array();
				if(isset($_POST['submit']))
				{
					$status = 'publish';
					$sess_msg = mlx_get_lang("Property Published Successfully");
					/*$user_email_template = '';*/
					
				}
				else if(isset($_POST['draft']))
				{
					$status = 'draft';
					$sess_msg = mlx_get_lang("Property Saved as Draft Successfully");
					$user_email_template = '';
				}
				else if(isset($_POST['pending']))
				{
					$status = 'pending';
					$sess_msg = mlx_get_lang("Property Submitted for Approval Successfully");
					$user_email_template = 'property_submitted_approval_email';
				}
				
				if(isset($multi_lang) && !empty($multi_lang))
				{
					$keys = array_keys( $multi_lang);
					$title = $multi_lang[$keys[0]]['title'];
				}
					
				$config = array( 'field' => 'slug', 'title' => 'title', 'table' => 'properties', 'id' => 'p_id');
				$this->load->library('Slug_lib', $config);
				
				$datap = array( 'title' => $title, );
				$slug = $this->slug_lib->create_uri($datap);
				
				$property_images = '';
				if(isset($addedImgFromMediaLibrary) && !empty($addedImgFromMediaLibrary))
				{
					$data_exp = explode(',',$addedImgFromMediaLibrary);
					$data_exp_array = array();
					foreach($data_exp as $k=>$v)
					{
						$data_exp_array[] = $this->DecryptClientId($v);
					}
					$property_images = implode(',',$data_exp_array);
				}
				
				if(isset($document_meta) && !empty($document_meta))
				{
					if(!isset($property_meta))
						$property_meta = array();
					
					foreach($document_meta as $dmk=>$dmv)
					{
						$data_exp = explode(',',$dmv);
						$data_exp_array = array();
						foreach($data_exp as $k=>$v)
						{
							$data_exp_array[] = $this->DecryptClientId($v);
						}
						$property_meta[$dmk] = implode(',',$data_exp_array);
					}
				}
				
					
					if(!isset($indoor_amenities))
						$indoor_amenities = array();
					if(!isset($outdoor_amenities))
						$outdoor_amenities = array();
					
					$distance_list_array = array();
					if(isset($distance_list) && !empty($distance_list))
					{
						foreach($distance_list as $dk=>$dv)
						{
							if($dv['direction'] != '' && $dv['distance'] != '' && $dv['distance_text'] != '')
							{
								$distance_list_array[str_replace('_',' ',$dk)] = $dv;
							}
								
						}
					}
					
					$video_url_string = '';
					if(isset($video_url) && !empty($video_url))
					{
						$emptyRemoved = array_filter($video_url);
						$video_url_string = implode(',',$emptyRemoved);
					}
					
					if(!isset($state)) $state = '';
					if(!isset($city)) $city = '';
					if(!isset($zipcode)) $zipcode = '';
					if(!isset($sub_area)) $sub_area = '';
					
					if(isset($multi_lang) && !empty($multi_lang))
					{
						$keys = array_keys( $multi_lang);
						$description = $multi_lang[$keys[0]]['description'];
						$short_description = $multi_lang[$keys[0]]['short_description'];
						$price = $multi_lang[$keys[0]]['price'];
						$seo_meta_keywords = $multi_lang[$keys[0]]['seo_meta_keywords'];
						$seo_meta_description = $multi_lang[$keys[0]]['seo_meta_description'];
						$address = $multi_lang[$keys[0]]['address'];
						
						$isPlugAct = $this->isPluginActive('property_locations');
						if($isPlugAct == true)
						{
							$country = '';
							if(isset($multi_lang[$keys[0]]['country']) && !empty($multi_lang[$keys[0]]['country']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['country']);
								
								if(count($sExp) > 1)
									$country = $sExp[1];
								else
									$country = $sExp[0];
							}
							
							$state = '';
							if(isset($multi_lang[$keys[0]]['state']) && !empty($multi_lang[$keys[0]]['state']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['state']);
								if(count($sExp) > 1)
									$state = $sExp[1];
								else
									$state = $sExp[0];
							}
							
							$city = '';
							if(isset($multi_lang[$keys[0]]['city']) && !empty($multi_lang[$keys[0]]['city']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['city']);
								if(count($sExp) > 1)
									$city = $sExp[3];
								else
									$city = $sExp[0];
							}
							
							if(isset($multi_lang[$keys[0]]['zipcode']) && !empty($multi_lang[$keys[0]]['zipcode']))
							{
								$zipcode = $multi_lang[$keys[0]]['zipcode'];
							}
							
							if(isset($multi_lang[$keys[0]]['sub_area']) && !empty($multi_lang[$keys[0]]['sub_area']))
							{
								$sub_area = $multi_lang[$keys[0]]['sub_area'];
							}
						}
						else
						{
							$multi_lang[$keys[0]]['state'] = $state;
							$multi_lang[$keys[0]]['city'] = $city;
							$multi_lang[$keys[0]]['zipcode'] = $zip_code;
							$zipcode = $zip_code;
						}
						
						
					}
					
					if(!empty($size))
						$size .= "~".$size_measure; 
					
					$user_id = $this->global_lib->DecryptClientId($user_id);
					
					if(!isset($lat))
						$lat = '';
					if(!isset($long))
						$long = '';
					
					$datai = array( 
									'title' => trim($title),
									'short_description' => trim($short_description),
									'description' => trim($description),
									'address' => $address,
									'street_address' => $street_address,
									'country' => $country,
									'state' => $state,
									'city' => $city,
									'zip_code' => $zipcode,
									'sub_area' => $sub_area,
									'lat' => $lat,
									'long' => $long,
									'price' => str_replace(',','',$price),
									'size' => $size,
									'property_type' => $this->DecryptClientId($property_type),
									'property_for' => strtolower($property_for),
									'bedroom' => $bedroom,
									'bathroom' => $bathroom,
									'garage' => $garage,
									'indoor_amenities' => json_encode($indoor_amenities),
									'outdoor_amenities' => json_encode($outdoor_amenities),
									'distance_list' => json_encode($distance_list_array),
									'video_urls' => $video_url_string,
									'property_images' => $property_images,
									'created_on' => time(),
									'status' => $status,
									'created_by' => $user_id, //$this->global_lib->DecryptClientId($user_id),
									'slug' => $slug,
									'seo_meta_keywords' => $seo_meta_keywords,
									'seo_meta_description' => $seo_meta_description,
									'property_status' => $property_status
									);
							
					if(isset($custom_fields) && count($custom_fields) > 0)
					{
						foreach($custom_fields as $k=>$v)
						{
							$datai[$k] = $v;
						}
						
					}
					
						
					$p_id = $this->Common_model->commonInsert('properties',$datai);
					
					
					
					if(isset($_POST['pending']))
					{
						$first_name = $this->global_lib->get_user_meta($user_id,'first_name');
						$last_name = $this->global_lib->get_user_meta($user_id,'last_name');
						
						$admin_ids = $this->global_lib->get_admin_user_ids();
						if(!empty($admin_ids))
						{
							foreach($admin_ids as $k=>$v)
							{
								$datai = array( 
									'p_id' => $p_id,
									'notif_text' => 'A Property submitted for approval by '.ucfirst($first_name).' '.ucfirst($last_name).'.',
									'notif_icon' => 'fa-clock-o text-yellow',
									'notif_by' => $user_id,
									'notif_for' => $v,
									'notif_on' => time(),
									'notif_status' => 'U',
									'prop_action' => 'submit'
								);
								$this->Common_model->commonInsert('notifications',$datai);
							}
						}
					}
					
					if(isset($multi_lang) && !empty($multi_lang))
					{
						foreach($multi_lang as $k=>$v)
						{
							if($v['title'] != '')
							{
								$datai = array( 			
									'title' => addslashes(trim($v['title'])),			
									'short_description' => addslashes(trim($v['short_description'])),			
									'description' => addslashes(trim($v['description'])),		
									'price' => str_replace(',','',$v['price']),
									'address' => addslashes(trim($v['address'])),
									'seo_meta_keywords' => addslashes(trim($v['seo_meta_keywords'])),
									'seo_meta_description' => addslashes(trim($v['seo_meta_description'])),
									'p_id' => $p_id,								
									'language' => $k
								);
								
								$datai['country'] = '';
								if(isset($v['country']))
									$datai['country'] = $v['country'];
								
								$datai['state'] = '';
								if(isset($v['state']))
									$datai['state'] = $v['state'];
								
								$datai['city'] = '';
								if(isset($v['city']))
									$datai['city'] = $v['city'];
								
								$datai['zip_code'] = '';
								if(isset($v['zipcode']))
									$datai['zip_code'] = $v['zipcode'];
								
								$datai['sub_area'] = '';
								if(isset($v['sub_area']))
									$datai['sub_area'] = $v['sub_area'];
								
								$this->Common_model->commonInsert('property_lang_details',$datai);
							}
						}
					}
					else
					{
						$default_language_code = $this->default_language;
						
						$datai = array( 			
							'title' => addslashes(trim($title)),	
							'short_description' => addslashes(trim($short_description)),	
							'description' => addslashes(trim($description)),		
							'price' => str_replace(',','',$price),
							'address' => addslashes(trim($address)),	
							'seo_meta_keywords' => addslashes(trim($seo_meta_keywords)),
							'seo_meta_description' => addslashes(trim($seo_meta_description)),
							'p_id' => $p_id,								
							'language' => $default_language_code
						);
						
						$datai['country'] = '';
						if(isset($country))
							$datai['country'] = $country;
						
						$datai['state'] = '';
						if(isset($state))
							$datai['state'] = $state;
						
						$datai['city'] = '';
						if(isset($city))
							$datai['city'] = $city;
						
						$datai['zip_code'] = '';
						if(isset($zip_code))
							$datai['zip_code'] = $zipcode;
						
						$datai['sub_area'] = '';
						if(isset($sub_area))
							$datai['sub_area'] = $sub_area;
						
						$this->Common_model->commonInsert('property_lang_details',$datai);
					}
					
					
					if(isset($property_meta))
					{
						foreach($property_meta as $meta_key=>$meta_val)
						{
							$datai = array( 
									'meta_key' => $meta_key,
									'meta_value' => $meta_val,
									'property_id' => $p_id,
									);
							$this->Common_model->commonInsert('property_meta',$datai);
						}
					}
					
					$user_type = $this->session->userdata('user_type');
					
					/*
					if($user_type != 'admin' && $status == 'publish')
					{
						$this->package_lib->add_credit_uses('post_property',$p_id,'property',$user_id);
						$this->package_lib->update_credits_by_user_id($user_id,'post_property_credit','minus_credit',1);
					}
					*/
					
					if($this->site_payments == 'Y'){
						if($user_type != 'admin'  && $status == 'publish')
						{
							$user_id = $this->session->userdata('user_id');
							$this->credit_id = $this->package_lib->get_credit_id_by_user_id($user_id, 'property', 'post_property' );
						
							
							$credit_used = $this->package_lib->check_credit_used('post_property',$p_id,'property');
							if(!$credit_used && $this->credit_id )
							{
								
							$this->package_lib->add_credit_uses('post_property',$p_id,'property',$user_id);
							$this->package_lib->update_credits_by_user_id($user_id,'post_property_credit','minus_credit',1);
							$this->package_lib->update_credits_updated_credit_for_user($this->credit_id);
							}
						}
					}
					
					
					if($user_email_template != '')
					{
						if($user_type != 'admin' )
						{
							$em_args = array();
							$em_args['property_id'] = $p_id;
							$this->global_lib->send_email_notifications_to_admin("property_submission_email_admin" , $em_args);
						}	
						
						
						if($status == 'pending')
						{
							/*$em_args['to_email'] = $user_email;
							$em_args['email_template'] = $user_email_template;	
							$this->email_lib->send_email_notification($em_args);*/
							
							$em_args = array();
							$em_args['property_id'] = $p_id;
							
							
							$this->global_lib->send_email_notifications_to_user( array($user_id), $user_email_template ,  $em_args);
						
						}
						
					}	
					
					
					
					
					$_SESSION['msg'] = '
								<div class="alert alert-success alert-dismissable" style="margin-top:10px; margin-bottom:10px;">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									'.$sess_msg.'
								</div>
								';
					
					if($status == 'N')
						redirect('/property/manage_inactive','location');	
					else 
						redirect('/property/manage','location');	
			
		}
		
		
		$data['property_types'] = $this->Common_model->commonQuery("SELECT * FROM `property_types` as prop 
									where prop.status = 'Y'
									order by prop.title ASC");	
		
		if($this->global_lib->get_option('property_amenities'))
		{
			$data['amenities_list'] = json_decode($this->global_lib->get_option('property_amenities'),true);
		}
		if($this->global_lib->get_option('property_distances'))
		{
			$data['distances_list'] = json_decode($this->global_lib->get_option('property_distances'),true);
		}
		$data['theme']=$theme;
		
		
		
		if($this->global_lib->get_option('property_custom_fields'))
		{
			$data['custom_field_list'] = json_decode($this->global_lib->get_option('property_custom_fields'),true);
		}
		
		$isPlugAct = $this->isPluginActive('document');
		if($isPlugAct == true)
		{
			$data['document_types'] = $this->Common_model->commonQuery("SELECT * FROM `property_doc_types` as prop order by prop.pdt_order ASC, prop.pdt_id DESC");
		}
		
		$data['user_list'] = $this->Common_model->commonQuery("SELECT user_type,user_id FROM `users` as u 
									where u.user_status = 'Y'
									order by u.user_name ASC");	
		
		$data['content'] = "$theme/property/add_new";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function edit($p_id = NULL)
	{	
	
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$data['size_units'] = $CI->config->item('size_units') ;
		
		if(isset($_POST['submit']) || isset($_POST['draft']) || isset($_POST['pending']))
		{
			$property_status = 'Public';
			extract($_POST);			
			
			$user_type = $this->session->userdata('user_type');
			
			if($this->site_payments == 'Y'){
				if(!$this->is_subscription_expires && $user_type != 'admin')
				{
					$_SESSION['msg'] = '
						<div class="alert alert-warning alert-dismissable" style="margin-top:10px; margin-bottom:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("You don't have sufficent subscription credits.").' 
						</div>
						';
					redirect('/property/manage','location');
				}
			}
				
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
				

				extract($_POST,EXTR_OVERWRITE);
				
				$status = 'draft';
				if(isset($_POST['submit']))
				{
					$status = 'publish';
				}
				else if(isset($_POST['draft']))
				{
					$status = 'draft';
				}
				else if(isset($_POST['pending']))
				{
					$status = 'pending';
				}
				
				
				
				$decId = $this->DecryptClientId($p_id);
				
				if(isset($multi_lang) && !empty($multi_lang))
				{
					$keys = array_keys( $multi_lang);
					$title = $multi_lang[$keys[0]]['title'];
				}
				
				$property_images = '';
				if(isset($addedImgFromMediaLibrary) && !empty($addedImgFromMediaLibrary))
				{
					$data_exp = explode(',',$addedImgFromMediaLibrary);
					$data_exp_array = array();
					foreach($data_exp as $k=>$v)
					{
						$data_exp_array[] = $this->DecryptClientId($v);
					}
					$property_images = implode(',',$data_exp_array);
				}
				
					if(isset($document_meta) && !empty($document_meta))
					{
						if(!isset($property_meta))
							$property_meta = array();
						
						foreach($document_meta as $dmk=>$dmv)
						{
							foreach($dmv as $dmkk=>$dmvv)
							{
								$data_exp = explode(',',$dmvv);
								$data_exp_array = array();
								foreach($data_exp as $k=>$v)
								{
									if(!empty($v))
										$data_exp_array[] = $this->DecryptClientId($v);
								}
								$property_meta[$dmkk][$dmk] = implode(',',$data_exp_array);
							}
						}
					}
					
					
					if(!isset($indoor_amenities))
						$indoor_amenities = array();
					if(!isset($outdoor_amenities))
						$outdoor_amenities = array();
					
					$distance_list_array = array();
					if(isset($distance_list) && !empty($distance_list))
					{
						foreach($distance_list as $dk=>$dv)
						{
							if( ($dv['distance'] != '' && $dv['distance'] > 0 ) && $dv['distance_text'] != '')
							{
								$distance_list_array[str_replace('_',' ',$dk)] = $dv;
							}
								
						}
					}
					
					$video_url_string = '';
					if(isset($video_url) && !empty($video_url))
					{
						$emptyRemoved = array_filter($video_url);
						$video_url_string = implode(',',$emptyRemoved);
					}
					
					$sub_area = '';
					$zipcode = '';
					if(!isset($state)) $state = '';
					
					if(isset($multi_lang) && !empty($multi_lang))
					{
						$keys = array_keys( $multi_lang);
						$description = $multi_lang[$keys[0]]['description'];
						$short_description = $multi_lang[$keys[0]]['short_description'];
						$price = $multi_lang[$keys[0]]['price'];
						$address = $multi_lang[$keys[0]]['address'];
						$seo_meta_keywords = $multi_lang[$keys[0]]['seo_meta_keywords'];
						$seo_meta_description = $multi_lang[$keys[0]]['seo_meta_description'];
						
						$isPlugAct = $this->isPluginActive('property_locations');
						if($isPlugAct == true)
						{
							$country = '';
							if(isset($multi_lang[$keys[0]]['country']) && !empty($multi_lang[$keys[0]]['country']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['country']);
								
								if(count($sExp) > 1)
									$country = $sExp[1];
								else
									$country = $sExp[0];
							}
							
							$state = '';
							if(isset($multi_lang[$keys[0]]['state']) && !empty($multi_lang[$keys[0]]['state']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['state']);
								if(count($sExp) > 1)
									$state = $sExp[1];
								else
									$state = $sExp[0];
							}
							
							$city = '';
							if(isset($multi_lang[$keys[0]]['city']) && !empty($multi_lang[$keys[0]]['city']))
							{
								$sExp = explode('~',$multi_lang[$keys[0]]['city']);
								if(count($sExp) > 1)
									$city = $sExp[3];
								else
									$city = $sExp[0];
							}
							$zipcode = '';
							if(isset($multi_lang[$keys[0]]['zipcode']) && !empty($multi_lang[$keys[0]]['zipcode']))
							{
								$zipcode = $multi_lang[$keys[0]]['zipcode'];
							}
							
							$sub_area = '';
							if(isset($multi_lang[$keys[0]]['sub_area']) && !empty($multi_lang[$keys[0]]['sub_area']))
							{
								$sub_area = $multi_lang[$keys[0]]['sub_area'];
							}
						}
						else
						{
							$multi_lang[$keys[0]]['state'] = $state;
							$multi_lang[$keys[0]]['city'] = $city;
							$multi_lang[$keys[0]]['zipcode'] = $zip_code;
							$zipcode = $zip_code;
						}
						
					}
					
					$description = addslashes($description);
					$short_description =  addslashes($short_description);
					
					if(!empty($size))
						$size .= "~".$size_measure; 
					
					if(!isset($lat))
						$lat = '';
					if(!isset($long))
						$long = '';
					
					$datai = array( 
									'title' => trim($title),
									'short_description' => trim($short_description),
									'description' => trim($description),
									'address' => $address,
									'street_address' => $street_address,
									'country' => $country,
									'state' => $state,
									'city' => $city,
									'zip_code' => $zipcode,
									'sub_area' => $sub_area,
									'lat' => $lat,
									'long' => $long,
									'price' => str_replace(',','',$price),
									'size' => $size,
									'property_type' => $this->DecryptClientId($property_type),
									'property_for' => strtolower($property_for),
									'bedroom' => $bedroom,
									'bathroom' => $bathroom,
									'garage' => $garage,
									'indoor_amenities' => json_encode($indoor_amenities),
									'outdoor_amenities' => json_encode($outdoor_amenities),
									'distance_list' => json_encode($distance_list_array),
									'video_urls' => $video_url_string,
									'property_images' => $property_images,
									'status' => $status,
									'slug' => $slug,
									'seo_meta_keywords' =>$seo_meta_keywords,
									'seo_meta_description' => $seo_meta_description,
									'created_by' => $this->global_lib->DecryptClientId($user_id),
									'property_status' => $property_status
									);
					
					
					if(isset($custom_fields) && count($custom_fields) > 0)
					{
						foreach($custom_fields as $k=>$v)
						{
							$datai[$k] = $v;
						}
						
					}
					
					if(isset($slug) && isset($old_slug) && !empty($slug) &&  $slug != $old_slug )
					{
						$config = array(
						'field' => 'slug',
						'title' => 'title',
						'table' => 'properties',
						'id' => 'p_id',
						);
						$this->load->library('Slug_lib', $config);
						
						$datap = array(
							'slug' => $slug,
						);
						$slug = $this->slug_lib->create_uri($datap);
						$datai['slug'] = $slug;
						
					}
				
				
				$this->Common_model->commonUpdate('properties',$datai,'p_id',$decId);
				
				
				if(isset($property_meta )){
					foreach($property_meta as $meta_key => $meta_value){
					
						if(is_array($meta_value))
						{
							$key = key($meta_value);
							
							if($key === 0)
							{
								
								$datai = array( 
									'meta_key' => $meta_key,
									'meta_value' => $meta_value[$key],
									'property_id' => $decId,
									);
								$this->Common_model->commonInsert('property_meta',$datai);
							
							
							}else{
								
								$this->Common_model->commonQuery("update property_meta set 
									meta_value = '".addslashes($meta_value[$key])."' 
									where property_id = $decId and meta_key = '$meta_key'");	

							}

						}		
					}
				}
				
				
				if(isset($multi_lang) && !empty($multi_lang))
				{
					foreach($multi_lang as $k=>$v)
					{
						
						if(isset( $v['property_delete']) && isset( $v['pld_id']) && !empty($v['pld_id']) && 
							( $v['property_delete'] == $v['pld_id'] ))
						{
							$this->Common_model->commonQuery("delete from property_lang_details
									where pld_id = ".$v['pld_id']);
							continue;		
						}
						
						
						if($v['title'] != '')
						{
							$property_lang_details = $this->Common_model->commonQuery("select * from property_lang_details
									where p_id = $decId and language = '$k' ");
							if($property_lang_details->num_rows() == 0)
							{
								
								$datai = array( 			
											'title' => addslashes(trim($v['title'])),									
											'description' => addslashes(trim($v['description'])),
											'short_description' => addslashes(trim($v['short_description'])),
											'seo_meta_keywords' => addslashes(trim($v['seo_meta_keywords'])),
											'seo_meta_description' => addslashes(trim($v['seo_meta_description'])),
											'p_id' => $decId,	
											'language' => $k,
											'price' => str_replace(',','',trim($v['price'])),
											'address' => addslashes(trim($v['address'])),
											);
								
								$datai['country'] = '';
								if(isset($v['country']))
									$datai['country'] = $v['country'];
								
								$datai['state'] = '';
								if(isset($v['state']))
									$datai['state'] = $v['state'];
								
								$datai['city'] = '';
								if(isset($v['city']))
									$datai['city'] = $v['city'];
								
								$datai['zip_code'] = '';
								if(isset($v['zipcode']))
									$datai['zip_code'] = $v['zipcode'];
								
								$datai['sub_area'] = '';
								if(isset($v['sub_area']))
									$datai['sub_area'] = $v['sub_area'];
								
								$this->Common_model->commonInsert('property_lang_details',$datai);
								
							}
							else
							{
								$this->Common_model->commonQuery("update property_lang_details set 
									  title = '".addslashes(trim($v['title']))."' 
									, short_description = '".addslashes(trim($v['short_description']))."'
									, description = '".addslashes(trim($v['description']))."'
									, price = '".str_replace(',','',trim($v['price']))."' 
									, address = '".addslashes(trim($v['address']))."'
									
									, country = '".addslashes(trim($v['country']))."'
									, state = '".addslashes(trim($v['state']))."'
									, city = '".addslashes(trim($v['city']))."'
									, zip_code = '".addslashes(trim($v['zipcode']))."'
									, sub_area = '".addslashes(trim($v['sub_area']))."'
									
									,seo_meta_keywords = '".addslashes(trim($v['seo_meta_keywords']))."' ,
									 seo_meta_description = '".addslashes(trim($v['seo_meta_description']))."' 
									where p_id = $decId and language = '$k'");
							}
						}
					}
				}
				else
				{
					$default_language_code = $this->default_language;
					
					$property_lang_details = $this->Common_model->commonQuery("select * from property_lang_details
							where p_id = $decId and language = '$default_language_code' ");
					if($property_lang_details->num_rows() == 0)
					{
						$datai = array( 			
									'title' => addslashes(trim($title)),	
									'short_description' => addslashes(trim($short_description)),			
									'description' => addslashes(trim($description)),	
									'seo_meta_keywords' => addslashes(trim($seo_meta_keywords)),
									'seo_meta_description' => addslashes(trim($seo_meta_description)),
									'p_id' => $decId,	
									'language' => $default_language_code,
									'price' => str_replace(',','',trim($price)),
									'address' => addslashes(trim($address)),	
									);
									
						$datai['country'] = '';
						if(isset($country))
							$datai['country'] = $country;
						
						$datai['state'] = '';
						if(isset($state))
							$datai['state'] = $state;
						
						$datai['city'] = '';
						if(isset($city))
							$datai['city'] = $city;
						
						$datai['zip_code'] = '';
						if(isset($zipcode))
							$datai['zip_code'] = $zipcode;
						
						$datai['sub_area'] = '';
						if(isset($sub_area))
							$datai['sub_area'] = $sub_area;
						
						$this->Common_model->commonInsert('property_lang_details',$datai);
					}
					else
					{
						$this->Common_model->commonQuery("update property_lang_details set 
							title = '".addslashes(trim($title))."' 
							, short_description = '".addslashes(trim($short_description))."' 
							, description = '".addslashes(trim($description))."' 
							, price = '".str_replace(',','',trim($price))."' 
							, address = '".addslashes(trim($address))."' 
							
							, country = '".addslashes(trim($v['country']))."'
							, state = '".addslashes(trim($v['state']))."'
							, city = '".addslashes(trim($v['city']))."'
							, zip_code = '".addslashes(trim($v['zipcode']))."'
							, sub_area = '".addslashes(trim($v['sub_area']))."'
							
							,seo_meta_keywords = '".addslashes(trim($seo_meta_keywords))."' 
							,seo_meta_description = '".addslashes(trim($seo_meta_description))."'
							where p_id = $decId and language = '$default_language_code'");
					}
				}
				
				$user_type = $this->session->userdata('user_type');
				
				if($this->site_payments == 'Y'){
					
					
				if(isset($current_status) && $current_status == 'pending' && $status == 'publish' && isset($prop_user_id))
				{
					$this->credit_id = $this->package_lib->get_credit_id_by_user_id($prop_user_id, 'property', 'post_property' );
					
					/*echo " here ". $prop_user_id; exit;*/
					$credit_used = $this->package_lib->check_credit_used('post_property',$decId,'property');
					if(!$credit_used && $this->credit_id )
					{
						$this->package_lib->add_credit_uses('post_property',$decId,'property',$prop_user_id);
						$this->package_lib->update_credits_by_user_id($prop_user_id,'post_property_credit','minus_credit',1);
						$this->package_lib->update_credits_updated_credit_for_user($this->credit_id); 
					}	
				}
				else 
					if($user_type != 'admin'  && $status == 'publish')
					{
						$user_id = $this->session->userdata('user_id');
						/*$credit_used = $this->package_lib->check_credit_used('post_property',$decId,'property');
						if(!$credit_used )
						{
							$this->package_lib->add_credit_uses('post_property',$decId,'property',$user_id);
							$this->package_lib->update_credits_by_user_id($user_id,'post_property_credit','minus_credit',1);
						}*/
						$this->credit_id = $this->package_lib->get_credit_id_by_user_id($user_id, 'property', 'post_property' );
					
						
						$credit_used = $this->package_lib->check_credit_used('post_property',$decId,'property');
						if(!$credit_used && $this->credit_id )
						{
							
						$this->package_lib->add_credit_uses('post_property',$decId,'property',$user_id);
						$this->package_lib->update_credits_by_user_id($client_id,'post_property_credit','minus_credit',1);
						$this->package_lib->update_credits_updated_credit_for_user($this->credit_id);
						}
					}
				}
						
				$_SESSION['msg'] = '
							<div class="alert alert-success alert-dismissable" style="margin-top:10px; margin-bottom:10px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.mlx_get_lang("Property Updated Successfully").'
							</div>
							';
				if($status == 'N')
					redirect('/property/manage_inactive','location');	
				else 
					redirect('/property/manage','location');		
			
		}
		
		if($this->global_lib->get_option('property_amenities'))
		{
			$data['amenities_list'] = json_decode($this->global_lib->get_option('property_amenities'),true);
		}
		if($this->global_lib->get_option('property_distances'))
		{
			$data['distances_list'] = json_decode($this->global_lib->get_option('property_distances'),true);
		}
		
		$decId = $this->DecryptClientId($p_id);
		$data['query'] = $prop_result = $this->Common_model->commonQuery("
				select *
				from properties 
				where p_id = $decId ");	
		
		$data['property_types'] = $this->Common_model->commonQuery("SELECT * FROM `property_types` as prop 
									where prop.status = 'Y'
									order by prop.title ASC");	
									
									
		$property_meta_res = $this->Common_model->commonQuery("select * from property_meta where property_id = $decId ");								
		$property_meta = array();
		foreach($property_meta_res->result() as $row){
			
			
			$property_meta [$row->meta_key] = array( 'meta_id' =>  $row->meta_id, 'meta_value' =>  $row->meta_value);
		}
		
		$data['user_list'] = $this->Common_model->commonQuery("SELECT user_type,user_id FROM `users` as u 
									where u.user_status = 'Y'
									order by u.user_name ASC");	
		
		
		if($this->global_lib->get_option('property_custom_fields'))
		{
			$data['custom_field_list'] = json_decode($this->global_lib->get_option('property_custom_fields'),true);
		}
		
		$isPlugAct = $this->isPluginActive('document');
		if($isPlugAct == true)
		{
			$data['document_types'] = $this->Common_model->commonQuery("SELECT * FROM `property_doc_types` as prop order by prop.pdt_order ASC, prop.pdt_id DESC");
		}
		
		
		$data['property_meta'] = $property_meta;
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/edit";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	
	public function custom_fields()
	{
		
		if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('global_lib');
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$data['user_type'] = $this->session->userdata('user_type');
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			extract($_POST);
			
			if(empty($user_id) || $user_id == 0)
			{	
				$_SESSION['msg'] = '<p class="error_msg">Session Expired.</p>';
				$_SESSION['logged_in'] = false;	
				$this->session->set_userdata('logged_in', false);
				redirect('/logins','location');
			}
			
			
			$custom_field_meta = array();
			if(isset($custom_field_name) && count($custom_field_name) > 0)
			{
				foreach($custom_field_name as $k=>$v)
				{
					if(!empty($v))
					{
						$slug = $custom_field_slug[$k];
						//$req = $custom_field_required[$k];
						$req = '';
						$inner_array = array('title' => trim($v),
											'slug' => $slug,
											'is_req' => $req
											);
						if(isset($custom_field_type) && isset($custom_field_type[$k]) && !empty($custom_field_type[$k]))
						{
							$inner_array['type'] = $custom_field_type[$k];
							if(isset($custom_field_option) && isset($custom_field_option[$k]))
							{
								$inner_array['options'] = $custom_field_option[$k];
							}
						}
					
						$custom_field_meta[] = $inner_array;
						
						$col_result = $this->Common_model->commonQuery("SHOW COLUMNS FROM properties LIKE '$slug'");
						if($col_result->num_rows() == 0)
						{
							$sql = "ALTER TABLE properties ADD  $slug varchar(255) DEFAULT ''";
							$this->Common_model->commonQuery($sql);							
						}
						
					}
				}
			}
			
			/*
			$custom_field_meta = array();
			if(count($custom_field_name) > 0)
			{
				foreach($custom_field_name as $k=>$v)
				{
					if(!empty($v))
					{
						$slug = $custom_field_slug[$k];
						$custom_field_meta[] = array('title' => trim($v),
													'slug' => $slug);
						$col_result = $this->Common_model->commonQuery("SHOW COLUMNS FROM properties LIKE '$slug'");
						if($col_result->num_rows() == 0)
						{
							$sql = "ALTER TABLE properties ADD  $slug varchar(255) DEFAULT ''";
							$this->Common_model->commonQuery($sql);							
						}
					}
				}
			}
			*/
			
			$this->global_lib->update_option('property_custom_fields',json_encode($custom_field_meta));
			
			$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" >
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							Custom Fields Updated Successfully.
						</div>
						';
			redirect('/property/custom_fields','location');	
				
		}
		
		if($this->global_lib->get_option('property_custom_fields'))
		{
			$data['custom_field_list'] = json_decode($this->global_lib->get_option('property_custom_fields'),true);
		}		
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/custom_fields";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	
	public function view($p_id = NULL)
	{
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		$data['currency_symbol'] = $this->global_lib->get_currency_symbol();
		
		if(isset($_POST['approve']) || isset($_POST['reject']))
		{
			
			extract($_POST);
			
			extract($_POST,EXTR_OVERWRITE);
			
			
			
			$decId = $this->DecryptClientId($p_id);
			
			$status = 'pending';
			$notif_text = '';
			$sess_msg = '';
			$prop_action = '';
			$user_email_template = '';
			if(isset($_POST['approve']))
			{
				$status = 'publish';
				$notif_text = 'Your request for property approval is accepted by Site Administrator.';
				$notif_icon = 'fa-check text-green';
				$sess_msg = mlx_get_lang("Property Approval Request Accepted Successfully");
				$prop_action = 'approve';
				$user_email_template = 'property_approve_email';
			}
			else if(isset($_POST['reject']))
			{
				$status = 'reject';
				$notif_text = 'Your request for property approval is reject by Site Administrator.';
				$notif_icon = 'fa-ban text-red';
				$sess_msg = mlx_get_lang("Property Approval Request Rejected");
				$prop_action = 'reject';
				
				$user_email_template = 'property_reject_email';
				
				$datai = array( 
						'meta_key' => 'reject_message',
						'meta_value' => $reject_message,
						'property_id' => $decId,
						);
				$this->Common_model->commonInsert('property_meta',$datai);
			}
			
			$datai = array( 'status' => trim($status) );
			$this->Common_model->commonUpdate('properties',$datai,'p_id',$decId);
			
			
			
			
			
			$prop_result = $this->Common_model->commonQuery("
				select prop.created_by from properties as prop where prop.p_id = $decId ");
			
			if($prop_result->num_rows() > 0)
			{
				$prop_row = $prop_result->row();
				$client_id = $prop_row->created_by;
				$user_id = $this->session->userdata('user_id');
				
				
				if(!empty($user_email_template))
				{
					$em_args = array();
					$em_args['property_id'] = $decId;
					$this->global_lib->send_email_notifications_to_user( array($client_id), $user_email_template ,  $em_args);
				}
				
				
				$datai = array( 'notif_status ' => 'R', 'prop_action' => 'complete');
				$this->Common_model->commonUpdate('notifications',$datai,'p_id',$decId);
				
				$datai = array( 
					'p_id' => $decId,
					'notif_text' => $notif_text,
					'notif_icon' => $notif_icon,
					'notif_by' => $user_id,
					'notif_for' => $client_id,
					'notif_on' => time(),
					'notif_status' => 'U',
					'prop_action' => $prop_action
				);
				$this->Common_model->commonInsert('notifications',$datai);
				
				$user_type = $this->session->userdata('user_type');
			
				if($this->site_payments == 'Y'){
				if($user_type == 'admin'  && $status == 'publish')
				{
				$this->credit_id = $this->package_lib->get_credit_id_by_user_id($client_id, 'property', 'post_property' );
				
				$user_id = $this->session->userdata('user_id');
				$credit_used = $this->package_lib->check_credit_used('post_property',$decId,'property');
				if(!$credit_used && $this->credit_id )
				{
					
				$this->package_lib->add_credit_uses('post_property',$decId,'property',$user_id);
				$this->package_lib->update_credits_by_user_id($client_id,'post_property_credit','minus_credit',1);
				$this->package_lib->update_credits_updated_credit_for_user($this->credit_id);
				}
				
				}
				}
			
				
			}
			
			
			$_SESSION['msg'] = '
					<div class="alert alert-success alert-dismissable" style="margin-top:10px; margin-bottom:0px;">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						'.$sess_msg.'
					</div>';
			redirect('/property/manage','location');	
			
		}
		
		$decId = $this->DecryptClientId($p_id);
		$prop_result = $this->Common_model->commonQuery("
				select prop.*,pt.title as prop_type_title, u.user_email, u.user_type from properties  as prop 
			    left join users as u on u.user_id = prop.created_by and u.user_status = 'Y'
			    inner join property_types as pt on pt.pt_id = prop.property_type
				where prop.p_id = $decId ");	
		
		
		if($prop_result->num_rows() == 0)
		{
			$_SESSION['msg'] = '
						<div class="alert alert-danger alert-dismissable" >
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Invalid Property ID").'
						</div>
						';
			redirect('/property/manage','location');
		}
		else
		{
			$data['single_prop'] = $prop_result->row();
		}
		
		$isPlugAct = $this->isPluginActive('document');
		if($isPlugAct == true)
		{
			$data['document_types'] = $this->Common_model->commonQuery("SELECT * FROM `property_doc_types` as prop order by prop.pdt_order ASC, prop.pdt_id DESC");
		}
		
		$data['property_meta'] = $this->global_lib->get_property_metadata($decId);
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/view";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	
	
	public function amenities()
	{
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$data['user_type'] = $this->session->userdata('user_type');
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			
			extract($_POST);
			
			if(empty($user_id) || $user_id == 0)
			{	
				$_SESSION['msg'] = '<p class="error_msg">'.mlx_get_lang("Session Expired").'</p>';
				$_SESSION['logged_in'] = false;	
				$this->session->set_userdata('logged_in', false);
				redirect('/logins','location');
			}
			
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
			
			if(isset($amenities) && !empty($amenities))
			{
				
				if(isset($amenities['indoor_amenities']) && !empty($amenities['indoor_amenities']))
				{
					foreach($amenities['indoor_amenities'] as $k=>$v)
					{
						
						$amenities['indoor_amenities'][$k] = $this->language_lib->get_normal_string($v);
					}
				}
				if(isset($amenities['outdoor_amenities']) && !empty($amenities['outdoor_amenities']))
				{
					foreach($amenities['outdoor_amenities'] as $k=>$v)
					{
						$amenities['outdoor_amenities'][$k] = $this->language_lib->get_normal_string($v);
					}
				}
			}
			
			$this->global_lib->update_option('property_amenities',json_encode($amenities));
			
			$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Amenities Updated Successfully").'
						</div>
						';
			redirect('/property/amenities','location');	
				
		}
		
		if($this->global_lib->get_option('property_amenities'))
		{
			$data['amenities_list'] = json_decode($this->global_lib->get_option('property_amenities'),true);
		}		
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/amenities";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function distances()
	{
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		$this->load->library('Global_lib');
		$data = $this->global_lib->uri_check();
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$data['user_type'] = $this->session->userdata('user_type');
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			extract($_POST);
			if(empty($user_id) || $user_id == 0)
			{	
				$_SESSION['msg'] = '<p class="error_msg">'.mlx_get_lang("Session Expired").'</p>';
				$_SESSION['logged_in'] = false;	
				$this->session->set_userdata('logged_in', false);
				redirect('/logins','location');
			}
			
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
			
			
			$this->global_lib->update_option('property_distances',json_encode($distances));
			
			$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Distances Updated Successfully").'
						</div>';
			redirect('/property/distances','location');	
		}
		
		if($this->global_lib->get_option('property_distances'))
		{
			$data['distances_list'] = json_decode($this->global_lib->get_option('property_distances'),true);
		}		
		
		$data['theme']=$theme;
		$data['content'] = "$theme/property/distances";
		$this->load->view("$theme/header",$data);
	}
	
	
	public function prop_type($pt_id = null)
	{
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$data['user_type'] = $this->session->userdata('user_type');
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			
			extract($_POST);
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
				
			$user_id = $this->session->userdata('user_id');
			
			$meta_options = array();
			if(isset($adv_search_options))
				$meta_options ['adv_search_options'] = $adv_search_options; 
				
			//print_r($meta_options); exit;	
			
			if(isset($property_type_id) && !empty($property_type_id))
			{
				$decId = $this->DecryptClientId($property_type_id);
				
				/*$slug = $this->global_lib->get_slug(trim($property_type_title));
				$qry = "select slug from property_types where slug like '%".$slug."%' 
						and pt_id != $decId 
						order by pt_id DESC limit 1";
				
				$result = $this->Common_model->commonQuery($qry);
				if($result->num_rows() > 0)
				{
						$row = $result->row();
						$old_slug = $row->slug;
						$slug_explode = explode('-',$old_slug);
						if(is_numeric($slug_explode[(count($slug_explode)-1)]))
						{
							$slug_count = $slug_explode[(count($slug_explode)-1)];
							$slug_count++;
						}
						else
							$slug_count = 1;
						$slug = $slug.'-'.$slug_count;
				}*/
				
				$config = array( 'field' => 'slug', 'title' => 'title', 'table' => 'property_types', 
				'id' => 'pt_id');
				$this->load->library('Slug_lib', $config);
				
				$property_type_title_for_slug = $this->language_lib->get_normal_string($property_type_title);
				
				$datap = array( 'title' => $property_type_title_for_slug, );
				$slug = $this->slug_lib->create_uri($datap);
				
				$datai = array( 
							'title' => trim($property_type_title),
							'slug' => $slug,
							'img_url' => $img_url,
							'meta_options' => json_encode($meta_options),
							'status' => $status,
							);
				
				$this->Common_model->commonUpdate('property_types',$datai,'pt_id',$decId);
				$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Type Updated Successfully").'
						</div>
						';
			}
			else
			{
				/*$slug = $this->global_lib->get_slug(trim($property_type_title));
				$qry = "select slug from property_types where slug like '%".$slug."%' order by pt_id DESC limit 1";
				$result = $this->Common_model->commonQuery($qry);
				if($result->num_rows() > 0)
				{
						$row = $result->row();
						$old_slug = $row->slug;
						$slug_explode = explode('-',$old_slug);
						if(is_numeric($slug_explode[(count($slug_explode)-1)]))
						{
							$slug_count = $slug_explode[(count($slug_explode)-1)];
							$slug_count++;
						}
						else
							$slug_count = 1;
						$slug = $slug.'-'.$slug_count;
				}*/
				
				$config = array( 'field' => 'slug', 'title' => 'title', 'table' => 'property_types', 
				'id' => 'pt_id');
				$this->load->library('Slug_lib', $config);
				
				$property_type_title_for_slug = $this->language_lib->get_normal_string($property_type_title);
				
				$datap = array( 'title' => $property_type_title_for_slug, );
				$slug = $this->slug_lib->create_uri($datap);
				
				
				$meta_options = array();
				if(isset($adv_search_options))
					$meta_options ['adv_search_options'] = $adv_search_options; 
				
				$datai = array( 
							'title' => trim($property_type_title),
							'slug' => $slug,
							'img_url' => $img_url,
							'meta_options' => json_encode($meta_options),
							'created_on' => time(),
							'status' => $status,
							'created_by' => $user_id,
							);
				$pt_id = $this->Common_model->commonInsert('property_types',$datai);
				/***
				ALTER TABLE `property_types` ADD `meta_options` TEXT NOT NULL AFTER `img_url`;
				**/
				
				
				$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable"  style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Type Added Successfully").'
						</div>
						';
			}
			
			redirect('/property/prop_type','location');	
				
		}
		
		$data['query'] = $this->Common_model->commonQuery("
						SELECT * FROM `property_types` as prop 
						order by prop.pt_id DESC");		
		
		if($pt_id != null)
		{
			$decId = $this->DecryptClientId($pt_id);
			$pt_result = $this->Common_model->commonQuery("select * from property_types where pt_id = $decId ");
			if($pt_result->num_rows() > 0)
			{
				$pt_row = $pt_result->row();
				$data['property_type_id'] = $pt_row->pt_id;
				$data['property_type_title'] = $pt_row->title;
				$data['property_type_img'] = $pt_row->img_url;
				$data['status'] = $pt_row->status;
				$data['meta_options'] = $pt_row->meta_options;
			}
		}
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/prop_type";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function doc_type($pdt_id = null)
	{
		$isPlugAct = $this->isPluginActive('document');
		if($isPlugAct != true)
		{
			redirect('/property/manage','location');
		}
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		$data['user_type'] = $this->session->userdata('user_type');
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			
			extract($_POST);
			foreach($_POST as $k=>$v)
			{
				$_POST[$k] = $this->security->xss_clean($v);
				$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
			}
			
			
			
			if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
			{
				foreach($_POST['multi_lang'] as $mk=>$mv)
				{
					foreach($mv as $mvk=>$mvv)
					{
						$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
					}
				}
			}
				
			$user_id = $this->session->userdata('user_id');
			
			
			if(isset($doc_type_id) && !empty($doc_type_id))
			{
				$decId = $this->DecryptClientId($doc_type_id);
				
				$pt_result = $this->Common_model->commonQuery("select slug from property_doc_types where pdt_id = $decId ");
				if($pt_result->num_rows() > 0)
				{
					$pt_row = $pt_result->row();
					$old_slug = $pt_row->slug;
				}
				
				$config = array( 'field' => 'slug', 'title' => 'title', 'table' => 'property_doc_types', 
				'id' => 'pdt_id');
				$this->load->library('Slug_lib', $config);
				
				$slug = $this->language_lib->get_normal_string($title);
				
				$datap = array( 'title' => $slug, );
				$slug = $this->slug_lib->create_uri($datap);
				
				$datai = array( 
							'title' => trim($title),
							'description' => trim($description),
							'is_required' => $is_required,
							'error_message' => trim($error_message),
							'slug' => $slug,
							'status' => $status,
							'pdt_order' => $pdt_order,
							);
				
				$this->Common_model->commonUpdate('property_doc_types',$datai,'pdt_id',$decId);
				
				
				$this->Common_model->commonQuery("UPDATE property_meta 
								SET meta_key = replace(meta_key, '$old_slug-ids', '$slug-ids')");
				
				$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Document Type Updated Successfully").'
						</div>
						';
			}
			else
			{
				
				$config = array( 'field' => 'slug', 'title' => 'title', 'table' => 'property_doc_types', 
				'id' => 'pdt_id');
				$this->load->library('Slug_lib', $config);
				
				$slug = $this->language_lib->get_normal_string($title);
				
				$datap = array( 'title' => $slug, );
				$slug = $this->slug_lib->create_uri($datap);
				
				
				$datai = array( 
							'title' => trim($title),
							'description' => trim($description),
							'is_required' => $is_required,
							'error_message' => trim($error_message),
							'slug' => $slug,
							'created_on' => time(),
							'status' => $status,
							'created_by' => $user_id,
							'pdt_order' => $pdt_order,
							);
				$pdt_id = $this->Common_model->commonInsert('property_doc_types',$datai);
				
				$_SESSION['msg'] = '
						<div class="alert alert-success alert-dismissable"  style="margin-bottom:0px; margin-top:10px;">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
							'.mlx_get_lang("Property Document Type Added Successfully").'
						</div>
						';
			}
			
			redirect('/property/doc_type','location');	
				
		}
		
		$data['query'] = $this->Common_model->commonQuery("
						SELECT * FROM `property_doc_types` as prop 
						order by prop.pdt_id DESC");		
		
		if($pdt_id != null)
		{
			$decId = $this->DecryptClientId($pdt_id);
			$pt_result = $this->Common_model->commonQuery("select * from property_doc_types where pdt_id = $decId ");
			if($pt_result->num_rows() > 0)
			{
				$pt_row = $pt_result->row();
				$data['doc_type_id'] = $pt_row->pdt_id;
				$data['title'] = $pt_row->title;
				$data['description'] = $pt_row->description;
				$data['is_required'] = $pt_row->is_required;
				$data['error_message'] = $pt_row->error_message;
				$data['status'] = $pt_row->status;
			}
		}
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/property/prop_doc_type";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function delete($rowid)
	{
		$CI =& get_instance();
		$this->load->library('Global_lib');
		if(!is_array($rowid))
			$rowid	= $this->global_lib->DecryptClientId($rowid);
		$this->load->model('Common_model');
			
		$tbl='properties';
		$pid='p_id';
		$url='/property/manage/';	 	
		$fld=mlx_get_lang("Property");
		
		$enable_property_soft_delete = $this->global_lib->get_option('enable_property_soft_delete');
		
		if(isset($enable_property_soft_delete) && $enable_property_soft_delete == 'Y')
		{
			$datai = array('deleted' => 'Y');
			$this->Common_model->commonUpdate($tbl,$datai,$pid,$rowid);
		}
		else
		{
			$this->Common_model->commonDelete('notifications',$rowid,'p_id' );
			$this->Common_model->commonDelete('property_lang_details',$rowid,'p_id' );
			$this->Common_model->commonDelete('property_meta',$rowid,'property_id' );
			
			$rows= $this->Common_model->commonDelete($tbl,$rowid,$pid );			
		}
		
		$rows = 1;
		
		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-bottom:0px;margin-top:10px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.$rows.' '.$fld.' '.mlx_get_lang("Deleted Successfully").'
							</div>
							';
		redirect($url,'location','301');	
	}
	
	public function delete_type($rowid)
	{
		
		$CI =& get_instance();
		$this->load->library('Global_lib');
		if(!is_array($rowid))
			$rowid	= $this->global_lib->DecryptClientId($rowid);
		$this->load->model('Common_model');
			
		$tbl='property_types';
		$pid='pt_id';
		$url='/property/prop_type/';	 	
		$fld=mlx_get_lang("Property");
		
		$pt_result = $this->Common_model->commonQuery("select img_url from property_types where pt_id = $rowid ");	
		if($pt_result->num_rows() > 0)
		{
			$pt_row = $pt_result->row();
			$photo_name = $pt_row->img_url;
			if(isset($photo_name) && !empty($photo_name) && file_exists('../uploads/prop_type/'.$photo_name))
				unlink('../uploads/prop_type/'.$photo_name);
		}
		
		$rows = $this->Common_model->commonDelete($tbl,$rowid,$pid );	
		
		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" >
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.$rows.' '.$fld.' '.mlx_get_lang("Deleted Successfully").'
							</div>
							';
		redirect($url,'location','301');	
	}
	
	public function delete_doc_type($rowid)
	{
		
		$CI =& get_instance();
		$this->load->library('Global_lib');
		if(!is_array($rowid))
			$rowid	= $this->global_lib->DecryptClientId($rowid);
		$this->load->model('Common_model');
			
		$tbl='property_doc_types';
		$pid='pdt_id';
		$url='/property/doc_type/';	 	
		$fld=mlx_get_lang("Documnent Type");
		
		
		$rows = $this->Common_model->commonDelete($tbl,$rowid,$pid );	
		
		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" >
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.$rows.' '.$fld.' '.mlx_get_lang("Deleted Successfully").'
							</div>
							';
		redirect($url,'location','301');	
	}
	
	public function delete_cf($slug = null)
	{
		
		$CI =& get_instance();
		$this->load->model('Common_model');
		$this->load->library('global_lib');
		if($this->global_lib->get_option('property_custom_fields'))
		{
			$custom_field_list = json_decode($this->global_lib->get_option('property_custom_fields'),true);
			if(isset($custom_field_list) && !empty($custom_field_list)) { 
				foreach($custom_field_list as $cfk => $cfv)
				{
					if($cfv['slug'] == $slug)
					{
						unset($custom_field_list[$cfk]);
					}
				}
			}
			$this->global_lib->update_option('property_custom_fields',json_encode($custom_field_list));
			
			$this->Common_model->commonQuery("ALTER TABLE properties DROP $slug");
		}
		
		$url='/property/custom_fields/';	 	
		$fld='Custom Field';
					
		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.$fld.' Deleted Successfully.
							</div>
							';
		redirect($url,'location','301');	
	}
	
	
	public function print_sql()
	{
		$CI =& get_instance();		
		$this->load->model('Common_model');
		$this->load->library('Global_lib');
		
		
		
		/*$qry = "ALTER TABLE `properties` CHANGE `bedroom` `bedroom` TEXT NOT NULL;";
		$result = $this->Common_model->commonQuery($qry);
		
		$qry = "ALTER TABLE `properties` CHANGE `bathroom` `bathroom` TEXT NOT NULL;";
		$result = $this->Common_model->commonQuery($qry);
		
		
		
		$qry = "ALTER TABLE `property_lang_details` ADD `address` MEDIUMTEXT NOT NULL AFTER `price`;";
		
				$result = $this->Common_model->commonQuery($qry);
				
		$qry = "		ALTER TABLE `property_lang_details` ADD `country` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `address`, ADD `state` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `country`, ADD `city` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `state`, ADD `zip_code` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `city`, ADD `sub_area` MEDIUMTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `zip_code`;";
				
		$result = $this->Common_model->commonQuery($qry);
		$qry = "ALTER TABLE `properties` ADD `sub_area` VARCHAR(255) NOT NULL AFTER `zip_code`;
		
		";
		

		
		$result = $this->Common_model->commonQuery($qry);*/
		
		
	}
	
	
	
}
