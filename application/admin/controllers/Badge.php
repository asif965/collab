<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Badge extends MY_Controller {	
	
	function __construct() {
        parent::__construct();
        if(!$this->isLogin())
		{
			
			redirect('/logins','location');
		}
		
		if(!$this->has_method_access())
		{
			redirect('/main/','location');
		}
    }
	
	public function index()
	{
		$this->manage();
	}
	
	
	public function manage()
	{
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		
		$data['query'] = $this->Common_model->commonQuery("
				select * from badges 
				order by badge_id DESC
				");	
		
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/badge/manage";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function add_new()
	{
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			extract($_POST);
			
			
			
				foreach($_POST as $k=>$v)
				{
					$_POST[$k] = $this->security->xss_clean($v);
					$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
				}
				
				if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
				{
					foreach($_POST['multi_lang'] as $mk=>$mv)
					{
						foreach($mv as $mvk=>$mvv)
						{
							$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
						}
					}
				}
				extract($_POST,EXTR_OVERWRITE);
				 
				 
				if(empty($user_id) || $user_id == 0)
				{	
					$_SESSION['msg'] = '<p class="error_msg">'.mlx_get_lang("Session Expired").'</p>';
					$_SESSION['logged_in'] = false;	
					$this->session->set_userdata('logged_in', false);
					redirect('/logins','location');
				}
				$cur_time = time();
				$datai = array( 
								'title' => $b_title,	
								'image' => $b_image,
								'short_description' => $short_description,
								'created_by' => $user_id,
								'created_at' => $cur_time,	
								'updated_at' => $cur_time,
								'status' => $b_status,
								); 
				$banner_id = $this->Common_model->commonInsert('badges',$datai);
				
				$_SESSION['msg'] = '
							<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.mlx_get_lang("Badge Added Successfully").'
							</div>
							';
				redirect('/badge/manage','location');	
			
		}
		
		$data['static_pages'] = $CI->config->item('static_pages') ;
		$data['property_list'] = $this->Common_model->commonQuery("
				select prop.title,prop.p_id from properties as prop
				where prop.status = 'Y' and ( prop.cur_status != 'reject')
				order by prop.p_id DESC
				");	
		$data['page_list'] = $this->Common_model->commonQuery("select p1.page_title,p1.page_id from pages p1 order by p1.page_title ASC");
		
		$data['theme']=$theme;
		
		$data['content'] = "$theme/badge/add_new";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	public function edit($c_id = NULL)
	{
		
		
		$CI =& get_instance();
		$theme = $CI->config->item('theme') ;
		
		$this->load->library('Global_lib');
		
		$data = $this->global_lib->uri_check();
		
		$data['myHelpers']=$this;
		$this->load->model('Common_model');
		$this->load->helper('text');
		
		
		if(isset($_POST['submit']) || isset($_POST['draft']))
		{
			extract($_POST);
			
				foreach($_POST as $k=>$v)
				{
					$_POST[$k] = $this->security->xss_clean($v);
					$_POST[$k] = str_replace('[removed]','',$_POST[$k]);
				}
				
				if(isset($_POST['multi_lang']) && !empty($_POST['multi_lang']))
				{
					foreach($_POST['multi_lang'] as $mk=>$mv)
					{
						foreach($mv as $mvk=>$mvv)
						{
							$_POST['multi_lang'][$mk][$mvk] = str_replace('[removed]','',$mvv);
						}
					}
				}
				
				extract($_POST,EXTR_OVERWRITE);
				
				
				$cId = $this->global_lib->DecryptClientId($b_id);
				
				$datai = array( 
								'title' => $b_title,	
								'image' => $b_image,
								'short_description' => $short_description,
								'status' => $b_status,
								); 
				$this->Common_model->commonUpdate('badges',$datai,'badge_id',$cId);
					
				$_SESSION['msg'] = '
							<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.mlx_get_lang("Badge Updated Successfully").'
						  </div>
							';
				redirect('/badge/manage','location');	
		
		}
		
		$data['b_id'] = $c_id;
		$decId = $this->global_lib->DecryptClientId($c_id);
		$data['query'] = $this->Common_model->commonQuery("
				select * from badges where badge_id = $decId");	
		
		$data['static_pages'] = $CI->config->item('static_pages') ;
		$data['property_list'] = $this->Common_model->commonQuery("
				select prop.title,prop.p_id from properties as prop
				where prop.status = 'Y' and ( prop.cur_status != 'reject')
				order by prop.p_id DESC
				");	
		$data['page_list'] = $this->Common_model->commonQuery("select p1.page_title,p1.page_id from pages p1 order by p1.page_title ASC");
		
		$data['theme']=$theme;
		
		$data['banner_for_lang_res'] = $this->Common_model->commonQuery("select * from banner_assigned_to where banner_id = $decId group by for_lang");
		
		$data['content'] = "$theme/badge/edit";
		
		$this->load->view("$theme/header",$data);
		
	}
	
	
	
	public function delete($rowid)
	{
		
		$CI =& get_instance();
		$this->load->library('Global_lib');
		if(!is_array($rowid))
			$rowid	= $this->global_lib->DecryptClientId($rowid);
		$this->load->model('Common_model');
			
		$tbl='badges';
		$pid='badge_id';
		$url='/badge/manage/';	 	
		$fld= mlx_get_lang("Badge");
		
		$result = $this->Common_model->commonQuery("select image from badges where badge_id = $rowid and image != ''");
		if($result->num_rows() > 0)
		{
			$img_row = $result->row();
			$photo_name = $img_row->image;
			if(isset($photo_name) && !empty($photo_name) && file_exists('../uploads/badge/'.$photo_name))
				unlink('../uploads/badge/'.$photo_name);
		}
		
		$rows= $this->Common_model->commonDelete($tbl,$rowid,$pid );			
		$_SESSION['msg'] = '<div class="alert alert-success alert-dismissable" style="margin-bottom:0px; margin-top:10px;">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
								'.$rows.' '.$fld.' '.mlx_get_lang("Deleted Successfully").'
							</div>
							';
		redirect($url,'location','301');	
	}
	
	
	
}
