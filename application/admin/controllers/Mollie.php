<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




class Mollie extends MY_Controller {
	
	var $api_key ;
	
	function __construct() {
        parent::__construct();
        if(!$this->isLogin())
		{
			redirect('/logins','location');
		}
		$CI =& get_instance();		
		$this->load->library('Global_lib');	
		
		$isPlugAct = $this->isPluginActive('payment');
		if($isPlugAct != true)
		{
			redirect('/main','location');
		}
		
		
		
		
		
		/* Mollie code*/
		$CI =& get_instance();	
		
		$payment_methods = $CI->global_lib->get_option('payment_methods');
		$payment_method = json_decode($payment_methods);
		/*echo "<pre>";print_r($payment_method );*/
		if(!empty($payment_method) && $payment_method->payment_method_mollie_section->is_enable == 'Y'){
			
			if(!empty($payment_method->payment_method_mollie_section->mollie_api_key) ){
				
				
					$this->api_key = $payment_method->payment_method_mollie_section->mollie_api_key;
					
			}
		
		}
		
		
		
		include APPPATH.'third_party/mollie/src/autoload.php';
		//include APPPATH.'third_party/mollie/src/MollieApiClient.php';
		//include APPPATH.'third_party/mollie/src/CompatibilityChecker.php';
		
		
		
    }
	
	public function index()	
	{		
		//print_r($_REQUEST);exit;
		if(isset($_POST['submit'])){
		
			$CI =& get_instance();
			$this->load->library('Package_lib');
			$this->load->model('Common_model');	
			
			$mollie = new \Mollie\Api\MollieApiClient();
			$mollie->setApiKey($this->api_key);
			
			extract($_POST);
			
			//echo floatval($_POST['item_price']);
			$amount = number_format((float)$_POST['item_price'], 2, '.', '');
			
			$orderId = $transaction_key = $this->package_lib->getToken(16);
			
			$payment = $mollie->payments->create([
				"amount" => [
					"currency" => $_POST['item_currency'],
					"value" => $amount
				],
				"description" => $_POST['item_name'],
				"redirectUrl" => base_url()."mollie/payment_update/?order_id=$orderId",
				/*"webhookUrl"  => base_url()."mollie/wh_payment_process/",*/
				"metadata" => [
					"order_id" => $orderId,
				],
			]);
			
			$package_id = $_POST['item_id'];
			$_SESSION['mollie_payment_id'] = $payment->id;
			$payment_method = "mollie";
			$package_details = $this->Common_model->commonQuery("select * from packages Where package_id='$package_id'");

			$transaction_amount = $package_details->row()->package_price;

			$details =[];
			foreach ($package_details->result() as $package_detail) {
				foreach ($package_detail as $key => $value) {
					$details[$key] = $value;
				}
			}
			$package_json_obj = json_encode($details);
			
			$user_id = $this->session->userdata('user_id');
			
			$tansaction_status =  $payment->status;
			
			$cur_time = time();
				$datai = array( 			
						'transaction_key' =>  $transaction_key,
						'packages_id' =>$package_id,
						'package_detail' => $package_json_obj,							
						'user_id' => $user_id,
						'payment_mode' => $payment_method,								
						'transaction_amount' => $transaction_amount,
						'transaction_date'=>$cur_time,
						'status'=>$tansaction_status
					);
				$transaction_id = $this->Common_model->commonInsert('transaction',$datai);
			
			$_SESSION['transaction_id'] = $transaction_id;
			$_SESSION['order_id'] = $orderId;
		
		//print_r($_POST); 
		
		header("Location: " . $payment->getCheckoutUrl(), true, 303);
		}else{
			redirect('/main/','location');
		}
	}
	
	public function payment_update(){
		
		/*print_r($_SESSION);	
		print_r($_GET);*/
		
		$mollie = new \Mollie\Api\MollieApiClient();
			$mollie->setApiKey($this->api_key);
		
		if(isset($_SESSION['mollie_payment_id']))
			$payment_id = $_SESSION['mollie_payment_id'];
		else
			$payment_id = 'unknown';
		
		if(isset($_SESSION['transaction_id']))
			$transaction_id = $_SESSION['transaction_id'];
		else
			$transaction_id = '0';
		
		if(isset($_SESSION['package_id']))
			$package_id = $_SESSION['package_id'];
		else
			$package_id = '0';
		
		if(isset($_SESSION['order_id']))
			$order_id = $_SESSION['order_id'];
		else
			$order_id = '0';
		
		
		
		$user_id = $this->session->userdata('user_id');
			
		$payment = $mollie->payments->get($payment_id);

		if ($payment->isPaid()&& !$payment->hasRefunds() && !$payment->hasChargebacks())
		{
			$this->load->library('Package_lib');
			/*echo "Payment received.";*/
			$CI =& get_instance();
			$CI->load->model('Common_model');
			
			$payment_details = array("payment_id"=> $payment_id,
										"order_id"=> $order_id,
										"package_id"=> $package_id,
										"status"=> "PAID");
			
			
			$transaction_meta = array();
			/*$transaction_meta['response'] = $rp_response;*/
			$transaction_meta['payment_details'] = $payment_details;
			$transaction_meta = json_encode($transaction_meta);
			
			$datai = array( 			
					'status' => 'Completed',
					'transaction_meta' => $transaction_meta/**/
					
				);
				$CI->Common_model->commonUpdate('transaction',$datai,'transaction_id',$transaction_id);
			
			$args = array();
			$args['payment_id'] = $payment_id;
			$args['transaction_id'] = $transaction_id;
			$args['user_id'] = $user_id;
			$args['package_id'] = $package_id;
			
			$args['trans_type'] = 'mollie';
			$args['transaction_type'] = 'Paid Via Mollie';
			$args['redirect'] = '/packages/payment_success/'; 
			/*$args['payment_id'] = $payment_id;
			$args['payment_id'] = $payment_id;*/
			
			
			$this->package_lib->payment_paid_action($args);
		}else{ echo "Payment error.";}	
		
		
	}	
	
		
	
	public function wh_payment_process(){
		
		
		$mollie = new \Mollie\Api\MollieApiClient();
		$mollie->setApiKey($this->api_key);
		
		
		try {
			/*
			 * Initialize the Mollie API library with your API key.
			 *
			 * See: https://www.mollie.com/dashboard/developers/api-keys
			 */
			//require "../initialize.php";

			/*
			 * Retrieve the payment's current state.
			 */
			$payment = $mollie->payments->get($_POST["id"]);
			$orderId = $payment->metadata->order_id;

			/*
			 * Update the order in the database.
			 */
			//database_write($orderId, $payment->status);

			$_SESSION['orderId'] = $orderId;
			$_SESSION['paymentId'] = $payment;
			
			if ($payment->isPaid() && !$payment->hasRefunds() && !$payment->hasChargebacks()) {
				/*
				 * The payment is paid and isn't refunded or charged back.
				 * At this point you'd probably want to start the process of delivering the product to the customer.
				 */
				 $_SESSION['status'] = 'PAID';
			} elseif ($payment->isOpen()) {
				/*
				 * The payment is open.
				 */
			} elseif ($payment->isPending()) {
				/*
				 * The payment is pending.
				 */
			} elseif ($payment->isFailed()) {
				/*
				 * The payment has failed.
				 */
			} elseif ($payment->isExpired()) {
				/*
				 * The payment is expired.
				 */
			} elseif ($payment->isCanceled()) {
				/*
				 * The payment has been canceled.
				 */
			} elseif ($payment->hasRefunds()) {
				/*
				 * The payment has been (partially) refunded.
				 * The status of the payment is still "paid"
				 */
			} elseif ($payment->hasChargebacks()) {
				/*
				 * The payment has been (partially) charged back.
				 * The status of the payment is still "paid"
				 */
			}
		} catch (\Mollie\Api\Exceptions\ApiException $e) {
			echo "API call failed: " . htmlspecialchars($e->getMessage());
		}
		
		
	}	
	
		
}
