<?php 

$lang['parana'] = 'Paraná';
$lang['belem-do-brejo-do-cruz'] = 'Belém do Brejo do Cruz';
$lang['baia-da-traicao'] = 'Baía da Traição';
$lang['paraiba'] = 'Paraíba';
$lang['aragoiania'] = 'Aragoiânia';
$lang['amorinopolis'] = 'Amorinópolis';
$lang['acreuna'] = 'Acreúna';
$lang['goias'] = 'Goiás';
$lang['pracuuba'] = 'Pracuúba';
$lang['macapa'] = 'Macapá';
$lang['calcoene'] = 'Calçoene';
$lang['amapa'] = 'Amapá';
$lang['ajman-city'] = 'Ajman City';
$lang['ajman-emirate'] = 'Ajman Emirate';
$lang['muzayri-'] = 'Muzayri‘';
$lang['al-ain-city'] = 'Al Ain City';
$lang['abu-dhabi-emirate'] = 'Abu Dhabi Emirate';
$lang['united-arab-emirates'] = 'United Arab Emirates';
$lang['apache-county'] = 'Apache County';
$lang['anchor-point'] = 'Anchor Point';
$lang['alexander-city'] = 'Alexander City';
$lang['united-states'] = 'United States';
$lang['no-state'] = 'No State';
$lang['uttar-pradesh'] = 'Uttar Pradesh';
$lang['madhya-pradesh'] = 'Madhya Pradesh';
$lang['bangla-nagar'] = 'Bangla Nagar';
$lang['mp-nagar'] = 'MP Nagar';
$lang['jnv-colony'] = 'JNV Colony';
$lang['mdv-colony'] = 'MDV Colony';
$lang['0'] = 'Paraná';
$lang['Brazil'] = 'Brazil';
$lang['Dubai'] = 'Dubai';
$lang['Manama'] = 'Manama';
$lang['Ajman City'] = 'Ajman City';
$lang['Ajman'] = 'Ajman';
$lang['Ajman Emirate'] = 'Ajman Emirate';
$lang['Musaffah'] = 'Musaffah';
$lang['Al Ain City'] = 'Al Ain City';
$lang['Abu Dhabi Emirate'] = 'Abu Dhabi Emirate';
$lang['United Arab Emirates'] = 'United Arab Emirates';
$lang['Bagdad'] = 'Bagdad';
$lang['Apache County'] = 'Apache County';
$lang['Alhambra'] = 'Alhambra';
$lang['Butte'] = 'Butte';
$lang['Barrow'] = 'Barrow';
$lang['Anchor Point'] = 'Anchor Point';
$lang['Alaska'] = 'Alaska';
$lang['Ashville'] = 'Ashville';
$lang['Argo'] = 'Argo';
$lang['Alexander City'] = 'Alexander City';
$lang['Alabaster'] = 'Alabaster';
$lang['Alabama'] = 'Alabama';
$lang['No State'] = 'No State';
$lang['Varanasi'] = 'Varanasi';
$lang['Kanpur'] = 'Kanpur';
$lang['Kannauj'] = 'Kannauj';
$lang['Lucknow'] = 'Lucknow';
$lang['Allahabad'] = 'Allahabad';
$lang['Uttar Pradesh'] = 'Uttar Pradesh';
$lang['Mandsaur'] = 'Mandsaur';
$lang['Ujjain'] = 'Ujjain';
$lang['Indore'] = 'Indore';
$lang['Bhopal'] = 'Bhopal';
$lang['Madhya Pradesh'] = 'Madhya Pradesh';
$lang['Sikar'] = 'Sikar';
$lang['Ganganagar'] = 'Ganganagar';
$lang['Bharatpur'] = 'Bharatpur';
$lang['Bangla Nagar'] = 'Bangla Nagar';
$lang['MP Nagar'] = 'MP Nagar';
$lang['JNV Colony'] = 'JNV Colony';
$lang['MDV Colony'] = 'MDV Colony';
$lang['Gangashahar'] = 'Gangashahar';
$lang['Alwar'] = 'Alwar';
$lang['Aptos Hills-Larkin Valley'] = 'Aptos Hills-Larkin Valley';
$lang['Acalanes Ridge'] = 'Acalanes Ridge';
$lang['Los Angeles'] = 'Los Angeles';
$lang['United States'] = 'United States';
$lang['Denver'] = 'Denver';
$lang['Colorado'] = 'Colorado';
$lang['California'] = 'California';
$lang['Phoenix'] = 'Phoenix';
$lang['Arizona'] = 'Arizona';
$lang['Jodhpur'] = 'Jodhpur';
$lang['Jaipur'] = 'Jaipur';
$lang['Gnagashahar'] = 'Gnagashahar';
$lang['Lalgarh'] = 'Lalgarh';
$lang['Bikaner'] = 'Bikaner';
$lang['Ajmer'] = 'Ajmer';
$lang['Rajasthan'] = 'Rajasthan';
$lang['Ludhiana'] = 'Ludhiana';
$lang['Amritsar'] = 'Amritsar';
$lang['Punjab'] = 'Punjab';
$lang['Maharashtra'] = 'Maharashtra';
$lang['Delhi'] = 'Delhi';
$lang['India'] = 'India';
$lang['Manage Featured'] = '';
$lang['Looking for Property'] = '';
$lang['East'] = '';
$lang['tegory Section'] = '';
$lang['Add New'] = '';
$lang['Manage Types'] = '';
$lang['Home'] = '';
$lang['Properties'] = '';
$lang['About Us'] = '';
$lang['Contact Us'] = '';
$lang['Recent Properties'] = '';
$lang['Bedrooms'] = '';
$lang['Bathrooms'] = '';
$lang['View More'] = '';
$lang['LOOKING FOR PROPERTY'] = '';
$lang['What kind of property are you looking for? We will help you'] = '';
$lang['Featured Properties'] = '';
$lang['About Realestate'] = '';
$lang['Navigations'] = '';
$lang['Follow Us'] = '';
$lang['Listing Types'] = '';
$lang['Select Property Type'] = '';
$lang['Offer Type'] = '';
$lang['Select Property For'] = '';
$lang['For Sale'] = '';
$lang['For Rent'] = '';
$lang['Select City'] = '';
$lang['All'] = '';
$lang['Sort by'] = '';
$lang['Price Ascending'] = '';
$lang['Home Type'] = '';
$lang['Beds'] = '';
$lang['Baths'] = '';
$lang['Contact Agent'] = '';
$lang['Name'] = '';
$lang['Phone'] = '';
$lang['Send Message'] = '';
$lang['Mortgage Calculator'] = '';
$lang['Use this calculator to estimate your monthly mortgage payment'] = '';
$lang['House Price'] = '';
$lang['Interest Rate'] = '';
$lang['Years'] = '';
$lang['Down Payment'] = '';
$lang['Mortgage Principle'] = '';
$lang['Total Payments'] = '';
$lang['Payment/Month'] = '';
$lang['Calculate'] = '';
$lang['Related Properties'] = '';
$lang['This field can not be 0!'] = '';
$lang['The Price field can not be 0!'] = '';
$lang['The Interest Rate field can not be 0!'] = '';
$lang['The Term field can not be 0!'] = '';
$lang['Videos'] = '';
$lang['Hospital'] = '';
$lang['Railway Station'] = '';
$lang['School'] = '';
$lang['Collage'] = '';
$lang['Post Office'] = '';
$lang['Bank'] = '';
$lang['Dairy'] = '';
$lang['Meter'] = '';
$lang['KM'] = '';
$lang['Air Conditioning'] = '';
$lang['Computer'] = '';
$lang['Heating'] = '';
$lang['Microwave'] = '';
$lang['Balcony'] = '';
$lang['Grill'] = '';
$lang['Parking'] = '';
$lang['Apartment'] = '';
$lang['SQ. FT'] = '';
$lang['Price Descending'] = '';
$lang['Subject'] = '';
$lang['Message'] = '';
$lang['Contact Info'] = '';
$lang['Search'] = '';
$lang['Sending'] = '';
$lang['You have malicious code in submitted form data.'] = '';
$lang['Please fill all required fields.'] = '';
$lang['about an hour'] = '';
$lang['ago'] = '';
$lang['yesterday'] = '';
$lang['days ago'] = '';
$lang['one month ago'] = '';
$lang['Hours'] = '';
$lang['Property For Sale'] = '';
$lang['Property For Rent'] = '';
$lang['Contact'] = '';
$lang['SQ FT'] = '';
$lang['Cable TV'] = '';
$lang['Dishwasher'] = '';
$lang['Internet'] = '';
$lang['Lift'] = '';
$lang['Pool'] = '';
$lang['Airpot'] = '';
$lang['Market'] = '';
$lang['Shopping Mall'] = '';
$lang['Admin'] = '';
$lang['Agent'] = '';
$lang['Condo'] = '';
$lang['Flat'] = '';
$lang['Villa'] = '';
$lang['Months Ago'] = '';
$lang['No Property Found Related Your Search Criteria.'] = '';
$lang['Ac'] = '';
$lang['Heater'] = '';
$lang['Something'] = '';
$lang['Fere'] = '';
$lang['Minutes Ago'] = '';
$lang['Test Type'] = '';
$lang['Parks'] = '';
$lang['An Hour Ago'] = '';
$lang['A Minute Ago'] = '';
$lang['Added for Comparison'] = '';
$lang['Compare'] = '';
$lang['Our Agents'] = 'abcdfererere';
$lang['Admin Settings'] = '';
$lang['Enable Auto Update Language Keywords'] = '';
$lang['Enable Property Compare on Front End'] = '';
$lang['Select Any Timezone'] = '';
$lang['Default'] = '';
$lang['Manage Bookings'] = '';
$lang['patient Number'] = '';
$lang['Bookings Details'] = '';
$lang['Add New Booking'] = '';
$lang['Bookings'] = '';
$lang['Categroy Details'] = '';
$lang['Edit Category'] = '';
$lang['Manage category'] = '';
$lang['Category Details'] = '';
$lang['Add New Category'] = '';
$lang['Doctor Categories'] = '';
$lang['patient Name'] = '';
$lang['patient Details'] = '';
$lang['Edit patient'] = '';
$lang['Re-active'] = '';
$lang['suspend'] = '';
$lang['Manage Patient'] = '';
$lang['Patient'] = '';
$lang['Doctor Category'] = '';
$lang['Pharmacy Details'] = '';
$lang['Edit Pharmacy'] = '';
$lang['Contact Number'] = '';
$lang['Pharmacies Name'] = '';
$lang['Pharmacies Details'] = '';
$lang['Manage Pharmacy'] = '';
$lang['Add New Pharmacy'] = '';
$lang['Pharmacies'] = '';
$lang['Edit Doctor'] = '';
$lang['Manage Doctors'] = '';
$lang['Doctor Details'] = '';
$lang['Add New Doctor'] = '';
$lang['Doctors'] = '';
$lang['Agent List'] = '';
$lang['Property by Country'] = '';
$lang['Property by Category'] = '';
$lang['Property for Rent'] = '';
$lang['Property for Sale'] = '';
$lang['Property by Locations'] = '';
$lang['Featured Property'] = '';
$lang['Recent Property'] = '';
$lang['Search Property'] = '';
$lang['Agent List Section'] = '';
$lang['Property by Country Section'] = '';
$lang['Property by Category Section'] = '';
$lang['Property for Rent Section'] = '';
$lang['Property for Sale Section'] = '';
$lang['Property by Locations Section'] = '';
$lang['Featured Property Section'] = '';
$lang['Recent Property Section'] = '';
$lang['Search Property Section'] = '';
$lang['About Section'] = '';
$lang['Contact Section'] = '';
$lang['Our Services Section'] = '';
$lang['Features Section'] = '';
$lang['Process Section'] = '';
$lang['Portfolio Section'] = '';
$lang['Client Section'] = '';
$lang['FAQ Section'] = '';
$lang['Testimonial Section'] = '';
$lang['Our Team Section'] = '';
$lang['Pricing Table Section'] = '';
$lang['Photo Gallery Section'] = '';
$lang['Slider Section'] = '';
$lang['Homepage Sections'] = '';
$lang['Homepage'] = '';
$lang['Default Language'] = '';
$lang['RTL'] = '';
$lang['LTR'] = '';
$lang['Disable'] = '';
$lang['Enable'] = '';
$lang['Enable Multi Language'] = '';
$lang['Add Video'] = '';
$lang['Character(s) Remaining'] = '';
$lang['Image file must be greater then 1900x1300'] = '';
$lang['Edit Page'] = '';
$lang['Updated On'] = '';
$lang['Manage Pages'] = '';
$lang['Page Content'] = '';
$lang['Page Title'] = '';
$lang['Page Details'] = '';
$lang['Add New Page'] = '';
$lang['Short Description'] = '';
$lang['Reject'] = '';
$lang['Approve'] = '';
$lang['Print'] = '';
$lang['Gallery'] = '';
$lang['Address Details'] = '';
$lang['Agent Details'] = '';
$lang['Property Status'] = '';
$lang['Property Price'] = '';
$lang['Date'] = '';
$lang['View Property'] = '';
$lang['Menu'] = '';
$lang['Add New Slider'] = '';
$lang['Manage Sliders'] = '';
$lang['Slider'] = '';
$lang['Manage Featured Properties'] = '';
$lang['Month'] = '';
$lang['For'] = '';
$lang['Type'] = '';
$lang['Manage In-Active Properties'] = '';
$lang['Language Settings'] = '';
$lang['Actions'] = '';
$lang['Keyword'] = '';
$lang['Manage Front Keywords'] = '';
$lang['Edit Property Type'] = '';
$lang['Add Property Type'] = '';
$lang['Manage Property Types'] = '';
$lang['Property Distances'] = '';
$lang['Select None'] = '';
$lang['Select All'] = '';
$lang['Manage Media'] = '';
$lang['Click on image to select multiple'] = '';
$lang['Property for Cities'] = '';
$lang['Enable Property for Cities'] = '';
$lang['Save'] = '';
$lang['Property Amenities'] = '';
$lang['Property Images'] = '';
$lang['Description'] = '';
$lang['Add New Property'] = '';
$lang['Edit Banner'] = '';
$lang['Manage Banners'] = '';
$lang['Banner Details'] = '';
$lang['Add New Banner'] = '';
$lang['Banner'] = '';
$lang['Password'] = '';
$lang['Add New User'] = '';
$lang['Repeat Password'] = '';
$lang['Reset Password'] = '';
$lang['User Name'] = '';
$lang['Select User Type'] = '';
$lang['Login Details'] = '';
$lang['Photo'] = '';
$lang['Email Address'] = '';
$lang['Last Name'] = '';
$lang['First Name'] = '';
$lang['User Details'] = '';
$lang['Edit User'] = '';
$lang['Are you sure you want to delete?'] = '';
$lang['Email'] = '';
$lang['Mobile No.'] = '';
$lang['User Type'] = '';
$lang['Username'] = '';
$lang['Full Name'] = '';
$lang['S No.'] = '';
$lang['Manage Users'] = '';
$lang['Site Domain E-mail'] = '';
$lang['Site Domain'] = '';
$lang['Copyright Text'] = '';
$lang['Footer Text'] = '';
$lang['Footer Settings'] = '';
$lang['Contact Form E-mail'] = '';
$lang['Contact Form Settings'] = '';
$lang['No'] = '';
$lang['Yes'] = '';
$lang['Require Admin Approval for Publish Property'] = '';
$lang['Property Settings'] = '';
$lang['Secret Key'] = '';
$lang['Site Key'] = '';
$lang['Google reCAPTCHA Settings'] = '';
$lang['Yellow Light'] = '';
$lang['Red Light'] = '';
$lang['Green Light'] = '';
$lang['Purple Light'] = '';
$lang['Black Light'] = '';
$lang['Blue Light'] = '';
$lang['Yellow'] = '';
$lang['Red'] = '';
$lang['Green'] = '';
$lang['Purple'] = '';
$lang['Black'] = '';
$lang['Blue'] = '';
$lang['Admin Skins'] = '';
$lang['Date Format'] = '';
$lang['Select Your Timezone'] = '';
$lang['Timezone'] = '';
$lang['Other Settings'] = '';
$lang['Lat/Long'] = '';
$lang['Contact E-mail'] = '';
$lang['Company E-mail'] = '';
$lang['Company Website'] = '';
$lang['Company Telephone No.'] = '';
$lang['Compony Mobile No.'] = '';
$lang['Company Address'] = '';
$lang['Fevicon Icon'] = '';
$lang['Website Logo'] = '';
$lang['Website Logo Text'] = '';
$lang['Website Title'] = '';
$lang['Total LandLord'] = '';
$lang['Total Builder'] = '';
$lang['Total Owners'] = '';
$lang['Total Agents'] = '';
$lang['Total Media'] = '';
$lang['Total Properties'] = '';
$lang['Sign In'] = '';
$lang['Sign in to Start Your Session'] = '';
$lang['Login'] = '';
$lang['Media Library'] = '';
$lang['Insert Into Property'] = '';
$lang['Click on image to select'] = '';
$lang['Save Publish'] = '';
$lang['Save as Draft'] = '';
$lang['Current Status'] = '';
$lang['Add From Media'] = '';
$lang['OR'] = '';
$lang['Upload Image'] = '';
$lang['Product Gallary'] = '';
$lang['Video URL'] = '';
$lang['Property Video'] = '';
$lang['North-West'] = '';
$lang['South-West'] = '';
$lang['South-East'] = '';
$lang['North-East'] = '';
$lang['South'] = '';
$lang['North'] = '';
$lang['West'] = '';
$lang['East'] = '';
$lang['Select Any Direction'] = '';
$lang['Outdoor Amenities'] = '';
$lang['Indoor Amenities'] = '';
$lang['Garages'] = '';
$lang['Bathroom'] = '';
$lang['Bedroom'] = '';
$lang['Sq. Foot'] = '';
$lang['Features'] = '';
$lang['Zip Code'] = '';
$lang['City'] = '';
$lang['Select Any State'] = '';
$lang['State'] = '';
$lang['Select Any Country'] = '';
$lang['Country'] = '';
$lang['Street Address'] = '';
$lang['Click on following link to get your Latitude and Longitude.'] = '';
$lang['Longitude'] = '';
$lang['Latitude'] = '';
$lang['GPS Coordinates'] = '';
$lang['Address'] = '';
$lang['In-Active'] = '';
$lang['Active'] = '';
$lang['Rent'] = '';
$lang['Sale'] = '';
$lang['Property For'] = '';
$lang['Property Type'] = '';
$lang['Per Month'] = '';
$lang['Property Details'] = '';
$lang['Edit Property'] = '';
$lang['Select Any City'] = '';
$lang['Delete'] = '';
$lang['Edit'] = '';
$lang['View'] = '';
$lang['Sq. Feet'] = '';
$lang['Action'] = '';
$lang['Created By'] = '';
$lang['Created On'] = '';
$lang['Is Featured?'] = '';
$lang['Size'] = '';
$lang['Price'] = '';
$lang['Title'] = '';
$lang['Image'] = '';
$lang['S.No.'] = '';
$lang['Manage Active Properties'] = '';
$lang['Status'] = '';
$lang['Meta Description'] = '';
$lang['Meta Keywords'] = '';
$lang['SEO Keywords'] = '';
$lang['Admin Keyword Settings'] = '';
$lang['Add More Language'] = '';
$lang['Select Any Currency'] = '';
$lang['Direction'] = '';
$lang['Currency'] = '';
$lang['Language'] = '';
$lang['Site Language'] = '';
$lang['All Notifications'] = '';
$lang['Please Wait'] = '';
$lang['Version'] = '';
$lang['Submit'] = '';
$lang['Update Language File'] = '';
$lang['Save Changes'] = '';
$lang['Select Any Language'] = '';
$lang['Language Title'] = '';
$lang['Manage Keywords'] = '';
$lang['Front Keyword Settings'] = '';
$lang['Change Password'] = '';
$lang['Admin Keywords Settings'] = '';
$lang['Front Keywords Settings'] = '';
$lang['Site Languages'] = '';
$lang['Social Settings'] = '';
$lang['General Settings'] = '';
$lang['Settings'] = '';
$lang['Page'] = '';
$lang['User'] = '';
$lang['Manage'] = '';
$lang['Media'] = '';
$lang['Distances'] = '';
$lang['Amenities'] = '';
$lang['Manage In-Active'] = '';
$lang['Manage Active'] = '';
$lang['Property'] = '';
$lang['Dashboard'] = '';
$lang['Sign Out'] = '';
$lang['Welcome'] = '';
$lang['View All'] = '';
$lang['notifications'] = '';
$lang['You have'] = '';
$lang['View Site'] = '';
$lang['Toggle Navigation'] = '';
$lang['Real Estate'] = '';
$lang['R E'] = '';
