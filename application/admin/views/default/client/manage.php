<?php $this->load->view("default/header-top");?>

<?php $this->load->view("default/sidebar-left");?>


<div class="content-wrapper">
<section class="content-header">
  <h1 class="page-title"><i class="fa fa-newspaper-o"></i> <?php //echo mlx_get_lang('Manage Blogs'); ?>Manage Clients 
  <a href="<?php echo base_url(array('client','add_new')); ?>" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right content-header-right-link">Add New</a>
  </h1>
  <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
	?> 
	
	<div class="row">
				<div class="col-md-12">
					<div class="card widget-inline">
						<div class="card-body p-0">
							<div class="row no-gutters">
								<div class="col-sm-2 col-xs-2">
									<a href="<?php echo base_url(array('client','manage','all')); ?>" class="text-secondary">
										<div class="card shadow-none m-0 br-0 <?php if(isset($cur_active_tab) && $cur_active_tab == 'all') echo 'active';  ?>">
											<div class="card-body text-center">
												<i class="fa fa-newspaper-o text-muted" style="font-size: 24px;"></i>
												<h3><span><?php if(isset($all_clients)) { echo $all_clients->num_rows(); } else echo '0';  ?></span></h3>
												<p class="text-muted font-15 mb-0"><?php //echo mlx_get_lang('All Blogs'); ?>All Clients</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-sm-2 col-xs-2">
									<a href="<?php echo base_url(array('client','manage','active')); ?>" class="text-secondary">
										<div class="card shadow-none m-0 br-0 <?php if((isset($cur_active_tab) && ($cur_active_tab == 'active' || $cur_active_tab == '')) || !isset($cur_active_tab)) echo 'active';  ?>">
											<div class="card-body text-center">
												<i class="fa fa-link text-muted" style="font-size: 24px;"></i>
												<h3><span><?php if(isset($active_clients)) { echo $active_clients->num_rows(); } else echo '0';  ?></span></h3>
												<p class="text-muted font-15 mb-0"><?php //echo mlx_get_lang('Active Blogs'); ?>Active Clients</p>
											</div>
										</div>
									</a>
								</div>

								<div class="col-sm-2 col-xs-2">
									<a href="<?php echo base_url(array('client','manage','inactive')); ?>" class="text-secondary">
										<div class="card shadow-none m-0 border-left br-0 <?php if(isset($cur_active_tab) && $cur_active_tab == 'inactive') echo 'active';  ?>">
											<div class="card-body text-center">
												<i class="fa fa-chain-broken text-muted" style="font-size: 24px;"></i>
												<h3><span><?php if(isset($inactive_clients)) { echo $inactive_clients->num_rows(); } else echo '0';  ?></span></h3>
												<p class="text-muted font-15 mb-0"><?php //echo mlx_get_lang('Inactive Blogs'); ?>Inactive Clients</p>
											</div>
										</div>
									</a>
								</div>

								<div class="col-sm-2 col-xs-2">
									<a href="<?php echo base_url(array('client','manage','pending')); ?>" class="text-secondary">
										<div class="card shadow-none m-0 border-left br-0 <?php if(isset($cur_active_tab) && $cur_active_tab == 'pending') echo 'active';  ?>">
											<div class="card-body text-center">
												<i class="fa fa-clock-o text-muted" style="font-size: 24px;"></i>
												<h3><span><?php if(isset($pending_clients)) { echo $pending_clients->num_rows(); } else echo '0';  ?></span></h3>
												<p class="text-muted font-15 mb-0"><?php //echo mlx_get_lang('Pending Blogs'); ?>Pending Clients</p>
											</div>
										</div>
									</a>
								</div>
								
								<div class="col-sm-2 col-xs-2">
									<a href="<?php echo base_url(array('client','manage','rejected')); ?>" class="text-secondary">
										<div class="card shadow-none m-0 border-left  <?php if(isset($cur_active_tab) && $cur_active_tab == 'rejected') echo 'active';  ?>">
											<div class="card-body text-center">
												<i class="fa fa-ban text-muted" style="font-size: 24px;"></i>
												<h3><span><?php if(isset($rejected_clients)) { echo $rejected_clients->num_rows(); } else echo '0';  ?></span></h3>
												<p class="text-muted font-15 mb-0"><?php //echo mlx_get_lang('Rejected Blogs'); ?>Rejected Clients</p>
											</div>
										</div>
									</a>
								</div>
							</div> 
						</div>
					</div> 
				</div> 
			</div>
</section>

<section class="content">

  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
	
	<div class="box-body content-box">
		  <table id="example2" class="table table-bordered table-hover datatable-element-scrollx">
			<thead>
			  <tr>
				
				<th width="30px"><?php echo mlx_get_lang('S.No.'); ?></th>
				<th width="150px">Client Name</th>
				<th>Username</th>
				<th>Email</th>
				<th>Location</th>
				<th><?php echo mlx_get_lang('Created On'); ?></th>
				<th><?php echo mlx_get_lang('Status'); ?></th>
				<th><?php echo mlx_get_lang('Action'); ?></th>
			  </tr>
			</thead>
			<tbody>
<?php  if ($query->num_rows() > 0)
{				
	$i=0;   
foreach ($query->result() as $row)
{ 
	$i++;
	
?>						
			  <tr>
			   
				<td><?php echo  $i; ?></td>
				<td>
					<?php echo $row->first_name.' '.$row->last_name;?>
				</td>
				<td> <?php echo $row->username; ?></td>
				<td>
					<?php echo $row->email;?>
				</td>
				<td>
					<?php
					$country = '';
					$state 	 = '';
					$zip 	 = '';
					 
					if($row->state != '')
					{
						$state = ' '.$row->state.' ';
					}
					if($row->zip != '')
					{
						$zip = $row->zip.', ';
					}
					if($row->country != '')
					{
						$country = $row->country;
					}
					echo $row->city.$state.$zip.$country;?>
				</td>
				<td> <?php echo date('M d, Y h:i A',$row->created_on);?></td>
				<td>
					<?php						
					   if($row->status == 'inactive') echo '<span class="label label-info">Inactive</span>'; 
					   else if($row->status == 'pending') echo '<span class="label label-warning">Pending</span>';
					   else if($row->status == 'active') echo '<span class="label label-success">Active</span>';
					   else if($row->status == 'reject') echo '<span class="label label-success">Active</span>';
					   else echo '-';
					?>
				</td>
				<td class="action_block">
					
					<a href="<?php $segments = array('client','view',$myHelpers->EncryptClientId($row->client_id)); 
					echo site_url($segments);?>" title="<?php echo mlx_get_lang('Edit'); ?>" class="btn btn-warning btn-xs" style="background:#00c0ef;border-color:#00c0ef;"><i class="fa fa-eye fa-2x"></i></a>
					
					<a href="<?php $segments = array('client','delete',$myHelpers->EncryptClientId($row->client_id)); 
					echo site_url($segments);?>" title="<?php echo mlx_get_lang('Delete'); ?>" 
					onclick="return confirm('<?php echo mlx_get_lang('Are you sure?'); ?>');"
					class="btn btn-danger btn-xs"><i class="fa fa-remove"></i></a>
					
				</td>
			  </tr>
<?php 	}
}	?>                      
			</tbody>
			
		  </table>
		</div>
  </div>
</section>
</div>