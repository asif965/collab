<?php $this->load->view("default/header-top"); ?>
<?php $this->load->view("default/sidebar-left"); ?>
<?php 
$site_language = $myHelpers->global_lib->get_option('site_language');
$enable_multi_language = $myHelpers->global_lib->get_option('enable_multi_language');
$default_language = $myHelpers->global_lib->get_option('default_language');
$locations = $this->global_lib->get_option('locations');
if(!empty($locations))
{
	$location_array = json_decode($locations,true);
}
$loc_tax_settings = $this->global_lib->get_option('loc_tax_settings');
$is_state_enable = false;
$is_city_enable = false;
$is_zipcode_enable = false;
$is_subarea_enable = false;
if(!empty($loc_tax_settings))
{
	$loc_tax_setting_array = json_decode($loc_tax_settings,true);
	
	if(isset($loc_tax_setting_array['state']['enabled']) && $loc_tax_setting_array['state']['enabled'] == true)
		$is_state_enable = true;
	
	if(isset($loc_tax_setting_array['city']['enabled']) && $loc_tax_setting_array['city']['enabled'] == true)
		$is_city_enable = true;
		
	if(isset($loc_tax_setting_array['zipcode']['enabled']) && $loc_tax_setting_array['zipcode']['enabled'] == true)
		$is_zipcode_enable = true;
		
	if(isset($loc_tax_setting_array['sub-area']['enabled']) && $loc_tax_setting_array['sub-area']['enabled'] == true)
		$is_subarea_enable = true;
}
?>
<div class="content-wrapper">
	<section class="content-header">
	  <h1 class="page-title"><i class="fa fa-home"></i> <?php echo mlx_get_lang('Homepage Sections'); ?></h1>
		<?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
		?> 
	</section>

	<section class="content">
		<?php 
		$attributes = array('name' => 'add_form_post','class' => 'homepage_section_form');		 			
		echo form_open_multipart('main/home_page',$attributes); ?>
		<div class="row">
			<div class="col-md-12">
			
				<div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?> homepage_section_container" >
					<div class="box-body">
					<?php 
					
					if(isset($content_sections) && !empty($content_sections)) 
					{ 
						
						
						
						if(isset($meta_content_lists) && !empty($meta_content_lists))
						{
							$old_content_sections = $content_sections;  
							$content_sections = array();
							
							foreach($meta_content_lists as $k=>$v)
							{
								
								if(isset($old_content_sections[$k]))
								{
									$content_sections[$k] = $old_content_sections[$k];
									unset($old_content_sections[$k]);
									if($k == 'video_section')
									{
										$content_sections[$k.'_1'] = array('section_type' => 'video', 'title' => mlx_get_lang('Videos (Youtube/Vimeo/Facebook/Self Hosted)'));
									}
								}
								else if((isset($v['section_type']) && $v['section_type'] == 'video') || ($k == 'video_section'))
								{
									$content_sections[$k] = array('section_type' => 'video', 'title' => mlx_get_lang('Videos (Youtube/Vimeo/Facebook/Self Hosted)'));
								}
								else
								{
									$content_sections[$k] = array('section_type' => 'dynamic', 'title' => 'Properties Section');
								}
							}
							
							
							if(!empty($old_content_sections))
							{
								foreach($old_content_sections as $k=>$v)
								{
									$content_sections[$k] = $old_content_sections[$k];
									unset($old_content_sections[$k]);
								}
							}
					  }
					  
					  
						
					  
					  $new_content_section = array();
					  if(array_key_exists('slider_section',$content_sections))
					  {
						  $new_content_section['slider_section'] = $content_sections['slider_section'];
						  unset($content_sections['slider_section']);
					  }
					    if(array_key_exists('search_section',$content_sections))
					    {
						  $new_content_section['search_section'] = $content_sections['search_section'];
						  unset($content_sections['search_section']);
					    }
					    if(array_key_exists('recent_blog_section',$content_sections))
					    {
							$isPlugAct = $myHelpers->isPluginActive('blog');
							if($isPlugAct != true)
							{
							  unset($content_sections['recent_blog_section']);
							}
					    }
						$content_sections = array_merge($new_content_section,$content_sections);
					  
						
					  
					  $dynamic_section = '';
					  $dynamic_video_section = '';
					  ?>
					  <ul class="todo-list ui-sortable">
							<?php 
							
							$manage_contents = array();
							
							$ds_count = 0;
							
							$ds_heading = '';
							
							
							
							foreach($content_sections  as $content_section_key => $content_section_value)
							{ 
								$ds_count++;
								$section_fields = $myHelpers->config->item($content_section_key."_fields") ;
								
								global $meta_content;
								$meta_content = array();
								
								$sec_key = str_replace('_section','',$content_section_key);
								
								$has_val_saved = false;
								if(isset($meta_content_lists) && isset($meta_content_lists[$content_section_key]))
								{
									$has_val_saved = true;
									foreach($meta_content_lists[$content_section_key] as $csk=>$csv)
									{
										${$csk} = $csv;
									}
								}	
								
								if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key != 'properties_section')
								{
									
									$section_fields = $myHelpers->config->item("properties_section_fields") ;
									
									$ds_heading = $heading;
									
								}
								else if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'video' && $content_section_key != 'video_section')
								{
									
									$section_fields = $myHelpers->config->item("video_section_fields") ;
									
									$ds_heading = $heading;
									
								}
								else
								{
									$ds_heading = '';
								}
								
								
								
								$section_cls = ' section_no_'.$ds_count.' ';
								if($content_section_key == 'slider_section' || $content_section_key == 'search_section')
									$section_cls .= 'fixed-section';
								
								
								
								if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key == 'properties_section')
								{
									$heading = '';
									$ds_inner_count = 0;
									
									ob_start();
								?>
										<li class="<?php echo $section_cls; ?> dynamic_section">
											<input type="hidden" value="dynamic" name="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>[section_type]">
											<div class="header-block">
											  <span class="handle ui-sortable-handle">
												<i class="fa fa-ellipsis-v"></i>
												<i class="fa fa-ellipsis-v"></i>
											  </span>
											  <span class="text"><?php echo mlx_get_lang(ucfirst($content_section_value['title'])); ?></span>
											  
											   <div class="radio_toggle_wrapper pull-right">
												<input type="radio" 
												<?php if((isset($is_enable) && $is_enable == 'Y') || !isset($is_enable)) { ?>
												checked="checked" 
												<?php } ?>
												id="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>_enable" value="Y" 
												name="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>[is_enable]" class="toggle-radio-button">
												<label for="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>_enable"><?php echo mlx_get_lang('Enable'); ?></label>
												
												<input type="radio" id="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>_disable" value="N" 
												<?php if(isset($is_enable) && $is_enable == 'N') { ?>
												checked="checked" 
												<?php } ?>
												name="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>[is_enable]" class="toggle-radio-button">
												<label for="<?php echo $content_section_key; ?>_<?php echo $ds_inner_count; ?>_disable"><?php echo mlx_get_lang('Disable'); ?></label>
											  </div>
										  
											  <?php if(!empty($section_fields)) { ?>
											  <div class="tools">
												<button class="btn btn-box-tool collapsed" ><i class="fa fa-chevron-down"></i></button>
												<button class="btn btn-danger btn-sm remove_ds_btn" title="<?php echo mlx_get_lang('Remove Section'); ?>" data-toggle="tooltip"><i class="fa fa-trash fa-2x"></i></button>
												<button class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> btn-sm clone_section_btn" title="<?php echo mlx_get_lang('Clone Section'); ?>" data-toggle="tooltip"><i class="fa fa-clone fa-2x"></i></button>
											  </div>
											  <?php } ?>
											  
											  
											  
											</div>
										  <?php if(!empty($section_fields)) { 
										  
											
										  ?>
										  <div class="section_fields hide">
											  <?php 
											   global $single_field,$content_type;
												
												
												
												foreach($section_fields as $k => $single_field){
													$content_type = $content_section_key.'_'.$ds_inner_count;
													if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_type')
													{
														$property_type_option = array();
														$property_type_option['all'] = 'All Types';
														$property_type_result = $this->Common_model->commonQuery("select * from property_types where status = 'Y' order by title ASC");
														if($property_type_result->num_rows() > 0)
														{
															foreach($property_type_result->result() as $pt_row)
															{
																$property_type_option[$pt_row->pt_id] = $pt_row->title;
															}
														}
														$single_field['default'] = 'all';
														$single_field['options'] = $property_type_option;
													}
													else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_for_lang')
													{
														$property_lang_option = array();
														
														if(isset($site_language) && !empty($site_language))
														{ 
															$site_language_array = json_decode($site_language,true);
															if(!empty($site_language_array)) 
															{
																$property_lang_option['all'] = 'All Languages';
																foreach($site_language_array as $aak=>$aav)
																{
																	if($aav['language'] == $default_language)
																	{
																		$new_value = $site_language_array[$aak];
																		unset($site_language_array[$aak]);
																		array_unshift($site_language_array, $new_value);
																		break;
																	}
																}
																foreach($site_language_array as $k=>$v) 
																{
																	if($v['status'] != 'enable')
																		continue;
																	$lang_exp = explode('~',$v['language']);
																	$lang_code = $lang_exp[1];
																	$lang_title = $lang_exp[0];
																	
																	$property_lang_option[$lang_code] = $lang_title;
																}
															}
														}
														$single_field['default'] = 'all';
														$single_field['options'] = $property_lang_option;
														
													}
													else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_state')
													{
														if(!$is_state_enable)
															continue;
													}
													else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_city')
													{
														if(!$is_city_enable)
															continue;
													}
													else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_zipcode')
													{
														if(!$is_zipcode_enable)
															continue;
													}
													else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_sub_area')
													{
														if(!$is_subarea_enable)
															continue;
													}
													
													if(isset($single_field['name']) && isset(${$single_field['name']}) && $has_val_saved)
													{
														global $meta_content;
														$meta_content[$single_field['name']] = ${$single_field['name']};
														
													}
													else if(!isset($meta_content))
													{
														global $meta_content;
														$meta_content = array();
													}
													
													
													if(isset($single_field['id']))
													{
														$single_field['id'] = $single_field['id'].'_'.$ds_inner_count;
													}
													
													
													if(isset($single_field['name']) && ( $single_field['name'] == 'property_for_lang' || 
													$single_field['name'] == 'property_type') )
														$meta_content[$single_field['name']] = 'all';
													else if(isset($single_field['name']) && $single_field['name'] == 'show_as')
														$meta_content[$single_field['name']] = 'grid';
													else if(isset($single_field['name']) && ( $single_field['name'] == 'heading' || 
													$single_field['name'] == 'sub_heading') )
														$meta_content[$single_field['name']] = '';
													else if(isset($single_field['name']) && ( $single_field['name'] == 'no_of_item_in_grid_list' || 
													$single_field['name'] == 'no_of_item_in_carousel'))
														$meta_content[$single_field['name']] = '6';
													else if(isset($single_field['name']) && ( $single_field['name'] == 'show_view_more' || 
													$single_field['name'] == 'show_nav' || $single_field['name'] == 'show_nav_dots'))
														$meta_content[$single_field['name']] = 'yes';
													else if(isset($single_field['name']) && $single_field['name'] == 'auto_start')
														$meta_content[$single_field['name']] = 'no';	
													else if(isset($single_field['name']) && $single_field['name'] == 'carousel_interval')
														$meta_content[$single_field['name']] = '5000';	
													$this->load->view("$theme/templates/templ-".$single_field['type'] ); 
												}
											  ?>
										  </div>
										  <?php } ?>
										</li>
								<?php
									$dynamic_section = ob_get_clean();
									$ds_count--;
									continue;
								}
								else if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'video' && $content_section_key == 'video_section')
								{
									
									$heading = '';
									
									$content_section_key = 'video_section';
									
									ob_start();
								?>
										<li class="<?php echo $section_cls; ?> dynamic_video_section">
											<input type="hidden" value="video" name="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>[section_type]">
											<div class="header-block">
											  <span class="handle ui-sortable-handle">
												<i class="fa fa-ellipsis-v"></i>
												<i class="fa fa-ellipsis-v"></i>
											  </span>
											  <span class="text"><?php echo mlx_get_lang(ucfirst($content_section_value['title'])); ?></span>
											  
											   <div class="radio_toggle_wrapper pull-right">
												<input type="radio" 
												<?php if((isset($is_enable) && $is_enable == 'Y') || !isset($is_enable)) { ?>
												checked="checked" 
												<?php } ?>
												id="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_enable" value="Y" 
												name="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>[is_enable]" class="toggle-radio-button">
												<label for="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_enable"><?php echo mlx_get_lang('Enable'); ?></label>
												
												<input type="radio" id="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_disable" value="N" 
												<?php if(isset($is_enable) && $is_enable == 'N') { ?>
												checked="checked" 
												<?php } ?>
												name="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>[is_enable]" class="toggle-radio-button">
												<label for="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_disable"><?php echo mlx_get_lang('Disable'); ?></label>
											  </div>
										  
											  <?php if(!empty($section_fields)) { ?>
											  <div class="tools">
												<button class="btn btn-box-tool collapsed" ><i class="fa fa-chevron-down"></i></button>
												<button class="btn btn-danger btn-sm remove_ds_btn" title="<?php echo mlx_get_lang('Remove Section'); ?>" data-toggle="tooltip"><i class="fa fa-trash fa-2x"></i></button>
												<button class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> btn-sm clone_section_btn" title="<?php echo mlx_get_lang('Clone Section'); ?>" data-toggle="tooltip"><i class="fa fa-clone fa-2x"></i></button>
											  </div>
											  <?php } ?>
											  
											  
											  
											</div>
										  <?php if(!empty($section_fields)) { 
										  
											
										  ?>
										  <div class="section_fields hide">
											  <?php 
											   global $single_field,$content_type;
												
												foreach($section_fields as $k => $single_field){
													$content_type = $content_section_key.'_'.$ds_count;
													
													if(isset($single_field['name']) && isset(${$single_field['name']}) && $has_val_saved)
													{
														global $meta_content;
														$meta_content[$single_field['name']] = ${$single_field['name']};
														
													}
													else if(!isset($meta_content))
													{
														global $meta_content;
														$meta_content = array();
													}
													
													if(isset($single_field['id']) && $single_field['id'] == 'video_section_for_lang')
													{
														$property_lang_option = array();
														
														if(isset($site_language) && !empty($site_language))
														{ 
															$site_language_array = json_decode($site_language,true);
															if(!empty($site_language_array)) 
															{
																$property_lang_option['all'] = 'All Languages';
																foreach($site_language_array as $aak=>$aav)
																{
																	if($aav['language'] == $default_language)
																	{
																		$new_value = $site_language_array[$aak];
																		unset($site_language_array[$aak]);
																		array_unshift($site_language_array, $new_value);
																		break;
																	}
																}
																
																foreach($site_language_array as $k=>$v) 
																{
																	if($v['status'] != 'enable')
																		continue;
																	$lang_exp = explode('~',$v['language']);
																	$lang_code = $lang_exp[1];
																	$lang_title = $lang_exp[0];
																	
																	$property_lang_option[$lang_code] = $lang_title;
																}
															}
														}
														$single_field['default'] = 'all';
														$single_field['options'] = $property_lang_option;
													}
													
													if(isset($single_field['id']))
													{
														$single_field['id'] = $single_field['id'].'_'.$ds_count;
													}
													
													
														
													$this->load->view("$theme/templates/templ-".$single_field['type'] ); 
												}
											  ?>
										  </div>
										  <?php } ?>
										</li>
								<?php
									$dynamic_video_section = ob_get_clean();
									//$ds_count--;
									continue;
								}
								
								/*
								echo '<pre>';
								print_r($section_fields);
								echo '</pre>';
								*/
							?>
							
							<li class="<?php echo $section_cls; ?>
								<?php 
								if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key != 'properties_section')
								{
									$ds_count++;
									echo 'dynamic_section de_'.$ds_count;
								} 
								?> "
								
							>
								<?php 
								if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key != 'properties_section')
								{
								?>
									<input type="hidden" value="dynamic" name="<?php echo $content_section_key; ?>[section_type]">
								<?php } ?>
								
								<?php 
								if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'video' && $content_section_key != 'video_section')
								{
								?>
									<input type="hidden" value="video" name="<?php echo $content_section_key; ?>[section_type]">
								<?php } ?>
								
								<div class="header-block">
								  <span class="handle ui-sortable-handle">
									<i class="fa fa-ellipsis-v"></i>
									<i class="fa fa-ellipsis-v"></i>
								  </span>
								  <span class="text"><?php echo (!empty($ds_heading) ? $ds_heading.' - ' : ''); ?><?php echo mlx_get_lang(ucfirst($content_section_value['title'])); ?></span>
								  
								   <div class="radio_toggle_wrapper pull-right">
									<input type="radio" 
									<?php if((isset($is_enable) && $is_enable == 'Y') || !isset($is_enable)) { ?>
									checked="checked" 
									<?php } ?>
									id="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_enable" value="Y" 
									name="<?php echo $content_section_key; ?>[is_enable]" class="toggle-radio-button">
									<label for="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_enable"><?php echo mlx_get_lang('Enable'); ?></label>
									
									<input type="radio" id="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_disable" value="N" 
									<?php if(isset($is_enable) && $is_enable == 'N') { ?>
									checked="checked" 
									<?php } ?>
									name="<?php echo $content_section_key; ?>[is_enable]" class="toggle-radio-button">
									<label for="<?php echo $content_section_key; ?>_<?php echo $ds_count; ?>_disable"><?php echo mlx_get_lang('Disable'); ?></label>
								  </div>
							  
								  <?php if(!empty($section_fields)) { ?>
								  <div class="tools">
									<button class="btn btn-box-tool collapsed" ><i class="fa fa-chevron-down"></i></button>
									
									<?php if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key != 'properties_section')
									{
									?>
										<button class="btn btn-danger btn-sm remove_ds_btn" title="<?php echo mlx_get_lang('Remove Section'); ?>" data-toggle="tooltip"><i class="fa fa-trash fa-2x"></i></button>
										<button class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> btn-sm clone_section_btn" title="<?php echo mlx_get_lang('Clone Section'); ?>" data-toggle="tooltip"><i class="fa fa-clone fa-2x"></i></button>
									<?php } ?>
									<?php if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'video' && $content_section_key != 'video_section')
									{
									?>
										<button class="btn btn-danger btn-sm remove_ds_btn" title="<?php echo mlx_get_lang('Remove Section'); ?>" data-toggle="tooltip"><i class="fa fa-trash fa-2x"></i></button>
										<button class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> btn-sm clone_section_btn" title="<?php echo mlx_get_lang('Clone Section'); ?>" data-toggle="tooltip"><i class="fa fa-clone fa-2x"></i></button>
									<?php } ?>
									
									<!--
									<?php if($content_section_key != 'slider_section' && $content_section_key != 'search_section') { ?>
										<button class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> btn-sm clone_section_btn" title="<?php echo mlx_get_lang('Clone Section'); ?>" data-toggle="tooltip"><i class="fa fa-clone fa-2x"></i></button>
									<?php } ?>
									-->
									
								  </div>
								  <?php } ?>
								</div>
								
							<?php if(!empty($section_fields)) { ?>
							  <div class="section_fields hide">
								  <?php 
								   global $single_field,$content_type;
									$content_type = $content_section_key;
									
									foreach($section_fields as $k => $single_field){
										
										
										if(isset($single_field['name']) && isset(${$single_field['name']}) && $has_val_saved)
										{
											global $meta_content;
											$meta_content[$single_field['name']] = ${$single_field['name']};
										}
										else if(!isset($meta_content))
										{
											global $meta_content;
											$meta_content = array();
										}
										
									
										if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'dynamic' && $content_section_key != 'properties_section')
										{
											if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_type')
											{
												$property_type_option = array();
												$property_type_option['all'] = 'All Types';
												$property_type_result = $this->Common_model->commonQuery("select * from property_types where status = 'Y' order by title ASC");
												if($property_type_result->num_rows() > 0)
												{
													foreach($property_type_result->result() as $pt_row)
													{
														$property_type_option[$pt_row->pt_id] = $pt_row->title;
													}
												}
												$single_field['default'] = 'all';
												$single_field['options'] = $property_type_option;
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_for_lang')
											{
												$property_lang_option = array();
												
												if(isset($site_language) && !empty($site_language))
												{ 
													$site_language_array = json_decode($site_language,true);
													if(!empty($site_language_array)) 
													{
														$property_lang_option['all'] = 'All Languages';
														foreach($site_language_array as $aak=>$aav)
														{
															if($aav['language'] == $default_language)
															{
																$new_value = $site_language_array[$aak];
																unset($site_language_array[$aak]);
																array_unshift($site_language_array, $new_value);
																break;
															}
														}
														foreach($site_language_array as $k=>$v) 
														{
															if($v['status'] != 'enable')
																continue;
															$lang_exp = explode('~',$v['language']);
															$lang_code = $lang_exp[1];
															$lang_title = $lang_exp[0];
															
															$property_lang_option[$lang_code] = $lang_title;
														}
													}
												}
												$single_field['default'] = 'all';
												$single_field['options'] = $property_lang_option;
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_country')
											{
												
												$country_code = 'all';
												$property_country_option = array();
												$property_country_option['all'] = 'All Countries';
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all')
												{
													$pfl = $meta_content['property_for_lang'];
													
													$lc_val = $this->global_lib->get_option('language_country_'.$pfl);
													
													if(!empty($lc_val))
													{
														$exp_lc_val = explode(',',$lc_val);
														
														foreach($exp_lc_val as $cc)
														{
															if(isset($location_array['countries'][$cc]))
															{
																$country_name = $location_array['countries'][$cc]['loc_title'];
																$country_code = $cc;
																if(isset($location_array['countries'][$country_code]))
																{
																	$property_country_option[mlx_get_norm_string($country_name).'~'.$country_code] = array(
																																'title' => $country_name,
																																'attributes' => array(
																																						'country_code' => $country_code,
																																					 ),
																															  );
																}
																
																
															}
														}
													}
													
												}
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] == 'all')
													$single_field['is_hidden'] = 'Y';
												
												$single_field['default'] = 'all';
												$single_field['options'] = $property_country_option;
												
												
												
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_state')
											{
												
												if(!$is_state_enable)
													continue;
												
												
												
												$state_code = 'all';
												$property_state_option = array();
												$property_state_option['all'] = 'All States';
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												isset($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$cc = $pce[1];
													$cn = $pce[0];
													
													if(!empty($cc) && isset($location_array['countries'][$cc]))
													{
														$country_name = $location_array['countries'][$cc]['loc_title'];
														
														if(isset($location_array['countries'][$cc]['states']))
														{
															
															foreach($location_array['countries'][$cc]['states'] as $skey=>$sval)
															{
																if($skey != 'no_state')
																{
																	$property_state_option[mlx_get_norm_string($sval['loc_title']).'~'.$skey] = array(
																														'title' => $sval['loc_title'],
																														'attributes' => array(
																																				'country_code' => $cc,
																																				'state_code' => $skey,
																																			 ),
																													  );
																	
																}
															}
														}
													}
													
													
												}
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] == 'all')
													$single_field['is_hidden'] = 'Y';
												$single_field['default'] = 'all';
												$single_field['options'] = $property_state_option;
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_city')
											{
												if(!$is_city_enable)
													continue;
												
												
												$state_code = 'all';
												$property_city_option = array();
												$property_city_option['all'] = 'All Cities';
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												isset($meta_content['property_state']) && !empty($meta_content['property_state']) && $meta_content['property_state'] != 'all' && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													$sc_exp = explode('~',$meta_content['property_state']);
													if(count($sc_exp) > 1)
														$state_code = $sc_exp[1];
													else
														$state_code = $sc_exp[0];
													$pfl = $meta_content['property_for_lang'];
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													if($is_state_enable)
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states'][$state_code]['cities']))
														{
															foreach($location_array['countries'][$lc_val]['states'][$state_code]['cities'] as $skey=>$sval)
															{
																$property_city_option[mlx_get_norm_string($sval['loc_title']).'~'.$skey] = array(
																														'title' => $sval['loc_title'],
																														'attributes' => array(
																																				'country_code' => $lc_val,
																																				'state_code' => $state_code,
																																				'city_code' => $skey
																																			 ),
																													  );
																	
																
															}
														}
													}
													else
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities']))
														{
															foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'] as $skey=>$sval)
															{
																$property_city_option[mlx_get_norm_string($sval['loc_title']).'~'.$skey] = array(
																														'title' => $sval['loc_title'],
																														'attributes' => array(
																																				'country_code' => $lc_val,
																																				'state_code' => 'no_state',
																																				'city_code' => $skey
																																			 ),
																													  );
																	
																
															}
														}
													}
												}
												else if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												!$is_state_enable && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													$pfl = $meta_content['property_for_lang'];
													
													
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													
													if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities']))
													{
														foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'] as $skey=>$sval)
														{
															$property_city_option[mlx_get_norm_string($sval['loc_title']).'~'.$skey] = array(
																													'title' => $sval['loc_title'],
																													'attributes' => array(
																																			'country_code' => $lc_val,
																																			'state_code' => 'no_state',
																																			'city_code' => $skey
																																		 ),
																												  );
																
															
														}
													}
													
												}
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] == 'all')
													$single_field['is_hidden'] = 'Y';
												$single_field['default'] = 'all';
												$single_field['options'] = $property_city_option;
												
												
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_zipcode')
											{
												if(!$is_zipcode_enable)
													continue;
												
												$state_code = 'all';
												$property_zipcode_option = array();
												$property_zipcode_option['all'] = 'All Zipcodes';
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												isset($meta_content['property_state']) && !empty($meta_content['property_state'])  && $meta_content['property_state'] != 'all' && 
												isset($meta_content['property_city']) && !empty($meta_content['property_city'])  && $meta_content['property_city'] != 'all' && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													
													$sc_exp = explode('~',$meta_content['property_state']);
													if(count($sc_exp) > 1)
														$state_code = $sc_exp[1];
													else
														$state_code = $sc_exp[0];
													
													$cc_exp = explode('~',$meta_content['property_city']);
													if(count($cc_exp) > 1)
														$city_code = $cc_exp[1];
													else
														$city_code = $cc_exp[0];
													
													$pfl = $meta_content['property_for_lang'];
													
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													if($is_state_enable)
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code]['zipcodes']))
														{
															foreach($location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code]['zipcodes'] as $skey=>$sval)
															{
																$property_zipcode_option[$sval] = $sval;
															}
														}
													}
													else
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['zipcodes']))
														{
															foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['zipcodes'] as $skey=>$sval)
															{
																$property_zipcode_option[$sval] = $sval;
															}
														}
													}
												}
												else if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												!$is_state_enable && 
												isset($meta_content['property_city']) && !empty($meta_content['property_city'])  && $meta_content['property_city'] != 'all' && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													$cc_exp = explode('~',$meta_content['property_city']);
													if(count($cc_exp) > 1)
														$city_code = $cc_exp[1];
													else
														$city_code = $cc_exp[0];
													
													$pfl = $meta_content['property_for_lang'];
													
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['zipcodes']))
													{
														foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['zipcodes'] as $skey=>$sval)
														{
															$property_zipcode_option[$sval] = $sval;
														}
													}
													
												}
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] == 'all')
													$single_field['is_hidden'] = 'Y';
												$single_field['default'] = 'all';
												$single_field['options'] = $property_zipcode_option;
											}
											else if(isset($single_field['id']) && $single_field['id'] == 'dynamic_property_sub_area')
											{
												if(!$is_subarea_enable)
													continue;
												
												$property_subarea_option = array();
												$property_subarea_option['all'] = 'All Subareas';
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												isset($meta_content['property_state']) && !empty($meta_content['property_state'])  && $meta_content['property_state'] != 'all' && 
												isset($meta_content['property_city']) && !empty($meta_content['property_city'])  && $meta_content['property_city'] != 'all' && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													
													$sc_exp = explode('~',$meta_content['property_state']);
													if(count($sc_exp) > 1)
														$state_code = $sc_exp[1];
													else
														$state_code = $sc_exp[0];
													
													$cc_exp = explode('~',$meta_content['property_city']);
													if(count($cc_exp) > 1)
														$city_code = $cc_exp[1];
													else
														$city_code = $cc_exp[0];
													
													$pfl = $meta_content['property_for_lang'];
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													if($is_state_enable)
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code]['sub_areas']))
														{
															foreach($location_array['countries'][$lc_val]['states'][$state_code]['cities'][$city_code]['sub_areas'] as $skey=>$sval)
															{
																$property_subarea_option[$sval] = $sval;
															}
														}
													}
													else
													{
														if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['sub_areas']))
														{
															foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['sub_areas'] as $skey=>$sval)
															{
																$property_subarea_option[$sval] = $sval;
															}
														}
													}
												}
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] != 'all' && 
												!$is_state_enable && 
												isset($meta_content['property_city']) && !empty($meta_content['property_city'])  && $meta_content['property_city'] != 'all' && 
												isset($meta_content['property_country']) && !empty($meta_content['property_country']) && $meta_content['property_country'] != 'all')
												{
													
													
													$cc_exp = explode('~',$meta_content['property_city']);
													if(count($cc_exp) > 1)
														$city_code = $cc_exp[1];
													else
														$city_code = $cc_exp[0];
													
													$pfl = $meta_content['property_for_lang'];
													
													$pc = $meta_content['property_country'];
													$pce = explode('~',$pc);
													$lc_val = $cc = $pce[1];
													$cn = $pce[0];
													
													
													if(!empty($lc_val) && isset($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['sub_areas']))
													{
														foreach($location_array['countries'][$lc_val]['states']['no_state']['cities'][$city_code]['sub_areas'] as $skey=>$sval)
														{
															$property_subarea_option[$sval] = $sval;
														}
													}
													
												}
												
												if(isset($meta_content['property_for_lang']) && $meta_content['property_for_lang'] == 'all')
													$single_field['is_hidden'] = 'Y';
												$single_field['default'] = 'all';
												$single_field['options'] = $property_subarea_option;
											}
											
										}
										if(isset($content_section_value['section_type']) && $content_section_value['section_type'] == 'video' && $content_section_key != 'video_section')
										{
											
											if(isset($single_field['id']) && $single_field['id'] == 'video_section_for_lang')
											{
												$property_lang_option = array();
												
												if(isset($site_language) && !empty($site_language))
												{ 
													$site_language_array = json_decode($site_language,true);
													if(!empty($site_language_array)) 
													{
														$property_lang_option['all'] = 'All Languages';
														foreach($site_language_array as $aak=>$aav)
														{
															if($aav['language'] == $default_language)
															{
																$new_value = $site_language_array[$aak];
																unset($site_language_array[$aak]);
																array_unshift($site_language_array, $new_value);
																break;
															}
														}
														
														foreach($site_language_array as $k=>$v) 
														{
															if($v['status'] != 'enable')
																continue;
															$lang_exp = explode('~',$v['language']);
															$lang_code = $lang_exp[1];
															$lang_title = $lang_exp[0];
															
															$property_lang_option[$lang_code] = $lang_title;
														}
													}
												}
												$single_field['default'] = 'all';
												$single_field['options'] = $property_lang_option;
											}
										}
										
										if(isset($single_field['id']))
											$single_field['id'] = $single_field['id'].'_'.$ds_count;
										
										$this->load->view("$theme/templates/templ-".$single_field['type'] ); 
									}
								  ?>
							  </div>
							  <?php } ?>
							</li>
						<?php } ?>
					  </ul>
					  <?php } ?>
					  
					  
					
		
					</div>
					<div class="box-footer">
						<button type="button" class="btn btn-default properties-section click" data-section="properties-section" >
							<?php echo mlx_get_lang('Add Properties Section'); ?></button>
						<button type="button" class="btn btn-default videos-section video_click" data-section="video-section" >
							<?php echo mlx_get_lang('Add Video Section'); ?></button>
						
						<button type="button" class="btn btn-default videos-section custom_html_js_click" data-section="custom-html-js-section" >
							<?php echo mlx_get_lang('Add Custom HTML/JS Code Section'); ?></button>
						
						 <button type="submit" name="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right submit-section-btn"><?php echo mlx_get_lang('Save'); ?></button>
					  </div>
				  </div>
			</div>
		
		</div>
		</form>
		
		<ul id="dyna-sections" style="display:none;">	
			<?php /*$this->load->view("default/homepage-sections/properties-section");*/ ?>
			<?php echo $dynamic_section; ?>
		</ul>
		
		<ul id="dyna-video-sections" style="display:none;">
			<?php echo $dynamic_video_section; ?>
		</ul>
			<?php $this->load->view("default/homepage-sections/custom-html-js-section"); ?>
	</section>
	
<style>
.clone_section_btn, .remove_ds_btn {
    margin-left: 10px;
}
</style>	
<script type="text/javascript">
	
	function update_homepage_dynamic_sections(content)
	{
		
		var row_count = 0;//parseInt($('.todo-list .dynamic_section').length)+1;
		
		var count = 1;
		while (count > 0) {
			var cpp = count+1;
			
		   if(!$('.todo-list .section_no_'+count).length && !$('.todo-list .de_'+count).length)
		   {
			   row_count = count;
			   break;
		   }
		   count++;
		}
		
		content.attr('class','dynamic_section section_no_'+row_count);
		
		content.find('.minimal').iCheck('destroy');	
		
		//content.find('.select2-container').hide();
		
		if($('.todo-list .dynamic_section').length >= 2)
		{
			/*
			content.find('select.dynamic_property_for_lang_country').select2({
				width:'100%'
			});
			content.find('select.dynamic_property_for_lang_state').select2({
				width:'100%'
			});
			content.find('select.dynamic_property_for_lang_city').select2({
				width:'100%'
			});
			content.find('select.dynamic_property_for_lang_zipcode').select2({
				width:'100%'
			});
			content.find('select.dynamic_property_for_lang_sub_area').select2({
				width:'100%'
			});
			*/
		}
		
		/*
		if(content.find('.select2_elem').length)
		{
			content.find('.select2_elem').each(function(e) {
				$(this).select2('destroy');
			});
		}
		*/
		
		
		if(content.find('select').length)
		{
			content.find('select').attr("id", content.find('select').attr("id").replace(/\d+/, row_count));
			content.find('select').attr("name", content.find('select').attr("name").replace(/\d+/, row_count));
		}
		if(content.find('input[type="text"]').length)
		{
			content.find('input[type="text"]').each(function() {
				$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('input[type="url"]').length)
		{
			content.find('input[type="url"]').each(function() {
				$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('input[type="radio"]').length)
		{
			content.find('.radio_toggle_wrapper input[type="radio"]').each(function() {
				$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('input[type="checkbox"]').length)
		{
			content.find('input[type="checkbox"]').each(function() {
				$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('input[type="number"]').length)
		{
			content.find('input[type="number"]').each(function() {
				$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('input[type="hidden"]').length)
		{
			content.find('input[type="hidden"]').each(function() {
				$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			});
		}
		if(content.find('label').length)
		{
			if(content.find('label').length == 1)
			{
				content.find('label').attr("for", content.find('label').attr("for").replace(/\d+/, row_count));
			}
			else
			{
				content.find('.radio_toggle_wrapper label').each(function() {
					$(this).attr("for", $(this).attr("for").replace(/\d+/, row_count));
				});
				
				content.find('label').each(function() {
					$(this).attr("for", $(this).attr("for").replace(/\d+/, row_count));
				});
			}
			
		}
		
		content.find('.inputtext').each(function(e) {
			$(this).attr("id", $(this).attr("id").replace(/\d+/, row_count));
			$(this).attr("name", $(this).attr("name").replace(/\d+/, row_count));
			
		});
		
		//content.find('.radio_toggle_wrapper input[type="radio"]:checked').next('label').trigger('click');
		content.find('.radio_toggle_wrapper input[type="radio"]:checked').each(function(e) {
			$(this).next('label').trigger('click');
		});
		
		content.find('.minimal').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue'
		});
		/*
		if($('.todo-list .dynamic_section').length > 2)
		{
			content.find('.select2_elem').each(function(e) {
				$(this).select2({
				  width : '100%'
				});
			});
		}
		*/
		/*
		$('select.dynamic_property_for_lang_country').select2({
			  width : '100%'
			});
		$('select.dynamic_property_for_lang_state').select2({
			  width : '100%'
			});
		$('select.dynamic_property_for_lang_city').select2({
			  width : '100%'
			});
		$('select.dynamic_property_for_lang_zipcode').select2({
			  width : '100%'
			});
		$('select.dynamic_property_for_lang_sub_area').select2({
			  width : '100%'
			});
		*/
		
		return content;	
	}
	
	jQuery("document").ready(function($){
		
		$('.select2_elem').each(function(e) {
			$(this).select2('destroy');
		});
		
		$('.todo-list .select2_elem').each(function(e) {
			$(this).select2({
			  width : '100%'
			});
		});
		
		$(".remove_ds_btn").on('click',function(){
			if(confirm('Do you really want to remove this section?'))
			{
				var thiss = $(this);
				thiss.parents('li').remove();
			}
			return false;
		});
		
		$(".click").on('click',function(){
			var content = $("#dyna-sections li" ).clone(true);
			content = update_homepage_dynamic_sections(content);
			content.find('.dynamic_property_for_lang_country').parents('.form-group').hide();
			content.find('.dynamic_property_for_lang_state').parents('.form-group').hide();
			content.find('.dynamic_property_for_lang_city').parents('.form-group').hide();
			content.find('.dynamic_property_for_lang_zipcode').parents('.form-group').hide();
			content.find('.dynamic_property_for_lang_sub_area').parents('.form-group').hide();
			$('.todo-list').append(content);
			return false;
		});
		
		
		$(".video_click").on('click',function(){
			var content = $("#dyna-video-sections li" ).clone(true);
			content = update_homepage_dynamic_sections(content);
			$('.todo-list').append(content);
			return false;
		});
		
		$('.parent_hidden_elem').parents('.form-group').hide().removeClass('parent_hidden_elem');
		
		
		if($('.dynamic_property_for_lang_opt:checked').length)
		{
			$('.dynamic_property_for_lang_opt:checked').each(function() {
				var thiss = $(this);
				if(thiss.val() != 'all')
				{
					thiss.parents('li').find('.dynamic_property_for_lang_country').parents('.form-group').show();
					thiss.parents('li').find('.dynamic_property_for_lang_state').parents('.form-group').show();
					thiss.parents('li').find('.dynamic_property_for_lang_city').parents('.form-group').show();
					thiss.parents('li').find('.dynamic_property_for_lang_zipcode').parents('.form-group').show();
					thiss.parents('li').find('.dynamic_property_for_lang_sub_area').parents('.form-group').show();
				}
			});
		}
		
		$('.dynamic_property_for_lang_opt').on('change',function() {
			var thiss = $(this);
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_country').parents('.form-group').hide();
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').parents('.form-group').hide();
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').parents('.form-group').hide();
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').parents('.form-group').hide();
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').parents('.form-group').hide();
			
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html('');
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html('<option value="all">All Zipcodes</option>').trigger('change');
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html('<option value="all">All Subareas</option>').trigger('change');
			
			if(thiss.val() != 'all')
			{
				
				$('.full_sreeen_overlay').show();
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_country').parents('.form-group').show();
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').parents('.form-group').show();
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').parents('.form-group').show();
				
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').parents('.form-group').show();
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').parents('.form-group').show();
				
				$.ajax({						
					url: base_url+'ajax/get_country_list_by_lang_callback_func',						
					type: 'POST',						
					success: function (res) 
					{		
						thiss.parents('.section_fields').find('.dynamic_property_for_lang_country').html(res.country_list).val('all').trigger('change');
						thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').html(res.state_list).val('all').trigger('change');
						thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html(res.city_list).val('all').trigger('change');
						thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html(res.zipcode_list).val('all').trigger('change');
						thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html(res.subarea_list).val('all').trigger('change');
						$('.full_sreeen_overlay').hide();
					},						
					data: {	lang_code : thiss.val() },						
					cache: false					
				});
				
			}
		});
		
		$('.dynamic_property_for_lang_country').on('change',function() {
			var thiss = $(this);
			var lang_code = thiss.parents('.section_fields').find('.dynamic_property_for_lang_opt:checked').val();
			
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').html('<option value="all">All States</option>').trigger('change');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html('<option value="all">All Cities</option>').trigger('change');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html('<option value="all">All Zipcodes</option>').trigger('change');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html('<option value="all">All Subareas</option>').trigger('change');
			
			
			if(thiss.val() != 'all' && (thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').length || 
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length))
			{
				
				$('.full_sreeen_overlay').show();
				var country_code = thiss.select2().find(":selected").data("country_code");
				
				$.ajax({						
					url: base_url+'ajax/get_state_or_city_list_by_lang_callback_func',						
					type: 'POST',						
					success: function (res) 
					{		
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_state').html(res.state_list).val('all').trigger('change');;
						
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html(res.city_list).val('all').trigger('change');;
						
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html(res.zipcode_list).val('all').trigger('change');;
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html(res.subarea_list).val('all').trigger('change');;
						$('.full_sreeen_overlay').hide();
					},						
					data: {	lang_code: lang_code, country_code : country_code },						
					cache: false					
				});
				
			}
		});
		
		$('.dynamic_property_for_lang_state').on('change',function() {
			var thiss = $(this);
			var lang_code = thiss.parents('.section_fields').find('.dynamic_property_for_lang_opt:checked').val();
			
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html('<option value="all">All Cities</option>');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html('<option value="all">All Zipcodes</option>').trigger('change');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html('<option value="all">All Subareas</option>').trigger('change');
			
			if(thiss.val() != 'all' && thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length)
			{
				$('.full_sreeen_overlay').show();
				var country_code = thiss.select2().find(":selected").data("country_code");
				var state_code = thiss.select2().find(":selected").data("state_code");
				
				$.ajax({						
					url: base_url+'ajax/get_city_list_from_state_by_lang_callback_func',						
					type: 'POST',						
					success: function (res) 
					{		
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_city').html(res.city_list).val('all').trigger('change');
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html(res.zipcode_list).val('all').trigger('change');
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html(res.subarea_list).val('all').trigger('change');
						$('.full_sreeen_overlay').hide();
					},						
					data: {	lang_code: lang_code, country_code : country_code, state_code : state_code },						
					cache: false					
				});
				
			}
		});
		
		$('.dynamic_property_for_lang_city').on('change',function() {
			var thiss = $(this);
			var lang_code = thiss.parents('.section_fields').find('.dynamic_property_for_lang_opt:checked').val();
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html('<option value="all">All Zipcodes</option>').trigger('change');
			if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
				thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html('<option value="all">All Subareas</option>').trigger('change');
			
			if(thiss.val() != 'all' && (
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length || 
			thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length))
			{
				$('.full_sreeen_overlay').show();
				var country_code = thiss.select2().find(":selected").data("country_code");
				var state_code = thiss.select2().find(":selected").data("state_code");
				var city_code = thiss.select2().find(":selected").data("city_code");
				$.ajax({						
					url: base_url+'ajax/get_zip_subarea_list_from_city_by_lang_callback_func',						
					type: 'POST',						
					success: function (res) 
					{		
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_zipcode').html(res.zipcode_list).val('all').trigger('change');
						if(thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').length)
							thiss.parents('.section_fields').find('.dynamic_property_for_lang_sub_area').html(res.subarea_list).val('all').trigger('change');
						$('.full_sreeen_overlay').hide();
					},						
					data: {	lang_code: lang_code, country_code : country_code, state_code : state_code , city_code : city_code},						
					cache: false					
				});
				
			}
		});
		
		
		$('.clone_section_btn').on('click',function() {
			var thiss = $(this);
			thiss.tooltip("hide");
			
			if($('.todo-list .dynamic_section').length == 1)
			{
				
				$('select.dynamic_property_for_lang_country').select2('destroy');
				$('select.dynamic_property_for_lang_state').select2('destroy');
				$('select.dynamic_property_for_lang_city').select2('destroy');
				$('select.dynamic_property_for_lang_zipcode').select2('destroy');
				$('select.dynamic_property_for_lang_sub_area').select2('destroy');
			}
			else
			{
				/*
				$('select.select2_elem').each(function(e) {
					$(this).select2('destroy');
				});
				*/
			}
			
			var cloned_elem = thiss.parents('li').clone(true);
			cloned_elem = update_homepage_dynamic_sections(cloned_elem);
			thiss.parents('li').after(cloned_elem);
			
			if($('.todo-list .dynamic_section').length <= 2)
			{
				$('.select2_elem').each(function(e) {
					$(this).select2({
					  width : '100%'
					});
				});
			}
			return false;
		});
		
	});

</script>
	
	
</div>