  <?php $this->load->view("default/header-top");?>
  
  <?php $this->load->view("default/sidebar-left");?>
<style>
.box-widget{
	box-shadow: none;
}
.widget-user i {
	font-size: 3em;
	color: #fff;
}
.widget-user .widget-user-image {
    position: absolute;
    top: 73px;
    left: 50%;
    margin-left: 0%;
    transform: translateX(-50%);
    padding: 18px;
    border: 3px solid #fff;
    border-radius: 50%;
	width: 90px;
	text-align: center;
}
.widget-user .box-footer {
    padding-top: 45px;
	border: 1px solid #ddd;
	border-top: 1px none;
}

</style>

<?php 

	$plugin_list = array(
		'google_analytics'	=> array(
								'title' => 'Google Analytics',
								'description' => 'Tracking data from Google Analytics',
								'icon' => 'fa-bar-chart',
								'status' => 'N',
								'settings' => 'settings/general_settings#google_analytics_settings' 
							),
		'database_backup' 	=> array(
								'title' => 'Database Backup',
								'description' => 'Backup database',
								'icon' => 'fa-database',
								'status' => 'N',
								'settings' => 'settings/db_settings' 
							),
		'blog' 				=> array(
								'title' => 'Blog',
								'description' => 'Manage blogs and their categories',
								'icon' => 'fa-newspaper-o',
								'status' => 'N',
								'settings' => 'settings/general_settings#blog_settings' 
							),
		'google_recaptcha' 	=> array(
								'title' => 'Google reCAPTCHA',
								'description' => 'Captcha for higher security',
								'icon' => 'fa-google',
								'status' => 'N',
								'settings' => 'settings/general_settings#google_recaptcha_settings' 
							),
		'cookie_consent' 	=> array(
								'title' => 'Cookie Consent',
								'description' => 'Cookie Consent',
								'icon' => 'fa-info',
								'status' => 'N',
								'settings' => 'settings/general_settings#cookie_settings' 
							),
		'payment' 	=> array(
								'title' => 'Payment',
								'description' => 'Payment options',
								'icon' => 'fa-credit-card',
								'status' => 'N',
								'settings' => 'settings/general_settings#cookie_settings' 
							),
		'openstreetmap' 	=> array(
								'title' => 'Open Street Map',
								'description' => 'Map',
								'icon' => 'fa-map',
								'status' => 'N',
								'help' => '#' 
							),
		
		'document' 	=> array(
								'title' => 'Document',
								'description' => 'Property documents',
								'icon' => 'fa-file',
								'status' => 'N',
								'settings' => '#' 
							),
		
		'property_locations' 	=> array(
								'title' => 'Property Locations',
								'description' => 'Property Locations',
								'icon' => 'fa-location-arrow',
								'status' => 'N',
								'help' => '#' 
							),
		
		'google_map' 	=> array(
								'title' => 'Google Map',
								'description' => 'Google Map',
								'icon' => 'fa-map-marker',
								'status' => 'N',
								'settings' => 'settings/general_settings#google_map_settings' 
							),
		
	);
?>
<script>
$(document).ready(function() {
	
	$('.change-status-btn').click(function() {
		var thiss = $(this);
		$(this).parents('.box-widget').find('.overlay').show();
		var cur_status = $(this).attr('data-status');
		var plugin_name = $(this).parents('.box-widget').attr('data-p_name');
		$.ajax({						
			url: base_url+'ajax/update_plugins_setting_callback_func',						
			type: 'POST',						
			success: function (res) 
			{			
				if(res == 'success')
				{
					thiss.parents('.box-widget').find('.cur_status').val(cur_status);
					thiss.parents('.box-widget').find('.deactive-block,.active-block').hide();
					if(cur_status == 'Y')
					{
						thiss.parents('.box-widget').find('.deactive-block').show();
					}
					else
					{
						thiss.parents('.box-widget').find('.active-block').show();
					}
					setTimeout(function() {
						window.location.reload();
					},3000);
				}
				else
				{
					window.location.reload();
				}
				thiss.parents('.box-widget').find('.overlay').hide();
				
				
				
			},						
			data: {plugin_name : plugin_name, cur_status : cur_status},						
			cache: false					
		});	
		return false;
	});
});
</script>

<div class="content-wrapper">
	<section class="content-header">
	  <h1 class="page-title"><i class="fa fa-plug"></i> <?php echo mlx_get_lang('Manage Plugins'); ?> </h1>
	  <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
		?>
	</section>

	<section class="content">
		<div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
			<div class="box-body content-box">
				<div class="row">
					
					<?php 
					
					$skin_class = $myHelpers->global_lib->get_skin_class();
					if($skin_class == 'default')
						$skin_class = 'gray';
					else if($skin_class == 'success')
						$skin_class = 'green';
					else if($skin_class == 'danger')
						$skin_class = 'red';
					else if($skin_class == 'warning')
						$skin_class = 'yellow';
					else if($skin_class == 'primary')
						$skin_class = 'blue';
					
					
					
					if(isset($plugin_list) && !empty($plugin_list))
					{
						$site_plugins_json = $this->global_lib->get_option('site_plugins');
						if(!empty($site_plugins_json))
							$site_plugins = json_decode($site_plugins_json,true);
						
						foreach($plugin_list as $k=>$v)
						{
							if(isset($site_plugins) && (array_search($k, $site_plugins) !== false)) {
								$v['status'] = 'Y';
							}
						?>
							<div class="col-md-4">
							  <div class="box box-widget widget-user" data-p_name="<?php echo $k; ?>">
								<div class="overlay" style="display:none;">
								  <i class="fa fa-refresh fa-spin"></i>
								</div>
								<input type="hidden" class="cur_status" value="<?php echo $v['status']; ?>">
								<div class="widget-user-header bg-<?php echo $skin_class; ?>-active" >
								  <h3 class="widget-user-username text-center"><?php echo ucwords($v['title']); ?></h3>
								  <!--<h5 class="widget-user-desc text-center"><?php echo ucwords($v['description']); ?></h5>-->
								</div>
								<div class="widget-user-image bg-<?php echo $skin_class; ?>">
								  <i class="fa <?php echo $v['icon']; ?>"></i>
								</div>
								<div class="box-footer">
								  <div class="row">
									<div class="col-sm-12 text-center active-block" style="<?php if($v['status'] == 'Y' || empty($v['status'])) echo 'display:none;';?>">
										<button class="btn btn-success change-status-btn"  data-status="Y" style="width:50%;">Activate</button>
									</div>
									<div class="deactive-block" style="<?php if($v['status'] == 'N') echo 'display:none;';?>">
										<div class="col-sm-6" >
											<button class="btn btn-block btn-warning  change-status-btn" data-status="N">Deactivate</button>
										</div>
										<div class="col-sm-6">
											<?php if(isset($v['settings'])) { ?>
												<a href="<?php echo base_url(array($v['settings'])); ?>" class="btn btn-block btn-default">Settings</a>
											<?php }else if(isset($v['help'])) { ?>
												<a href="<?php echo base_url(array($v['help'])); ?>" class="btn btn-block btn-default">Help</a>
											<?php  } ?>
										</div>
									</div>
								  </div>
								</div>
							  </div>
							</div>
						<?php
						}
					}
					?>
					
				</div>
			</div>
		</div>
	</section>
</div>
   