				
<?php 

$countries = file_get_contents(base_url("../locations/json/countries.json"));	
$countries = json_decode($countries, true);


?>	
<div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
	  <div class="box-header with-border">
		  <h3 class="box-title"> 
		  <?php echo mlx_get_lang('Add Countries'); ?></h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
	  </div>
		<div class="box-body">
			  <label for="country"><?php echo mlx_get_lang('Select Countries'); ?> <span class="required">*</span></label>
			  <select name="country[]" multiple class="form-control no_clean select2_elem"   id="country" >
			  <option value=""><?php echo mlx_get_lang('Select Countries'); ?></option> 
			  <?php
			  foreach($countries as $country){
					echo '<option value="'.$country['countryCode'].'~'.$country['countryName'].'~'.$country['geonameId'].'">'.mlx_get_lang($country['countryName']).'</option>';	
				}
			  ?>
			  </select>
		</div>						
		<div class="box-footer">
			<button name="add_country" type="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right" id="add_country"><?php echo mlx_get_lang('Add'); ?></button>
		</div>
</div>
				
				