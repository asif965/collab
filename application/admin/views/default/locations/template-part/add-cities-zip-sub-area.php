<?php 

//global $loc_tax_settings;		
$locations = json_decode($locations, true);
$loc_tax_settings = json_decode($loc_tax_settings,true); 

?>
<div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>" id="city_zip_sub_area">
	  <div class="box-header with-border">
		  <h3 class="box-title"> 
		  <?php echo mlx_get_lang('Add Zipcode/Sub-Area'); ?></h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
	  </div>
		<div class="box-body">
			
			<div class="form-group">
			  <label for="city"><?php echo mlx_get_lang('City'); ?> </label>
			  <input type="text" name="" id="cnt_city_zip_sub_area" class="form-control" 
			  readonly placeholder="<?php echo mlx_get_lang('Select a city from left'); ?>" />
			  
			<input type="hidden" name="cnt_city_id_for_zip_sub_area" id="cnt_city_id_for_zip_sub_area" class="form-control"   />
			  
			  
			</div>
			
			<?php
			
			
			if($loc_tax_settings['zipcode']['enabled'] ){
			?>
			
			<div class="form-group">
			  <label for="zipcode"><?php echo mlx_get_lang('Zipcode'); ?> </label>
			  <input type="text" name="cnt_city_zipcode" id="cnt_city_zipcode" class="form-control" 
			  placeholder="<?php echo mlx_get_lang('Enter zipcode comma(,) separated'); ?>" />
			  
			</div>
			<?php } ?>
			
			<?php
			if( $loc_tax_settings['sub-area']['enabled']){
			?>
			<div class="form-group">
			  <label for="sub-area"><?php echo mlx_get_lang('Sub-Area'); ?> </label>
			  <input type="text" name="cnt_city_sub_area" id="cnt_city_sub_area" 
			  class="form-control"  auto-complete="off"
			  placeholder="<?php echo mlx_get_lang('Enter Sub-Area comma(,) separated'); ?>" />
			  
			</div>
			<?php } ?>
			
		</div>						
		<div class="box-footer">
		
		
		
		<button name="add_city_zip_sub_area" type="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right" id="add_city_zip_sub_area"><?php echo mlx_get_lang('Add'); ?></button>
		</div>
</div>
			
<script>

jQuery(document).ready(function($)
{
	
	var base_url = '<?php echo base_url();?>';	

	$(".add-city-zip-sub-area").on('click',function() {
	
	var thiss = $(this);
	var city_id = thiss.attr("data-city_id");
	
	var city_title = thiss.attr("data-city_title");
	$("#cnt_city_zip_sub_area").val(city_title);
	
	$("#cnt_city_id_for_zip_sub_area").val(city_id);
	
	var header_height = $('header.main-header').outerHeight() + 15;
		$('html, body').animate({
			scrollTop: $('#city_zip_sub_area').offset().top - header_height 
		}, 1000);
	
	if($('#cnt_city_zipcode').length)
	{
		$('#cnt_city_zipcode').focus();
	}
	else if($('#cnt_city_sub_area').length)
	{
		$('#cnt_city_sub_area').focus();
	}
		
	/*if(city_id){
		$.ajax({						
			url: base_url+'ajax_locations/get_cities_from_citys',						
			type: 'POST',						
			success: function (res) 
			{		
				if(res != 'cities not found')
					$("#cnt-city-city").html(res);
					
					
			},						
			data: {	city_id : city_id},						
			cache: false					
		});	
		
	}*/
	
	return false;
});



});

</script>			