<?php $this->load->view("default/header-top");?>

<?php $this->load->view("default/sidebar-left");?>


<div class="content-wrapper">
<section class="content-header">
  <h1 class="page-title"><!--<i class="fa fa-cc-stripe"></i> --> <?php echo mlx_get_lang('Razorpay CheckOut'); ?>  
  </h1>

  <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
	?> 
</section>

<section class="content">
	<?php 
	
	$attributes = array('name' => 'add_form_post','class' => 'form add_package_form');		 			
	//echo form_open_multipart('packages/confirmation',$attributes); 
	?>
	
	<input type="hidden" name="user_id" class="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
	
	<div class="row">
	<div class="col-md-12">   
	   
	  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
		<div class="box-header with-border">
		  <h3 class="box-title"><?php echo mlx_get_lang('Package Confirmation'); ?></h3>
		  <div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		  </div>
		</div>
		  <div class="box-body">
						<input title="item_id" name="item_id" type="hidden" value="<?php echo $query->row()->package_id; ?>">
						<input title="item_name" name="item_name" type="hidden" value="<?php echo $query->row()->package_name; ?>">
                       
                        <input title="item_description" name="item_description" type="hidden" value="<?php echo $query->row()->package_type; ?>">
                        
                        <input title="item_price" name="item_price" type="hidden" value="<?php echo $query->row()->package_price; ?>">
						
						<input title="item_currency" name="item_currency" type="hidden" value="<?php echo $query->row()->package_currency; ?>">
                        
            <h4><?php echo mlx_get_lang('You have Selected'); ?> :- <?php echo $query->row()->package_name; ?></h4>

            <h5><?php echo mlx_get_lang('Price of Package'); ?> :- <?php echo $query->row()->package_currency .' '. $query->row()->package_price; ?></h5>

            <h6><?php echo mlx_get_lang('Package Type'); ?> :- <?php  if($query->row()->package_type == 'subscription') { echo $query->row()->package_type; } else{
                echo $query->row()->package_type;
            } ?></h6>
            <h6><?php echo mlx_get_lang('Package Duration'); ?> :- <?php  if($query->row()->package_life == '0 days') { echo 'Unlimited'; } else{
                echo $query->row()->package_life;
            } ?></h6>
							
			<button type="submit" name="submit" class="buy_now btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right submit-form-btn" 
				data-amount="<?php echo $query->row()->package_price * 100; ?>" 
				data-id="<?php echo $query->row()->package_id; ?>"
				data-name="<?php echo $query->row()->package_name; ?>" 
				data-desc="<?php //echo $query->row()->product_description; ?>"
				data-currency="<?php echo $query->row()->package_currency; ?>"
				data-order="<?php echo rand(1,99); ?>"
			id="save_publish"><?php echo mlx_get_lang('Pay Now'); ?></button>
		
		 </div>
		
	  </div>
</div>
 
	  
</section>
</div>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var SITEURL = "<?php echo base_url(); ?>";
  var Order_id;
  $('body').on('click', '.buy_now', function(e){
    var totalAmount = $(this).attr("data-amount");
    var product_id =  $(this).attr("data-id");
    var product_name =  $(this).attr("data-name");
    var product_des =  $(this).attr("data-desc");
	var currency =  $(this).attr("data-currency");
   
    $.ajax({
            url: SITEURL + 'razorpay/rp_createorder',
            type: 'post',
            dataType: 'json',
            data: {
                 totalAmount : totalAmount ,
				 currency : currency ,
            }, 
            success: function (res) {
              Order_id = res.order_id;
              var options = {
                    "key": res.api_key,
                    //"amount": (1*100), // 2000 paise = INR 20
                    "name": product_name,
                    "description": product_des,
                    "image": "",
                    "order_id": Order_id ,
                    "handler": function (response){
							/*console.log(response); return false;*/
                          $.ajax({
                            url: SITEURL + 'razorpay/rp_payment_success',
                            type: 'post',
                            //dataType: 'json',
                            data: {
                                rp_payment_id: response.razorpay_payment_id , 
								rp_response :response,
								totalAmount : totalAmount ,
								package_id : product_id,
                                rp_order_id: response.razorpay_order_id ,
								rp_signature: response.razorpay_signature,
                            }, 
                            success: function (res) {
								console.log(res);
								window.location.href = SITEURL + res.red_url;
                            },
							error : function (err1,err2,err3) {
								/*alert(err1+' 1 '+err2+' '+err3);*/
                            },
                        }); 
                    },
                    /*"prefill": {
                        "name": "Gaurav Kumar",
                        "email": "customer@merchant.com",
                        "contact": "9999999999"
                    },
                    "notes": {
                        "address": "Some Notes Goes here"
                    },*/
                    "theme": {
                        "color": "#28a745"
                    }
                  };
                  var rzp1 = new Razorpay(options);
                  rzp1.open();
            }
        }); 

  
  
  e.preventDefault();
  });
 
</script>