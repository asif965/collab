
<?php $this->load->view("default/header-top");?>

<?php $this->load->view("default/sidebar-left");?>


<div class="content-wrapper">
<section class="content-header">
  <h1 class="page-title"><i class="fa fa-building"></i>   
  

  <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
	?> 
</section>

<section class="content">

  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
	
	<div class="box-body content-box">
       
    <div class="contact-form">
        <div>
            <h2 >Dear Member</h2>
			<p>Your Order is <?php echo $result['state']; ?> and Amount Was <?php echo $result['total']; ?> and Tax Was 
			<?php echo $result['tax']; ?> at <?php echo $result['creation_time']; ?>.
			</p>
            <span >Your payment was successful, thank you for purchase.</span><br/>
        </div>
    </div>
		</div>

  </div>
</div>
</section>
</div>

