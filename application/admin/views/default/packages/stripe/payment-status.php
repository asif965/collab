
<?php $this->load->view("default/header-top");?>

<?php $this->load->view("default/sidebar-left");?>


<div class="content-wrapper">
<section class="content-header">
  <h1 class="page-title"><i class="fa fa-building"></i>   
  

  <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg']))
			{
				echo $_SESSION['msg'];
				unset($_SESSION['msg']);
			}
	?> 
</section>

<section class="content">

  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
	
	<div class="box-body content-box">

<?php if(!empty($order)){  
	
 ?>
 
    <!-- Display transaction status -->
    <?php if($order->status == 'Completed'){ 
		$product = json_decode($order->package_detail);
	?>
    <h1 class="success">Your Payment has been Successful!</h1>
    <?php }else{ ?>
    <h1 class="error">The transaction was successful! But your payment has been failed!</h1>
    <?php } ?>
	
    <h4>Payment Information</h4>
    <p><b>Transaction ID:</b> <?php echo $order->transaction_key; ?></p>
    <p><b>Paid Amount:</b> <?php echo $product->package_price.' '.$product->package_currency; ?></p>
    <p><b>Payment Status:</b> <?php echo $order->status; ?></p>
	
    <h4>Product Information</h4>
    <p><b>Name:</b> <?php echo $product->package_name; ?></p>
    <p><b>Price:</b> <?php echo $product->package_price.' '.$product->package_currency; ?></p>
<?php }else{ ?>
    <h1 class="error">The transaction has failed</h1>
<?php } ?>
</div>
</div>
</section>
</div>

