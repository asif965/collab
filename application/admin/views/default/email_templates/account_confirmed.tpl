Greetings from {website_title} Team!

Thanks for joining with us. Hope you will like our service.

Your account is confirmed, now you can continue with user panel, {admin_url}

Regards
{website_title} Team!

{front_url}