Congratulations! Your property {property_title} on {website_title} has been approved.

Here is the direct link  of your property {property_title_linkable} for direct access.

{property_title_linkable}

Thanks for your submission!

Regards,

{website_title} Team!

{front_url}