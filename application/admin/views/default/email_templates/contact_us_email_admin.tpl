Dear admin {website_title} a contact form submitted, here are the details:

Contact Name          : {contact_name}
Contact Email         : {contact_email}
Subject		          : {contact_subject}

Message:

{contact_message}

