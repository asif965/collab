Greetings from {website_title} Team!

Thanks for registering with us. Before you get started please activate your account by clicking on the link below
{account_confirmation_link}


After your account activation you will be able to continue with the user panel.

Regards
{website_title} Team!

{front_url}