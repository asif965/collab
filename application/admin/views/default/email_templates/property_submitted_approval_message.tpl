Hurrah, your property  {property_title} on {website_title} has been submitted for approval by admin. You will get notifications when your property get approved or status change.

Regards,
{website_title} Team!

{front_url}