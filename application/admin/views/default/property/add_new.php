<?php 
$user_type = $this->session->userdata('user_type');
$short_desc_limit = 250;
	

?>

<?php $this->load->view("default/header-top");?>
<?php $this->load->view("default/sidebar-left");?>
<style>
.custom_field_container .form-control ,
.variation_size_container .form-control {
    display: inline;
    width: 48%;
}
.custom_field_container .form-control:last-child {
	float:right;
}
</style>
<script>

	function RemoveRougeChar(convertString){
		/*var n = convertString.toString();
		//var newvalue = n.replace(/,/g, ''); 
		//var valuewithcomma = newvalue.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		//return valuewithcomma;*/ 
		return convertString;
	}
	
	$(document).ready(function() {
		
		/*
		$("#price").on("change",function(){
			var $this = $(this);
			var price = $(this).val().replace(/,/g,"");
			$this.val(price);
		}).on("focus",function(e){
			var $this = $(this);
			var num = $this.val().replace(/,/g,"");
			$this.val(num);
		}).on("blur", function(e){
			var $this = $(this);
			var num = $this.val();	
			var num2 = RemoveRougeChar(num);
			$this.val(num2);
		});
		*/
		
		$('.country_list').change(function() {
			var thiss = $(this);
			var country =  thiss.val();
			$('.state_list').html('');
			$('.city_list').html('');
			$('.zipcode_list').html('');
			$('.sub_area_list').html('');
			
			if(country != '')
			{
				
				var stateSplit = country.split('~');
				country = stateSplit[0];
				
				$('.full_sreeen_overlay').show();
				$('.state_list').html('<option value="">Loading...</option>');
				$.ajax({						
					url: base_url+'ajax_locations/get_states_from_countries',						
					type: 'POST',						
					success: function (res) 
					{		
						if(res != 'state not found')
							$('.state_list').html(res);
						$('.full_sreeen_overlay').hide();
							
					},						
					data: {	country : country},						
					cache: false					
				});
			}
			return false;
		}); 
		
		$('.state_list').change(function() 
		{
			var thiss = $(this);
			var state_id = thiss.val();
			var country_code = $('.country_list').val();
			$('.city_list').html('');
			$('.zipcode_list').html('');
			$('.sub_area_list').html('');
			
			if(state_id != '')
			{
				$('.full_sreeen_overlay').show();
				
				var stateSplit = country_code.split('~');
				country_code = stateSplit[0];
				
				var stateSplit = state_id.split('~');
				state_id = stateSplit[2];
				
				$('.city_list').html('<option value="">Loading...</option>');
				$.ajax({						
					url: base_url+'ajax_locations/get_cities_from_states',						
					type: 'POST',						
					success: function (res) 
					{		
						if(res != 'cities not found')
							$(".city_list").html(res);
						$('.full_sreeen_overlay').hide();	
					},						
					data: {	state_id : state_id, country_code : country_code},						
					cache: false					
				});	
				
			}
			return false;
		});
		
		$('.city_list').change(function() 
		{
			var thiss = $(this);
			var city_id = thiss.val();
			var country_code = $('.country_list').val();
			var state_id = $('.state_list').val();
			var stateSplit = state_id.split('~');
			state_id = stateSplit[0];
			
			$('.zipcode_list').html('');
			$('.sub_area_list').html('');
			
			if(city_id != '')
			{
				$('.full_sreeen_overlay').show();
				
				var stateSplit = country_code.split('~');
				country_code = stateSplit[0];
				
				var stateSplit = city_id.split('~');
				city_id = stateSplit[4];
				
				$('.zipcode_list').html('<option value="">Loading...</option>');
				$('.sub_area_list').html('<option value="">Loading...</option>');
				
				$.ajax({						
					url: base_url+'ajax_locations/get_zipcode_subarea_from_cities',						
					type: 'POST',						
					success: function (res) 
					{		
						$(".zipcode_list").html(res.zipcode_list);
						$(".sub_area_list").html(res.sub_area_list);
						$('.full_sreeen_overlay').hide();	
					},						
					data: {	state_id : state_id, country_code : country_code,city_id : city_id},						
					cache: false					
				});	
				
			}
			return false;
		});
		
	});
	
</script>

<script>
$(document).ready(function() {
	
	$('.loc_country_list').change(function() {
		var thiss = $(this);
		var country_code = thiss.find('option:selected').attr('data-country_code');
		
		if(thiss.parents('.location-fields').find('.loc_state_list').length)
			thiss.parents('.location-fields').find('.loc_state_list').html('<option value="">Select Any State</option>');//.select2("val", "");
		if(thiss.parents('.location-fields').find('.loc_city_list').length)
		{
			thiss.parents('.location-fields').find('.loc_city_list').html('<option value="">Select Any City</option>');//.select2("val", "");
		}
		if(thiss.parents('.location-fields').find('.zipcode_list').length)
			thiss.parents('.location-fields').find('.zipcode_list').html('<option value="">Select Any Zipcode</option>');//.select2("val", "");
		if(thiss.parents('.location-fields').find('.sub_area_list').length)
			thiss.parents('.location-fields').find('.sub_area_list').html('<option value="">Select Any Sub Area</option>');//.select2("val", "");
		
		
		if(country_code != '' && (thiss.parents('.location-fields').find('.loc_state_list').length || 
		thiss.parents('.location-fields').find('.loc_city_list').length))
		{
			$('.full_sreeen_overlay').show();
			$.ajax({						
				url: base_url+'ajax_locations/get_state_city_name_list_callback_func',						
				type: 'POST',						
				success: function (res) 
				{		
					if(thiss.parents('.location-fields').find('.loc_state_list').length)
						thiss.parents('.location-fields').find('.loc_state_list').html(res.state_list);
					if(thiss.parents('.location-fields').find('.loc_city_list').length)
						thiss.parents('.location-fields').find('.loc_city_list').html(res.city_list);
					$('.full_sreeen_overlay').hide();
				},						
				data: {	country_code : country_code},						
				cache: false					
			});
		}
		return false;
		
	});
	
	$('.loc_state_list').change(function() {
		var thiss = $(this);
		var country_code = thiss.find('option:selected').attr('data-country_code');
		var state_name = thiss.find('option:selected').attr('data-full_value');
		var state_code = thiss.find('option:selected').attr('data-state_code');
		
		if(thiss.parents('.location-fields').find('.loc_city_list').length)
			thiss.parents('.location-fields').find('.loc_city_list').html('<option value="">Select Any City</option>');//.select2("val", "");
		if(thiss.parents('.location-fields').find('.zipcode_list').length)
			thiss.parents('.location-fields').find('.zipcode_list').html('<option value="">Select Any Zipcode</option>');//.select2("val", "");
		if(thiss.parents('.location-fields').find('.sub_area_list').length)
			thiss.parents('.location-fields').find('.sub_area_list').html('<option value="">Select Any Sub Area</option>');//.select2("val", "");
		
		if(country_code != '' && state_code != '' && state_name != '' && thiss.parents('.location-fields').find('.loc_city_list').length)
		{
			
			$('.full_sreeen_overlay').show();
			$.ajax({						
				url: base_url+'ajax_locations/get_city_name_list_callback_func',						
				type: 'POST',						
				success: function (res) 
				{		
					thiss.parents('.location-fields').find('.loc_city_list').html(res);
					$('.full_sreeen_overlay').hide();
				},						
				data: {	country_code : country_code, state_code : state_code},						
				cache: false					
			});
		}
		return false;
		
	});
	
	$('.loc_city_list').change(function() {
		var thiss = $(this);
		var country_code = thiss.find('option:selected').attr('data-country_code');
		var state_code = thiss.find('option:selected').attr('data-state_code');
		var city_code = thiss.find('option:selected').attr('data-city_code');
		var city_name = thiss.attr('data-full_value');
		thiss.parents('.location-fields').find('.zipcode_list').html('<option value="">Select Any Zipcode</option>');//.select2("val", "");
		thiss.parents('.location-fields').find('.sub_area_list').html('<option value="">Select Any Sub Area</option>');//.select2("val", "");
		if(country_code != '' && state_code != '' && city_code != '' && city_name != '')
		{
			$('.full_sreeen_overlay').show();
			$.ajax({						
				url: base_url+'ajax_locations/get_zip_sub_area_name_list_callback_func',						
				type: 'POST',						
				success: function (res) 
				{		
					thiss.parents('.location-fields').find('.zipcode_list').html(res.zipcode_list);
					thiss.parents('.location-fields').find('.sub_area_list').html(res.subarea_list);
					$('.full_sreeen_overlay').hide();
				},						
				data: {	country_code : country_code, state_code : state_code, city_code : city_code},						
				cache: false					
			});
		}
		return false;
		
	});

});
</script>

<?php
$isDocAct = $myHelpers->isPluginActive('document');
if($isDocAct == true)
{

$document_file_type = $myHelpers->global_lib->get_option('document_file_type');
$file_accept_array = array();
$document_file_ext_array = array();

$file_accept_types = array('jpeg' => 'image/jpeg',
						   'jpg' => 'image/jpeg',
						   'png' => 'image/png',
						   'gif' => 'image/gif',
						   'pdf' => 'application/pdf',
						   'doc' => 'application/msword',
						   'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
						   'txt' => 'text/plain',
						   'xls' => 'application/vnd.ms-excel',
						   'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
						   'ppt' => 'application/vnd.ms-powerpoint',
						   'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
						);

if(isset($document_file_type) && !empty($document_file_type))
{
	$document_file_type_array = json_decode($document_file_type,true);
	
	if(count($document_file_type_array) > 0)
	{
		foreach($document_file_type_array as $k=>$v)
		{
			if(array_key_exists($v,$file_accept_types))
			{
				$file_accept_array[] = $file_accept_types[$v];
			}
			$f_exp = explode('~',$v);
			$document_file_ext_array[] = "'".$f_exp[0]."'";
		}
	}
}
$document_file_ext_string = implode(',',$document_file_ext_array);
$document_file_size = $this->global_lib->get_option('document_file_size');
if(empty($document_file_size) || !isset($document_file_size))
{
	$document_file_size = 2;
}
$file_size_limit = $document_file_size*pow(1024,2);
$file_accept_string = implode(', ',$file_accept_array);
?>
<script>
var id;
 $(document).ready(function () { 
		
		$('.document_uploader').on('change',function(){
			$('.full_sreeen_overlay').show();
			id = $(this).attr('id');
			var thiss = $(this);
			var image_type = $(this).attr('data-type');
			var data = new FormData();
			var unsupported_file = 0;
			var invalid_file = 0;
			var valid_file = 0;
			var files = $('#'+id).prop('files');
			
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				var ext = file.name.split('.').pop().toLowerCase();
				
				var file_size = file.size;
				<?php 
					if(!empty($document_file_ext_string)) { ?>
					if(jQuery.inArray(ext, [<?php echo $document_file_ext_string; ?>]) == -1) 
					{
						unsupported_file++;
					}
					else if(file_size > <?php echo $file_size_limit; ?> )
					{
						invalid_file++;
					}
					else
					{
						valid_file++;
						data.append('mFile[]', file, file.name);
					}
				<?php }else{ ?>
					if(file_size > <?php echo $file_size_limit; ?> )
					{
						invalid_file++;
					}
					else
					{
						valid_file++;
						data.append('mFile[]', file, file.name);
					}
				<?php } ?>
			}
			if(valid_file > 0)
			{
				$('#'+id+'_progress').show();
				data.append('user_type',image_type);
				$.ajax({
					url: '<?php echo site_url();?>/documents/upload_documents_callback_func',
					type: 'POST',
					data: data,
					cache: false,
					enctype: 'multipart/form-data',
					contentType: false,
					processData: false,
					xhr: function() {
						var myXhr = $.ajaxSettings.xhr();
						if(myXhr.upload){
							myXhr.upload.addEventListener('progress',progress, false);
						}
						return myXhr;
						
						
					},
					success: function (res) {
						$('.content-header .alert').remove();
						$('#'+id+'_progress').hide();
						res.invalid_file += invalid_file;
						if(res.invalid_file > 0)
						{
							var msg = '<div class="alert alert-danger alert-dismissable">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								res.invalid_file+' File Failed to Upload Due File Size Exceed to Uploaded Size Limit.'+
							'</div>';
							thiss.parents('.document-block').find('.box-body').prepend(msg);
						}
						if(res.upload_failed_file > 0)
						{
							var msg = '<div class="alert alert-danger alert-dismissable">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								res.upload_failed_file+' File Failed to Upload Due to an Error Occured While Uploading.'+
							'</div>';
							thiss.parents('.document-block').find('.box-body').prepend(msg);
						}
						res.unsupported_file += unsupported_file;
						if(res.unsupported_file > 0)
						{
							var msg = '<div class="alert alert-warning alert-dismissable">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								res.unsupported_file+' File Failed to Upload Due to Unsupported File Formats.'+
							'</div>';
							thiss.parents('.document-block').find('.box-body').prepend(msg);
						}
						if(res.valid_file > 0)
						{
							$.each(res.uploaded_image_array, function(k, v) {
								var output_string = '<div class="col-md-3 document_images">'+
								'<div class="document_images_inner" data-toggle="tooltip" title="" data-original-title="'+v.img_name+'">'+
									'<img src="'+v.thumb_url+'" width="100%" data-img_id="'+v.enc_att_id+'">'+
									'<a href="#" class="select-check remove_document_from_list" id="image_'+v.att_id+'" data-type="documents"'+ 
									'data-file_type="'+v.type+'"'+
									'data-att_id="'+v.att_id+'" data-name="image_'+v.att_id+'"><i class="fa fa-remove"></i></a>'+
									'<input type="hidden" name="" id="image_'+v.att_id+'_hidden" value="'+v.img_name+'">'+
								'</div>'+
								'</div>';
								thiss.parents('.document-block').find('.product-document-container').prepend(output_string);
								
								var old_img_data = $('#'+id+'_hidden').val();
								var new_img_data = v.enc_att_id;
								if(old_img_data != "")
								{
									new_img_data = old_img_data+','+v.enc_att_id;
								}
								$('#'+id+'_hidden').val(new_img_data);
							});
							var msg = '<div class="alert alert-success alert-dismissable">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								res.valid_file+' File Uploaded Successfully.'+
							'</div>';
							thiss.parents('.document-block').find('.box-body').prepend(msg);
						}
						
						$('.alert').delay(10000).fadeOut('slow');
						$('.full_sreeen_overlay').hide();
					},
					error: function(data){
						console.log("error");
						console.log(data);
					},
					
				});
			}
			else if(invalid_file > 0 || unsupported_file > 0)
			{
				if(invalid_file > 0)
				{
					var msg = '<div class="alert alert-danger alert-dismissable">'+
						'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
						invalid_file+' File Failed to Upload Due File Size Exceed to Uploaded Size Limit.'+
					'</div>';
					thiss.parents('.document-block').find('.box-body').prepend(msg);
				}
				if(unsupported_file > 0)
				{
					var msg = '<div class="alert alert-warning alert-dismissable">'+
						'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
						unsupported_file+' File Failed to Upload Due to Unsupported File Formats.'+
					'</div>';
					thiss.parents('.document-block').find('.box-body').prepend(msg);
				}
				
			}
			$('.alert').delay(10000).fadeOut('slow');
			$('.full_sreeen_overlay').hide();
		 });
		
		$('.add_property_form').submit(function() {
			$('.alert').hide();
			var has_error = false;
			var jump_to = '';
			if($("input[name^='document_meta']").length)
			{
				$("input[name^='document_meta']").each(function() {
					if($(this).attr('required') && $(this).val() == '')
					{
						has_error = true;
						var dm_id = $(this).parents('.box').attr('id')
						if(jump_to == '')
							jump_to = dm_id;
						$(this).parents('.box').find('.alert').show();
					}
				});
			}
			if(has_error)
			{
				$('html, body').animate({
					scrollTop: $("#"+jump_to).offset().top - 60
				}, 500);
				$('.alert').delay(5000).fadeOut('slow');
				return false;
			}
		});
	}); 
   
	function progress(e){
		
		if(e.lengthComputable){
		   $('#'+id+'_progress').show();
			var percentComplete = (e.loaded / e.total) * 100;
			$('#'+id+'_progress').attr({value:percentComplete});
		}
		
		
	}
	
</script>
<?php } ?>  

<?php 
	$site_language = $myHelpers->global_lib->get_option('site_language');
	$enable_multi_language = $myHelpers->global_lib->get_option('enable_multi_language');
	$default_language = $myHelpers->global_lib->get_option('default_language');
	$locations = $myHelpers->global_lib->get_option('locations');
	if(!empty($locations))
	{
		$loc_list = json_decode($locations,true);
	}
?>		

      <div class="content-wrapper">
        <section class="content-header">
          <h1 class="page-title"><i class="fa fa-plus"></i> <?php echo mlx_get_lang('Add New Property'); ?> </h1>
		  <?php 
		  
		  if($this->site_payments == 'Y' &&  $this->post_property_credit <= 0  && $user_type != 'admin'){ ?>
		   <div class="alert alert-warning alert-dismissable show_always" style="margin-top:10px; margin-bottom:0px;">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<?php echo mlx_get_lang('You don\'t have sufficient credits for post Property.'); ?>
			</div>
		  <?php } ?>
        </section>

        <section class="content">
		  <?php 
			
			$location_plugin_active = true;
			$isPlAct = $myHelpers->isPluginActive('property_locations');
			if($isPlAct == true)
			{
				$location_plugin_active = false;
			}
			
			$attributes = array('name' => 'add_form_post','class' => 'form add_property_form');		 			
			echo form_open_multipart('property/add_new',$attributes); ?>
			
			<div class="row">
			<div class="col-md-8">   
			   
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?> ">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Property Details'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
				  
					<?php 
					
					if(isset($enable_multi_language) && $enable_multi_language == 'Y')
					{
						if(isset($site_language) && !empty($site_language)) 
						{ 
							$site_language_array = json_decode($site_language,true);
							
							
							if(!empty($site_language_array)) 
							{ 
								foreach($site_language_array as $aak=>$aav)
								{
									if($aav['language'] == $default_language)
									{
										$new_value = $site_language_array[$aak];
										unset($site_language_array[$aak]);
										array_unshift($site_language_array, $new_value);
										break;
									}
								}
								
					?>
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
							  <?php 
								$n=0;
								foreach($site_language_array as $k=>$v) 
								{ 
								
								if($v['status'] != 'enable')
									continue;
								
								$n++; 
								
								$lang_exp = explode('~',$v['language']);
								$lang_code = $lang_exp[1];
								$lang_title = $lang_exp[0];
								?>
								<li <?php if($n == 1) echo 'class="active"'; ?>>
									<a href="#<?php echo $lang_code; ?>" data-toggle="tab"><?php echo ucfirst($lang_title); ?></a>
								</li>
							  <?php } ?>
							</ul>
							<div class="tab-content">
							  <?php 
								$n=0;
								foreach($site_language_array as $k=>$v) { 
								
								if($v['status'] != 'enable')
									continue;
						
								$n++; 
								$lang_exp = explode('~',$v['language']);
								$lang_code = $lang_exp[1];
								$lang_title = $lang_exp[0];
								?>
								
									<div class="<?php if($n == 1) echo 'active'; ?> tab-pane" id="<?php echo $lang_code; ?>">
										<div class="form-group">
										  <label for="title_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Title'); ?> <?php if($n == 1) {?><span class="text-red">*</span><?php } ?></label>
										  <input type="text" class="form-control" <?php if($n == 1) {?>required="required"<?php } ?> 
										  name="multi_lang[<?php echo $lang_code; ?>][title]" id="title_<?php echo $lang_code; ?>">
										</div>
										
										<div class="form-group">
										  <label for="short_description_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Short Description'); ?></label>
										  <textarea class="form-control short-description-element" maxlength="<?php echo $short_desc_limit; ?>" rows="3" 
										  id="short_description_<?php echo $lang_code; ?>" name="multi_lang[<?php echo $lang_code; ?>][short_description]"></textarea>
										  <span class="rchars" id="rchars_<?php echo $lang_code; ?>"><?php echo $short_desc_limit; ?></span> <?php echo mlx_get_lang('Character(s) Remaining'); ?>
										</div>
										
										<div class="form-group">
										  <label for="description_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Description'); ?> <?php if($n == 1) {?><span class="text-red">*</span><?php } ?></label>
										  <textarea class="form-control ckeditor-element" 
										  data-lang_code="<?php echo $lang_code; ?>" data-lang_dir="<?php echo $v['direction']; ?>" 
										  rows="3" id="description_<?php echo $lang_code; ?>" 
										  <?php if($n == 1) {?>required<?php } ?> name="multi_lang[<?php echo $lang_code; ?>][description]" ></textarea>
										</div>
										
										<div class="form-group">
											<label for="price_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Price'); ?> <?php if($n == 1) {?><span class="text-red">*</span><?php } ?></label>
											<div class="input-group">
												<span class="input-group-addon">
												  <?php echo $myHelpers->global_lib->get_currency_symbol($v['currency']); ?>
												</span>
												<input type="number" min="0" class="form-control" <?php if($n == 1) {?>required<?php } ?> name="multi_lang[<?php echo $lang_code; ?>][price]" id="price_<?php echo $lang_code; ?>" >
												<span class="input-group-addon property_type_rent_block" style="display:none;">
												  <?php echo mlx_get_lang('Per Month'); ?>
												</span>
											</div>
										</div>
										
										
										<div class="form-group">
										  <label for="address_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Address'); ?> <?php if($n == 1) {?><span class="text-red">*</span><?php } ?></label>
										  <textarea class="form-control" <?php if($n == 1) {?>required<?php } ?> id="address_<?php echo $lang_code; ?>" name="multi_lang[<?php echo $lang_code; ?>][address]"></textarea>
										</div>
										
										<?php 
										if(!$location_plugin_active)
										{ 
											
											$argss = array(
															'lang_code' => $lang_code,
															'n' => $n
														  );
											$this->load->view('default/property/property_location_multi_lang_fields',$argss);
										} 
										?>
										
										<div class="form-group">
										  <label for="meta_keywrod_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Meta Keywords'); ?></label>
										  <input type="text" class="form-control" name="multi_lang[<?php echo $lang_code; ?>][seo_meta_keywords]" id="meta_keywrod_<?php echo $lang_code; ?>" 
										  value="">
										</div>
										
										<div class="form-group">
										  <label for="meta_description_<?php echo $lang_code; ?>"><?php echo mlx_get_lang('Meta Description'); ?></label>
										  <textarea class="form-control" rows="3" id="meta_description_<?php echo $lang_code; ?>" name="multi_lang[<?php echo $lang_code; ?>][seo_meta_description]" ></textarea>
										</div>
										
									</div>
								<?php } ?>
							</div>
						  </div>
						 
					  <?php }} ?>
					
					<?php }else{ 
						
						/*$site_language_array = json_decode($site_language,true);*/
						
						$default_currency = $this->site_currency;
						
						/*$lang_code = $this->default_lang_code;*/
						$lang_code = $this->default_language;
						
						?>
						
						<div class="form-group">
						  <label for="title"><?php echo mlx_get_lang('Title'); ?> <span class="required">*</span></label>
						  <input type="text" class="form-control" required="required" name="title" id="title" >
						</div>
						
						<div class="form-group">
						  <label for="short_description"><?php echo mlx_get_lang('Short Description'); ?></label>
						  <textarea class="form-control short-description-element" rows="3" id="short_description" name="short_description" 
						   maxlength="<?php echo $short_desc_limit; ?>"></textarea>
						  <span class="rchars" id="rchars"><?php echo $short_desc_limit; ?></span> <?php echo mlx_get_lang('Character(s) Remaining'); ?>
						</div>
						
						<div class="form-group">
						  <label for="description"><?php echo mlx_get_lang('Description'); ?> <span class="required">*</span></label>
						  <textarea class="form-control ckeditor-element" required data-lang_code="<?php echo $lang_code; ?>" rows="2" id="description" name="description"></textarea>
						</div>
						
						<div class="form-group">
							<label for="price"><?php echo mlx_get_lang('Price'); ?> <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon">
								<?php echo $myHelpers->global_lib->get_currency_symbol($default_currency); ?>
								  <!--<i class="fa fa-usd"></i> -->
								</span>
								<input type="number" min="0"  class="form-control" required="required" name="price" id="price" >
								<span class="input-group-addon property_type_rent_block" style="display:none;">
								  <?php echo mlx_get_lang('Per Month'); ?>
								</span>
							</div>
						</div>
						
						<div class="form-group">
						  <label for="address"><?php echo mlx_get_lang('Address'); ?> <span class="text-red">*</span></label>
						  <textarea class="form-control" required id="address" name="address"></textarea>
						</div>
						
						<?php if(!$location_plugin_active)
						{ 
							$argss = array(
										'n' => 1,
										'lang_code' => $lang_code
									  );
							$this->load->view('default/property/property_location_fields',$argss);
						} 
						?>
						
						<div class="form-group">
						  <label for="meta_keywrod"><?php echo mlx_get_lang('Meta Keywords'); ?></label>
						  <input type="text" class="form-control" name="seo_meta_keywords" id="meta_keywrod" 
						  value="">
						</div>
						
						<div class="form-group">
						  <label for="meta_description"><?php echo mlx_get_lang('Meta Description'); ?></label>
						  <textarea class="form-control" rows="3" id="meta_description" name="seo_meta_description" ></textarea>
						</div>
						
					<?php } ?>
					
                 	
					
					<div class="form-group">
						<label class="control-label"><?php echo mlx_get_lang('Property Type'); ?> <span class="required">*</span></label>
						<?php 
						if(isset($property_types) && $property_types->num_rows()>0)
						{ ?>
						<div class="radio_toggle_wrapper ">
							<?php $n=0;
							foreach($property_types->result() as $row)
							{ $n++;
							?>
								<input type="radio" 
								<?php if($n==1) echo ' checked="checked" '; ?> 
								id="property_type_<?php echo $myHelpers->EncryptClientId($row->pt_id); ?>" value="<?php echo $myHelpers->EncryptClientId($row->pt_id); ?>" 
								name="property_type" class="toggle-radio-button">
								<label for="property_type_<?php echo $myHelpers->EncryptClientId($row->pt_id); ?>"><?php echo ucfirst($row->title); ?></label>
							<?php } ?>
						</div>
						<?php }else{ ?>
							<p class="no-margin"><?php echo mlx_get_lang('Property Type Not Available Now'); ?></p>
						<?php } ?>
					</div>
					
					
					
					<div class="form-group">
						<label for="property_type_status"><?php echo mlx_get_lang('Property For'); ?> <span class="required">*</span></label>
						 <div class="radio_toggle_wrapper ">
							<input type="radio" checked="checked" id="property_for_sale" value="sale" 
							name="property_for" 
							class="toggle-radio-button">
							<label for="property_for_sale"><?php echo mlx_get_lang('Sale'); ?></label>
							
							<input type="radio" id="property_for_rent" value="rent" name="property_for" 
							class="toggle-radio-button">
							<label for="property_for_rent"><?php echo mlx_get_lang('Rent'); ?></label>
						</div>
					</div>
					
					<!--
					<div class="form-group">
						<label for="property_type_status"><?php echo mlx_get_lang('Status'); ?></label>
						 <div class="radio_toggle_wrapper ">
							<input type="radio" checked="checked" id="status_y" value="Y" 
							name="status" class="toggle-radio-button">
							<label for="status_y"><?php echo mlx_get_lang('Active'); ?></label>
							
							<input type="radio" id="status_n" value="N" name="status" 
							class="toggle-radio-button">
							<label for="status_n"><?php echo mlx_get_lang('In-Active'); ?></label>
						</div>
					</div>
					-->
					
					<?php 
					$user_type = $this->session->userdata('user_type');
					if($user_type == 'admin')
					{
					?>
						<div class="row">
							<div class="form-group col-md-6">
							  <label for="user_id"><?php echo mlx_get_lang('Property Added By'); ?> <span class="required">*</span></label>
							  
							  <select class="form-control select2_elem" name="user_id" id="user_id" required>
								  <option value=""><?php echo mlx_get_lang('Select Any User'); ?></option>
								  <?php 
								  if(isset($user_list) && $user_list->num_rows() > 0)
								  {
									  foreach($user_list->result() as $u_row)
									  {
										  $first_name = $myHelpers->global_lib->get_user_meta($u_row->user_id,'first_name');
										  $last_name = $myHelpers->global_lib->get_user_meta($u_row->user_id,'last_name');
										 
										  if(!empty($last_name))
											  $first_name .= ' '.$last_name;
										  echo '<option value="'.$myHelpers->global_lib->EncryptClientId($u_row->user_id).'">'.ucfirst($first_name).' ('.ucfirst($u_row->user_type).')</option>';
									  }
								  }
								  ?>
							  </select>
							</div>
						</div>
					<?php
					}
					else
					{
						
						echo '<input type="hidden" name="user_id" class="user_id" value="'.$myHelpers->global_lib->EncryptClientId($this->session->userdata("user_id")).'">';
						
					}
					?>
					
				 </div>
                 
			  </div>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Locations'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
						
						<?php 
						$isOsmAct = $myHelpers->isPluginActive('google_map');
						if($isOsmAct == true)
						{
							$enable_google_map_js_api = $myHelpers->global_lib->get_option('enable_google_map_js_api');
							$google_map_js_api_key = $myHelpers->global_lib->get_option('google_map_js_api_key');
							$google_map_center_latitude = $myHelpers->global_lib->get_option('google_map_center_latitude');
							$google_map_center_longitude = $myHelpers->global_lib->get_option('google_map_center_longitude');
							
							if($enable_google_map_js_api == 'Y' && !empty($google_map_js_api_key))
							{
						?>
						
						<div class="form-group ">
							<label for="google_map_locations"><?php echo mlx_get_lang('Google Map Locations'); ?></label>
							<div class="row">
								<div class="col-md-5">
									<div class="input-group">
										<span class="input-group-addon"><?php echo mlx_get_lang('Latitude'); ?></span>
										<input id="property_latitude" type="text" class="form-control" name="lat"
										value="">
									</div>
								</div>
								<div class="col-md-5">
									<div class="input-group">
										<span class="input-group-addon"><?php echo mlx_get_lang('Longitude'); ?></span>
										<input type="text" id="property_longitude" class="form-control" name="long" 
										value="">
									</div>
								</div>
								<div class="col-md-2 text-center">
									<a  href="#popme" 
									data-map_lat="<?php echo $google_map_center_latitude; ?>"
									data-map_lng="<?php echo $google_map_center_longitude; ?>"
									data-api_key="<?php echo $google_map_js_api_key; ?>"
									class="btn btn-block btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> popup-property" data-toggle="tooltip" title="<?php echo mlx_get_lang('Fetch From Map'); ?>"><i class="fa fa-map-marker"></i></a>
									<div class="white-popup mfp-hide" id="popme">
										<div id="map" style="width: 100%; min-height: 500px"></div>
									</div>
								</div>
							</div>
							
						</div>
						
						<?php 
							}
						}
						?>
						<div class="form-group hide col-md-12">
							  <label for="street_number"><?php echo mlx_get_lang('Street Address'); ?> </label>
							  <input type="text" class="form-control"  id="street_number" name="street_address">
							</div>
						<?php 
						if($location_plugin_active)
						{
						?>
						<div class="row">
							
							
							
							
							<div class="form-group col-md-6 hide">
							  <label for="country"><?php echo mlx_get_lang('Country'); ?> <span class="required">*</span></label>
							  
							  <select class="form-control" name="country" id="country">
								  <option value=""><?php echo mlx_get_lang('Select Any Country'); ?></option>
								  <?php 
								  if(isset($country_list) && !empty($country_list))
								  {
									  foreach($country_list as $k=>$v)
									  {
										  echo '<option value="'.$v['countryName'].'~'.$v['countryCode'].'">'.$v['countryName'].'</option>';
									  }
								  }
								  ?>
							  </select>
							</div>
							
							
							<?php 
							$enable_property_for_states = $myHelpers->global_lib->get_option('enable_property_for_states');
							if(isset($enable_property_for_states) && $enable_property_for_states == 'Y')
							{
							?>
							<div class="form-group col-md-6">
							  <label for="state"><?php echo mlx_get_lang('State'); ?> <span class="required">*</span></label>
							  <select class="form-control" name="state" id="state" required>
								  <option value=""><?php echo mlx_get_lang('Select Any State'); ?></option>
								  <?php 
								  
								  $property_for_states = $myHelpers->global_lib->get_option('property_for_states');
								  if(!empty($property_for_states))
								  {
									  $property_for_state_array = json_decode($property_for_states,true);
									  if(!empty($property_for_state_array))
									  {
										  foreach($property_for_state_array as $k=>$v)
										  {
											  $accent_string = strtolower($v);
											  $norm_string = $myHelpers->language_lib->get_normal_string($accent_string);
											  $norm_string = str_replace(' ','-',$norm_string);
											  echo '<option value="'.$norm_string.'">'.ucfirst($v).'</option>';
										  }
									  }
								  }
								  ?>
							  </select>
							</div>
							<?php } ?>
							
							<?php 
							$enable_property_for_cities = $myHelpers->global_lib->get_option('enable_property_for_cities');
							if(isset($enable_property_for_cities) && $enable_property_for_cities == 'Y')
							{
							?>
							<div class="form-group col-md-6">
							  <label for="city"><?php echo mlx_get_lang('City'); ?> <span class="required">*</span></label>
							  <select class="form-control" required name="city" id="city">
								  <option value=""><?php echo mlx_get_lang('Select Any City'); ?></option>
								  <?php 
								  $property_for_cities = $myHelpers->global_lib->get_option('property_for_cities');
								  if(!empty($property_for_cities))
								  {
									  $property_for_cities_array = json_decode($property_for_cities,true);
									  if(!empty($property_for_cities_array))
									  {
										  foreach($property_for_cities_array as $k=>$v)
										  {
											  $accent_string = strtolower($v);
											  $norm_string = $myHelpers->language_lib->get_normal_string($accent_string);
											  $norm_string = str_replace(' ','-',$norm_string);
											  echo '<option value="'.$norm_string.'">'.$v.'</option>';
										  }
									  }
								  }
								  ?>
							  </select>
							</div>
							<?php } ?>
							
							<div class="form-group col-md-6">
							  <label for="postal_code"><?php echo mlx_get_lang('Zip Code'); ?> </label>
							  <input type="text" class="form-control"  id="postal_code" name="zip_code">
							</div>
						</div>
						<?php }else{ ?>
							
						<?php } ?>
						
						<?php 
						$isOsmAct = $myHelpers->isPluginActive('openstreetmap');
						if($isOsmAct == true)
						{
						?>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									  <label for="openstreetmap"><?php echo mlx_get_lang('Open Street Map'); ?></label>
									  <textarea class="form-control no_clean openstreetmap" id="openstreetmap" name="property_meta[openstreetmap_embed_code]"  rows="3" col="3"> </textarea>
									  <p class="help-block">
									  <a href="https://www.openstreetmap.org/" target="_blank"><?php echo mlx_get_lang('Open Street Map'); ?> </a><?php echo mlx_get_lang('Open the link, Copy iframe code and paste above '); ?>
									  </p>
									
									</div>
								</div>
							</div>
						<?php
						}
						?>
						
						
				  </div>
			  </div>
			
		
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Features'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
					  <div class="form-group">
						<label for="size"><?php echo mlx_get_lang('Size'); ?> </label>
						<div class="input-group">
							<span class="input-group-addon">
							  <i class="fa fa-expand"></i>
							</span>
							<input type="text" class="form-control"  name="size" id="size" >
							<!--<span class="input-group-addon">
							  <?php //echo mlx_get_lang('Sq. Foot'); ?>
							</span> -->
							<input type="hidden" class="form-control" value="Sq Feet" 
							name="size_measure" id="size_measure">
							<div class="input-group-btn">
							  <button type="button" class="btn btn-default dropdown-toggle size_measure" data-toggle="dropdown" 
							  aria-expanded="false">
							  
							  <?php echo mlx_get_lang('Sq Feet'); ?>&nbsp;&nbsp;<span class="fa fa-caret-down"></span></button>
							  <ul class="dropdown-menu size_measure_menus">
								<li><a data-val="<?php echo mlx_get_lang('Sq Feet'); ?>"><?php echo mlx_get_lang('Sq Feet'); ?></a></li>
								<li><a data-val="<?php echo mlx_get_lang('Sq Meter'); ?>"><?php echo mlx_get_lang('Sq Meter'); ?></a></li>
								<li><a data-val="<?php echo mlx_get_lang('Sq Yard'); ?>"><?php echo mlx_get_lang('Sq Yard'); ?></a></li>
								<?php 
								  if(isset($size_units) && !empty($size_units))
								  {
									  foreach($size_units as $suv)
									  {
									?>
										<li><a data-val="<?php echo mlx_get_lang($suv); ?>"><?php echo mlx_get_lang($suv); ?></a></li>
									<?php
									  }
								  }
								  ?>
							  </ul>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="bedroom"><?php echo mlx_get_lang('Bedroom'); ?> </label>
						<div class="input-group">
							<span class="input-group-addon">
							  <i class="fa fa-bed"></i>
							</span>
							<input type="text"  value="0"  class="form-control"  name="bedroom" id="bedroom" >
						</div>
					</div>
					
					<div class="form-group">
						<label for="bathroom"><?php echo mlx_get_lang('Bathroom'); ?> </label>
						<div class="input-group">
							<span class="input-group-addon">
							  <i class="fa fa-bathtub"></i>
							</span>
							<input type="text"  value="0"  class="form-control"  name="bathroom" id="bathroom" >
						</div>
					</div>
					
					
					<div class="form-group">
						<label for="garage"><?php echo mlx_get_lang('Garages'); ?> </label>
						<div class="input-group">
							<span class="input-group-addon">
							  <i class="fa fa-car"></i>
							</span>
							<input type="text"  value="0"  class="form-control"  name="garage" id="garage" >
						</div>
					</div>
				  </div>
              </div>
			  
			  <?php if(isset($amenities_list['indoor_amenities']) && !empty($amenities_list['indoor_amenities'])){ ?>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Indoor Amenities'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
					  <div class="row">
						<?php 
						foreach($amenities_list['indoor_amenities'] as $k=>$v){
						?>
							
							<div class="col-md-6 text-right">
								<label class="pull-left" for="<?php echo $v; ?>"><?php echo ucfirst($v); ?></label>
								<input id="<?php echo $v; ?>" class="minimal" type="checkbox" name="indoor_amenities[]" value="<?php echo ucfirst($v); ?>" >
							</div>
							
						<?php } ?>
					 </div>	
				  </div>
              </div>
			  <?php } ?>
			  
			  <?php if(isset($amenities_list['outdoor_amenities']) && !empty($amenities_list['outdoor_amenities'])) { ?>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Outdoor Amenities'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
                    <div class="row">
						<?php 
						foreach($amenities_list['outdoor_amenities'] as $k=>$v){
						?>
							
							<div class="col-md-6 text-right">
								<label class="pull-left" for="<?php echo $v; ?>"><?php echo ucfirst($v); ?></label>
								<input id="<?php echo $v; ?>" class="minimal" type="checkbox" name="outdoor_amenities[]" value="<?php echo ucfirst($v); ?>" >
							</div>
							
						<?php } ?>
					 </div>	
				  </div>
              </div>
			  <?php } ?>
			  
			  <?php if(isset($distances_list) && !empty($distances_list)){ ?>
			  <?php 
			  $direction_list = array('East' => 'East',
									  'West' => 'West',
									  'North' => 'North',
									  'South' => 'South',
									  'North-East' => 'North-East',
									  'South-East' => 'South-East',
									  'South-West' => 'South-West',
									  'North-West' => 'North-West',
									  );
			  ?>
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Distances'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
					  
						<?php 
						foreach($distances_list as $k=>$v){
						?>
							<div class="row form-group">
								<div class="col-md-4">
									<label for="<?php echo $v; ?>"><?php echo ucfirst($v); ?></label>
								</div>
								<div class="col-md-4">
									<select name="distance_list[<?php echo str_replace(' ','_',$v); ?>][direction]" id="<?php echo str_replace(' ','_',$v); ?>" class="direction">
										<option value=""><?php echo mlx_get_lang('Select Any Direction'); ?></option>
										<?php if(isset($direction_list) && !empty($direction_list)) { 
										foreach($direction_list as $dk=>$dv){
										?>
											<option value="<?php echo $dk; ?>"><?php echo mlx_get_lang($dv); ?></option>
										<?php } } ?>
									</select>
								</div>
								<div class="col-md-4">
									<div class="input-group">
										<input type="number" min="0" step=".1" value="0" name="distance_list[<?php echo str_replace(' ','_',$v); ?>][distance]"class="form-control">
										<div class="input-group-btn">
										  <input type="hidden" name="distance_list[<?php echo str_replace(' ','_',$v); ?>][distance_text]" value="Meter">
										  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Meter&nbsp;&nbsp;<span class="fa fa-caret-down"></span></button>
										  <ul class="dropdown-menu custom-dropdown-menu">
											<li><a href="#"><?php echo mlx_get_lang('Meter'); ?></a></li>
											<li><a href="#"><?php echo mlx_get_lang('KM'); ?></a></li>
										  </ul>
										</div>
									</div>
								</div>
							</div>	
						<?php } ?>
					 
				  </div>
              </div>
			  <?php } ?>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>" >
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Property Images'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body">
                    
					
					<div class="form-group" align="center">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							   <div class="property_pl_image_container">
									<label class="custom-file-upload" data-element_id="" data-type="media" id="pl_file_uploader_1">
										<?php echo mlx_get_lang('Drop images here'); ?>
										<br />
										<strong><?php echo mlx_get_lang('OR'); ?></strong>
										<br />
										<?php echo mlx_get_lang('Click here to select images'); ?>
									</label>
									<progress class="pl_file_progress" value="0" max="100" style="display:none;"></progress>
									<a class="pl_file_link" href="" download="" style="display:none;">
										<img src="" style="width:50%;">
									</a>
									<a class="ppl_file_remove_img" title="Remove Image" href="#" style="display:none;"><i class="fa fa-remove"></i></a>
									<input type="hidden" name="blog_image" value="" class="pl_file_hidden">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<br>
								<span class="or"><?php echo mlx_get_lang('OR'); ?></span>
								<br>
								<br>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<a onclick="lazy_load_on_media_img()" href="#" class="custom-file-upload add_from_media_btn "><i class="fa fa-camera"></i>&nbsp;<?php echo mlx_get_lang('Add From Media'); ?></a>
								<input type="hidden" name="addedImgFromMediaLibrary" value="">
								
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="product-gallary-container">
							
						</div>
					</div>
					
					
					
				 </div>
                
              </div>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo mlx_get_lang('Property Video'); ?></h3>
				  <div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				  </div>
                </div>
                  <div class="box-body vdo_url_container">
					  <div class="form-group">
						  <label for="video_url_1"><?php echo mlx_get_lang('Video URL'); ?> </label>
						  <div class="input-group">
							<input type="url" class="form-control video_url"  id="video_url_1" name="video_url[]">
							
							<span class="input-group-addon remove-video-link">
							  <i class="fa fa-remove"></i>
							</span>
							
						  </div>
						</div>
				  </div>
				  <div class="box-footer">
					 <button type="button" class="btn btn-default pull-right add_more_vdo_btn"><i class="fa fa-plus"></i> <?php echo mlx_get_lang('Add Video'); ?></button>
				  </div>
              </div>
				
				<?php 
			   if(isset($custom_field_list) && !empty($custom_field_list)) {
				  
			   ?>
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?>">
					<div class="box-header with-border">
					  <h3 class="box-title"><?php echo mlx_get_lang('Custom Fields'); ?></h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					  </div>
					</div>
					<div class="box-body">
						
						<?php 
						$n=0;
						foreach($custom_field_list as $cfk=>$cfv)
						{
							$n++;
							$hasChecked = '';
							$curValue = '';
						?>
							<div class="form-group">
							  <label for="custom_field_<?php echo $n; ?>"><?php echo mlx_get_lang($cfv['title']); ?> <?php if($n == 1 && 0) {?><span class="text-red">*</span><?php } ?></label>
							  <input type="text" class="form-control" <?php if($n == 1 && 0) {?>required="required"<?php } ?> 
							  name="custom_fields[<?php echo $cfv['slug']; ?>]" id="custom_field_<?php echo $n; ?>">
							</div>
							
						<?php } ?>
								
					</div>
              </div>
			  <?php } ?>
				
			  
			  
			  
		
			
				<?php 
				$isDocAct = $myHelpers->isPluginActive('document');
				if($isDocAct == true)
				{
				
					if(isset($document_types) && $document_types->num_rows() > 0)
					{
						foreach($document_types->result() as $doc_type_row)
						{
							$field_id = $doc_type_row->slug.'_field';
					?>
					  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?> document-block" id="<?php echo $doc_type_row->slug; ?>">
						<div class="box-header with-border">
						  <h3 class="box-title"><?php echo ucfirst($doc_type_row->title).' - '.mlx_get_lang('Document Type'); ?> <?php if($doc_type_row->is_required == 'Y') { echo '<span class="required">*</span>'; } ?></h3>
						  <div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  </div>
						</div>
						<div class="box-body">
							<div class="alert alert-danger alert-dismissable"  style="margin-bottom:10px; margin-top:0px; display:none;">
								<?php echo (!empty($doc_type_row->error_message)?$doc_type_row->error_message:'This field is required.'); ?>
							</div>
							<div class="form-group" align="center">
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									   <div class="property_document_pl_image_container">
											<label class="custom-file-upload" data-element_id="" data-type="documents" id="pl_file_uploader_<?php echo $field_id; ?>">
												<?php echo mlx_get_lang('Drop documents here'); ?>
												<br />
												<strong><?php echo mlx_get_lang('OR'); ?></strong>
												<br />
												<?php echo mlx_get_lang('Click here to select documents'); ?>
											</label>
											<progress class="pl_file_progress" value="0" max="100" style="display:none;"></progress>
											<a class="pl_file_link" href="" download="" style="display:none;">
												<img src="" style="width:50%;">
											</a>
											<a class="ppl_file_remove_img" title="Remove Image" href="#" style="display:none;"><i class="fa fa-remove"></i></a>
											<input type="hidden" name="blog_image" value="" class="pl_file_hidden">
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<br>
										<span class="or"><?php echo mlx_get_lang('OR'); ?></span>
										<br>
										<br>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<a onclick="lazy_load_on_media_img()"href="#" class="custom-file-upload add_from_document_btn"><i class="fa fa-folder"></i>&nbsp;<?php echo mlx_get_lang('Add From Documents'); ?></a>
										<input type="hidden" id="<?php echo $field_id; ?>_hidden" name="document_meta[<?php echo $doc_type_row->slug; ?>-ids]" value="" <?php if($doc_type_row->is_required == 'Y') { echo 'required'; } ?>>
									</div>
								</div>
								
							</div>
							
							<div class="product-document-container row">
							
							</div>
							
						</div>
						<?php if(isset($doc_type_row->description) && !empty($doc_type_row->description)){ ?>
							<div class="box-footer">
								<p class="help-block" style="margin:0px;"><strong><?php echo mlx_get_lang('Note'); ?></strong> : <?php echo $doc_type_row->description; ?></p>
							</div>
						<?php } ?>
					  </div>
				<?php }
					} 
						
				}	
				?>  
			
			
		
		</div>
		  
		  <div class="col-md-4">
			  
			  <div class="box box-<?php echo $myHelpers->global_lib->get_skin_class(); ?> sticky_sidebar">
				  <div class="box-header with-border">
					  <h3 class="box-title"><?php echo mlx_get_lang('Status'); ?></h3>
					  <div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						
					  </div>
					</div>
					 <div class="box-body">
					 	<label style="margin-bottom: 15px;">Property Status</label>
						<div class="form-group">
							<div class="radio" style="float: left;margin-top: -5px;margin-right: 7px;">
								<label style="padding-left:0px;">
								  <input type="radio" class="flat-green" name="property_status" id="property_status" value="Public" checked="checked">
								  &nbsp; &nbsp;Public
								</label>
							</div>
							<div class="radio" style="float: left;">
								<label style="padding-left:0px;">
								  <input type="radio" class="flat-red" name="property_status" id="property_status" value="Private" >
								  &nbsp; &nbsp;Private
								</label>
							 </div>
						</div>
					</div>
					 <div class="box-footer" style="border-top:0px;">
						<?php if($user_type == 'admin')
						{ 
						?>
						<button type="submit" name="draft" class="btn btn-draft btn-default" id="save_draft"><?php echo mlx_get_lang('Save as Draft'); ?></button>
						<button name="submit" type="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right" id="save_publish"><?php echo mlx_get_lang('Save Publish'); ?></button>
						<?php 
						}
						else 
						{
							$has_req = $myHelpers->global_lib->get_option('admin_approval_require_for_property');
							if($has_req == 'N')
							{
							?>
								<button type="submit" name="draft" class="btn btn-draft btn-default" id="save_draft"><?php echo mlx_get_lang('Save as Draft'); ?></button>
								<button name="submit" type="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right" id="save_publish"><?php echo mlx_get_lang('Save Publish'); ?></button>
							<?php
							}
							else
							{
							?>
								<button name="pending" type="submit" class="btn btn-<?php echo $myHelpers->global_lib->get_skin_class(); ?> pull-right" id="save_publish"><?php echo mlx_get_lang('Submit for Approval'); ?></button>
							<?php
							}
						}
						?>
					  </div>
			   </div>
			   
			   
		  </div>
		  
		  
		  </div>
			  
			  </form>
        </section>
      </div>
	  
<script > 
function lazy_load_on_media_img()
{
	setTimeout(function(){
		$('.media_img_block li').each(function(){
			$(this).find('.lazy-img-elem').lazy({
				effect: "fadeIn",
				effectTime: 500,
				threshold: 0,
				afterLoad: function(element) {
					element.parent().removeClass('lazy-load-processing');
				},
			}).trigger("appear");;
			$(this).find('.lazy-img-elem').parent().removeClass('lazy-load-processing');
			var src = $(this).find('.lazy-img-elem').attr('data-src');
			$(this).find('.lazy-img-elem').attr('src',src);
		});
	},1000); 
}
	
$(document).ready(function(){
	$('.dropdown-menu.size_measure_menus li').click(function() {
		var data_val = $(this).find('a').attr('data-val');
		
		$(this).parents('.input-group-btn').find('.dropdown-toggle').html(data_val+'&nbsp;&nbsp;<span class="fa fa-caret-down"></span>');
		$(this).parents('.input-group-btn').removeClass('open');
		$(this).parents('.input-group').find('#size_measure').val(data_val);
		return false;
	});
	
	
});
</script >	

