<?php 
	$user_type = $this->session->userdata('user_type');
	$enable_homepage_section = $this->global_lib->get_option('enable_homepage_section');
?>
<aside class="main-sidebar">
<section class="sidebar">
  <ul class="sidebar-menu">
	<?php
		
		$menu_items =  $myHelpers->config->item('sidebar_left');  
		
		
		foreach($menu_items as $k => $menu_item)
		{	
			if($menu_item['class'] == 'payments' || $menu_item['class'] == 'packages')
			{
				$isPlugAct = $myHelpers->isPluginActive('payment');
				if($isPlugAct != true)
				{
					continue;
				}
			}
			if($menu_item['method'] == 'home_page' && $enable_homepage_section != 'Y')
			{
				continue;
			}
			
			if(!$myHelpers->has_menu_access($menu_item['class'] , $user_type))
			{
				continue;
			}
			
			if($menu_item['method'] == 'packages' && $this->site_payments != 'Y')
			{
				continue;
			}
			
			if($menu_item['class'] == 'blog')
			{
				$isPlugAct = $myHelpers->isPluginActive('blog');
				if($isPlugAct != true)
				{
					continue;
				}
				
			}
			
			if($menu_item['class'] == 'documents')
			{
				$isPlugAct = $myHelpers->isPluginActive('document');
				if($isPlugAct != true)
				{
					continue;
				}
			}
			
			if($menu_item['class'] == 'locations')
			{
				$isPlugAct = $myHelpers->isPluginActive('property_locations');
				if($isPlugAct != true)
				{
					continue;
				}
				
			}
			
			if($menu_item['class'] == 'main' && $menu_item['method'] == 'google_analytics')
			{
				$isPlugAct = $myHelpers->isPluginActive('google_analytics');
				if($isPlugAct != true)
				{
					continue;
				}
			}
			
			if(isset($menu_item['item'])) 
			{	$nav_items = $menu_item['item'];	
				$mi_class = "treeview ";
			}else {
				$mi_class = "";
			}
			
			if($menu_item['method'] != '' && $func == $menu_item['method'] && $class == $menu_item['class'])
				$mi_class = " class='$mi_class active' ";
			else if( $menu_item['method'] == '' && $class == $menu_item['class'] ) 	
				$mi_class = " class='$mi_class active' ";
			else 			
				$mi_class = " class='$mi_class ' ";
			
			if($menu_item['link'] != '#') 			$link = base_url(explode("/",$menu_item['link']));
			else			$link = $menu_item['link'];
			
			if(!empty($menu_item['icon_class']))	$icon_text = "<i class='".$menu_item['icon_class']."'></i> ";
			else			$icon_text = "";
				
			if(!empty($menu_item['collapse_class']))	$collapse_text = "<i class='".$menu_item['collapse_class']."'></i> ";
			else			$collapse_text = "";
					
			?>
			<li <?php  echo $mi_class; ?>>
			  <a href="<?php echo $link; ?>"> 	<?php echo $icon_text; ?>
				<span><?php echo mlx_get_lang($menu_item['text']); ?></span> 	<?php echo $collapse_text; ?>   </a>
			<?php	
			
			if(isset($menu_item['item'])) {
				$nav_items = $menu_item['item'];	
				
			?>
		  <ul class="treeview-menu">
		  <?php 
		  
			foreach($nav_items as  $k_inner => $item)
			{	
				$mit_class = ""; 
				
				if(!$myHelpers->has_menu_access($item['class']."||".$item['method'] , $user_type))
				{
					continue;
				}
				
				if($item['class'] == 'settings' && $item['method'] == 'db_settings')
				{
					$isPlugAct = $myHelpers->isPluginActive('database_backup');
					if($isPlugAct != true)
					{
						continue;
					}
				}
				else if($item['class'] == 'property' && $item['method'] == 'doc_type')
				{
					$isPlugAct = $myHelpers->isPluginActive('document');
					if($isPlugAct != true)
					{
						continue;
					}
				}
				
				if( $class == $item['class'] && $func == $item['method'])		$mit_class = " class='active' ";
				else				$mit_class = " class='' ";
				
				if($item['link'] != '#')				$link = base_url(explode("/",$item['link']));
				else				$link = $item['link'];
				
				if(!empty($item['icon_class']))			$icon_text = "<i class='".$item['icon_class']."'></i> ";
				else				$icon_text = "";
					
				if(!empty($item['collapse_class']))		$collapse_text = "<i class='".$item['collapse_class']."'></i> ";
				else				$collapse_text = "";
				
				?>
				<li <?php  echo $mit_class; ?>>
				<a href="<?php echo $link; ?>">
				<?php echo $icon_text; ?>
				<span><?php echo mlx_get_lang($item['text']); ?></span>
				<?php echo $collapse_text; ?>
				</a></li>
				<?php	}
					echo "</ul>";
				?>	</li>		<?php
			}	
		}
	?>	
  </ul>
</section>
</aside>