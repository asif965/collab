<?php $this->load->view("default/header-top");?>
<?php $this->load->view("default/sidebar-left");?>
<?php $user_type = $this->session->userdata('user_type'); ?>

<div class="content-wrapper">

	<section class="content-header">
		<?php if(isset($inactive_agents) && $inactive_agents->num_rows() > 0){ ?>
			<div class="alert alert-warning alert-dismissable show_always">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $inactive_agents->num_rows(); ?> <?php echo mlx_get_lang('Agent is in-active'); ?>. <a href="<?php echo base_url(array('user','manage')); ?>"><?php echo mlx_get_lang('Click Here'); ?></a> <?php echo mlx_get_lang('to see the list'); ?>.
			</div>
		<?php } ?>
	  <h1 class="page-title"><i class="fa fa-dashboard"></i> <?php echo mlx_get_lang('Dashboard'); ?></h1>
	</section>
	
	<section class="content">
		
		<div class="row">
			
			<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon"><i class="fa fa-building bg-blue" ></i></span>
                <div class="info-box-content ">
                  <span class="info-box-number"><?php if(isset($total_properties)) { echo $total_properties->num_rows(); } else echo '0';  ?></span>
				  <span class="info-box-text"><?php echo mlx_get_lang('Total Properties'); ?></span>
                  
				</div>
              </div>
            </div>
			
			<!--
			<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-image"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?php echo mlx_get_lang('Total Media'); ?> </span>
                  <span class="info-box-number"><?php if(isset($total_media)) { echo $total_media->num_rows(); } else echo '0';  ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description">
                    <a href="<?php echo site_url(array('media','manage')); ?>"><?php echo mlx_get_lang('View All'); ?></a>
                  </span>
                </div>
              </div>
            </div>
			-->
			
			<?php if($user_type == 'admin'){ ?>
				<div class="col-md-3 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon"><i class="fa fa-user bg-green"></i></span>
					<div class="info-box-content">
					  <span class="info-box-number"><?php if(isset($total_agents)) { echo $total_agents->num_rows(); } else echo '0';  ?></span>
					  <span class="info-box-text"><?php echo mlx_get_lang('Total Agents'); ?></span>
					  
					</div>
				  </div>
				</div>
				
				<div class="col-md-3 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon"><i class="fa fa-user bg-orange"></i></span>
					<div class="info-box-content">
					  <span class="info-box-number"><?php if(isset($total_owners)) { echo $total_owners->num_rows(); } else echo '0';  ?></span>
					  <span class="info-box-text"><?php echo mlx_get_lang('Total Owners'); ?></span>
					  
					</div>
				  </div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon"><i class="fa fa-user bg-red"></i></span>
					<div class="info-box-content">
					  <span class="info-box-number"><?php if(isset($total_builders)) { echo $total_builders->num_rows(); } else echo '0';  ?></span>
					  <span class="info-box-text"><?php echo mlx_get_lang('Total Builder'); ?></span>
					  
					</div>
				  </div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon"><i class="fa fa-user bg-maroon"></i></span>
					<div class="info-box-content">
					  <span class="info-box-number"><?php if(isset($total_landlords)) { echo $total_landlords->num_rows(); } else echo '0';  ?></span>
					  <span class="info-box-text"><?php echo mlx_get_lang('Total LandLord'); ?></span>
					  
					</div>
				  </div>
				</div>
				
				
				
				
			<?php } ?>
			
			<?php 
			$isPlugAct = $myHelpers->isPluginActive('google_analytics');
			if($isPlugAct == true)
			{
			
			?>
			<div class="col-md-3 col-sm-6 col-xs-12">
			  <div class="info-box">
				<span class="info-box-icon"><i class="fa fa-newspaper-o bg-purple"></i></span>
				<div class="info-box-content">
				  <span class="info-box-number"><?php if(isset($total_blogs)) { echo $total_blogs->num_rows(); } else echo '0';  ?></span>
				  <span class="info-box-text"><?php echo mlx_get_lang('Total Blogs'); ?></span>
				  
				</div>
			  </div>
			</div>
			<?php 
				
			}
			?>
			
	</section>

  </div>

     