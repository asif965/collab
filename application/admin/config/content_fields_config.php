<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


	$config['yes_no_options'] =  array();
	$config['yes_no_options'] ['yes'] = "Yes";  
	$config['yes_no_options'] ['no'] = "No";  		

	$config['active_inactive'] ['active'] = "Active";  
	$config['active_inactive'] ['inactive'] = "inactive";  
	
	
	$config ['content_sections'] = array();  
	
	$config ['content_sections'] ["slider_section"] = array('title' => 'Slider');  

	$config ['slider_section_fields'] = array();  
	
	$config ['slider_section_fields'] []  = array( 	'id'=> 'slider_show_nav', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['slider_section_fields'] []  = array( 	'id'=> 'slider_show_nav_dots', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['slider_section_fields'] []  = array( 	'id'=> 'slider_auto_start_slider', 'name' => 'auto_start_slider', 'title' => 'Auto Start', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['slider_section_fields'] []  = array( 	'id'=> 'slider_interval', 'name' => 'slider_interval', 'title' => 'Interval', 
											'parent_class' => 'slider_block_related yes_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '3000', 'min' => '0', 'step' => '500', 'max' => '10000');
	
	/* Search Block */
	
	$config ['content_sections'] ["search_section"] = array('title' => 'Search');  

	$config ['search_section_fields'] = array();  
	
	$config ['search_section_fields'] []  = array( 	'id'=> 'search_show_advance_search', 'name' => 'show_advance_search', 
											'title' => 'Enable Advanced Search?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'options' => 'yes_no_options'
											 );
	
										
	/* Search Property Section */
	/*
	$config ['content_sections'] ["search_property_section"] = array('title' => 'Search Property');  
	
	$config ['search_property_section_fields'] = array();  
	
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Search Property', );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );
	
	$config ['search_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_show_popup_btn', 'name' => 'show_popup_btn', 'title' => 'Show Popup Button to View Large Image?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_show_load_more_btn', 'name' => 'show_load_more_btn', 'title' => 'Show Load More Button?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'options' => 'yes_no_options'
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'photo_gallary_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_no_of_photo', 'name' => 'no_of_photo', 'title' => 'No. of Photo to Show', 
											'parent_class' => 'photo_gallary_grid_block_related grid_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '8', 'min' => '0', 'step' => '1');
											
    $config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_no_of_photo_in_carousel', 'name' => 'no_of_item_in_carousel', 'title' => 'No. of Photo in Carousel', 
											'parent_class' => 'photo_gallary_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '4', 'min' => '0', 'step' => '1');											
											
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'show_nav_in_photo_gallery_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'photo_gallary_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_photo_gallery_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'photo_gallary_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'auto_start_in_photo_gallery_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'photo_gallary_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'photo_gallery_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'photo_gallery_slider_block_related yes_block photo_gallary_grid_block_related',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
    $config ['search_property_section_fields'] []  = array( 	'id'=> 'photo_gallery_carousel_pause', 'name' => 'carousel_pause', 'title' => 'Pause', 
											'parent_class' => 'photo_gallery_slider_block_related yes_block photo_gallary_grid_block_related',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'none',
											'options' => array('none' => 'None', 'hover' => 'Hover')
											 );
	*/
	/* Recent Property Section */
	
	$config ['content_sections'] ["recent_property_section"] = array('title' => 'Recent Properties');  

	$config ['recent_property_section_fields'] = array();  
 
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'recent_property_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Recent Properties', );
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'recent_property_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	
	$config ['recent_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'recent_property_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_property_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'no_of_recent_property_in_grid_list', 'name' => 'no_of_item_in_grid_list', 'title' => 'No. of Property', 
											'parent_class' => 'recent_property_grid_block_related grid_block',
											'type' => 'number-field',	'required' => '', 'default' => '6', 'min' => '0', 'step' => '1', 'max' => 12);
											
    $config ['recent_property_section_fields'] []  = array( 	'id'=> 'no_of_recent_property_in_carousel', 'name' => 'no_of_item_in_carousel', 
											'title' => 'No. of Property', 
											'parent_class' => 'recent_property_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '6', 'min' => '0', 'step' => '1' , 'max' => 12);											
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'show_nav_in_recent_property_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'recent_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_recent_property_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'recent_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'auto_start_in_recent_property_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'recent_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_property_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'recent_property_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'recent_property_slider_block_related yes_block recent_property_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
   
	$config ['recent_property_section_fields'] []  = array( 	'id'=> 'show_view_more_btn_in_recent_property', 'name' => 'show_view_more', 'title' => 'Show View More Button ?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );										
	
	/* Property by Locations Section */
	
	$config ['content_sections'] ["property_type_section"] = array('title' => 'Property Types');  

	$config ['property_type_section_fields'] = array();  
 
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'property_type_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Looking for Property', );
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'property_type_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	
	$config ['property_type_section_fields'] []  = array( 'type' => 'hr-field' );
										
    $config ['property_type_section_fields'] []  = array( 	'id'=> 'no_of_property_type_in_carousel', 'name' => 'no_of_item_in_carousel', 'title' => 'No. of Property Type in Carousel', 
											'type' => 'number-field',	'required' => 'required', 'default' => '3', 'min' => '3', 'step' => '1', 'max' => '5');	
											
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'show_nav_in_property_type_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_property_type_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'auto_start_in_property_type_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'property_type_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['property_type_section_fields'] []  = array( 	'id'=> 'property_type_slider_block_related_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'property_type_slider_block_related yes_block ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
    
	/* Featured Property Section */
	
	$config ['content_sections'] ["featured_property_section"] = array('title' => 'Featured Properties');  

	$config ['featured_property_section_fields'] = array();  
 
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'featured_property_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Featured Property', );
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'featured_property_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['featured_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'featured_property_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'featured_property_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'no_of_featured_property_in_grid_list', 'name' => 'no_of_item_in_grid_list', 'title' => 'No. of Property', 
											'parent_class' => 'featured_property_grid_block_related grid_block',
											'type' => 'number-field',	'required' => '', 'default' => '6', 'min' => '0', 'step' => '1', 'max' => 12);
											
    $config ['featured_property_section_fields'] []  = array( 	'id'=> 'no_of_featured_property_in_carousel', 'name' => 'no_of_item_in_carousel', 
											'title' => 'No. of Property', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '6', 'min' => '0', 'step' => '1' , 'max' => 12);											
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'show_nav_in_featured_property_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_featured_property_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'auto_start_in_featured_property_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'featured_property_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'featured_property_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'featured_property_slider_block_related yes_block featured_property_grid_block_related ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
   
	$config ['featured_property_section_fields'] []  = array( 	'id'=> 'show_view_more_btn_in_featured_property', 'name' => 'show_view_more', 'title' => 'Show View More Button ?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );	
	
	/* Recent Viewed Section */
	
	$config ['content_sections'] ["recent_viewed_property_section"] = array('title' => 'Recent Viewed Properties');  

	$config ['recent_viewed_property_section_fields'] = array();  
 
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'recent_viewed_property_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Recent Viewed Property', );
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'recent_viewed_property_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['recent_viewed_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'recent_viewed_property_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_viewed_property_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'no_of_recent_viewed_property_in_grid_list', 'name' => 'no_of_item_in_grid_list', 'title' => 'No. of Property', 
											'parent_class' => 'recent_viewed_property_grid_block_related grid_block',
											'type' => 'number-field',	'required' => '', 'default' => '6', 'min' => '0', 'step' => '1', 'max' => 12);
											
    $config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'no_of_recent_viewed_property_in_carousel', 'name' => 'no_of_item_in_carousel', 
											'title' => 'No. of Property', 
											'parent_class' => 'recent_viewed_property_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '6', 'min' => '0', 'step' => '1' , 'max' => 12);											
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'show_nav_in_recent_viewed_property_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'recent_viewed_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_recent_viewed_property_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'recent_viewed_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'auto_start_in_recent_viewed_property_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'recent_viewed_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_viewed_property_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_viewed_property_section_fields'] []  = array( 	'id'=> 'recent_viewed_property_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'recent_viewed_property_slider_block_related yes_block recent_viewed_property_grid_block_related ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
	
	/* Latest Blog Section */
	
	$config ['content_sections'] ["recent_blog_section"] = array('title' => 'Recent Blogs');  

	$config ['recent_blog_section_fields'] = array();  
 
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'recent_blog_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Recent Blog', );
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'recent_blog_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['recent_blog_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'recent_blog_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_blog_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'no_of_recent_blog_in_grid_list', 'name' => 'no_of_item_in_grid_list', 
											'title' => 'No. of Blogs', 
											'parent_class' => 'recent_blog_grid_block_related grid_block',
											'type' => 'number-field',	'required' => '', 'default' => '6', 'min' => '0', 'step' => '1', 'max' => 12);
											
    $config ['recent_blog_section_fields'] []  = array( 	'id'=> 'no_of_recent_blog_in_carousel', 'name' => 'no_of_item_in_carousel', 
											'title' => 'No. of Blogs', 
											'parent_class' => 'recent_blog_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '6', 'min' => '0', 'step' => '1' , 'max' => 12);											
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'show_nav_in_recent_blog_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'recent_blog_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_recent_blog_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'recent_blog_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'auto_start_in_recent_blog_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'recent_blog_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'recent_blog_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'recent_blog_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'recent_blog_slider_block_related yes_block recent_blog_grid_block_related ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
   
	$config ['recent_blog_section_fields'] []  = array( 	'id'=> 'show_view_more_btn_in_recent_blog', 'name' => 'show_view_more', 
											'title' => 'Show View More Button ?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );	
	
	
	/* Video Section */
	
	$config ['content_sections'] ["video_section"] = array('title' => 'Videos (Youtube/Vimeo/Facebook/Self Hosted)','section_type' => 'video');  

	$config ['video_section_fields'] = array();  
 
	$config ['video_section_fields'] []  = array( 	'id'=> 'video_section_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );
	
	$config ['video_section_fields'] []  = array( 	'id'=> 'video_section_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['video_section_fields'] []  = array( 	'id'=> 'video_section_for_lang', 'name' => 'video_lang', 'title' => 'Video for Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => 'video_for_lang_opt', 
											'options' => ''
											 );
	
	$config ['video_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['video_section_fields'] []  = array( 	'id'=> 'video_url', 'name' => 'video_url', 'title' => 'Video URL', 
											'type' => 'video-url',	'required' => '', 'default' => '',
											 );
	
	/* Dynamic Property Section */
	
	$config ['content_sections'] ["properties_section"] = array('title' => 'Properties Section','section_type' => 'dynamic');  

	$config ['properties_section_fields'] = array();  
 
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => '', );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['properties_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_for', 'name' => 'property_for', 'title' => 'Property For', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'sale',
											'class' => '', 
											'options' => array('sale' => 'Sale', 'rent' => 'Rent')
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_type', 'name' => 'property_type', 'title' => 'Property Type', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => '',
											'class' => '', 
											'options' => '',
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_for_lang', 'name' => 'property_for_lang', 'title' => 'Property for Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_opt', 
											'options' => ''
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_country', 'name' => 'property_country', 'title' => 'Country', 
											'type' => 'select',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_country parent_hidden_elem', 
											'options' => array('all' => 'All Countries')
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_state', 'name' => 'property_state', 'title' => 'State', 
											'type' => 'select',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_state parent_hidden_elem', 
											'options' => array('all' => 'All States')
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_city', 'name' => 'property_city', 'title' => 'City', 
											'type' => 'select',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_city parent_hidden_elem', 
											'options' => array('all' => 'All Cities')
											 );
	
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_zipcode', 'name' => 'property_zipcode', 'title' => 'Zipcode', 
											'type' => 'select',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_zipcode parent_hidden_elem', 
											'options' => array('' => 'All Zipcodes')
											 );
											 
	$config ['properties_section_fields'] []  = array( 	'id'=> 'dynamic_property_sub_area', 'name' => 'property_sub_area', 'title' => 'Sub Area', 
											'type' => 'select',	'required' => '', 'default' => '',
											'class' => 'dynamic_property_for_lang_sub_area parent_hidden_elem', 
											'options' => array('' => 'All Sub Areas')
											 );
	
	
	$config ['properties_section_fields'] []  = array( 'type' => 'hr-field' );
	
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'featured_property_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'featured_property_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'no_of_featured_property_in_grid_list', 'name' => 'no_of_item_in_grid_list', 'title' => 'No. of Property', 
											'parent_class' => 'featured_property_grid_block_related grid_block',
											'type' => 'number-field',	'required' => '', 'default' => '6', 'min' => '0', 'step' => '1', 'max' => 12);
											
    $config ['properties_section_fields'] []  = array( 	'id'=> 'no_of_featured_property_in_carousel', 'name' => 'no_of_item_in_carousel', 
											'title' => 'No. of Property', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '6', 'min' => '0', 'step' => '1' , 'max' => 12);											
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'show_nav_in_featured_property_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_featured_property_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'auto_start_in_featured_property_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'featured_property_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'featured_property_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['properties_section_fields'] []  = array( 	'id'=> 'featured_property_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'featured_property_slider_block_related yes_block featured_property_grid_block_related ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
   
	$config ['properties_section_fields'] []  = array( 	'id'=> 'show_view_more_btn_in_featured_property', 'name' => 'show_view_more', 'title' => 'Show View More Button ?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );	
	
	/* Property by Locations Section */
	/*
	$config ['content_sections'] ["property_by_locations_section"] = array('title' => 'Property by Locations');  

	$config ['property_by_locations_section_fields'] = array();  
 
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'testimonial_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Testimonial', );
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'testimonial_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	
	$config ['search_property_section_fields'] []  = array( 'type' => 'hr-field' );
										
    $config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'no_of_testimonial_in_carousel', 'name' => 'no_of_item_in_carousel', 'title' => 'No. of Testimonial in Carousel', 
											'type' => 'number-field',	'required' => 'required', 'default' => '1', 'min' => '1', 'step' => '1', 'max' => '3');	
											
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'show_nav_in_testimonial_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_testimonial_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'auto_start_in_testimonial_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'testimonial_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'testimonial_slider_block_related_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'testimonial_slider_block_related yes_block ',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500');
											
    $config ['property_by_locations_section_fields'] []  = array( 	'id'=> 'testimonial_slider_block_related_carousel_pause', 'name' => 'carousel_pause', 'title' => 'Pause', 
											'parent_class' => 'testimonial_slider_block_related yes_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'none',
											'options' => array('none' => 'None', 'hover' => 'Hover')
											 );
	*/
	/* Property for Sale Section */
	/*
	$config ['content_sections'] ["property_for_sale_section"] = array('title' => 'Property for Sale');  

	$config ['property_for_sale_section_fields'] = array();  
 
	
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'client_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Client', );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'client_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['search_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'client_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'client_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'no_of_client_in_grid', 'name' => 'no_of_client_in_grid', 'title' => 'No. of Client', 
											'parent_class' => 'client_grid_block_related grid_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '3', 'min' => '0', 'step' => '1');
											
    $config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'no_of_client_in_carousel', 'name' => 'no_of_item_in_carousel', 'title' => 'No. of Testimonial in Carousel', 
											'parent_class' => 'client_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '3', 'min' => '0', 'step' => '1');	

	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'show_nav_in_client_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'client_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_client_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'client_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'auto_start_in_client_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'client_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'client_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'client_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'client_slider_block_related yes_block client_grid_block_related',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500');
											
    $config ['property_for_sale_section_fields'] []  = array( 	'id'=> 'client_carousel_pause', 'name' => 'carousel_pause', 'title' => 'Pause', 
											'parent_class' => 'client_slider_block_related yes_block client_grid_block_related',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'none',
											'options' => array('none' => 'None', 'hover' => 'Hover')
											 );
	*/
	/* Property for Rent Section */
	/*
	$config ['content_sections'] ["property_for_rent_section"] = array('title' => 'Property for Rent');  

	$config ['property_for_rent_section_fields'] = array();  
 
	
	$config ['property_for_rent_section_fields'] []  = array( 	'id'=> 'portfolio_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Portfolio', );
	
	$config ['property_for_rent_section_fields'] []  = array( 	'id'=> 'portfolio_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	*/
	/* Property by Category Section */
	/*
	$config ['content_sections'] ["property_by_category_section"] = array('title' => 'Property by Category');  

	$config ['property_by_category_section_fields'] = array();  
	
	
	$config ['property_by_category_section_fields'] []  = array( 	'id'=> 'process_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Process', );
	
	$config ['property_by_category_section_fields'] []  = array( 	'id'=> 'process_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	*/
	/* Property by Country Section */

	/*
	$config ['content_sections'] ["property_by_country_section"] = array('title' => 'Property by Country');  

	$config ['property_by_country_section_fields'] = array();  
 
	
	$config ['property_by_country_section_fields'] []  = array( 	'id'=> 'features_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Features', );
	
	$config ['property_by_country_section_fields'] []  = array( 	'id'=> 'features_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	*/
	
	/* Agent List Section */

	/*
	$config ['content_sections'] ["agent_list_section"] = array('title' => 'Agent List');  

	$config ['agent_list_section_fields'] = array();  
 
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'services_heading', 'name' => 'heading', 'title' => 'Heading', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Services', );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'services_sub_heading', 'name' => 'sub_heading', 'title' => 'Sub Heading', 
											'type' => 'text-field',	'required' => '', 'default' => '', );								
	
	$config ['search_property_section_fields'] []  = array( 'type' => 'hr-field' );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'services_show_as', 'name' => 'show_as', 'title' => 'Show as', 
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'grid',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'services_grid_block_related'),
											'options' => array('grid' => 'Grid', 'carousel' => 'Carousel')
											 );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'no_of_services_in_grid', 'name' => 'no_of_services_in_grid', 'title' => 'No. of Services', 
											'parent_class' => 'services_grid_block_related grid_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '3', 'min' => '2', 'step' => '1' , 'max' => '4');
											
    $config ['agent_list_section_fields'] []  = array( 	'id'=> 'no_of_services_in_carousel', 'name' => 'no_of_item_in_carousel', 'title' => 'No. of Services', 
											'parent_class' => 'services_grid_block_related carousel_block',
											'type' => 'number-field',	'required' => 'required', 'default' => '3', 'min' => '2', 'step' => '1', 'max' => '4');	

	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'show_nav_in_service_carousel', 'name' => 'show_nav', 'title' => 'Show Navigation Icon?', 
											'parent_class' => 'services_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'show_nav_dots_in_service_carousel', 'name' => 'show_nav_dots', 'title' => 'Show Navigation Dots?', 
											'parent_class' => 'services_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'yes',
											'options' => 'yes_no_options'
											 );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'auto_start_in_service_carousel', 'name' => 'auto_start', 'title' => 'Auto Start', 
											'parent_class' => 'services_grid_block_related carousel_block',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'no',
											'class' => 'show_hide_block_btn', 
											'attributes' => array('data-target'=>'service_slider_block_related'),
											'options' => 'yes_no_options'
											 );
	
	$config ['agent_list_section_fields'] []  = array( 	'id'=> 'service_carousel_interval', 'name' => 'carousel_interval', 'title' => 'Interval', 
											'parent_class' => 'service_slider_block_related yes_block services_grid_block_related',
											'type' => 'number-field',	'required' => 'required', 'default' => '5000', 'min' => '0', 'step' => '500', 'max' => '10000');
											
    $config ['agent_list_section_fields'] []  = array( 	'id'=> 'service_carousel_pause', 'name' => 'carousel_pause', 'title' => 'Pause', 
											'parent_class' => 'service_slider_block_related yes_block services_grid_block_related',
											'type' => 'radio-toggle',	'required' => 'required', 'default' => 'none',
											'options' => array('none' => 'None', 'hover' => 'Hover')
											 );
	
	*/

										  /* Payment Methos */


											  /* Stripe */ 
	
	$config ['payment_methods'] ["payment_method_stripe_section"] = array('title' => 'Stripe Payment Gateway');  

	$config ['payment_method_stripe_section_fields'] = array();  
 
	
	$config ['payment_method_stripe_section_fields'] []  = array( 	'id'=> 'method_name_stripe', 'name' => 'method_stripe', 'title' => 'Method Name', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Use: Pay online via Stripe', );

	$config ['payment_method_stripe_section_fields'] []  = array( 	'id'=> 'stripe_client_id', 'name' => 'stripe_client_id', 'title' => 'Client ID', 

											'type' => 'text-field',	'required' => 'required', 'default' => 'pk_test_7XJekDehXaxssmHNfkQMG4aG', );

	$config ['payment_method_stripe_section_fields'] []  = array( 	'id'=> 'stripe_client_secret', 'name' => 'stripe_client_secret', 'title' => 'Secret', 

											'type' => 'text-field',	'required' => 'required', 'default' => 'secret', );
	
	
	/* Paypal */ 
	$config ['payment_methods'] ["payment_method_paypal_section"] = array('title' => 'Paypal Payment Gateway');  

	$config ['payment_method_paypal_section_fields'] = array();  
	
	$config ['payment_method_paypal_section_fields'] []  = array( 	'id'=> 'method_name_paypal', 'name' => 'method_paypal', 'title' => 'Method Name', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Use: Pay online via paypale', );

	$config ['payment_method_paypal_section_fields'] []  = array( 	'id'=> 'paypal_client_id', 'name' => 'paypal_client_id', 'title' => 'Client ID', 

											'type' => 'text-field',	'required' => 'required', 'default' => 'pk_test_7XJekDehXaxssmHNfkQMG4aG', );

	$config ['payment_method_paypal_section_fields'] []  = array( 	'id'=> 'paypal_client_secret', 'name' => 'paypal_client_secret', 'title' => 'Secret', 

											'type' => 'text-field',	'required' => 'required', 'default' => 'password', );
	
	
	
	/* RazorPay */ 
	$config ['payment_methods'] ["payment_method_razorpay_section"] = array('title' => 'RazorPay Payment Gateway');  

	$config ['payment_method_razorpay_section_fields'] = array();  
	
	$config ['payment_method_razorpay_section_fields'] []  = array( 	'id'=> 'method_name_razorpay', 'name' => 'method_razorpay', 'title' => 'Method Name', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Use: Pay online via RazorPay', );

	$config ['payment_method_razorpay_section_fields'] []  = array( 	'id'=> 'razorpay_api_key', 'name' => 'razorpay_api_key', 'title' => 'API Key', 

											'type' => 'text-field',	'required' => 'required', 'default' => '', );

	$config ['payment_method_razorpay_section_fields'] []  = array( 	'id'=> 'razorpay_api_secret', 'name' => 'razorpay_api_secret', 'title' => 'API Secret', 

											'type' => 'password-field',	'required' => 'required', 'default' => '', );
	
	/* Mollie */ 
	$config ['payment_methods'] ["payment_method_mollie_section"] = array('title' => 'Mollie Payment Gateway');  

	$config ['payment_method_mollie_section_fields'] = array();  
	
	$config ['payment_method_mollie_section_fields'] []  = array( 	'id'=> 'method_name_mollie', 
																	'name' => 'method_mollie', 
																	'title' => 'Method Name', 
																	
																	'type' => 'text-field',	
																	'required' => 'required', 
																	'default' => 'Use: Pay online via Mollie', );

	$config ['payment_method_mollie_section_fields'] []  = array( 	'id'=> 'mollie_api_key', 
																	'name' => 'mollie_api_key', 'title' => 'API Key', 
																	'type' => 'text-field',	
																	'required' => 'required', 'default' => '', );

	
	
	
	
	
	
	/* Cash On Delievery*/ 
	$config ['payment_methods'] ["payment_method_cod_section"] = array('title' => 'COD Mode');  

	$config ['payment_method_cod_section_fields'] = array();  


	$config ['payment_method_cod_section_fields'] []  = array( 	'id'=> 'method_cod', 'name' => 'method_cod', 'title' => 'Method Name', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Use: Cash on delivery (COD)', );

	$config ['payment_method_cod_section_fields'] []  = array( 	'id'=> 'method_cod_guide', 'name' => 'cod_payment_guide', 'title' => 'Payment guide', 
	
											'type' => 'text-field',	'required' => 'required', 'default' => 'Displayed on the notice of successful purchase and payment page', );

	/* Bank Transfer */ 
	$config ['payment_methods'] ["payment_method_bank_section"] = array('title' => 'Bank Transfer Mode');  

	$config ['payment_method_bank_section_fields'] = array();  


	$config ['payment_method_bank_section_fields'] []  = array( 	'id'=> 'method_bank', 'name' => 'method_bank_transfer', 'title' => 'Method Name', 
											
											'type' => 'text-field',	'required' => 'required', 'default' => 'Use: Bank Transfer', );

	$config ['payment_method_bank_section_fields'] []  = array( 	'id'=> 'method_bank_guide', 'name' => 'bank_transfer_guide', 'title' => 'Payment guide', 
	
											'type' => 'text-field',	'required' => 'required', 'default' => 'Displayed on the notice of successful purchase and payment page', );


	
	$config ['seo_static_pages']  = array();  										
	$config ['seo_static_pages'] ["homepage"] = array('title' => 'SEO for Home Page');
	
	$config ['seo_static_pages'] ["register"] = array('title' => 'SEO for Register Page');
	$config ['seo_static_pages'] ["contact"] = array('title' => 'SEO for Contact Page');	
	
	
	$config ['seo_static_pages'] ["search"] = array('title' => 'SEO for Search Property Page');
	$config ['seo_static_pages'] ["property-for-sale"] = array('title' => 'SEO for Search Property For Sale Page');
	$config ['seo_static_pages'] ["property-for-rent"] = array('title' => 'SEO for Search Property For Rent Page');
	$config ['seo_static_pages'] ["all_properties"] = array('title' => 'SEO for All Properties Page');
	$config ['seo_static_pages'] ["agents"] = array('title' => 'SEO for Agents Page');
	$config ['seo_static_pages'] ["blogs"] = array('title' => 'SEO for Blogs Page');
	$config ['seo_static_pages'] ["blog_categories"] = array('title' => 'SEO for Blog Categories Page');
	
	/*$config ['seo_static_pages'] ["search"] = array('title' => 'SEO for Search Page');
	$config ['seo_static_pages'] ["search"] = array('title' => 'SEO for Search Page');
	$config ['seo_static_pages'] ["search"] = array('title' => 'SEO for Search Page');*/
											
											
	$config ['seo_detail_fieds']  = array(); 													
	$config ['seo_detail_fieds'] []  = array( 'id'=> 'meta_keywords', 
														'name' => 'meta_keywords', 
														'title' => 'Meta Keywords', 
														'type' => 'text-field',	
														'required' => '', 
														'default' => '', );
	
	$config ['seo_detail_fieds'] []  = array( 'id'=> 'meta_description', 
														'name' => 'meta_description', 
														'title' => 'Meta Description', 
														'type' => 'textarea',	
														'required' => '', 
														'default' => '', );
											
	
	
	
	
	
	
	



										 
		