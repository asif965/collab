<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


	
	/* Email Templates */
	$config ['email_template_shortcodes'] = array(
										"option_shortcode" =>  array(	'{website_title}' => 'Website Title'),
										
										
										
																		
										"keyword_shortcode" =>  array(	'{front_url}' => 'Website URL',
																		'{admin_url}' => 'Admin URL'),
										
										"link_shortcode" =>  array(	'{forgot_password_link}' => 'Link for Reset Password',
																	'{account_confirmation_link}' => 'Link for Account Confirmation',
																	),
										
										"property_shortcode" =>  array(	'{property_title}' => 'Property Title',
																	'{property_link}' => 'Direct link of Property',
																	'{property_title_linkable}' => 'Property Title with Direct Link',
																	'{property_id}' => 'Property Unique ID',
																	),
																	
										"user_shortcode" =>  array(	'{user_name}' => 'Username',
																	'{user_pass}' => 'Passwrod',
																	'{user_id}' => 'User Unique ID',
																	'{user_email}' => 'User E-mail'),
										"user_meta_shortcode" =>  array(	
																	'{first_name}' => 'User First Name',
																	'{last_name}' => 'User Last Name',
																	),
										"client_shortcode" =>  array(	
																	'{first_name}' => 'First Name'
																	),							
																	
											);	
	
	
	

	$config ['email_templates_sections'] = array();  
	
	/* Register Email */
	
	$config ['email_templates_sections'] ["register_email"] = array('title' => 'Register Email');  

	$config ['register_email_fields'] = array();  
	
	$config ['register_email_fields'] []  = array( 	'id'=> 'register_email_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Thanks for Register');
	
	$config ['register_email_fields'] []  = array( 	'id'=> 'register_email_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/user_register',
											'default' => '');
	
	$config ['register_email_fields'] []  = array( 	'id'=> 'register_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Account Confirmation */
	
	$config ['email_templates_sections'] ["account_confirmation_email"] = array('title' => 'Account confirmation Email');  

	$config ['account_confirmation_email_fields'] = array();  
	
	$config ['account_confirmation_email_fields'] []  = array( 	'id'=> 'account_confirmation_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	
											'required' => 'required', 
											
											'default' => '{website_title} - Account Confirmation Email');
	
	$config ['account_confirmation_email_fields'] []  = array( 	'id'=> 'account_confirmation_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/account_confirmation',
											'default' => '');
	
	$config ['account_confirmation_email_fields'] []  = array( 	'id'=> 'account_confirmation_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Account Confirmed */
	
	$config ['email_templates_sections'] ["account_confirmed_email"] = array('title' => 'Account Confirmed Email');  

	$config ['account_confirmed_email_fields'] = array();  
	
	$config ['account_confirmed_email_fields'] []  = array( 	'id'=> 'account_confirmed_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	
											'required' => 'required', 
											
											'default' => '{website_title} - Account Confirmed');
	
	$config ['account_confirmed_email_fields'] []  = array( 	'id'=> 'account_confirmed_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/account_confirmed',
											'default' => '');
	
	$config ['account_confirmed_email_fields'] []  = array( 	'id'=> 'account_confirmed_email_lang', 'name' => 'email_lang', 
											'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	
	
	/* Forgot Password */
	
	$config ['email_templates_sections'] ["forgot_password_email"] = array('title' => 'Forgot Password Email');  

	$config ['forgot_password_email_fields'] = array();  
	
	$config ['forgot_password_email_fields'] []  = array( 	'id'=> 'forgot_password_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Forgot Password Email');
	
	$config ['forgot_password_email_fields'] []  = array( 	'id'=> 'forgot_password_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/forgot_password',
											 'default' => '');
	
	$config ['forgot_password_email_fields'] []  = array( 	'id'=> 'forgot_password_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Contact Us */
	
	$config ['email_templates_sections'] ["contact_us_email"] = array('title' => 'Contact Us Email');  

	$config ['contact_us_email_fields'] = array();  
	
	$config ['contact_us_email_fields'] []  = array( 	'id'=> 'contact_us_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Thanks for Contact us');
	
	
	$config ['contact_us_email_fields'] []  = array( 	'id'=> 'contact_us_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/contact_us_message',
											'default' => '');
	
	$config ['contact_us_email_fields'] []  = array( 	'id'=> 'contact_us_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Property Submission */
	
	$config ['email_templates_sections'] ["property_submission_email"] = array('title' => 'Property Submission Email');  

	$config ['property_submission_email_fields'] = array();
	
	$config ['property_submission_email_fields'] []  = array( 	'id'=> 'property_submission_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 
											'default' => '{website_title} - Property Submitted Successfully');
	
	$config ['property_submission_email_fields'] []  = array( 	'id'=> 'property_submission_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/property_submission_message',
											'default' => '');
	
	$config ['property_submission_email_fields'] []  = array( 	'id'=> 'property_submission_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Property Approve */
	
	$config ['email_templates_sections'] ["property_approve_email"] = array('title' => 'Property Approve Email');  

	$config ['property_approve_email_fields'] = array();
	
	$config ['property_approve_email_fields'] []  = array( 	'id'=> 'property_approve_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 
											'default' => '{website_title} - Property Approved Successfully');
	
	$config ['property_approve_email_fields'] []  = array( 	'id'=> 'property_approve_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/property_approve_message',
											'default' => '');
	
	$config ['property_approve_email_fields'] []  = array( 	'id'=> 'property_approve_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/* Property Submitted Approval */
	
	$config ['email_templates_sections'] ["property_submitted_approval_email"] = array('title' => 'Property Submitted Approval Email');  

	$config ['property_submitted_approval_email_fields'] = array();
	
	$config ['property_submitted_approval_email_fields'] []  = array( 	'id'=> 'property_submitted_approval_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 
											'default' => '{website_title} - Property Submitted for Approval');
	
	$config ['property_submitted_approval_email_fields'] []  = array( 	'id'=> 'property_submitted_approval_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/property_submitted_approval_message',
											'default' => '');
	
	$config ['property_submitted_approval_email_fields'] []  = array( 	'id'=> 'property_submitted_approval_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	
	/* Property Reject */
	
	$config ['email_templates_sections'] ["property_reject_email"] = array('title' => 'Property Reject Email');  

	$config ['property_reject_email_fields'] = array();
	
	$config ['property_reject_email_fields'] []  = array( 	'id'=> 'property_reject_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 
											'default' => '{website_title} - Property Submission Rejected');
	
	$config ['property_reject_email_fields'] []  = array( 	'id'=> 'property_reject_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/property_reject_message',
											'default' => '');
	
	$config ['property_reject_email_fields'] []  = array( 	'id'=> 'property_reject_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
											 
											 
											 
	

	/* Contact Us Email - Admin */
	
	$config ['email_templates_sections'] ["contact_us_email_admin"] = array('title' => 'Contact Us Email - Admin');  

	$config ['contact_us_email_admin_fields'] = array();  
	
	$config ['contact_us_email_admin_fields'] []  = array( 	'id'=> 'contact_us_admin_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Contact Email Sent');
	
	
	$config ['contact_us_email_admin_fields'] []  = array( 	'id'=> 'contact_us_admin_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/contact_us_email_admin',
											'default' => '');
	
	$config ['contact_us_email_admin_fields'] []  = array( 	'id'=> 'contact_us_email_admin_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
											 
											 
	/* New User Register - Admin */
	
	$config ['email_templates_sections'] ["new_user_registered_email_admin"] = array('title' => 'New User Register Email - Admin');  

	$config ['new_user_registered_email_admin_fields'] = array();  
	
	$config ['new_user_registered_email_admin_fields'] []  = array( 	'id'=> 'user_registered_email_admin_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - New User Registered');
	
	
	$config ['new_user_registered_email_admin_fields'] []  = array( 	'id'=> 'user_registered_email_admin_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/user_registered_email_admin',
											'default' => '');
	
	$config ['new_user_registered_email_admin_fields'] []  = array( 	'id'=> 'user_registered_email_admin_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );

	/* Property Submission Email - Admin */
	
	$config ['email_templates_sections'] ["property_submission_email_admin"] = array('title' => 'Property Submission Email - Admin');  

	$config ['property_submission_email_admin_fields'] = array();  
	
	$config ['property_submission_email_admin_fields'] []  = array( 	'id'=> 'property_submission_email_subject', 'name' => 'subject', 
											'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 
											'default' => '{website_title} - New Property Submitted');
	
	
	$config ['property_submission_email_admin_fields'] []  = array( 	'id'=> 'property_submission_email_message', 'name' => 'message', 
											'title' => 'Message', 
											'parent_class' => '','class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/property_submission_email_admin',
											'default' => '');
	
	$config ['property_submission_email_admin_fields'] []  = array( 	'id'=> 'property_submission_email_admin_lang', 'name' => 'email_lang', 
											'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
											 	
	/* Client Register Email */
	
	$config ['email_templates_sections'] ["client_register_email"] = array('title' => 'Client Register Email');  

	$config ['client_register_email_fields'] = array();  
	
	$config ['client_register_email_fields'] []  = array( 	'id'=> 'register_email_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Thanks for Register');
	
	$config ['client_register_email_fields'] []  = array( 	'id'=> 'register_email_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/user_register',
											'default' => '');
	
	$config ['client_register_email_fields'] []  = array( 	'id'=> 'register_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );
	
	/*Agent Email Template*/
	$config ['email_templates_sections'] ["agent_inquiry_email"] = array('title' => 'Agent Inquiry Email');  

	$config ['agent_inquiry_email_fields'] = array();  
	
	$config ['agent_inquiry_email_fields'] []  = array( 	'id'=> 'inquiry_email_subject', 'name' => 'subject', 'title' => 'Subject', 
											'parent_class' => '',
											'type' => 'text-field',	'required' => 'required', 'default' => '{website_title} - Thanks for Register');
	
	$config ['agent_inquiry_email_fields'] []  = array( 	'id'=> 'inquiry_email_message', 'name' => 'message', 'title' => 'Message', 
											'parent_class' => '', 'class' => 'ckeditor-element',
											'type' => 'textarea',	
											'required' => 'required', 
											'template' => 'email_templates/user_register',
											'default' => '');
	
	$config ['agent_inquiry_email_fields'] []  = array( 	'id'=> 'inquiry_email_lang', 'name' => 'email_lang', 'title' => 'Email Languages', 
											'type' => 'radio-toggle',	'required' => '', 'default' => '',
											'class' => '', 
											'options' => ''
											 );										 
	
	$CI =& get_instance();	

	foreach($config as $k => $v)	
		$CI->config->set_item($k , $v);
			
