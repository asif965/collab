<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/PayPal-PHP-SDK/paypal/rest-api-sdk-php/sample/bootstrap.php'); // require paypal files
use PayPal\Api\ItemList;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Amount;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;

class Paypal_lib {
	
	public $_api_context;
	
	function  __construct()
    {
        
				
        $CI =& get_instance();	
		
		$payment_methods = $CI->global_lib->get_option('payment_methods');
		$payment_method_paypal = json_decode($payment_methods);
		$paypal_id = '';
		$paypal_secret='';
		
		if($payment_method_paypal->payment_method_paypal_section->is_enable == 'Y'){
			
			if(!empty($payment_method_paypal->payment_method_paypal_section->paypal_client_id) &&
			!empty($payment_method_paypal->payment_method_paypal_section->paypal_client_secret)){
				
				
					$paypal_id = $payment_method_paypal->payment_method_paypal_section->paypal_client_id;
					$paypal_secret=$payment_method_paypal->payment_method_paypal_section->paypal_client_secret;
			}
		
		}
		
		

        $this->_api_context = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $paypal_id,$paypal_secret
            )
        );
    }
	
	public function createPayment($post = array()){
			$CI =& get_instance();	
			
			//var_dump($post);exit;
			extract($post);
			$CI->load->config('paypal');
			// setup PayPal api context
			$this->_api_context->setConfig($CI->config->item('settings'));
			$id =array(
				'package_id'=>$item_id,
			);
			
			$CI->session->set_userdata($id);

		// ### Payer
		// A resource representing a Payer that funds a payment
		// For direct credit card payments, set payment method
		// to 'credit_card' and add an array of funding instruments.
		
        $payer = new Payer();
		$payer->setPaymentMethod("paypal");
		
		 ### Itemized information
		// (Optional) Lets you specify item wise
		// information
        $item1["name"] = $item_name;
        $item1["description"] = $item_description;
        $item1["currency"] = $item_currency;
        $item1["quantity"] =1;
        $item1["price"] = $item_price;

        $itemList = new ItemList();
        $itemList->setItems(array($item1));
		
		/*
		$amount = new Amount();
		$amount->setCurrency('USD')
			   ->setTotal(0.99)
			   ;*/
	   
		// ### Additional payment details
		// Use this optional field to set additional
		// payment information such as tax, shipping
		// charges etc.
        $details['tax'] = 0;
        $details['subtotal'] = $item_price;
		// ### Amount
		// Lets you specify a payment amount.
		// You can also specify additional details
		// such as shipping, tax.
		$amount['currency'] = $item_currency;
        $amount['total'] = $details['tax'] + $details['subtotal'];
        $amount['details'] = $details;
		// ### Transaction
		// A transaction defines the contract of a
		// payment - what is the payment for and who
		// is fulfilling it.
        $transaction['description'] ='Packege '.$item_description.'';
        $transaction['amount'] = $amount;
        $transaction['invoice_number'] = uniqid();
        $transaction['item_list'] = $itemList;
		
        // ### Redirect urls
		// Set the urls that the buyer must be redirected to after
		// payment approval/ cancellation.
        $baseUrl = base_url();
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($baseUrl."packages/getPaymentStatus")
            ->setCancelUrl($baseUrl."packages/getPaymentStatus");
		
		// ### Payment
		// A Payment Resource; create one using
		// the above types and intent set to sale 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
			//var_dump($payer);exit;
        try {
			
            $payment->create($this->_api_context);
			/*
			echo '<pre>';
			var_dump($payment);exit;
			*/
			
        } catch (Exception $ex) {
			
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            //ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $ex);
			
			// Don't spit out errors or use "exit" like this in production code
			echo '<pre>';print_r(json_decode($ex->getData()));
	
            exit(1);
        }
		foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        if(isset($redirect_url)) {
            /** redirect to paypal **/
            redirect($redirect_url);
        }
		
	}
	
	function refund_payment(){
        $refund_amount = $this->input->post('refund_amount');
        $saleId = $this->input->post('sale_id');
        $paymentValue =  (string) round($refund_amount,2); ;

		// ### Refund amount
		// Includes both the refunded amount (to Payer)
		// and refunded fee (to Payee). Use the $amt->details
		// field to mention fees refund details.
        $amt = new Amount();
        $amt->setCurrency('INR')
            ->setTotal($paymentValue);

		// ### Refund object
        $refundRequest = new RefundRequest();
        $refundRequest->setAmount($amt);

		// ###Sale
		// A sale transaction.
		// Create a Sale object with the
		// given sale transaction id.
        $sale = new Sale();
        $sale->setId($saleId);
        try {
            // Refund the sale
            // (See bootstrap.php for more on `ApiContext`)
            $refundedSale = $sale->refundSale($refundRequest, $this->_api_context);
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            ResultPrinter::printError("Refund Sale", "Sale", null, $refundRequest, $ex);
            exit(1);
        }

		// NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        ResultPrinter::printResult("Refund Sale", "Sale", $refundedSale->getId(), $refundRequest, $refundedSale);

        return $refundedSale;
    }
	
	
    

	
	
	
	
	
}



/* End of file Myhelpers.php */