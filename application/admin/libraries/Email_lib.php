<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP; 
 
class Email_lib{ 
    
    var $api_error; 
	var $default_mailer;
    var $smtp_host , $smtp_port , $smtp_username , $smtp_password , $smtp_encryption , $smtp_auth ;
	
	var $email_template_shortcodes;
	
    function __construct(){ 
        
		$this->api_error = ''; 
        $CI =& get_instance(); 
        
        	
		$email_setting = $CI->global_lib->get_option('email_setting');
		$email_setting = json_decode($email_setting);
		
		/*$data['email_setting'] = $this->global_lib->get_option('email_setting');	
		
		print_r($email_setting);*/
		
		$this->email_template_shortcodes = '';
		
		$this->default_mailer = $CI->global_lib->get_option('default_mailer');
		
		if($this->default_mailer == 'smtp'){
			
			if(	isset($email_setting->smtp_host) && !empty($email_setting->smtp_host) ){
				$this->smtp_host  = $email_setting->smtp_host;
			}else{ echo "SMTP host not set ..."; exit;}
			
			if(	isset($email_setting->smtp_port) && !empty($email_setting->smtp_port) ){
				$this->smtp_port  = $email_setting->smtp_port;
			}else{ echo "SMTP port not set ..."; exit;}
			
			if(	isset($email_setting->smtp_username) && !empty($email_setting->smtp_username) ){
				$this->smtp_username  = $email_setting->smtp_username;
			}else{ echo "SMTP username not set ..."; exit;}
			
			if(	isset($email_setting->smtp_password) && !empty($email_setting->smtp_password) ){
				$this->smtp_password  = $email_setting->smtp_password;
			}else{ echo "SMTP pasword not set ..."; exit;}
			
			if(	isset($email_setting->smtp_encryption) && !empty($email_setting->smtp_encryption) ){
				$this->smtp_encryption  = $email_setting->smtp_encryption;
			}else{ echo "SMTP encryption not set ..."; exit;}
			
			if(	isset($email_setting->smtp_auth) && !empty($email_setting->smtp_auth) ){
				$this->smtp_auth  = $email_setting->smtp_auth;
			}else{ echo "SMTP host not set ..."; exit;}
			
			require_once(APPPATH . 'third_party/php_mailer/vendor/autoload.php'); 
		}
		
		
		
		
    } 
	
	function get_email_template_details($email_template){
		
		$CI =& get_instance();
		$email_templates_sections = $CI->config->item('email_templates_sections');
		$CI->load->library('Global_lib');
		
		$email_templates = $CI->global_lib->get_option('email_templates');
		if(isset($email_templates) && !empty($email_templates))
		{
			$email_templates = json_decode($email_templates,true);
		}
		if(count($email_templates) > 0){
			
			foreach($email_templates  as $key => $val ){
				if($key == $email_template){
					return json_encode($val);
				}
			}
		}
		return 'invalid_template';
		
	}
	
	function get_email_template_shortcodes($args = array()){
		
		$CI =& get_instance();
		$CI->load->library('Global_lib');
		$CI->load->model('Common_model');
		$email_template_shortcodes = $CI->config->item('email_template_shortcodes');
		$email_template_shortcodes_parsed = array();
		extract($args);
		
		
		if(!empty($email_template_shortcodes)  )
		{
			$n=0;
			$user_data = array();
			if( isset($user_id) && !empty($user_id)){
				
				
				$options=array('where'=>array("user_id" => $user_id));
				$user_exists = $CI->Common_model->commonSelect('users',$options);
				
				if($user_exists->num_rows() > 0 )
				{
					$user_data = (array) $user_exists->row(); 	
					
				}	
			}
			$property_data = array();
			if( isset($property_id) && !empty($property_id)){
				
				
				$options=array('where'=>array("p_id" => $property_id));
				$property_exists = $CI->Common_model->commonSelect('properties',$options);
				
				if($property_exists->num_rows() > 0 )
				{
					$property_data = (array) $property_exists->row(); 	
					
				}	
			}
			/*print_r($property_data);
			print_r($user_data);
			print_r($email_template_shortcodes);
			print_r($args);*/
			
			foreach($email_template_shortcodes as $shortcode_type => $shortcode_list)
			{
				foreach($shortcode_list as $k => $v)
				{
					$key = str_replace("}","",str_replace("{","",$k));
					
					if($shortcode_type == 'user_shortcode' && isset($user_id) && !empty($user_id)){
						if(isset($user_data['user_id']) && $user_data['user_id'] == $user_id){
							
							
							/*$email_template_shortcodes['user_shortcode'][$k] =	$user_data[$key];*/
							$email_template_shortcodes_parsed[$k] =	$user_data[$key];
						}
					}
					
					if($shortcode_type == 'property_shortcode' && isset($property_id) && !empty($property_id)){
						if(isset($property_data['p_id']) && $property_data['p_id'] == $property_id){
							
							$key = str_replace("property_","",$key);
							
							if(isset($property_data[$key]))
							{
								$email_template_shortcodes_parsed[$k] =	$property_data[$key];
							}else if($key == 'link'){
								
								/*if(isset($property_data['status']) && $property_data['status'] =='publish'){*/
									$segments = array('property',$property_data['slug']."~".$property_data['p_id']); 
									$link = str_replace("/admin","",  site_url($segments));
									$email_template_shortcodes_parsed[$k] =	$link;
								/*}*/
							}else if($key == 'title_linkable'){
								
									$title = $property_data['title'];
									$segments = array('property',$property_data['slug']."~".$property_data['p_id']); 
									$link = str_replace("/admin","",  site_url($segments));
									$email_template_shortcodes_parsed[$k] = "<a href='$link'>$title</a>";
								
							}	
						}
					}
					
					
					if($shortcode_type == 'option_shortcode'){
						$option_val = $CI->global_lib->get_option($key);
						/*$email_template_shortcodes['option_shortcode'][$k] =	$option_val;*/
						$email_template_shortcodes_parsed[$k] =	$option_val;
					}
					if($shortcode_type == 'keyword_shortcode'){
						if(function_exists($key))
							$email_template_shortcodes_parsed[$k] =	call_user_func($key);
					}
					if($shortcode_type == 'link_shortcode'){
						/*if(isset($args['url']))
							$email_template_shortcodes_parsed[$k] =	$args['url'];*/
							
						if(isset($args[$key.'_url']))
							$email_template_shortcodes_parsed[$k] =	$args[$key.'_url'];	
					}
					if($shortcode_type == 'user_meta_shortcode' && isset($user_id) && !empty($user_id)){
						
						
						$user_meta_val = $CI->global_lib->get_user_meta($user_id , $key);
						/*$email_template_shortcodes['user_meta_shortcode'][$k] =	$user_meta_val;*/
						$email_template_shortcodes_parsed[$k] =	$user_meta_val;
						
					}
					
					if($shortcode_type == 'client_shortcode'){
						//print_r($client_shortcode);
						$email_template_shortcodes_parsed['{first_name}'] =	$client_shortcode['first_name'];
						$email_template_shortcodes_parsed['{last_name}'] =	$client_shortcode['last_name'];
						$email_template_shortcodes_parsed['{register_link}'] =	$client_shortcode['register_link'];						
					}
				}
			}
		}		
				
		
		$this->email_template_shortcodes = $email_template_shortcodes_parsed;
	}
 
	function get_email_template_parsed($message){
		
		
		if(count($this->email_template_shortcodes) > 0){
			foreach($this->email_template_shortcodes as $shortcode => $replace_val){
				$message = str_replace($shortcode , $replace_val , $message);
			}
		}
		
		return $message;
	}
 
 
	function send_email_notification($args = array()){
		
		
		$mail = new PHPMailer(true);
		$CI =& get_instance();
		$email_templates_sections = $CI->config->item('email_templates_sections');
		
		extract($args);
		
		if(!isset($to_email) || empty($to_email)){ echo "no email selected"; exit;}
		if(!isset($email_template) || empty($email_template)){ echo "no email template selected"; exit;}

		$template_details =  $this->get_email_template_details($email_template);
		if($template_details == 'invalid_template' ) { echo 'wrong or invalid template selected'; exit;}

		$template_details = json_decode($template_details,true);
		extract($template_details);
		
		//if(empty($this->email_template_shortcodes))
		
		/*$args = array();
		$args['user_id'] = 5 ;*/
 		$this->get_email_template_shortcodes($args);
		
		/*print_r($this->email_template_shortcodes); */
		
		$subject = $this->get_email_template_parsed($subject);
		$message = $this->get_email_template_parsed($message);
		/*print_r($subject); 
		print_r($message); 
		exit;*/
		
		try {
			
			$mail->SMTPDebug = 0;                      
			$mail->isSMTP(true);                       
			 
			 
			$mail->Host       = $this->smtp_host;                    
			$mail->SMTPAuth   = $this->smtp_auth;
			$mail->Username   = $this->smtp_username;
			$mail->Password   = $this->smtp_password;
			
			
			$mail->SMTPSecure = $this->smtp_encryption;
			
			$mail->Port       = $this->smtp_port;                                    
			
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);


			/*$mail->setFrom('realestate@mindlogixtech.com', 'RealEstateWeb');*/
			$mail->setFrom($this->smtp_username);
			
			
			$mail->addAddress($to_email);               // Name is optional

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
			//echo $to_email;
			//echo $message.'AAA';exit;
			$mail->send();
			/*echo 'Message has been sent';*/
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		
		
	}
 
 
	function test_email_notifications($args = array()){
		
		
		
		$mail = new PHPMailer(true);
		$CI =& get_instance();
		$email_templates_sections = $CI->config->item('email_templates_sections');
		
		extract($args);
		
		if(!isset($to_email) || empty($to_email)){ echo "no email selected"; exit;}
		if(!isset($email_template) || empty($email_template)){ echo "no email template selected"; exit;}

		$template_details =  $this->get_email_template_details($email_template);
		if($template_details == 'invalid_template' ) { echo 'wrong or invalid template selected'; exit;}

		$template_details = json_decode($template_details,true);
		extract($template_details);
		
		
		
		$args = array();
		/*$args['user_id'] = 5 ;
		$args['property_id'] = 92 ;*/
 		$this->get_email_template_shortcodes($args);
		/*print_r($args);
		print_r($template_details);
		print_r($this->email_template_shortcodes); */
		
		$subject = $this->get_email_template_parsed($subject);
		$message = $this->get_email_template_parsed($message);
		/*print_r($subject); 
		print_r($message); 
		exit;*/
		
		try {
			
			$mail->SMTPDebug = 0;                      
			$mail->isSMTP(true);                       
			 
			 
			$mail->Host       = $this->smtp_host;                    
			$mail->SMTPAuth   = $this->smtp_auth;
			$mail->Username   = $this->smtp_username;
			$mail->Password   = $this->smtp_password;
			
			
			$mail->SMTPSecure = $this->smtp_encryption;
			
			$mail->Port       = $this->smtp_port;                                    
			
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);


			/*$mail->setFrom('realestate@mindlogixtech.com', 'RealEstateWeb');*/
			$mail->setFrom($this->smtp_username);
			
			
			$mail->addAddress($to_email);               // Name is optional

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = $subject;
			$mail->Body    = $message;
			//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
			
			$mail->send();
			echo 'Message has been sent';
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}
		
		
	}
 
	
}