<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 
class Stripe_lib{ 
    var $CI; 
    var $api_error; 
	var $stripe_id;
     
    function __construct(){ 
        $this->api_error = ''; 
        $this->CI =& get_instance(); 
        
        
        // Include the Stripe PHP bindings library 
        require APPPATH .'third_party/stripe/init.php'; 
        	
			
		$payment_methods = $this->CI->global_lib->get_option('payment_methods');
		$payment_method_stripe = json_decode($payment_methods);
		
		
		$stripe_id = '';
		$stripe_secret='';
		
		if($payment_method_stripe->payment_method_stripe_section->is_enable == 'Y'){
			
			if(!empty($payment_method_stripe->payment_method_stripe_section->stripe_client_id) &&
			!empty($payment_method_stripe->payment_method_stripe_section->stripe_client_secret)){
				
				
					$this->stripe_id = $stripe_id = $payment_method_stripe->payment_method_stripe_section->stripe_client_id;
					 $stripe_secret=$payment_method_stripe->payment_method_stripe_section->stripe_client_secret;
			}
		
		} 
		
        // Set API key 
        \Stripe\Stripe::setApiKey($stripe_secret); 
		
		
    } 
 
	function get_stripe_id(){
		return $this->stripe_id;
	}
    function addCustomer($email, $token,$name,$address){ 
			
        try { 
            // Add customer to stripe 
            $customer = \Stripe\Customer::create(array( 
				'name' => $name,
                'email' => $email, 
				'address' => $address,
                'source'  => $token 
            ));
            return $customer; 
        }catch(Exception $e) { 
            echo $this->api_error = $e->getMessage(); 
            return false; 
        } 
    } 
     
    function createCharge($customerId, $itemName, $itemPrice, $itemCurrency){ 
		
        // Convert price to cents 
        $itemPriceCents = ($itemPrice*100);
		//$itemPriceCents = $itemPrice;		
        //$currency = $this->CI->config->item('stripe_currency'); 
         
        try { 
			
            // Charge a credit or a debit card 
            $charge = \Stripe\Charge::create(array( 
                'customer' => $customerId, 
                'amount'   => $itemPriceCents, 
                'currency' => $itemCurrency, 
                'description' => $itemName 
            )); 
             
            // Retrieve charge details 
            $chargeJson = $charge->jsonSerialize(); 
            return $chargeJson; 
        }catch(Exception $e) { 
			
            echo $this->api_error = $e->getMessage(); 
			//print_r($this->api_error);exit;
            return false; 
        } 
    } 
}